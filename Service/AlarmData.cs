﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.ZebraV2Seq.Service
{
    public enum MachineStatus
    {
        Init,
        SelfCheck,
        Idle,
        Wash,
        Incorporation,
        Image,
        WaitImage,
        DataHandle,
        Stopping
    }

    public class AlarmData
    {
        private object _value;
        private string _description;
        private List<MachineStatus> _showErrorInStatus;
        private List<MachineStatus> _pauseInStatus;
        private List<MachineStatus> _stopInStatus;

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public List<MachineStatus> ShowErrorInStatus
        {
            get
            {
                return _showErrorInStatus;
            }

            set
            {
                _showErrorInStatus = value;
            }
        }

        public List<MachineStatus> PauseInStatus
        {
            get
            {
                return _pauseInStatus;
            }

            set
            {
                _pauseInStatus = value;
            }
        }

        public List<MachineStatus> StopInStatus
        {
            get
            {
                return _stopInStatus;
            }

            set
            {
                _stopInStatus = value;
            }
        }

        public object Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }
    }
}
