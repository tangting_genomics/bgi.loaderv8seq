﻿using Autofac;
using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.ZLimsAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BGI.RPC;
using System.Diagnostics;
using BGI.Common.LogStore;
using BGI.Control.Slide;
using BGI.Scripting.Reagent;
using System.Threading;
using BGI.OSIIP;

namespace BGI.ZebraV2Seq.Service
{
    public class ZLimsMgr
    {
        private Logger _Logger = LogMgr.GetLogger("ZLimsMgrBase");
        private const string CATEGORY_ZLIMS = "ZLIMS";
        private const string CATEGORY_SYSTEM = "System";
        private bool _isOnlineMode = true;
        public bool IsOnlineMode
        {
            get
            {
                return _isOnlineMode;
            }
        }
        private bool _zlimsIsSimulated = false;
        private string _zlimsBaseUrl = string.Empty;
        private IContainer _container = null;
        private ContainerBuilder _builder = null;

        private LogStorgeOperate LogStore;

        #region user
        private string _ProductUser = string.Empty;
        private string _ProductPwd = string.Empty;

        private string _ResearchUser = string.Empty;
        private string _ResearchPwd = string.Empty;

        private string _GuestUser = string.Empty;
        private string _GuestUserPwd = string.Empty;

        private string _UserName = string.Empty;
        #endregion

        private ZLimsMgr()
        {
            // Firstly, check the running mode: online or standalone
            //IsOnlineMode = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_ZLIMS, "IsOnlineMode");

            _zlimsIsSimulated = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_ZLIMS, "IsSimulated");
            _zlimsBaseUrl = string.Format("http://{0}:{1}/", ConfigMgr.Get.GetCurValue<string>("ZLIMS", "BaseUrl"), ConfigMgr.Get.GetCurValue<string>("ZLIMS", "Port"));

            this._ProductUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ProductUserName");
            this._ProductPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ProductUserPwd");

            this._ResearchUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ResearchUserName");
            this._ResearchPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ResearchUserPwd");

            this._GuestUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "GuestUserName");
            this._GuestUserPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "GuestUserPwd");
            _builder = new ContainerBuilder();

            LogStore = LogStorgeOperate.GetInstance();
            if (IsOnlineMode)
            {
                if (_zlimsIsSimulated)
                {
                    _builder.RegisterInstance(new SZLimsApiService(_zlimsBaseUrl)).As<IZLimsApiService>();

                    // Check the script directory, create it if not exists
                    string scriptHome = ConfigMgr.Get.GetCurValue<string>("System", "ScriptHomePath");
                    string scriptPath = Path.Combine(scriptHome);
                    if (!Directory.Exists(scriptPath))
                    {
                        Directory.CreateDirectory(scriptPath);
                    }
                }
                else
                {
                    _builder.RegisterInstance(new ZLimsApiService(_zlimsBaseUrl)).As<IZLimsApiService>();
                }
            }
            else
            {
                // else, in standalone mode, we use a 'fake ZLIMS'
                _builder.RegisterInstance(new NZLimsApiService(_zlimsBaseUrl)).As<IZLimsApiService>();
            }

            _container = _builder.Build();
        }

        static object lockobj = new object();
        static ZLimsMgr _instance = null;
        public static ZLimsMgr Instance()
        {
            lock (lockobj)
            {
                if (_instance == null)
                {
                    lock (lockobj)
                    {
                        _instance = new ZLimsMgr();
                    }
                }
                return _instance;
            }
        }

        #region Check whether zlims is simulated
        /// <summary>
        ///  Check whether zlims is simulated
        /// </summary>
        /// <returns></returns>
        public bool IsZlimsSimulated()
        {
            return this._zlimsIsSimulated;
        }
        #endregion

        #region Authenicate from limis
        /// <summary>
        /// Authenicate from limis
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public ResultInfo Authenticate(string userName, string password)
        {
            string user_role = string.Empty;

            try
            {
                #region Authenticate
                using (var scope = _container.BeginLifetimeScope())
                {
                    ResultInfo resultInfo = scope.Resolve<IZLimsApiService>().Authenticate(userName, password);

                    if (resultInfo.IsSuccess)
                    {
                        user_role = JsonConvert.DeserializeAnonymousType(resultInfo.ResultData.ToString(), new { user_type = string.Empty }).user_type;
                        user_role = string.IsNullOrEmpty(user_role) ? "porduction_user" : user_role;

                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The Authenticate method of ZLimsApiService called success.");
                        _UserName = userName;
                        return new ResultInfo()
                        {
                            IsSuccess = true,
                            ResultData = user_role,
                        };
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The Authenticate method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
            return new ResultInfo()
            {
                IsSuccess = false,
            };
        }
        #endregion

        #region Send instrument status
        /// <summary>
        /// send instrument status
        /// </summary>
        /// <param name="status">instrument status, may be Idle/Running/Error</param>
        public void SendInstrumentStatus(string status)
        {
            try
            {
                #region SendInstrumentStatus
                string machine_id = Environment.MachineName;
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    zlimsApiService.SendInstrumentStatus(Instrument_Type.MGISEQ_2000, machine_id, status);
                }
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }
        #endregion

        #region Send message to zlims
        /// <summary>
        /// Send message to zlims
        /// </summary>
        /// <param name="msg">message content</param>
        /// <param name="msgType">message type, the value may be "error/warning/operation"</param>
        public void SendeMessageToZLims(int flag, int id, string _flowcell_id, string datetime, string msg, string msgType)
        {
            if (string.IsNullOrEmpty(msg))
            {
                throw new Exception("The msg should not be null in SendeMessageToZLims method.");
            }

            if (string.IsNullOrEmpty(msgType))
            {
                throw new Exception("The msgType should not be null in SendeMessageToZLims method, it may be error/warning/operation.");
            }

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            string runmessage_type = string.Compare(msgType, "Info", true) == 0 ? "OPERATION" : "SYSTEM";
            string level = string.Compare(msgType, "Info", true) == 0 ? "INFORMATION" : msgType.ToUpper();

            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();

                    var notification = new NotificationInfo()
                    {
                        level = level,
                        type = "RUNMESSAGE",
                        sender = Environment.MachineName,
                        notice_mode = new List<string> { "SYSTEMINSIDE" },
                        message = msg,
                        metadata = new NotificationMetadata()
                        {
                            runmessage_type = runmessage_type,
                            flowcell_id = _flowcell_id,
                            software_version = version,
                            report_date = datetime
                        }
                    };
                    var resultInfo = zlimsApiService.SendNotification(notification);
                    if (resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The SendeErrorMessageToZLims method of ZLimsApiService called success.");
                        LogStore.UpdateLogViewInfo(flag, id);
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The SendeErrorMessageToZLims method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }
        #endregion

        #region Check Zlims running status
        /// <summary>
        /// Check Zlims running status
        /// </summary>
        /// <returns></returns>
        public bool IsZlimsOnline()
        {
            bool result = false;
            try
            {
                #region Check zlims running status
                using (var scope = _container.BeginLifetimeScope())
                {
                    ResultInfo resultInfo = scope.Resolve<IZLimsApiService>().CheckRunningStatus();

                    if (!resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CheckRunningStatus method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }

                    result = resultInfo.IsSuccess;
                }
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }

            return result;
        }
        #endregion

        #region Check whether dnb_id is validation or not
        /// <summary>
        /// Check whether dnb_id is validation or not
        /// </summary>
        /// <param name="dnb_id">dnb id</param>
        /// <returns></returns>
        public bool IsDnbIdValidation(string dnb_id)
        {
            bool flag = false;

            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    string part_number = "xx-DNB-xx";
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();

                    string resource_type_id = zlimsApiService.GetResourcesTypeId(part_number).ResultData.ToString();
                    var resultInfo = zlimsApiService.GetResources(resource_type_id, dnb_id);
                    var resourceInfos = resultInfo.ResultData != null ? resultInfo.ResultData as List<ResourceInfo> : new List<ResourceInfo>();
                    if (resourceInfos.Count > 0)
                    {
                        flag = true;
                    }

                    this._Logger.Log()(LogSeverityEnum.Debug, "ZLIMS API INFORMATION, Validate the dnb_id successfully.");
                }
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }

            return flag;
        }
        #endregion

        #region Send create task to limis
        /// <summary>
        /// Send create load dnb task or sequence task to limis
        /// </summary>
        /// <param name="phraseType"></param>
        public string SendCreateLoadDNBTaskToLimis(string _flowcell_id, ReagentKit _ReagentKit, string sampleId)
        {            
            string dnbTaskId = string.Empty;
            return dnbTaskId;
            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    //Send create load dnb task to limis
                    LoadDnbTaskConfig dnbTaskConfig = new LoadDnbTaskConfig()
                    {
                        task_id = "load_dnb",
                        task_config_id = "load_dnb_nifty",
                        machine_id = Instrument_Type.MGISEQ_2000 + "," + Environment.MachineName,
                        operator_id = _UserName,
                        resources = new List<TaskResource>()
                                    {
                                        new TaskResource() { part_num = "xx-FLOWCELL-xx", values=new List<string> { _flowcell_id } },
                                        new TaskResource() { part_num = "ReagentKit1", values=new List<string> { _ReagentKit.Name } },
                                        new TaskResource() { part_num = "xx-DNB-xx", values=new List<string> { sampleId } }
                                    },
                        inputs = new LoadDnbInput()
                        {
                            flowcell_id = _flowcell_id,
                            lanes = new List<LaneInfo>()
                                        {
                                            new LaneInfo() { lane_id = "L01", dnb_id =sampleId  }
                                        }
                        }
                    };
                    var resultInfo = zlimsApiService.CreateLoadDnbTask(dnbTaskConfig);
                    if (resultInfo.IsSuccess)
                    {
                        dnbTaskId = (resultInfo.ResultData as TaskInstantId).task_inst_id.ToString();
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The CreateLoadDnbTask method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CreateLoadDnbTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
            return dnbTaskId;
        }

        public string SendCreateSequencingTaskToLimis(string _flowcell_id, ReagentKit _ReagentKit)
        {
            string sequenceId = string.Empty;
            return sequenceId;
            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    //send create sequence task to limis
                    SequenceTaskConfig sequenceConfig = new SequenceTaskConfig()
                    {
                        task_id = "sequencing",
                        task_config_id = "sequencing_nifty",
                        association_id = _flowcell_id,
                        machine_id = Instrument_Type.MGISEQ_2000 + "," + Environment.MachineName,
                        operator_id = _UserName,
                        resources = new List<TaskResource>()
                                    {
                                        new TaskResource() { part_num="xx-FLOWCELL-xx",values= new List<string>() { _flowcell_id } },
                                        new TaskResource() { part_num="Sequencing kit1",values= new List<string>() { _ReagentKit.Name } }
                                    },
                        inputs = new SequenceInput() { flowcell_id = _flowcell_id }
                    };
                    var resultInfo = zlimsApiService.CreateSequenceTask(sequenceConfig);
                    if (resultInfo.IsSuccess)
                    {
                        var taskInstantId = resultInfo.ResultData as SequenceTaskInstantId;
                        sequenceId = taskInstantId.task_inst_id.ToString();
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The CreateSequenceTask method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CreateSequenceTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }

            return sequenceId;
        }
        #endregion

        #region Send Fastq To Remote Server
        public string SendFastqToRemoteServer(string local_fastq_path, string _flowcell_id)
        {
            List<string> rsync_host_list = new List<string>();
            List<string> rsync_username_list = new List<string>();
            List<string> rsync_password_list = new List<string>();

            #region get the information about rysnc server from isw configuration
            bool isSendFastqToServer = ConfigMgr.Get.GetCurValue<bool>("System", "IsSendFastqToRemoteServer");
            string rsyncHostList = ConfigMgr.Get.GetCurValue<string>("System", "FasqServerAddress");
            if (!string.IsNullOrEmpty(rsyncHostList))
            {
                rsync_host_list.AddRange(rsyncHostList.Split(','));
            }
            string rsyncUserNameList = ConfigMgr.Get.GetCurValue<string>("System", "FastqServerUserName");
            if (!string.IsNullOrEmpty(rsyncUserNameList))
            {
                rsync_username_list.AddRange(rsyncUserNameList.Split(','));
            }
            string rsyncPasswordList = ConfigMgr.Get.GetCurValue<string>("System", "FastqServerPassword");
            if (!string.IsNullOrEmpty(rsyncPasswordList))
            {
                rsync_password_list.AddRange(rsyncPasswordList.Split(','));
            }

            #endregion

            using (var scope = _container.BeginLifetimeScope())
            {
                // 1、sync fastq files to rsync the servers
                var zlimsApiService = scope.Resolve<IZLimsApiService>();

                #region upload fastq files to rsync servers
                if (isSendFastqToServer)
                {
                    this._Logger.Log(LogSeverityEnum.Debug, "INFO, The rsync_host_list are " + string.Join(",", rsync_host_list) + ".");

                    for (int i = 0; i < rsync_host_list.Count; i++)
                    {
                        int uploadTimes = 0;
                        bool isUploadFastqFilesSuccess = false;
                        Stopwatch timer = new Stopwatch();
                        timer.Start();
                        while (!isUploadFastqFilesSuccess && uploadTimes < 3)
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, $"START, Start uploading fastq file to {rsync_host_list[i]} server, user name and password are {rsync_username_list[i]} and {rsync_password_list[i]}");

                            try
                            {
                                // upload fastq files
                                isUploadFastqFilesSuccess = zlimsApiService.UploadFastqFiles(
                                    rsync_host_list[i],
                                    rsync_username_list[i],
                                    rsync_password_list[i],
                                    Path.Combine(local_fastq_path, "OutputFq", _flowcell_id).Replace(":", "").Replace(@"\", @"/"),_flowcell_id);

                                // upload metrics files
                                zlimsApiService.UploadFastqFiles(
                                    rsync_host_list[i],
                                    rsync_username_list[i],
                                    rsync_password_list[i],
                                    Path.Combine(local_fastq_path, "workspace", _flowcell_id, @"L01\metrics").Replace(":", "").Replace(@"\", @"/"),
                                    Path.Combine(_flowcell_id, @"L01\metrics").Replace(@"\", @"/"));
                            }
                            catch (Exception e)
                            {
                                _Logger.Log()(LogSeverityEnum.Error, "Exception, Upload fastq file exception, the message is: {0}", e.ToString());
                            }

                            uploadTimes++;
                            if (!isUploadFastqFilesSuccess)
                            {
                                Thread.Sleep(60 * 1000);
                            }
                        }
                        timer.Stop();
                        if (isUploadFastqFilesSuccess)
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, "SUCCESS, Upload fastq file success, it spent about " + timer.Elapsed.ToString() + ".");
                            return "success";
                        }
                        else
                        {
                            this._Logger.Log(LogSeverityEnum.Error, "ERROR, Upload fastq file failure, it spent about " + timer.Elapsed.ToString() + ".");
                        }
                    }
                }
                else
                {
                    this._Logger.Log(LogSeverityEnum.Debug, "INFO, The value of IsSendFastqToServer is false.");
                }
                #endregion
                return "failure";
            }
        }
        #endregion

        #region Send Image To Remote Server
        public string SendImageToRemoteServer(string _flowcell_id)
        {
            List<string> rsync_host_image_list = new List<string>();
            List<string> rsync_username_image_list = new List<string>();
            List<string> rsync_password_image_list = new List<string>();
            #region get the information about rysnc server from isw configuration
            bool isSendImageToServer = ConfigMgr.Get.GetCurValue<bool>("System", "isSendImageToRemoteServer");
            var local_image_path = ConfigMgr.Get.GetCurValue<string>("System", "ImageSavePath");


            string rsyncHostImageList = ConfigMgr.Get.GetCurValue<string>("System", "ImageServerAddress");
            if (!string.IsNullOrEmpty(rsyncHostImageList))
            {
                rsync_host_image_list.AddRange(rsyncHostImageList.Split(','));
            }
            string rsyncUserNameImageList = ConfigMgr.Get.GetCurValue<string>("System", "ImageServerUserName");
            if (!string.IsNullOrEmpty(rsyncUserNameImageList))
            {
                rsync_username_image_list.AddRange(rsyncUserNameImageList.Split(','));
            }
            string rsyncPasswordImageList = ConfigMgr.Get.GetCurValue<string>("System", "ImageServerPassword");
            if (!string.IsNullOrEmpty(rsyncPasswordImageList))
            {
                rsync_password_image_list.AddRange(rsyncPasswordImageList.Split(','));
            }
            //string rsyncPathImageList = ConfigMgr.Get.GetCurValue<string>("", "");
            //if (!string.IsNullOrEmpty(rsyncPathImageList))
            //{
            //    rsync_path_image_list.AddRange(rsyncPathImageList.Split(','));
            //}
            #endregion

            using (var scope = _container.BeginLifetimeScope())
            {
                // 1、sync fastq files to rsync the servers
                var zlimsApiService = scope.Resolve<IZLimsApiService>();

                #region upload image files to rsync servers
                if (isSendImageToServer)
                {
                    this._Logger.Log(LogSeverityEnum.Debug, "INFO, The rsync_host_image_list are " + string.Join(",", rsync_host_image_list) + ".");

                    for (int i = 0; i < rsync_host_image_list.Count; i++)
                    {
                        int uploadTimes = 0;
                        bool isUploadImageFilesSuccess = false;
                        Stopwatch timer = new Stopwatch();
                        timer.Start();
                        while (!isUploadImageFilesSuccess && uploadTimes < 3)
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, $"START, Start uploading image file to {rsync_host_image_list[i]} server, user name and password are {rsync_username_image_list[i]} and {rsync_password_image_list[i]}");

                            try
                            {
                                // upload image files
                                isUploadImageFilesSuccess = zlimsApiService.UploadFastqFiles(
                                    rsync_host_image_list[i],
                                    rsync_username_image_list[i],
                                    rsync_password_image_list[i],
                                    Path.Combine(local_image_path, _flowcell_id).Replace(":", "").Replace(@"\", @"/"), _flowcell_id);
                            }
                            catch (Exception e)
                            {
                                _Logger.Log()(LogSeverityEnum.Error, "Exception, Upload image file exception, the message is: {0}", e.ToString());
                            }

                            uploadTimes++;
                            if (!isUploadImageFilesSuccess)
                            {
                                Thread.Sleep(60 * 1000);
                            }
                        }
                        timer.Stop();
                        if (isUploadImageFilesSuccess)
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, "SUCCESS, Upload image file success, it spent about " + timer.Elapsed.ToString() + ".");
                            return "success";
                        }
                        else
                        {
                            this._Logger.Log(LogSeverityEnum.Error, "ERROR, Upload image file failure, it spent about " + timer.Elapsed.ToString() + ".");
                        }
                    }
                }
                else
                {
                    this._Logger.Log(LogSeverityEnum.Debug, "INFO, The value of IsSendImageToServer is false.");
                }
                #endregion
            }
            return "failure";
        }
        #endregion

        #region Send complete task to limis
        /// <summary>
        /// Send complete task to limis
        /// </summary>
        public void SendCompleteLoadDNBTaskToLimis(string dnbTaskId)
        {
            return;
            try
            {
                #region SendCompleteTaskToLimis
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    if (string.IsNullOrEmpty(dnbTaskId))
                    {
                        return;
                    }
                    LoadDnbTaskConfig dnbConfig = new LoadDnbTaskConfig()
                    {
                        completion_date = DateTime.UtcNow.ToString("u"),
                        completion_status = "success",
                        inputs = new LoadDnbInput() { load_dnb_status = "success_manual" }
                    };
                    var resultInfo = zlimsApiService.CompleteLoadDnbTask(dnbTaskId, dnbConfig);
                    if (resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The CompleteLoadDnbTask method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CompleteLoadDnbTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }
        public void SendCompleteSequencingTaskToLimis(string sequenceId, string local_fastq_path, string _flowcell_id)
        {

            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();

                    if (string.IsNullOrEmpty(sequenceId))
                    {
                        return;
                    }

                    var flowCellQCInfos = GetLaneQCFromFastq(local_fastq_path, _flowcell_id);
                    SequenceTaskConfig sequenceConfig = new SequenceTaskConfig()
                    {
                        completion_date = DateTime.UtcNow.ToString("u"),
                        completion_status = "success",
                        inputs = new SequenceInput()
                        {
                            sequencing_data_path = Path.Combine(local_fastq_path, "OutputFq", _flowcell_id),
                            copying_status = "success",
                            lanes = new List<SequenceLaneInfo>()
                        }
                    };
                    foreach (string lane_id in flowCellQCInfos.Keys)
                    {
                        IList<SequenceSampleInfo> sampleInfos = new List<SequenceSampleInfo>();
                        foreach (string barcode_id in flowCellQCInfos[lane_id].Keys)
                        {
                            sampleInfos.Add(new SequenceSampleInfo()
                            {
                                barcode_id = barcode_id,
                                raw_gc = flowCellQCInfos[lane_id][barcode_id]["raw_gc"].ToString(),
                                q20 = flowCellQCInfos[lane_id][barcode_id]["q20"].ToString(),
                                q30 = flowCellQCInfos[lane_id][barcode_id]["q30"].ToString()
                            });
                        }

                        sequenceConfig.inputs.lanes.Add(new SequenceLaneInfo()
                        {
                            lane_id = lane_id,
                            reads = "reads",
                            raw_gc = flowCellQCInfos[lane_id].Values.Select(m => m.Where(n => n.Key == "raw_gc").Select(n => n.Value)).Select(p => p.Average()).ToString(),
                            q20 = flowCellQCInfos[lane_id].Values.Select(m => m.Where(n => n.Key == "q20").Select(n => n.Value)).Select(p => p.Average()).ToString(),
                            q30 = flowCellQCInfos[lane_id].Values.Select(m => m.Where(n => n.Key == "q30").Select(n => n.Value)).Select(p => p.Average()).ToString(),
                            status = "success",
                            samples = sampleInfos
                        });
                    }
                    var resultInfo = zlimsApiService.CompleteSequenceTask(sequenceId, sequenceConfig);
                    if (resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The CompleteSequenceTask method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CompleteSequenceTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }

        public void SendCompleteSequencingTaskToLimis(string sequenceId, string _flowcell_id)
        {

            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();

                    if (string.IsNullOrEmpty(sequenceId))
                    {
                        return;
                    }

                    SequenceTaskConfig sequenceConfig = new SequenceTaskConfig()
                    {
                        completion_date = DateTime.UtcNow.ToString("u"),
                        completion_status = "success",
                        inputs = new SequenceInput()
                        {
                            sequencing_data_path = "E://SaveData",
                            copying_status = "success",
                            lanes = new List<SequenceLaneInfo>()
                        }
                    };

                    IList<SequenceSampleInfo> sampleInfos = new List<SequenceSampleInfo>();

                    sampleInfos.Add(new SequenceSampleInfo()
                    {
                        barcode_id = "1",
                        raw_gc = "10",
                        q20 = "20",
                        q30 = "30"
                    });


                    sequenceConfig.inputs.lanes.Add(new SequenceLaneInfo()
                    {
                        lane_id = "L01",
                        reads = "reads",
                        raw_gc = "10",
                        q20 = "20",
                        q30 = "30",
                        status = "success",
                        samples = sampleInfos
                    });

                    var resultInfo = zlimsApiService.CompleteSequenceTask(sequenceId, sequenceConfig);
                    if (resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The CompleteSequenceTask method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CompleteSequenceTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }
        #endregion

        #region Update Sequencing QC Data
        public void SendQcDatasToLimis(List<QCDataValue> curQCData, string _lane_id, int cycleNum, string sequenceId)
        {
            if (string.IsNullOrEmpty(sequenceId))
            {
                return;
            }
            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    var qcTaskConfig = new SequenceQCTaskConfig()
                    {
                        inputs = new SequencingQCInput()
                        {
                            version = "V2",
                            qc_data = new List<CycleQCInfo>()
                        }
                    };
                    var cycleQCInfo = new CycleQCInfoV2()
                    {
                        lane_id = _lane_id,
                        cycle_id = cycleNum,
                        Offset_A_X = curQCData.Where(m => m.QCMetricName.Contains("Offset_A_X")).FirstOrDefault().QCValue.ToString(),
                        Offset_C_Y = curQCData.Where(m => m.QCMetricName.Contains("Offset_C_Y")).FirstOrDefault().QCValue.ToString(),
                        Offset_T_X = curQCData.Where(m => m.QCMetricName.Contains( "Offset_H_X")).FirstOrDefault().QCValue.ToString(),
                        Offset_G_Y = curQCData.Where(m => m.QCMetricName.Contains( "Offset_H_Y")).FirstOrDefault().QCValue.ToString(),
                        Signal_A = curQCData.Where(m => m.QCMetricName.Contains( "Signal_L")).FirstOrDefault().QCValue.ToString(),
                        Signal_T = curQCData.Where(m => m.QCMetricName.Contains( "Signal_H")).FirstOrDefault().QCValue.ToString(),
                        Background_A = curQCData.Where(m => m.QCMetricName.Contains( "Background_H")).FirstOrDefault().QCValue.ToString(),
                        Background_C = curQCData.Where(m => m.QCMetricName.Contains( "Background_L")).FirstOrDefault().QCValue.ToString(),
                        ESR = curQCData.Where(m => m.QCMetricName.Contains( "ESR")).FirstOrDefault().QCValue.ToString(),
                        SNR_A = curQCData.Where(m => m.QCMetricName.Contains( "SNR_H")).FirstOrDefault().QCValue.ToString(),
                        SNR_C = curQCData.Where(m => m.QCMetricName.Contains( "SNR_L")).FirstOrDefault().QCValue.ToString(),
                        RHO_A = curQCData.Where(m => m.QCMetricName.Contains( "RHO_A_H")).FirstOrDefault().QCValue.ToString(),
                        RHO_G = curQCData.Where(m => m.QCMetricName.Contains( "RHO_A_L")).FirstOrDefault().QCValue.ToString(),
                        RHO_C = curQCData.Where(m => m.QCMetricName.Contains( "RHO_C_H")).FirstOrDefault().QCValue.ToString(),
                        RHO_T = curQCData.Where(m => m.QCMetricName.Contains( "RHO_T_L")).FirstOrDefault().QCValue.ToString(),
                        Base_A = curQCData.Where(m => m.QCMetricName.Contains( "Base_A")).FirstOrDefault().QCValue.ToString(),
                        Base_C = curQCData.Where(m => m.QCMetricName.Contains( "Base_C")).FirstOrDefault().QCValue.ToString(),
                        Base_G = curQCData.Where(m => m.QCMetricName.Contains( "Base_G")).FirstOrDefault().QCValue.ToString(),
                        Base_T = curQCData.Where(m => m.QCMetricName.Contains( "Base_T")).FirstOrDefault().QCValue.ToString(),
                        Base_N = curQCData.Where(m => m.QCMetricName.Contains( "Base_N")).FirstOrDefault().QCValue.ToString(),
                        Fit = curQCData.Where(m => m.QCMetricName.Contains( "Fit")).FirstOrDefault().QCValue.ToString(),
                        BIC = curQCData.Where(m => m.QCMetricName.Contains( "BIC")).FirstOrDefault().QCValue.ToString(),
                        Lag_A = curQCData.Where(m => m.QCMetricName.Contains( "Lag_H")).FirstOrDefault().QCValue.ToString(),
                        Lag_C = curQCData.Where(m => m.QCMetricName.Contains( "Lag_L")).FirstOrDefault().QCValue.ToString(),
                        Runon_A = curQCData.Where(m => m.QCMetricName.Contains( "Runon_H")).FirstOrDefault().QCValue.ToString(),
                        Runon_C = curQCData.Where(m => m.QCMetricName.Contains( "Runon_L")).FirstOrDefault().QCValue.ToString()
                    };
                    qcTaskConfig.inputs.qc_data.Add(cycleQCInfo);

                    var resultInfo = zlimsApiService.UpdateSequenceQCTask(sequenceId, qcTaskConfig);
                    if (resultInfo.IsSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The UpdateSequenceQCTask method of ZLimsApiService called success. the qc info: " + JsonConvert.SerializeObject(qcTaskConfig));
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The UpdateSequenceQCTask method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ". the qc info: " + JsonConvert.SerializeObject(qcTaskConfig));
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
        }
        #endregion

        #region Get sequence type list
        /// <summary>
        /// get all sequence types information by dnb_id
        /// </summary>
        /// <param name="dnb_id"></param>
        /// <returns></returns>
        public string[] GetSequenceType(/*string dnb_id*/)
        {
            IList<string> resultList = new List<string>();

            //not simulate zlims
            try
            {
                #region GetSequenceType
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    ResultInfo resultInfo = scope.Resolve<IZLimsApiService>().GetSequenceType();

                    if (resultInfo.IsSuccess && resultInfo.ResultData != null)
                    {
                        var list = JsonConvert.DeserializeObject<IList<ContentInfo>>(resultInfo.ResultData.ToString());
                        if (list != null && list.Count > 0)
                        {
                            resultList = list.Select(m => m.content_name).ToList();
                        }

                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The GetSequenceType method of ZLimsApiService called success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The GetSequenceType method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }
            return resultList.ToArray();
        }

        #endregion

        #region Get sequence script files
        /// <summary>
        /// get all sequence scripts information by sequenceType
        /// </summary>
        /// <param name="sequenceType">dnb id</param>
        /// <returns></returns>
        public bool DownloadSequenceScripts(string sequenceType, ref string wfScript, ref string imgScript, ref string bioScript)
        {
            bool result = false;

            try
            {
                #region GetSequenceType
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    ResultInfo resultInfo = scope.Resolve<IZLimsApiService>().GetSequenceScripts(sequenceType);

                    if (resultInfo.IsSuccess)
                    {
                        var list = JsonConvert.DeserializeObject<IList<ContentInfo>>(resultInfo.ResultData.ToString());
                        if (list != null && list.Count > 0)
                        {
                            foreach (ContentInfo con in list)
                            {
                                string fileName = JsonConvert.DeserializeAnonymousType(con.metadata.ToString(), new { script_type = string.Empty }).script_type;
                                //TODO need to confirm file name of scripts
                                if (fileName.IndexOf("workflow") != -1)
                                {
                                    wfScript = con.content;
                                    fileName = "workflow";
                                }
                                else if (fileName.IndexOf("image") != -1)
                                {
                                    imgScript = con.content;
                                    fileName = "image";
                                }
                                else if (fileName.IndexOf("bio") != -1)
                                {
                                    bioScript = con.content;
                                    fileName = "bio";
                                }
                                var scriptPath = ConfigMgr.Get.GetCurValue<string>("System", "ScriptHomePath");
                                DirectoryInfo dir = new DirectoryInfo(Path.Combine(scriptPath, "productui"));
                                if (!dir.Exists) dir.Create();
                                DirectoryInfo dir2 = new DirectoryInfo(Path.Combine(scriptPath, "productui", sequenceType));
                                if (!dir2.Exists) dir2.Create();
                                //File.WriteAllText(Path.Combine(@"C:\BGI\Scripts", fileName + ".py"), con.content);
                                File.WriteAllText(Path.Combine(scriptPath, "productui", sequenceType, fileName + ".py"), con.content);
                            }

                            result = true;

                            this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, The DownloadSequenceScripts method of ZLimsApiService called success.");
                        }
                        else
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, But the DownloadSequenceScripts method of ZLimsApiService got nothing.");
                        }
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The DownloadSequenceScripts method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }

            return result;
        }
        #endregion

        #region Download Barcode File
        /// <summary>
        /// Download BarcodeFile
        /// </summary>
        /// <param name="barcode_length">barcode_length</param>
        /// <param name="_Slide">_Slide</param>
        /// <returns></returns>
        public void DownloadBarcodeFile(int barcode_length, string flowcellid)
        {
            try
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    string barcode_fileName = "barcode.csv";
                    string barcode_filePath = ConfigMgr.Get.GetCurValue<string>("System", "BarcodeFilePath");
                    if (!string.IsNullOrEmpty(barcode_filePath))
                    {
                        var result = zlimsApiService.GetFlowcellInfo(flowcellid);
                        var librarySampleInfoList = (result.ResultData as FlowcellInfo)?.lanes?.SingleOrDefault()?.samples;
                        if (result.IsSuccess && librarySampleInfoList != null && librarySampleInfoList.Count > 0)
                        {
                            StringBuilder strBarcode = new StringBuilder();
                            librarySampleInfoList = librarySampleInfoList.OrderBy(m => int.Parse(m.barcode_id)).ToList();
                            foreach (LibrarySampleInfo sampleInfo in librarySampleInfoList)
                            {
                                strBarcode.AppendLine(sampleInfo.barcode_id + "," + (barcode_length > 0 ? sampleInfo.atcg.Substring(0, barcode_length) : sampleInfo.atcg));
                            }
                            if (!Directory.Exists(barcode_filePath))
                            {
                                Directory.CreateDirectory(barcode_filePath);
                            }
                            File.WriteAllText(Path.Combine(barcode_filePath, barcode_fileName), strBarcode.ToString());

                            this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API SUCCESS, Write barcode file successfully, and the written content is: " + strBarcode.ToString() + ".");
                        }
                        else
                        {
                            this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, Write barcode file failure, get nothing about barcode information or the failure message is : " + result.ErrorMessage.ToString() + ".");
                        }
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Debug, "The BarcodeFilePath parameter of remote configuration should not be null.");
                    }
                }
            }
            catch (Exception ex)
            {
                this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API EXCEPTION, Write barcode file exception, and the exception message is : " + ex.ToString() + ".");
            }
        }
        #endregion

        #region Get QC information from fastq file
        /// <summary>
        /// Get QC information from fastq file
        /// </summary>
        /// <param name="fastqPath">fqstq file path, until flowcell path</param>
        /// <returns>
        /// return the below data structure:
        /// [
        ///     "lane_id":[
        ///         "barcode_id":[
        ///             "qc_key_1": "qc_value_1",
        ///             "qc_key_2": "qc_value_2"
        ///         ]
        ///     ]
        ///  ]
        /// </returns>
        private IDictionary<string, IDictionary<string, IDictionary<string, float>>> GetLaneQCFromFastq(string local_fastq_path, string _flowcell_id)
        {
            var fastqPath = Path.Combine(local_fastq_path, "OutputFq", _flowcell_id);
            if (!Directory.Exists(fastqPath))
            {
                throw new Exception("GetLaneQCFromFastq method exception: the fastqLanPath(" + fastqPath + ") is not exist.");
            }

            //the data structure of laneQCInfo
            //[
            //  "lane_id":[
            //      "barcode_id":[
            //          "qc_key_1": "qc_value_1",
            //          "qc_key_2": "qc_value_2"
            //      ]
            //  ]
            //]
            IDictionary<string, IDictionary<string, IDictionary<string, float>>> flowCellQCInfo = new Dictionary<string, IDictionary<string, IDictionary<string, float>>>();

            string[] lanePaths = Directory.GetDirectories(fastqPath);
            foreach (string lanePath in lanePaths)
            {
                string lane_id = new DirectoryInfo(lanePath).Name;
                flowCellQCInfo.Add(lane_id, new Dictionary<string, IDictionary<string, float>>());

                string[] fastqFilePaths = Directory.GetFiles(lanePath).Where(m => m.EndsWith(".fq.fqStat.txt")).ToArray();
                foreach (string fastqFilePath in fastqFilePaths)
                {
                    string fastqFileName = new FileInfo(fastqFilePath).Name;
                    string barcode = fastqFileName.Substring(fastqFileName.LastIndexOf('_') + 1).Split('.')[0];
                    if (barcode != "undecoded" && barcode != "conflict")
                    {
                        flowCellQCInfo[lane_id].Add(barcode, new Dictionary<string, float>());
                        foreach (string line in File.ReadLines(fastqFilePath))
                        {
                            string[] qcData = line.Split('\t');
                            if (qcData[0] != "#Pos")
                            {
                                switch (qcData[0])
                                {
                                    case "#ReadNum":
                                        flowCellQCInfo[lane_id][barcode].Add("reads", float.Parse(qcData[1]));
                                        break;
                                    case "#GC%":
                                        flowCellQCInfo[lane_id][barcode].Add("raw_gc", float.Parse(qcData[1]));
                                        break;
                                    case "#Q20%":
                                        flowCellQCInfo[lane_id][barcode].Add("q20", float.Parse(qcData[1]));
                                        break;
                                    case "#Q30%":
                                        flowCellQCInfo[lane_id][barcode].Add("q30", float.Parse(qcData[1]));
                                        break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return flowCellQCInfo;
        }
        #endregion
    }
}