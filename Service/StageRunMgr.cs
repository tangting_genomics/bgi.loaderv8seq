//-----------------------------------------------------------------------------
// Copyright (c) 2016 BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;

using BGI.Common.Logging;
using BGI.Common.Utils;
using BGI.Common.Config;
using BGI.RPC;
using BGI.Scripting;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BGI.RPC.Data;
using System.Linq;
using System.Xml;
using Newtonsoft.Json;
using BGI.OSIIP;
using System.Collections.Specialized;

namespace BGI.ZebraV2Seq.Service
{
    public enum ScriptErrorEnum
    {
        NoError = 0,
        ParseError = 2,
        CompileError = 4,
        EmptyParaError = 6,
        InvalidParaError = 8,
        InvalidPhaseError = 10,
        DiskNotEnough = 12,
        OtherError = 99,
    }
    /// <summary>
    /// Class to manage scripting for a particular Stage.
    /// Script runs are done here, as well as all monitoring of a scripts activity and interaction with the script.
    /// Only one Script can be run on a Stage at a time.
    /// For V0.1, there will only be one of these.  For V2 probably two.
    /// This is started by the ZebraScheduler and only ends when the Schedulre is shutting down.
    /// </summary>
    public class StageRunMgr : IStageRunMgrIPC
    {
        private Logger _Logger = LogMgr.GetLogger("Scripting");
        
        private const int MAIN_LOOP_SLEEP_MS = 1000;
        private ThreadState _CurState = ThreadState.Sleeping;
        private readonly object _LockObj = new object();

        ScriptStateEnum _CurScriptState = ScriptStateEnum.Idle;


        private string _experimentType = string.Empty;
        private string _sampleId = string.Empty;
        private string imageSavePath = string.Empty;

        private string _KitBarcode = null;
        private bool _IsSetKitBarcode = false;
        private List<ScriptMessageInfo> _MessageL = new List<ScriptMessageInfo>();
        private List<ServiceMessageInfo> _AlarmMessageList = new List<ServiceMessageInfo>();
        private List<ServiceMessageInfo> _LogMessageList = new List<ServiceMessageInfo>();

        private string _CurrentScriptFileFullName = null;
        private int _HeartbeatCount = 0;
        private string _CurLanguage = "en-US";

        public string StageRunName { get; private set; }
        public string FlowcellBarcode { get; private set; } = null;
        public StageRunMgrIPCService StageService { get; private set; } = null;
        private ScriptTypeEnum? _CurrentScriptType = null;
        private List<QCDataValue> qcDataList = new List<QCDataValue>();         //QC

        private Dictionary<string, string> _ExperimentInfo = new Dictionary<string, string>();
        private enum ThreadState
        {
            Stopped,
            Stopping,
            Sleeping,
            Working
        }

        public ScriptTypeEnum? CurrentScriptType
        {
            get { return _CurrentScriptType; }
        }

        public ScriptStateEnum ScriptState
        {
            get
            {
                return _CurScriptState;
            }
            private set
            {
                _CurScriptState = value;
            }
        }

        public StageRunMgr(string ServiceName)
        {
            this.StageRunName = ServiceName;
            if (ServiceName.EndsWith("A")) _Logger = LogMgr.GetLogger($"ScriptingA");
            else if(ServiceName.EndsWith("B"))_Logger = LogMgr.GetLogger($"ScriptingB");
        }

        public void Init()
        {
            LogSeverityEnum sev = ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel");
            _Logger.SetMinLogLevel(sev);
            ConfigMgr.Get.RegisterLogLevelUpdate(_Logger, "System", "LogLevel");

            _Logger.Log()(LogSeverityEnum.Debug, "Top of Loop.");

            StageService = new StageRunMgrIPCService(this.StageRunName);
            StageService.Init(this);
            StageService.SendScriptStatus(_CurScriptState);

            _DeviceMgrAccess = DeviceMgrZebrav2.GetAccess(this);

            // Instantiate script API singletons
            //BioOperations.DeviceManager = DeviceMgrZebrav2.GetBase;
            //ImageOperations.DeviceManager = DeviceMgrZebrav2.GetBase;
            //WorkFlowOperations.DeviceManager = DeviceMgrZebrav2.GetBase;
        }

        private void OnServiceMessageEvent(object sender, ServiceMessageInfo e)
        {
            if (e.MessageType == ScriptMessageSeverityEnum.Info)
            {
                AddLogMessage(e);
            }
        }

        public string ScriptHomePath
        {
            get
            {
                string rtnVal = ConfigMgr.Get.GetCurValue<string>("System", "ScriptHomePath");
                if (!rtnVal.EndsWith("\\"))
                {
                    throw new BGIException("ScriptHomePath value must end in backslash to be valid path.");
                }
                return rtnVal;
            }
            set { }
        }

        public bool IsUsedEncryptScript
        {
            get
            {
                try
                {
                    return ConfigMgr.Get.GetCurValue<bool>("System", "IsUsedEncryptScript");
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"Get IsUsedEncryptScript Failed {ex.ToString()}");
                    return false;
                }
                
            }
        }

        // Script Runner thread management
        private IScript _ScriptRun = null;

        private DeviceMgrAccess _DeviceMgrAccess;

        public void SafeMainLoop()
        {
            try
            {
                MainLoop();
            }
            finally  // All exceptions' messages & stack traces had been recorded in MainLoop.
            {
                ;
            }
        }

        public void MainLoop()
        {
            bool exit = false;
            int iResult = 0;
            DateTime wakeTime = DateTime.UtcNow.AddMinutes(-1);
            // OK, now loop to do any status polling
            do
            {
                bool doWork = false;

                // Main loop sleep to avoid cpu-bound process
                Thread.Sleep(100);

                // Should we do work now or wait more
                lock (_LockObj)
                {
                    if (_CurState == ThreadState.Sleeping
                        || _CurState == ThreadState.Working)
                    {
                        if (wakeTime < DateTime.UtcNow)
                        {
                            doWork = true;
                        }
                    }
                }

                if (doWork)
                {

                    lock (_LockObj)
                    {
                        _CurState = ThreadState.Working;
                    }

                    // Do the work and handle certain types of exceptions.  All other exceptions should
                    // fall back up the stack and be caught by the global unhandled exception handler.
                    try
                    {
                        _HeartbeatCount++;
                        
                        // Check for request to stop processing
                        lock (_LockObj)
                        {
                            if (_CurState == ThreadState.Stopping)
                            {
                                //break;
                            }
                        }

                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "WashError", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.CheckWashError(this.StageRunName) }));
                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "SequenceError", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.CheckSequenceError(this.StageRunName) }));

                        _Logger.Log()(LogSeverityEnum.Debug, "Entering working area at state {0}.", _CurScriptState);

                        // The following is a state engine for running scripts on the system for this stage.

                        switch (_CurScriptState)
                        {
                            case ScriptStateEnum.Idle:
                                // Nothing to do but wait for client to start something.
                                break;
                            case ScriptStateEnum.SetupRun:

                                if (FlowcellBarcode != null && _KitBarcode != null && _CurrentScriptFileFullName != null)
                                {
                                    SetupScriptRunByFileName();
                                }
                                break;
                            case ScriptStateEnum.ReadyToRun:
                                // all data needed for run found and valid.  Changed to ReadyToRun by Server to tell Client we are ready to start.
                                break;
                            case ScriptStateEnum.ScriptRunning:
                                
                                break;
                            case ScriptStateEnum.ScriptPausing:
                                _CurScriptState = ScriptStateEnum.ScriptPaused;
                                break;
                            case ScriptStateEnum.ScriptPaused:
                                break;
                            case ScriptStateEnum.ScriptStopping:
                                //_CurScriptState = ScriptStateEnum.Idle;
                                break;
                            case ScriptStateEnum.Unknown:
                                break;
                            default:
                                _Logger.Log()(LogSeverityEnum.Error, "Unsupported Script Status of {0} found.", _CurScriptState);
                                break;
                        }
                        // Now tell client current state in case it changed.
                        StageService.SendScriptStatus(_CurScriptState);

                    }
                    catch (BGIException e)
                    {
                        // Handle BGI Exceptions here if possible.
                        _Logger.Log("MainLoop", e);
                        SendScriptMessage(e.Message);
                        HandleError(e);
                        StartSetup();

                    }

                    int msToWait = MAIN_LOOP_SLEEP_MS;

                    wakeTime = DateTime.UtcNow.AddMilliseconds(msToWait);
                    _Logger.Log()(LogSeverityEnum.Debug, "Sleeping for {0} ms.", msToWait);

                }

                lock (_LockObj)
                {
                    // Check if this thread is trying to stop
                    if (_CurState == ThreadState.Stopping)
                    {
                        _Logger.Log()(LogSeverityEnum.Info, "Stopping");

                        _CurState = ThreadState.Stopped;
                    }

                    // Exit the loop if we are marked stopped
                    if (_CurState == ThreadState.Stopped)
                    {
                        exit = true;
                    }
                }

            } while (!exit);

            _Logger.Log()(LogSeverityEnum.Info, "Exiting");
            iResult = 0;

            if (iResult != 0)
            {
                //throw new BGIException(String.Format("Error returned from Disconnect: {0}", PlateGripperObj.GetErrorMessage()));
            }

            _Logger.Log()(LogSeverityEnum.Warning, "Exiting main processing loop");
        }

        private void HandleError(Exception e)
        {
            var handle = AppDomain.CurrentDomain.GetData("RuntimeErroHandle")
                                                as Func<Exception, Action<string>, Action<string>, Task[]>;

            //handle?.Invoke(e, msg => DeviceMgrZebrav01.Get.SendeMessageToZLims(msg, "error"),
            //    msg => SendScriptMessage(msg));

            handle?.Invoke(e, msg => AddAlarmMessage(new ServiceMessageInfo() { Message = msg, MessageType = ScriptMessageSeverityEnum.Error }),
                msg => SendScriptMessage(msg));
        }

        public void AddAlarmMessage(ServiceMessageInfo msg)
        {
            lock (_AlarmMessageList)
            {
                if(msg == null)
                {
                    _Logger.Log()(LogSeverityEnum.Warning, $"Wit Null Message");
                    return;
                }
                if (msg.Message == null || string.IsNullOrEmpty(msg.Message.Trim()) || string.IsNullOrWhiteSpace(msg.Message.Trim()))
                {
                    _Logger.Log()(LogSeverityEnum.Warning, $"With Empty Message");
                    return;
                }
                if(_AlarmMessageList.Exists(p => p.Message == msg.Message))
                {
                    return;
                }
                int messageCount = 0;
                if (_AlarmMessageList.Count > 0)
                {
                    messageCount = _AlarmMessageList[_AlarmMessageList.Count - 1].MessageId + 1;
                }
                else
                {
                    messageCount += 1;
                }
                // TODO format datetime string
                msg.MessageId = messageCount;
                _AlarmMessageList.Add(msg);
                StageService.SendAlarmMessageId(messageCount);
            }
        }

        /// <summary>
        /// Get the type of script operation API needed for the script.  Only one is allowed.
        /// </summary>
        /// <param name="_CurrentScriptFileFullName"></param>
        /// <returns></returns>
        private ScriptTypeEnum GetScriptTypeFromFile(string _CurrentScriptFileFullName)
        {
            FileInfo scriptFI = new FileInfo(_CurrentScriptFileFullName);
            Regex regex = new Regex(@"(?i)^.*globals\(\)\.get\('(\w+)'\)\s*$");
            ScriptTypeEnum? retVal = null;
            string scriptTypeStr = null;
            // TODO check script fileneme exists
            FileStream fs = new FileStream(scriptFI.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (StreamReader sr = new StreamReader(fs))
            {
                string curLine = null;
                while ((curLine = sr.ReadLine()) != null)
                {
                    Match m = regex.Match(curLine);
                    if (m.Success)
                    {
                        if (scriptTypeStr != null)
                        {
                            throw new BGIException("More than one Operation mode (BIO, IMG, WF) used in script.  Only one allowed.");
                        }
                        scriptTypeStr = m.Groups[1].Value;
                        break;
                    }
                }
            }
            GetScriptType(scriptTypeStr, ref retVal);
            if (!retVal.HasValue)
            {
                throw new BGIException("Could not find valid operation type for script {0}", _CurrentScriptFileFullName);
            }
            return retVal.Value;
        }


        private ScriptTypeEnum GetScriptTypeFromStr(string fileContent)
        {
            Regex regex = new Regex(@"(?i)^.*globals\(\)\.get\('(\w+)'\)\s*$");
            ScriptTypeEnum? retVal = null;
            string scriptTypeStr = null;

            string[] lines = fileContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                Match m = regex.Match(line);
                if (m.Success)
                {
                    if (scriptTypeStr != null)
                    {
                        throw new BGIException("More than one Operation mode (BIO, IMG, WF) used in script.  Only one allowed.");
                    }
                    scriptTypeStr = m.Groups[1].Value;
                    break;
                }
            }

            GetScriptType(scriptTypeStr, ref retVal);
            if (!retVal.HasValue)
            {
                throw new BGIException("Could not find valid operation type for script {0}", _CurrentScriptFileFullName);
            }

            return retVal.Value;
        }

        private void GetScriptType(string scriptTypeStr, ref ScriptTypeEnum? retVal)
        {
            if (scriptTypeStr != null)
            {
                if (scriptTypeStr.Equals(String.Format("{0}{1}", typeof(BioOperations).Name, "Class")))
                {
                    retVal = ScriptTypeEnum.BIO;
                }
                else if (scriptTypeStr.Equals(String.Format("{0}{1}", typeof(ImageOperations).Name, "Class")))
                {
                    retVal = ScriptTypeEnum.IMG;
                }
                else if (scriptTypeStr.Equals(String.Format("{0}{1}", typeof(WorkFlowOperations).Name, "Class")))
                {
                    retVal = ScriptTypeEnum.WF;
                }
                else if (scriptTypeStr.Equals(String.Format("{0}{1}", typeof(EngineerOperations).Name, "Class")))
                {
                    retVal = ScriptTypeEnum.ENGINEER;
                }
            }
        }

        public int GetHeartbeat()
        {
            return _HeartbeatCount;
        }

        private void Script_OnStatusMessageTrace(object sender, StatusEventArgs e)
        {
            SendScriptMessage(e.Message);
        }

        private void SendScriptMessage(string message)
        {
            lock (_MessageL)
            {
                int messageCount = _MessageL.Count + 1;
                // TODO format datetime string
                ScriptMessageInfo smi = new ScriptMessageInfo(messageCount, ScriptMessageSeverityEnum.Info, DateTime.Now.ToString(), message);
                _MessageL.Add(smi);
                StageService.SendScriptMessageId(messageCount);
            }
        }

        public void Stop()
        {
            lock (_LockObj)
            {
                if (_CurState == ThreadState.Working)
                {
                    _Logger.Log()(LogSeverityEnum.Info, "Signal to stop working thread received.  Setting state to Stopping.");
                }
                _CurState = ThreadState.Stopping;
            }
        }

        public void AddQCValues(List<QCDataValue> qcDatas)
        {
            lock (_LockObj)
            {
                qcDataList.AddRange(qcDatas);
            }
        }

        #region Interface Commands

        public void StartSetup()
        {
            FlowcellBarcode = null;
            _KitBarcode = null;
            _MessageL.Clear();
            _CurScriptState = ScriptStateEnum.SetupRun;
            _CurrentScriptFileFullName = null;
        }

        public void OverrideFlowcellBarcode(string barcode)
        {
            try
            {
                if(string.Compare(FlowcellBarcode,barcode,true) != 0)
                {
                    FlowcellBarcode = barcode;
                    if (!string.IsNullOrEmpty(barcode))
                        DeviceMgrZebrav2.Get.SetSlide(this.StageRunName, barcode,false);
                    else
                        DeviceMgrZebrav2.Get.SetSlide(this.StageRunName, barcode,true);
                }
            }
            catch(BGIRemoteException ex)
            {
                FlowcellBarcode = string.Empty;
                _Logger.Log()(LogSeverityEnum.Error, ex.ice_message_);
                throw ex;
            }
        }

        public void ScanFlowcellBarcode()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to scan flowcell barcode.");

            // TODO: Trigger system to scan flowcell barcode.  When barcode received send it to client.
            FlowcellBarcode = DeviceMgrZebrav2.Get.SendBarcode(this.StageRunName);
            if (!string.IsNullOrEmpty(FlowcellBarcode))
                DeviceMgrZebrav2.Get.SetSlide(this.StageRunName, FlowcellBarcode,false);
            else
                DeviceMgrZebrav2.Get.SetSlide(this.StageRunName, FlowcellBarcode,true);
            this.StageService.SendFlowcellBarcode(FlowcellBarcode);
            //FlowcellBarcode = "0"; // testing
        }

        public void ScanReagentKitBarcode()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to scan kit barcode.");

            // TODO: Trigger system to scan Kit barcode.  When barcode received send it to client.

            _KitBarcode = "0"; // testing
        }

        public void SaveSelectedScriptFileName(string strFileName)
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to set user selected script file Name.");
            _CurrentScriptFileFullName = System.IO.Path.Combine(ScriptHomePath, strFileName);
        }

        public int RunScriptNow(string strFileName)
        {
            throw new NotImplementedException();
        }

        public ScriptRunInfo GetScriptRunInfo(int scriptRunId)
        {
            throw new NotImplementedException();
        }

        public void StartRun()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to start script run.");
            this.ClearLog();
            _MessageL.Clear();
            // use background work to run the script
            var bkWorker = new System.ComponentModel.BackgroundWorker();
            bkWorker.DoWork += (x, y) =>
            {
                Thread.CurrentThread.Name = "ScriptRun";
                bool hasError = false;
                try
                {
                    DeviceMgrZebrav2.Get.InitSequenceMap(this.StageRunName);
                    List<ServiceMessageInfo> alarList = new List<ServiceMessageInfo>();
                    lock (_AlarmMessageList)
                    {
                        foreach(ServiceMessageInfo msg in _AlarmMessageList)
                        {
                            alarList.Add(msg);
                        }
                        if(this.CurrentScriptType == ScriptTypeEnum.WF)
                        {
                            _AlarmMessageList.Clear();
                        }
                        else
                        {
                            _AlarmMessageList.RemoveAll(a => a.MessageType != ScriptMessageSeverityEnum.Info);
                            alarList.RemoveAll(a => a.MessageType == ScriptMessageSeverityEnum.Info);
                        }
                        StageService.SendAlarmMessageId(alarList.Count+1);
                    }

                    lock (_LogMessageList)
                    {
                        foreach(ServiceMessageInfo msg in alarList)
                        {
                            _LogMessageList.Add(msg);
                            //AddLogMessage(msg);
                        }
                        StageService.SendLogMessageId(_LogMessageList.Count);
                    }

                    
                    if (_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.WF)
                    {
                        DeviceMgrZebrav2.Get.StartRecordMetric(this.StageRunName);
                        DeviceMgrZebrav2.Get.ReagentNeedleDown(this.StageRunName);
                        DeviceMgrZebrav2.Get.UnLock(true, this.StageRunName);
                        DeviceMgrZebrav2.Get.PrepareImage(this.StageRunName);
                    }
                    DeviceMgrZebrav2.Get.SetLedIndicatorColor(Control.Device.LedStatus.Running);
                    _CurScriptState = ScriptStateEnum.ScriptRunning;
                    // The next line is a blocking command in this inline thread which is run below by RunWorkerAsync.
                    _ScriptRun.Run();

                    // TODO detect if script sys.exit was called with non-zero value. This means error from script.
                    _Logger.Log()(LogSeverityEnum.Info, "Script ended normally.");
                }
                catch (ScriptReturnException sre)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Info, "Exception returned from .NET ({0}).", sre.Message);
                    _CurScriptState = ScriptStateEnum.ScriptStopping;
                }
                catch (ScriptStopException)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Info, "Script stopped by user command.");
                }
                catch (ScriptAbortException)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Info, "Script aborted by user command.");
                    _CurScriptState = ScriptStateEnum.ScriptStopping;
                }
                catch(ScriptRunException ex)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Error, $"Script with Error:{ex.ToString()}");
                    CreateScriptRunnerExceptionMsg(ex);
                }
                catch (BGIException ex)
                {
                    hasError = true;
                    if (ex.InnerException is ScriptRunException)
                    {
                        ScriptRunException inner = ex.InnerException as ScriptRunException;
                        if(inner != null)
                        {
                            CreateScriptRunnerExceptionMsg(inner);
                        }
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    _Logger.Log("Script Exception caught.", ex);
                    _CurScriptState = ScriptStateEnum.ScriptStopping;
                }
                finally
                {
                    this._IsSetKitBarcode = false;
                    if (_ScriptRun != null)
                    {
                        
                        if(_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.WF)
                        {
                            DeviceMgrZebrav2.Get.StopRecordMetric(this.StageRunName);
                            DeviceMgrZebrav2.Get.ResetBIO(this.StageRunName);
                        }
                        if(_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.BIO)
                        {
                            DeviceMgrZebrav2.Get.ResetBIO(this.StageRunName);
                        }
                        DeviceMgrZebrav2.Get.ResetStatus(this.StageRunName, hasError);

                        _ScriptRun.Reset();
                    }
                    if (!hasError)
                    {
                        if (DeviceMgrZebrav2.Get.MachineStatusMap.ContainsKey(this.StageRunName))
                            DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] = MachineStatus.Idle;
                        DeviceMgrZebrav2.Get.SetLedIndicatorColor(Control.Device.LedStatus.Ready);
                    }
                    _ExperimentInfo.Clear();
                    _ScriptRun = null;
                    FlowcellBarcode = string.Empty;
                    _CurScriptState = ScriptStateEnum.Idle;
                }
            };
            bkWorker.RunWorkerAsync();

            //_CurScriptState = ScriptStateEnum.ScriptRunning;
        }


        public void PauseScript()
        {
            if(_CurScriptState == ScriptStateEnum.ScriptPausing)
            {
                _Logger.Log()(LogSeverityEnum.Info, $"Script is Pausing");
                return;
            }
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to pause script run.");
            if (_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.WF)
            {
                DeviceMgrZebrav2.Get.UnLock(false, this.StageRunName);
            }
            if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.Image)
            {
                DeviceMgrZebrav2.Get.ScannerPause(this.StageRunName);
            }
            else if(DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.WaitImage)
            {
                DeviceMgrZebrav2.Get.PauseImageWait(this.StageRunName);
            }
            else
            {
                _ScriptRun.RequestPause();
            }
            _CurScriptState = ScriptStateEnum.ScriptPausing;
        }

        public void ResumeScript()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to resume script run.");
            if (_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.WF)
            {
                DeviceMgrZebrav2.Get.UnLock(true, this.StageRunName);
                //Thread.Sleep(1000*)
            }
            if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.Image)
            {
                DeviceMgrZebrav2.Get.ScannerResume(this.StageRunName);
            }
            else if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.WaitImage)
            {
                DeviceMgrZebrav2.Get.ResumeImageWait(this.StageRunName);
            }
            else _ScriptRun.Resume();
            _CurScriptState = ScriptStateEnum.ScriptRunning;
        }

        public void StopScript()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to stop script run.");
            try
            {
                //if(_CurScriptState == ScriptStateEnum.ScriptStopping)
                //{
                //    if(DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] != MachineStatus.Stopping)
                //    {
                //        _Logger.Log()(LogSeverityEnum.Info, "Script is Stopping");
                //        return;
                //    }
                //}


                ServiceMessageInfo msg = RemoveAlarmMessage(Properties.Resource.strDuplicateSampleId);
                if (msg != null)
                {
                    AddLogMessage(msg);
                }

                msg = RemoveAlarmMessage(Properties.Resource.strDuplicateFlowcellId);
                if (msg != null)
                {
                    AddLogMessage(msg);
                }

                msg = RemoveAlarmMessage(Properties.Resource.strDuplicateReagentKitId);
                if (msg != null)
                {
                    AddLogMessage(msg);
                }

                if (!string.IsNullOrEmpty(_sampleId))
                {
                    _sampleId = string.Empty;
                    DeviceMgrZebrav2.Get.ClearSampleId(this.StageRunName);
                }
                if (!string.IsNullOrEmpty(FlowcellBarcode))
                {
                    FlowcellBarcode = string.Empty;
                    DeviceMgrZebrav2.Get.ClearSlide(this.StageRunName);
                }
                if (!string.IsNullOrEmpty(_KitBarcode))
                {
                    _KitBarcode = string.Empty;
                    DeviceMgrZebrav2.Get.ClearReagent(this.StageRunName);
                }
                
                if(_CurScriptState < ScriptStateEnum.ScriptRunning)
                {
                    _CurScriptState = ScriptStateEnum.Idle;
                    return;
                }
                if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.Image)
                {
                    DeviceMgrZebrav2.Get.ScannerStop(this.StageRunName);
                }
                else if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.Stopping)
                {
                    _CurScriptState = ScriptStateEnum.ScriptStopping;
                    throw new ScriptStopException("ImageStop");
                }
                else if (DeviceMgrZebrav2.Get.MachineStatusMap[this.StageRunName] == MachineStatus.WaitImage)
                {
                    _CurScriptState = ScriptStateEnum.ScriptStopping;
                    DeviceMgrZebrav2.Get.StopImageWait(this.StageRunName);
                }
                else _ScriptRun.RequestStop();
                _CurScriptState = ScriptStateEnum.ScriptStopping;
            }
            catch(Exception e)
            {
                _CurScriptState = ScriptStateEnum.ScriptStopping;
                throw e;
            }
        }

        public void AbortScript()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Client sent command to abort script run.");
            // TODO abort thread the hard way.  Kill it
            _CurScriptState = ScriptStateEnum.Unknown;
        }

        public ScriptMessageInfo[] GetScriptMessages(int startMessageId)
        {
            _Logger.Log()(LogSeverityEnum.Debug, "Client requested new list of messages from id {0}.", startMessageId);
            if (startMessageId < _MessageL.Count)
                return _MessageL.GetRange(startMessageId, _MessageL.Count - startMessageId).ToArray();
            return new ScriptMessageInfo[0];
        }

        public void SendReagentKit(string reagentKit)
        {
            try
            {
                
                if (string.Compare(_KitBarcode, reagentKit, true) != 0)
                {
                    _KitBarcode = reagentKit;
                    if (!string.IsNullOrEmpty(_KitBarcode))
                        DeviceMgrZebrav2.Get.SetReagent(this.StageRunName, _KitBarcode);
                }
                this._IsSetKitBarcode = true;
            }
            catch (BGIRemoteException ex)
            {
                this._IsSetKitBarcode = false;
                _KitBarcode = string.Empty;
                _Logger.Log()(LogSeverityEnum.Error, ex.ice_message_);
                throw ex;
            }
        }

        public void SetupScriptFile(string scriptFile)
        {
            _CurrentScriptFileFullName = Path.Combine(ScriptHomePath, scriptFile);
            //ScriptEncryptandDecrypt encrypt = new ScriptEncryptandDecrypt();
            //string fileContent = encrypt.PathDecryptScriptFile(_CurrentScriptFileFullName);
            //SetupScriptRunByFileContent(fileContent);
            SetupScriptRunByFileName();
        }

        private void SetupScriptRunByFileName()
        {
            try
            {
                ScriptTypeEnum sType = GetScriptTypeFromFile(_CurrentScriptFileFullName);
                SetupScriptFile(sType);
                if(sType == ScriptTypeEnum.ENGINEER)
                    _ScriptRun.InitScriptFile(_CurrentScriptFileFullName);
                _ScriptRun.ScriptFileContent = null;
                _ScriptRun.ScriptFileName = _CurrentScriptFileFullName;
                _CurScriptState = ScriptStateEnum.ReadyToRun;
                _CurrentScriptType = sType;
            }
            catch (Exception ex)
            {
                string strDesc = EnumUtils.GetDescriptionByName<DeviceCodeEnum>(DeviceCodeEnum.Others);
                int errCode = (int)ScriptErrorEnum.ParseError;
                if (StageRunName.EndsWith("B")) errCode = (int)ScriptErrorEnum.ParseError + 1;
                string msg = $"{Properties.Resource.strRecipeError}{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}";
                CreateAndSendMesssage(msg, $"{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}");
                _Logger.Log()(LogSeverityEnum.Error, "Set up script file by file name error,filename={0},ex={1}", _CurrentScriptFileFullName, ex);
            }
        }

        private void SetupScriptFile(ScriptTypeEnum sType)
        {

            switch (sType)
            {
                case ScriptTypeEnum.BIO:
                    _ScriptRun = new BioOperations((IBioOperations)_DeviceMgrAccess);
                    if(!_IsSetKitBarcode)
                        SetReagentKit();
                    break;
                case ScriptTypeEnum.IMG:
                    _ScriptRun = new ImageOperations((IImageOperations)_DeviceMgrAccess);
                    break;
                case ScriptTypeEnum.WF:
                    _ScriptRun = new WorkFlowOperations((IWorkFlowOperations)_DeviceMgrAccess);
                    WorkFlowOperations workflowOperations = _ScriptRun as WorkFlowOperations;
                    if (workflowOperations != null)
                    {
                        workflowOperations.BioOperationsAPI = new BioOperations((IBioOperations)_DeviceMgrAccess);
                        workflowOperations.ImageOperationsAPI = new ImageOperations((IImageOperations)_DeviceMgrAccess);
                    }
                    else
                    {
                        throw new BGIException("Unknown workflow type");
                    }
                    //SetReagentKit();
                    break;
                case ScriptTypeEnum.ENGINEER:
                    _ScriptRun = new EngineerOperations((IEngineerOperations)DeviceMgrZebrav2.GetAccess(this));
                    break;
                default:
                    throw new BGIException("Unsupported script type.");
            }
            _ScriptRun.OnStatusMessageTrace -= Script_OnStatusMessageTrace;
            _ScriptRun.OnStatusMessageTrace += Script_OnStatusMessageTrace;
        }

        public void SetupScriptRunByFileContent(string fileContent)
        {
            try
            {
                ScriptTypeEnum sType = GetScriptTypeFromStr(fileContent);
                SetupScriptFile(sType);
                _ScriptRun.ScriptFileName = null;
                _ScriptRun.ScriptFileContent = fileContent;
                _CurScriptState = ScriptStateEnum.ReadyToRun;
                _CurrentScriptType = sType;
            }
            catch (Exception ex)
            {
                string strDesc = EnumUtils.GetDescriptionByName<DeviceCodeEnum>(DeviceCodeEnum.Others);
                int errCode = (int)ScriptErrorEnum.ParseError;
                if (StageRunName.EndsWith("B")) errCode = (int)ScriptErrorEnum.ParseError + 1;
                string msg = $"{Properties.Resource.strRecipeError}{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}";
                CreateAndSendMesssage(msg, $"{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}");
                _Logger.Log()(LogSeverityEnum.Error, "Set up script file by file content error:{0}", ex);
            }
        }

        private void SetReagentKit()
        {
            if (!string.IsNullOrEmpty(_KitBarcode))
            {
                DeviceMgrZebrav2.Get.SetReagent(this.StageRunName, _KitBarcode);
            }
            else
            {
                DeviceMgrZebrav2.Get.SetReagent(this.StageRunName, string.Empty);
                _Logger.Log()(LogSeverityEnum.Warning, "Reaggent key is null or empty,set reagent key to default");
            }
        }

        public QCDataValue[] GetQCValues(string qcMetricName, int startCycleNum)
        {
            return qcDataList.FindAll(p => p.QCMetricName == qcMetricName && p.cycleNum >= startCycleNum).ToArray();
        }

        public void SendSampleId(string sampleId)
        {
            try
            {
                if (string.Compare(_sampleId, sampleId, true) != 0)
                {
                    _sampleId = sampleId;
                    DeviceMgrZebrav2.Get.SetSampleId(this.StageRunName, sampleId);
                }
            }
            catch(BGIRemoteException ex)
            {
                _sampleId = string.Empty;
                _Logger.Log()(LogSeverityEnum.Error, ex.ice_message_);
                throw ex;
            }
            
        }

        public LoginedInfo Authenicate(string userName, string password)
        {
            return DeviceMgrZebrav2.Get.Authenicate(userName, password);
        }

        public string GetLanguage()
        {
            string langType = ConfigMgr.Get.GetCurValue<string>("System", "Language");
            return langType;
            //return "Chinese";
        }

        public void SetLanguage(string language)
        {
            try
            {
                SaveParamToConfig("System", "Language", language);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "save setLanguage config error,ex={0}", ex);
            }
        }

        public void SetCurrentCulture(string lang)
        {
            _CurLanguage = lang;
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(_CurLanguage);
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(_CurLanguage);
            Properties.Resource.Culture = new System.Globalization.CultureInfo(_CurLanguage);
        }

        public Dictionary<string, string> GetExperimentInfo()
        {
            if (_ExperimentInfo == null) _ExperimentInfo = new Dictionary<string, string>();
            if (_ExperimentInfo.ContainsKey("SelfCheckStatus")) _ExperimentInfo["SelfCheckStatus"] = DeviceMgrZebrav2.Get.SelfCheckStatus;
            else _ExperimentInfo.Add("SelfCheckStatus", DeviceMgrZebrav2.Get.SelfCheckStatus);

            string userName = DeviceMgrZebrav2.Get.CurrentUser;
            string userPwd = DeviceMgrZebrav2.Get.CurrentUserPwd;
            string userRole = DeviceMgrZebrav2.Get.CurrentUserRole;
            if (_ExperimentInfo.ContainsKey("UserName")) _ExperimentInfo["UserName"] = userName;
            else _ExperimentInfo.Add("UserName", userName);

            if (_ExperimentInfo.ContainsKey("UserPwd")) _ExperimentInfo["UserPwd"] = userPwd;
            else _ExperimentInfo.Add("UserPwd", userPwd);

            if (_ExperimentInfo.ContainsKey("UserRole")) _ExperimentInfo["UserRole"] = userRole;
            else _ExperimentInfo.Add("UserRole", userRole);
            bool isFastChip = DeviceMgrZebrav2.Get.IsFastFlowcell(this.StageRunName);
            if (_ExperimentInfo.ContainsKey("IsFastChip")) _ExperimentInfo["IsFastChip"] = isFastChip.ToString();
            else _ExperimentInfo.Add("IsFastChip", isFastChip.ToString());
            Dictionary<string, MessageType> tempStatus = DeviceMgrZebrav2.Get.CurrentMessageStatus;
            if (tempStatus != null && tempStatus.ContainsKey(this.StageRunName) && 
                (ScriptState==ScriptStateEnum.ScriptPaused || ScriptState==ScriptStateEnum.ScriptPausing || ScriptState==ScriptStateEnum.ScriptRunning))
            {
                if (_ExperimentInfo.ContainsKey("LastOperation")) _ExperimentInfo["LastOperation"] = ((int)tempStatus[this.StageRunName]).ToString();
                else
                {
                    _ExperimentInfo.Add("LastOperation", ((int)tempStatus[this.StageRunName]).ToString());
                }
            }
            return _ExperimentInfo;
        }

        public void DownloadScriptFile(out string workFlowScript, out string imageScript, out string biochemistryScript)
        {
            _CurScriptState = ScriptStateEnum.SetupRun;
            workFlowScript = "";
            imageScript = "";
            biochemistryScript = "";
            //
            string rootPath = string.Empty;
            if (!DeviceMgrZebrav2.Get.IsZlimsSimulated)
            {
                // TODO get script file stream from limis
                //DeviceMgrZebrav2.Get.DownloadSequenceScripts(_experimentType, ref workFlowScript, ref imageScript, ref biochemistryScript);
                rootPath = Path.Combine(ScriptHomePath, "Product");
            }
            else
            {
                rootPath = Path.Combine(ScriptHomePath, _experimentType, "Normal");
                if (DeviceMgrZebrav2.Get.IsFastFlowcell(this.StageRunName))
                    rootPath = Path.Combine(ScriptHomePath, _experimentType, "Fast");
            }
            
            //string errMsg = Properties.Resource.strse
            workFlowScript = ReadScript(Path.Combine(rootPath, "workflow.py"));
            biochemistryScript = ReadScript(Path.Combine(rootPath, "bio.py"));
            imageScript = ReadScript(Path.Combine(rootPath, "img.py"));

            if (IsUsedEncryptScript)
            {
                ScriptEncryptandDecrypt encrypt = new ScriptEncryptandDecrypt();
                imageScript = encrypt.DecrytScriptFiletoString(imageScript);
                workFlowScript = encrypt.DecrytScriptFiletoString(workFlowScript);
                biochemistryScript = encrypt.DecrytScriptFiletoString(biochemistryScript);
            }
            int read1Len = -1;
            int read2Len = -1;
            int barcodeLen = -1;
            string readInfo = GetTotalLenFromContent(workFlowScript, out read1Len, out read2Len, out barcodeLen);
            int readLen = -1;
            if (read1Len < 0 || read2Len < 0 || barcodeLen < 0 || string.IsNullOrEmpty(readInfo)) readLen = -1;
            else
            {
                int endCycProcessMode = 0;// 
                StringCollection sc = ConfigMgr.Get.GetCurValue<StringCollection>("BaseCall", "EndCycProcessMode");
                if(sc.Count > 0)
                {
                    if(sc.Count > 1)
                    {
                        if (this.StageRunName.EndsWith("A"))
                            int.TryParse(sc[0], out endCycProcessMode);
                        else if (this.StageRunName.EndsWith("B"))
                            int.TryParse(sc[1], out endCycProcessMode);
                    }
                    else int.TryParse(sc[0], out endCycProcessMode);
                    if (endCycProcessMode == (int)EndCycProcessMode.R1Only && read1Len > 0)
                    {
                        read1Len += 1;
                    }
                    else if (endCycProcessMode == (int)EndCycProcessMode.R2Only && read2Len > 0)
                    {
                        read2Len += 1;
                    }
                    else if (endCycProcessMode == (int)EndCycProcessMode.R1R2Both)
                    {
                        if (read1Len > 0)
                            read1Len += 1;
                        if (read2Len > 0)
                            read2Len += 1;
                    }
                }
                
                readLen = read1Len + read2Len + barcodeLen;
            }
            //cycinfo = {'read1_len': 3, 'read2_len':0, 'barcode_len':0}
            if(readLen > 0)
            {
                string tInfo = Str.SafeFmt(@"'read1_len': {0}, 'read2_len':{1}, 'barcode_len':{2}", read1Len, read2Len, barcodeLen);
                string newInfo = "cycinfo = {" + $"{tInfo}" + "}";
                workFlowScript = workFlowScript.Replace(readInfo, newInfo);
            }

            string msg = string.Empty;
            if ((string.IsNullOrEmpty(workFlowScript) || string.IsNullOrEmpty(imageScript) || string.IsNullOrEmpty(biochemistryScript)) || readLen <= 0)
            {
                string strDesc = EnumUtils.GetDescriptionByName<DeviceCodeEnum>(DeviceCodeEnum.Others);
                int errCode = (int)ScriptErrorEnum.ParseError;
                if (StageRunName.EndsWith("B")) errCode = (int)ScriptErrorEnum.ParseError + 1;
                msg = $"{Properties.Resource.strRecipeError}{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}";
                CreateAndSendMesssage(msg, $"{(int)WorkFlowCodeEnum.Sequence}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}");
                return;
            }
            if (!string.IsNullOrEmpty(msg))
            {
                DeviceMgrZebrav2.Get.RemoveScriptError(this.StageRunName, msg);
            }
            if (_ExperimentInfo == null) _ExperimentInfo = new Dictionary<string, string>();
            if (_ExperimentInfo.ContainsKey("ReadLength")) _ExperimentInfo["ReadLength"] = readLen.ToString();
            else _ExperimentInfo.Add("ReadLength", readLen.ToString());
            string scriptFileContent = biochemistryScript + Environment.NewLine + imageScript + Environment.NewLine + workFlowScript;
            SetupScriptRunByFileContent(scriptFileContent);
            _CurrentScriptType = ScriptTypeEnum.WF;

        }

        private string ReadScript(string filePath)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        string content = sr.ReadToEnd();
                        return content;
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"read file[{filePath}] error:{ex}");
                return string.Empty;
            }
        }

        private string GetTotalLenFromContent(string content,out int read1Len,out int read2Len,out int barcodeLen)
        {
            read1Len = -1;
            read2Len = -1;
            barcodeLen = -1;

            if (!string.IsNullOrEmpty(content))
            {
                string runInfo = string.Empty;
                string[] lines = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 0)
                {
                    bool isStartComment = false;
                    string commentStr = "'";
                    foreach (string line in lines)
                    {
                        string temp = line.Trim();

                        if (temp.StartsWith("#") || string.IsNullOrEmpty(temp)) continue;

                        if (!isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                        {
                            commentStr = temp[0].ToString();
                            isStartComment = true;
                            continue;
                        }
                        if (isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                        {
                            if (temp[0].ToString() == commentStr)
                            {
                                isStartComment = false;
                                continue;
                            }
                        }
                        if (isStartComment) continue;
                        if (temp.StartsWith("cycinfo"))
                        {
                            runInfo = temp;
                            temp = temp.Replace(" ", "");
                            if (temp.IndexOf("{") > -1 && temp.IndexOf("}") > -1)
                            {
                                int startIndex = temp.IndexOf("{") + 1;
                                int endIndex = temp.IndexOf("}");
                                string readStr = temp.Substring(startIndex, endIndex - startIndex);
                                string[] readArray = readStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string read in readArray)
                                {
                                    string[] ar = read.Split(':');
                                    if (ar != null && ar.Length > 1)
                                    {
                                        if(string.Compare(ar[0], @"'read1_len'", true) == 0 || string.Compare(ar[0], @"""read1_len""", true) == 0)
                                        {
                                            int.TryParse(ar[1], out read1Len);
                                        }
                                        if(string.Compare(ar[0], @"'read2_len'", true) == 0 || string.Compare(ar[0], @"""read2_len""", true) == 0)
                                        {
                                            int.TryParse(ar[1], out read2Len);
                                        }
                                        if(string.Compare(ar[0], @"'barcode_len'",true) == 0 || string.Compare(ar[0], @"""barcode_len""", true) == 0)
                                        {
                                            int.TryParse(ar[1], out barcodeLen);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return runInfo;
            }
            return string.Empty;
        }

        private int GetTotalLenFromContent(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                string[] lines = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                if(lines.Length > 0)
                {
                    int readLen = 0;
                    bool isStartComment = false;
                    string commentStr = "'";
                    foreach (string line in lines)
                    {
                        string temp = line.Trim();

                        if (temp.StartsWith("#") || string.IsNullOrEmpty(temp)) continue;

                        if (!isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                        {
                            commentStr = temp[0].ToString();
                            isStartComment = true;
                            continue;
                        }
                        if (isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                        {
                            if (temp[0].ToString() == commentStr)
                            {
                                isStartComment = false;
                                continue;
                            }
                        }
                        if (isStartComment) continue;
                        if (temp.StartsWith("cycinfo"))
                        {
                            temp = temp.Replace(" ", "");
                            if (temp.IndexOf("{") > -1 && temp.IndexOf("}") > -1)
                            {
                                int startIndex = temp.IndexOf("{") + 1;
                                int endIndex = temp.IndexOf("}");
                                string readStr = temp.Substring(startIndex, endIndex - startIndex);
                                string[] readArray = readStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (string read in readArray)
                                {
                                    string[] ar = read.Split(':');
                                    if (ar != null && ar.Length > 1)
                                    {
                                        readLen += Convert.ToInt32(ar[1]);
                                    }
                                }
                            }
                        }
                    }
                    return readLen;
                }
                
            }
            return -1;
        }

        private string ReadScriptByLine(string filePath,out int readLen)
        {
            readLen = 0;
            try
            {
                StringBuilder builder = new StringBuilder();
                int findCnt = 0;
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        bool isStartComment = false;
                        string commentStr = "'";
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string temp = line.Trim();

                            if (temp.StartsWith("#") || string.IsNullOrEmpty(temp)) continue;

                            if (!isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                            {
                                commentStr = temp[0].ToString();
                                isStartComment = true;
                                continue;
                            }
                            if (isStartComment && (temp.StartsWith("'") || temp.StartsWith(@"""")))
                            {
                                if (temp[0].ToString() == commentStr)
                                {
                                    isStartComment = false;
                                    continue;
                                }
                            }
                            if (isStartComment) continue;
                            if (temp.StartsWith("cycinfo"))
                            {
                                temp = temp.Replace(" ", "");
                                if (temp.IndexOf("{") > -1 && temp.IndexOf("}") > -1)
                                {
                                    int startIndex = temp.IndexOf("{") + 1;
                                    int endIndex = temp.IndexOf("}");
                                    string readStr = temp.Substring(startIndex, endIndex - startIndex);
                                    string[] readArray = readStr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string read in readArray)
                                    {
                                        string[] ar = read.Split(':');
                                        if (ar != null && ar.Length > 1)
                                        {
                                            readLen += Convert.ToInt32(ar[1]);
                                        }
                                    }
                                    findCnt++;
                                }
                            }
                            builder.Append($"{line}\r\n");
                        }
                    }
                }
                if (findCnt > 0)
                {
                    return builder.ToString();
                }
                else
                {
                    readLen = -1;
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"read file[{filePath}] error:{ex}");
                readLen = -1;
                return string.Empty;
            }
        }
        public void UIOperation(string message, string datetime, string errcode)
        {
            
            lock (_LogMessageList)
            {
                int messageCount = _LogMessageList.Count + 1;
                ServiceMessageInfo smi = new ServiceMessageInfo();
                smi.MessageType = ScriptMessageSeverityEnum.Info;
                smi.MessageId = messageCount;
                smi.Message = message;
                smi.Datetime = datetime;
                smi.FlowCellId = StageRunName;
                _LogMessageList.Add(smi);
                StageService.SendLogMessageId(messageCount);
            }
            //this.SendeMessageToZLims(message, "operation");

        }

        public List<ServiceMessageInfo> GetAlarmMessages(int startMessageId)
        {
            if (_AlarmMessageList.Count > startMessageId)
                return _AlarmMessageList.GetRange(startMessageId, _AlarmMessageList.Count - startMessageId);
            if(_AlarmMessageList.Count == startMessageId && startMessageId >0)
                return _AlarmMessageList.GetRange(startMessageId-1, 1);
            return new List<ServiceMessageInfo>();
        }

        public List<ServiceMessageInfo> GetLogMessages(int startMessageId)
        {
            if (_LogMessageList.Count > startMessageId)
            {
                return _LogMessageList.GetRange(startMessageId, _LogMessageList.Count - startMessageId);
            }
                
            return new List<ServiceMessageInfo>();
        }

        public void ClearAlarmMessages(List<int> ids)
        {
            _AlarmMessageList.RemoveAll(p => ids.Exists(q => q == p.MessageId));
        }

        public void SendExperimentType(string experimentType)
        {
            _experimentType = experimentType;
            string wf, bio, img;
            DownloadScriptFile(out wf, out img, out bio);
        }

        public void ReagentNeedle(bool up)
        {
            throw new NotImplementedException();
        }

        public void SetBuzzerVolume(int frenquency)
        {
            DeviceMgrZebrav2.Get.SetBuzzerPWM((ushort)frenquency);
        }

        public void SetBaseCallIpAddr(string ipAddr, int port)
        {
            //throw new NotImplementedException();
        }

        public void SetZLIMSIpAddr(string ipAddr, int port)
        {
            try
            {
                SaveParamToConfig("ZLIMS", "BaseUrl", ipAddr);
                SaveParamToConfig("ZLIMS", "Port", port.ToString());
                //DeviceMgrZebrav01.Get.ZLIMSIPAddrReload();
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "save zlims config error,ex={0}", ex);
            }
        }

        public List<string> GetSequenceTypes(string sampleId)
        {
            var types = new List<string>();
            try
            {
                string rootPath = ScriptHomePath;
                DirectoryInfo dir = new DirectoryInfo(rootPath);
                if (!dir.Exists) return types;
                var subdirs = dir.GetDirectories();
                foreach (var subdir in subdirs)
                {
                    if (subdir.Name == "common" || subdir.Name == ".vscode")
                        continue;
                    var files = subdir.GetFiles("*.py",SearchOption.AllDirectories);
                    if (files.Any(p1 => p1.Name.Equals("bio.py",StringComparison.OrdinalIgnoreCase))
                    && files.Any(p2 => p2.Name.Equals("img.py", StringComparison.OrdinalIgnoreCase))
                    && files.Any(p3 => p3.Name.Equals("workflow.py", StringComparison.OrdinalIgnoreCase)))
                    {
                        types.Add(subdir.Name);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "sampleId:{0},GetSequenceTypes Error,{1} " ,sampleId, ex.ToString());
            }
            return types;
        }

        public bool SetClearData(List<string> datas)
        {
            return this.ClearHistory(datas);
        }

        public void StartRunLehgth(int read1length, int read2length, int bardcodelength)
        {
            throw new NotImplementedException();
        }

        public IpAddr GetZLIMSAddr()
        {
            return new IpAddr()
            {
                IP = ConfigMgr.Get.GetCurValue<string>("ZLIMS", "BaseUrl"),
                Port = ConfigMgr.Get.GetCurValue<int>("ZLIMS", "Port")
            };
        }

        public void SendStageRunMessage(string type, string message)
        {
            StageService.SendStageRunMessage(type, message);
        }

        public void SendRemainingTime(double remainingTime)
        {
            StageService.SendRemainingTime(remainingTime);
        }
        #endregion


        #region selfcheck

        public void StartSelfCheck(DeviceCheckTypeEnum checkType)
        {
            Task.Factory.StartNew(() =>
            {
                DeviceMgrZebrav2.Get.StartSelfCheck(checkType,this.StageRunName);
            });
        }


        private void DeviceInitCheck()
        {
            while (!DeviceMgrZebrav2.Get.IsInitDone)
            {
                //Wait Device Init Done
                Thread.Sleep(1000);
            }

            bool hasError = false;

            SelfCheck selfCheck = new SelfCheck();

            if (!DeviceMgrZebrav2.Get.IsInitDone)
            {
                selfCheck.DeviceType = "Done";
                selfCheck.CheckInfo = "Fail";
                SendSelfInfo(selfCheck);
                return;
            }


            selfCheck.DeviceType = "Done";
            if (hasError)
                selfCheck.CheckInfo = "Fail";
            else
            {
                selfCheck.CheckInfo = "Success";
                selfCheck.Progress = 1;
            }
            Thread.Sleep(500);
            SendSelfInfo(selfCheck);

        }
        private void SendSelfInfo(SelfCheck checkMsg)
        {
            string message = JsonConvert.SerializeObject(checkMsg);
            this.SendStageRunMessage(MessageType.SelfCheck.ToString(), message);
        }

        public void AddLogMessage(ServiceMessageInfo msg)
        {
            lock (_LogMessageList)
            {
                if(msg == null)
                {
                    _Logger.Log()(LogSeverityEnum.Warning, $"With Null Message");
                    return;
                }
                if (msg.Message == null || string.IsNullOrEmpty(msg.Message.Trim()) || string.IsNullOrWhiteSpace(msg.Message.Trim()))
                {
                    _Logger.Log()(LogSeverityEnum.Warning, $"With Empty Message");
                    return;
                }
                int messageCount = _LogMessageList.Count;
                
                messageCount += 1;
                _LogMessageList.Add(msg);
                if(_AlarmMessageList.Exists(p=>p.Message == msg.Message))
                {
                    StageService.SendLogMessageId(0);
                }
                else StageService.SendLogMessageId(messageCount);
            }
            
        }

        private int CheckDiskSpace()
        {
            string diskPath = ConfigMgr.Get.GetCurValue<string>("System", "ImageSavePath");
            if (string.IsNullOrEmpty(diskPath)) return 2;
            DirectoryInfo dir = new DirectoryInfo(diskPath);
            diskPath = dir.Root.Name;
            string diskName = diskPath.Remove(diskPath.Length - 2);
            int space = OSUtils.GetDiskFreeSpace(diskName);
            int minSpace = ConfigMgr.Get.GetCurValue<int>("System", "MinSpace_GB");
            if (space <= minSpace)
            {
                ServiceMessageInfo msg = new ServiceMessageInfo();
                msg.MessageId = 0x4A;
                msg.Datetime = DateTime.UtcNow.ToString();
                msg.MessageType = ScriptMessageSeverityEnum.Error;
                msg.InstructmentId = Environment.MachineName;
                msg.UserName = Environment.UserName;
                msg.FlowCellId = FlowcellBarcode;
                msg.Message = "not enough space";
                //SendServiceMessage(msg);
                SendDataList();
                return 2;
            }
            else
            {

                //_stageRunMgr.RemoveAlarmMessage(Properties.Resource.strDiskError);
                // RemoveAlarmMessage(Properties.Resource.strDiskError);

                return 1;
            }
        }

        public void SendDataList()
        {
            var rootPath = ConfigMgr.Get.GetCurValue<string>("System", "ImageSavePath");
            if (!string.IsNullOrEmpty(rootPath))
            {
                DirectoryInfo dir = new DirectoryInfo(rootPath);
                DirectoryInfo[] subFolder = dir.GetDirectories();
                if (subFolder != null && subFolder.Length > 0)
                {
                    SortAsFolderCreationTime(ref subFolder);
                    List<string> folder = new List<string>();
                    for (int i = 0; i < subFolder.Length - 1; i++)
                    {
                        folder.Add(subFolder[i].Name);
                    }
                    this.StageService.SendDataList(folder);
                }
                else
                {   // Empty
                    this.StageService.SendDataList(new List<string>());
                }
            }
        }

        public bool IsExistAlarmMessage(string msg)
        {
            lock (_AlarmMessageList)
            {
                return _AlarmMessageList.Exists(p => p.Message == msg);
            }
        }

        public bool IsExistAlarmMessageType(ScriptMessageSeverityEnum MessageType)
        {
            lock (_AlarmMessageList)
            {
                return _AlarmMessageList.Exists(p => p.MessageType == MessageType);
            }
        }

        public int GetAlarmMessageCount()
        {
            lock (_AlarmMessageList)
            {
                return _AlarmMessageList.Count;
            }
        }

        public ServiceMessageInfo GetAlarmMessage(string alarmMsg)
        {
            lock (_AlarmMessageList)
            {
                if (!string.IsNullOrEmpty(alarmMsg)) return _AlarmMessageList.Find(p => p.Message == alarmMsg);
                return null;
            }
        }

        public ServiceMessageInfo RemoveAlarmMessage(string alarmMsg)
        {
            lock (_AlarmMessageList)
            {
                int count = 0;
                ServiceMessageInfo alarm = null;
                if (_AlarmMessageList.Exists(p => p.Message == alarmMsg))
                {
                    alarm = _AlarmMessageList.Find(p => p.Message == alarmMsg);
                    count = _AlarmMessageList.RemoveAll(p => p.Message == alarmMsg);
                }
                StageService.SendAlarmMessageId(count);
                return alarm;
            }
        }

        private void CreateScriptRunnerExceptionMsg(ScriptRunException exception)
        {
            int errCode = exception.ErrorNo;
            if (errCode > (int)ScriptErrorEnum.OtherError)
            {
                if(StageRunName.EndsWith("B")) errCode = (int)ScriptErrorEnum.CompileError+1;
                else if (StageRunName.EndsWith("A")) errCode = (int)ScriptErrorEnum.CompileError;
            }
                
            int workFlowCode = (int)WorkFlowCodeEnum.Sequence;
            if(_CurrentScriptType.HasValue && _CurrentScriptType.Value == ScriptTypeEnum.BIO)
            {
                if (!string.IsNullOrEmpty(_CurrentScriptFileFullName))
                {
                    if (_CurrentScriptFileFullName.Contains("with_cleantubing"))
                    {
                        workFlowCode = (int)WorkFlowCodeEnum.Wash;
                    }
                    else if (_CurrentScriptFileFullName.Contains("with_naoh"))
                    {
                        workFlowCode = (int)WorkFlowCodeEnum.DeepWash;
                    }
                    else if (_CurrentScriptFileFullName.Contains("cleantubing"))
                    {
                        workFlowCode = (int)WorkFlowCodeEnum.CleanTubing;
                    }
                }
            }
            string strDesc = EnumUtils.GetDescriptionByName<DeviceCodeEnum>(DeviceCodeEnum.Others);
            CreateAndSendMesssage($"{Properties.Resource.strRecipeError}{workFlowCode}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}", $"{workFlowCode}-{strDesc}{(int)DeviceCodeEnum.Others}-{errCode}");
        }

        private void CreateAndSendMesssage(string msg, string errCode)
        {
            string flowCellId = string.IsNullOrEmpty(FlowcellBarcode) ? string.Empty : FlowcellBarcode;
            DeviceMgrZebrav2.Get.SendScriptError(this.StageRunName, msg, flowCellId, errCode);
        }
        #endregion

        #region Clear History Data
        private void SortAsFolderCreationTime(ref DirectoryInfo[] arrFi, bool isAsc = true)
        {
            Array.Sort(arrFi, delegate (DirectoryInfo x, DirectoryInfo y)
            {
                if (isAsc)
                    return x.CreationTime.CompareTo(y.CreationTime);
                else
                    return y.CreationTime.CompareTo(x.CreationTime);
            });
        }

        private void DeleteFolder(string folderPath)
        {
            if (!string.IsNullOrEmpty(folderPath))
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);
                if (dir.Exists)
                {
                    try
                    {
                        dir.Delete(true);
                    }
                    catch (Exception ex)
                    {
                        _Logger.Log()(LogSeverityEnum.Error, $"Delete Folder [{dir.FullName}] Failed.{ex.Message}");
                    }
                }
            }
        }

        private void DeleteFile(FileInfo[] files)
        {
            if (files != null && files.Length > 0)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Exists)
                    {
                        try
                        {
                            files[i].Delete();
                        }
                        catch (Exception ex)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"Delete Log File [{files[i].Name}] Failed. {ex.Message}");
                        }

                    }
                }
            }
        }

        public void ClearLog()
        {
            int remainMonth = 1;// ConfigMgr.Get.GetCurValue<int>("System", "LogRetentionTime_month");
            string iswLogPath = @"C:\BGI\Logs\";
            DirectoryInfo dir = new DirectoryInfo(iswLogPath);
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles();
                FileInfo[] delFiles = files.Where(a => DateTime.Compare(a.CreationTimeUtc.Date, DateTime.UtcNow.AddMonths(-1 * remainMonth).AddDays(-1).Date) <= 0).ToArray();
                DeleteFile(delFiles);
            }

            string kvpLogPath = @"C:\BGI\Logs\kvplog\";
            dir = new DirectoryInfo(kvpLogPath);
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles();
                FileInfo[] delFiles = files.Where(a => DateTime.Compare(a.CreationTimeUtc.Date, DateTime.UtcNow.AddMonths(-1 * remainMonth).AddDays(-1).Date) <= 0).ToArray();
                DeleteFile(delFiles);
            }


            string basecallLogPath = @"C:\Program Files (x86)\Basecall\";
            dir = new DirectoryInfo(basecallLogPath);
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles("*.log");
                FileInfo[] delFiles = files.Where(a => DateTime.Compare(a.CreationTimeUtc.Date, DateTime.UtcNow.AddMonths(-1 * remainMonth).AddDays(-1).Date) <= 0).ToArray();
                DeleteFile(delFiles);
            }

        }

        public bool ClearHistory(List<string> dataFolders)
        {
            if (dataFolders != null && dataFolders.Count > 0)
            {
                string rootPath = string.Empty;
                string basecallFQ = @"D:\result\OutputFq";
                string basecallImg = @"D:\result\saveImage";
                string basecallSpace = @"D:\result\workspace";
                if (string.IsNullOrEmpty(imageSavePath))
                {
                    rootPath = ConfigMgr.Get.GetCurValue<string>("System", "ImageSavePath");
                }
                else rootPath = imageSavePath;
                if (!string.IsNullOrEmpty(rootPath))
                {
                    for (int i = 0; i < dataFolders.Count; i++)
                    {
                        string folderName = dataFolders[i];
                        DeleteFolder(Path.Combine(rootPath, folderName));
                        DeleteFolder(Path.Combine(basecallFQ, folderName));
                        DeleteFolder(Path.Combine(basecallImg, folderName));
                        DeleteFolder(Path.Combine(basecallSpace, folderName));
                    }
                }

                //RemoveAlarmMessage(Properties.Resource.strDiskError);
                //LogStore = LogStorgeOperate.GetInstance();
                //LogStore.DeleteLogsRecord();
            }
            return true;
        }

        #endregion

        #region config save
        //save param to config and save the Param to service.xml
        private void SaveParamToConfig(string category, string param, string value)
        {
            ConfigMgr.Get.SetCurValue(category, param, value, string.Empty);
            if (ConfigMgr.Get.GetDBConnectStatus())
            {
                Stat Issuccess = ConfigMgr.Get.SaveParameter(category, param);
                _Logger.Log()(LogSeverityEnum.Info, "Modify the Remote Config Parameter is {0}", Issuccess);
                SaveParaToXml(category, param, value);
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Remote config is offline");
                SaveParaToXml(category, param, value);
            }
        }

        //save param to xml if config is offline
        private void SaveParaToXml(string category, string param, string value)
        {
            string configPath = @"C:\BGI\Config\BGI.ZebraV2Seq.Service.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(configPath);
            XmlNode node = doc.SelectSingleNode("Configs/InstrumentConfig[Para_Set_Name='" + category + "' and Para_Name='" + param + "']").SelectSingleNode("Cur_Value").FirstChild;
            if (node != null) node.Value = value;
            doc.Save(configPath);
        }

        #endregion

    }
}
