﻿//-----------------------------------------------------------------------------
// Copyright 2016 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

using BGI.Common.Logging;

namespace BGI.ZebraV2Seq.Service
{
    public partial class ServiceEntry : ServiceBase
    {
        #region Constants

        // Custom Commands for Windows Service - Valid values 128..255
        private const int STOP_SERVICE_REQUEST = 128;
        
        #endregion

        private Logger _Logger = LogMgr.GetLogger("Scheduler");
        readonly ZebraScheduler _ZebraScheduler = null;
        readonly Thread _SchedThread = null;

        public ServiceEntry()
        {
            InitializeComponent();

            this.ServiceName = "BGI.ZebraV2Seq.Service";
            this.AutoLog = true;
            this.CanStop = true;

            _ZebraScheduler = new ZebraScheduler();
            _SchedThread = new Thread(new ThreadStart(_ZebraScheduler.MainLoop));
            _SchedThread.Name = "Scheduler";
        }

        public void StartConsole(string[] args)
        {
            OnStart(args);
        }

        public void StopConsole()
        {
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            _Logger.Log()(LogSeverityEnum.Info, "Starting the worker thread.");
            _SchedThread.Start();
        }

        protected override void OnStop()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Stopping the worker thread.");
            _ZebraScheduler.Stop();
            _SchedThread.Join();
        }

        protected override void OnCustomCommand(int command)
        {
            _Logger.Log()(LogSeverityEnum.Warning, "Received custom command {0}", command);

            base.OnCustomCommand(command);

            switch (command)
            {
                case STOP_SERVICE_REQUEST:
                    {
                        _Logger.Log()(LogSeverityEnum.Info, "Service {0} recieved request to stop by command {1} ", _SchedThread.Name, STOP_SERVICE_REQUEST);
                        _ZebraScheduler.RequestStop();

                        new Action(() =>
                            {
                                checkStopRequest();
                            }
                        ).BeginInvoke(null, null);
                    }
                    break;
                default:
                    {
                        _Logger.Log()(LogSeverityEnum.Error, "Invalid Custom Command ({0}) received.  Ignoring.", command);
                    }
                    break;
            }
        }

        private void checkStopRequest()
        {
            while (_ZebraScheduler.ReadyToStop() == false)
            {
                Thread.Sleep(1000); // 1 sec
            }
            this.Stop();
        }



    }
}
