﻿using BGI.Control;
using BGI.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BGI.Common.Utils.ImageUtil;
using BGI.Control.Device;
using BGI.Common.Logging;
using BGI.RPC.Data;
using Newtonsoft.Json;
using BGI.Common.Config;
using System.Collections.Specialized;
using BGI.Common.Utils.Stats;
using BGI.Control.Metrics;
using BGI.Control.Data;
using BGI.OSIIP;
using BGI.RPC;
using BGI.Common.Utils;
using BGI.Control.Scanner;

namespace BGI.ZebraV2Seq.Service
{
    public class DeviceMgrAccess : DeviceMgrBase, IBioOperations, IImageOperations, IWorkFlowOperations, IEngineerOperations
    {
        private Logger _Logger = null;
        private StageRunMgr _StageRunMgr = null;
        private string _RunName = string.Empty;

        private RunInfo CurrentInfo = null;
        private string operation = string.Empty;
        private string[] operationArray = null;
        private string operationKey = string.Empty;
        private MessageType phaseType = MessageType.None;
        private string _Slide = string.Empty;
        private int metricsCycle = -1;
        private MachineStatus machineStatus = MachineStatus.Init;

        private StringCollection _loadDnbTimeMinute;
        private StringCollection _sequenceTimeMinute;
        private StringCollection _barcodeTimeMinute;
        private StopwatchStats[] sequenceWatchStats = null;

        private double sequenceTotalTime = 0;
        private double loadDNBTime = 0;
        private double sequenceTime = 0;
        private double barcodeTime = 0;
        private StopwatchStats washCleantubeStats = new StopwatchStats("wash cleantube stats");

        private CycleMetrics cycleMetrics;

        private int _index = 0;

        private object lockObj_B = new object();

        #region Properties
        #region config time
        private StringCollection LoadDnbTimeMinute
        {
            get
            {
                try
                {
                    if (_loadDnbTimeMinute == null)
                    {
                        _loadDnbTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "LoadDnbTimeMinute");
                    }
                    return _loadDnbTimeMinute;
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _loadDnbTimeMinute = new StringCollection();
                    _loadDnbTimeMinute.Add("85");
                    return _loadDnbTimeMinute;
                }
                
            }
        }
        private StringCollection SequenceTimeMinute
        {
            get
            {
                try
                {
                    if (_sequenceTimeMinute == null)
                    {
                        _sequenceTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "SequenceTimeMinute");
                    }
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _sequenceTimeMinute = new StringCollection();
                    _sequenceTimeMinute.Add("3.2");
                    _sequenceTimeMinute.Add("7");
                    _sequenceTimeMinute.Add("3.3");
                }
                
                return _sequenceTimeMinute;
            }
        }
        private StringCollection BarcodeTimeMinute
        {
            get
            {
                try
                {
                    if (_barcodeTimeMinute == null)
                    {
                        _barcodeTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "BarcodeTimeMinute");
                    }
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _barcodeTimeMinute = new StringCollection();
                    _barcodeTimeMinute.Add("40");
                }
                
                return _barcodeTimeMinute;
            }
        }

        private double WashTimeMinute
        {
            get
            {
                try
                {
                    return ConfigMgr.Get.GetCurValue<double>("System", "WashTimeMinute");
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    return 18;
                }
                
            }
        }

        private double WashWithNaOHTimeMinute
        {
            get
            {
                try
                {
                    return ConfigMgr.Get.GetCurValue<double>("System", "WashWithNaOHTimeMinute");
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    return 13;
                }

            }
        }

        private double CleanTubeMinute
        {
            get
            {
                try
                {
                    return ConfigMgr.Get.GetCurValue<double>("System", "CleanTubeMinute");
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    return 25;
                }
                
            }
        }
        private double WashWithCleantubeTimeMinute
        {
            get
            {
                try
                {
                    return ConfigMgr.Get.GetCurValue<double>("System", "WashWithCleantubeTimeMinute");
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    return 70;
                }
                
            }
        }
        #endregion
        #endregion
        /// <summary>
        /// Disabled default constructor
        /// </summary>
        private DeviceMgrAccess() { }

        public DeviceMgrAccess(StageRunMgr srm)
        {
            _Logger = LogMgr.GetLogger($"{srm.StageRunName}");
            if (srm.StageRunName.EndsWith("B")) _index = 1;
            _Logger.Log()(LogSeverityEnum.Info, "Creating StageMgrAccess for {0}", srm.StageRunName);
            _StageRunMgr = srm;
            this._RunName = srm.StageRunName;
        }

        #region IBioOperations
        public void Reagent(bool fromByAir = true)
        {
            try
            {
                DeviceMgrZebrav2.Get.Reagent(fromByAir, _RunName);
            }
            catch(BGIDeviceException ex)
            {
                ServiceMessageInfo msgInfo = CreateDeviceMessage(ex, phaseType, machineStatus);
                throw;
            }
            catch(Exception ex)
            {
                int errcode = (int)ReagentNeedleErrCodeEnum.DownError;
                if (fromByAir)
                {
                    errcode = (int)ReagentNeedleErrCodeEnum.UpError;
                }
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.ReagentNeedle, errcode, ex.Message);
                ServiceMessageInfo msgInfp = CreateDeviceMessage(inner,phaseType, machineStatus);
                throw;
            }
            
        }

        public void WaitForFluidicsAvailable()
        {
            // TODO
        }

        public void WaitForFluidicsReady()
        {
            // TODO
        }

        // Implement Incubation API, put in so that the build is built
        public TimeSpan Incubation(TimeSpan span)
        {
            // TODO Questions remain if this should even be here.  Keep in API only?  SA
            return DeviceMgrZebrav2.Get.Incubation(this._RunName, span);
        }

        public void DoReagentFlow(string reagentName, float volume, float rate)
        {
            DeviceMgrZebrav2.Get.DoReagentFlow(reagentName, volume, rate, _RunName);
        }

        public void SetTemp(double tempSetVal)
        {
            try
            {
                DeviceMgrZebrav2.Get.SetTemp(tempSetVal, _RunName);
            }
            catch(BGIDeviceException ex)
            {
                ServiceMessageInfo msgInfo = CreateDeviceMessage(ex, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(msgInfo, this._RunName, false);
                throw;
            }
            
        }

        public void SetTempAndWait(double tempSetVal)
        {
            throw new NotImplementedException();
        }

        public void WaitForTemp()
        {
            throw new NotImplementedException();
        }


        public double GetCurrentTemp()
        {
            return DeviceMgrZebrav2.Get.GetCurrentTemp(this._RunName);
        }

        public double GetTempSetpoint()
        {
            return DeviceMgrZebrav2.Get.GetTempSetpoint(this._RunName);
        }

        public void Reagent(string reagentName, double volume, double aspirateRate, float dispenseRate, int delayMs, bool FromBypass = false)
        {
            DeviceMgrZebrav2.Get.Reagent(reagentName, volume, aspirateRate, dispenseRate, delayMs, _RunName, FromBypass);
        }

        public void Reagent(int pos, double volume, double aspirateRate, float dispenseRate, int delayMs, bool FromBypass = false)
        {
            try
            {
                DeviceMgrZebrav2.Get.Reagent(pos, volume, aspirateRate, dispenseRate, delayMs, _RunName, FromBypass);
            }
            catch (BGIDeviceException ex)
            {
                ServiceMessageInfo msgInfo = CreateDeviceMessage(ex,phaseType,machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(msgInfo, this._RunName, false);
                ResetBIO();
                throw;
            }
            catch(Exception ex)
            {
                ServiceMessageInfo msgInfo = CreateDeviceMessage(ex, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(msgInfo, this._RunName, false);
                ResetBIO();
                throw;
            }
        }

        public void Reagent(int aspiratePos, double aspirateVolume, double aspirateRate, bool aspirateFromBypass, int dispensePos, double dispenseRate, bool dispenseFromBypass, int delayMs)
        {
            DeviceMgrZebrav2.Get.Reagent(aspiratePos, aspirateVolume, aspirateRate, aspirateFromBypass, dispensePos, dispenseRate, dispenseFromBypass, delayMs, _RunName);
        }

        public void SetExperimentTypePhaseAndOperation(string operation, string stepKey)
        {
            
            operationArray = SplitOperationKey(stepKey);
            phaseType = (MessageType)Convert.ToInt32(operationArray[0].ToString());
            if (phaseType > MessageType.None)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"Invalid Phase Type {operationArray[0]}");
                ScriptRunException innerException = new ScriptRunException((int)ScriptErrorEnum.InvalidPhaseError + _index);
                throw new BioException($"Invalid Phase Type {operationArray[0]}",innerException);
            }
            SetMachineStatusByPhaseType(phaseType);
            DeviceMgrZebrav2.Get.SetExperimentTypePhaseAndOperation(this._RunName, operation, stepKey);
        }

        public void FinishScript(string stepKey)
        {
            DeviceMgrZebrav2.Get.FinishScript(this._RunName, stepKey);
        }
        

        public void NeedleUp()
        {
            DeviceMgrZebrav2.Get.ReagentNeedleUp(this._RunName);
        }

        public void NeedleDown()
        {
            DeviceMgrZebrav2.Get.ReagentNeedleDown(this._RunName);
        }

        public void NeedleHalt()
        {
            //DeviceMgrZebrav2.Get.ReagentNeedleHalt(this._RunName);
        }

        #endregion

        #region IImageOperations

        public void WaitForImagingAvailable()
        {
            throw new NotImplementedException();
        }

        public void WaitForImagingReady()
        {
            try
            {
                DeviceMgrZebrav2.Get.WaitForImagingReady(this._RunName);
            }
            catch(BGIDeviceException ex)
            {
                ServiceMessageInfo msgInfo = CreateDeviceMessage(ex, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(msgInfo, this._RunName, false);
                throw;
            }
            
        }

        public void AutoFocusSweep(Field field, int count, double step, out IDictionary<string, object> curve)
        {
            DeviceMgrZebrav2.Get.AutoFocusSweep(field, count, step, out curve);
        }

        public void CleavageImage()
        {
            DeviceMgrZebrav2.Get.CleavageImage(this._RunName);
        }

        public void Registration()
        {
            DeviceMgrZebrav2.Get.Registration();
        }

        public Field GoToField(int lane, int row, int col)
        {
            return DeviceMgrZebrav2.Get.GoToField(lane, row, col);
        }

        public void Image()
        {
            try
            {
                DeviceMgrZebrav2.Get.Image(this._RunName);
            }
            catch (BGIDeviceException ex)
            {
                ServiceMessageInfo msg = CreateDeviceMessage(ex, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(msg, this._RunName, false);
                ResetImage();
                throw;
            }
            catch (AbortedByUserException ex)
            {
                ResetImage();
                throw;
            }
            catch (Exception ex)
            {
                string errMsg = $"{Properties.Resource.strImgError}{(int)WorkFlowCodeEnum.SequenceImg}-S-{(int)ScannerErrorEnum.Other}";
                ServiceMessageInfo msg = CreateMessage(ScriptMessageSeverityEnum.Error, errMsg, $"{(int)WorkFlowCodeEnum.SequenceImg}-S-{(int)ScannerErrorEnum.Other}");
                DeviceMgrZebrav2.Get.SendServiceMessage(msg, this._RunName, false);
                ResetImage();
                throw;
            }
        }


        public void Location(int retical)
        {
            DeviceMgrZebrav2.Get.Location(retical);
        }

        public void OpenLaser(LaserColor color, bool isOpen)
        {
            DeviceMgrZebrav2.Get.OpenLaser(color, isOpen);
        }


        public void SetLaserPower(LaserColor color, float power)
        {
            DeviceMgrZebrav2.Get.SetLaserPower(color, power);
        }

        public void StartRecord()
        {
            DeviceMgrZebrav2.Get.StartRecord();
        }

        public void StopRecord()
        {
            DeviceMgrZebrav2.Get.StopRecord();
        }

        public void TakePicture(Field field)
        {
            DeviceMgrZebrav2.Get.TakePicture(field);
        }

        public void ThetaCorrection()
        {
            DeviceMgrZebrav2.Get.ThetaCorrection();
        }

        public void XAxisCalibration()
        {
            DeviceMgrZebrav2.Get.XAxisCalibration();
        }

        public void XYAxisCalibration()
        {
            DeviceMgrZebrav2.Get.XYAxisCalibration();
        }

        public void YAxisCalibration()
        {
            DeviceMgrZebrav2.Get.YAxisCalibration();
        }

        #endregion

        #region IWorkFlowOperations

        ///
        public int GetCurrentPositionNumber()
        {
            return DeviceMgrZebrav2.Get.GetCurrentPositionNumber(this._RunName);
        }

        public void SetCurrentPositionNumber(int currPos)
        {
            //_StageRunMgr.CurrentPositionNumber = currPos;
        }

        public void SetActionName(string actionName)
        {
            //_StageRunMgr.ActionName = actionName;
        }

        //Reset cycle number when a new script is started
        public void ResetCylceNumber()
        {
            DeviceMgrZebrav2.Get.ResetCylceNumber(this._RunName);
        }


        public void SetExperimentInfo(RunInfo info)
        {
            DeviceMgrZebrav2.Get.SetExperimentInfo(this._RunName,info);
        }

        public void SetExperimentTypePhaseAndOperation(string messageType, string phase, string operation, string stepKey)
        {
            throw new NotImplementedException();
        }

        public void BypassWash(double volume)
        {
            DeviceMgrZebrav2.Get.BypassWash(this._RunName, volume);
        }
        #endregion

        #region Private Methods

        private void SendStageRunMessage(string message)
        {
            _StageRunMgr.StageService.SendStageRunMessage(phaseType.ToString(), message);
        }

        public string[] GetSequenceType()
        {
            return DeviceMgrZebrav2.Get.GetSequenceType(_RunName);
        }

        private void ResetBIO()
        {
            List<BGIDeviceException> lst = DeviceMgrZebrav2.Get.ResetBIO(this._RunName);
            foreach (BGIDeviceException dev in lst)
            {
                ServiceMessageInfo lstInfo = CreateDeviceMessage(dev, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(lstInfo, this._RunName, false);
            }
        }

        private void ResetImage()
        {
            try
            {
                List<BGIDeviceException> lst = DeviceMgrZebrav2.Get.ScannerReset(this._RunName);
                foreach (BGIDeviceException dev in lst)
                {
                    ServiceMessageInfo devMsg = CreateDeviceMessage(dev, phaseType, machineStatus);
                    DeviceMgrZebrav2.Get.SendServiceMessage(devMsg, this._RunName, false);
                }
            }
            catch (BGIDeviceException inner)
            {
                ServiceMessageInfo devMsg = CreateDeviceMessage(inner, phaseType, machineStatus);
                DeviceMgrZebrav2.Get.SendServiceMessage(devMsg, this._RunName, false);
            }
            catch (Exception inner)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"ResetImage failed {inner.ToString()}");
                string errMsg = $"{Properties.Resource.strImgError}{(int)WorkFlowCodeEnum.SequenceImg}-S-{(int)ScannerErrorEnum.Other}";
                ServiceMessageInfo devMsg = CreateMessage(ScriptMessageSeverityEnum.Error, errMsg, $"{(int)WorkFlowCodeEnum.SequenceImg}-S-{(int)ScannerErrorEnum.Other}");
                DeviceMgrZebrav2.Get.SendServiceMessage(devMsg, this._RunName, false);
            }
        }

        private ServiceMessageInfo CreateMessage(ScriptMessageSeverityEnum msgType, string msg,string errCode)
        {
            ServiceMessageInfo serviceMesssageInfo = new ServiceMessageInfo();
            serviceMesssageInfo.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            serviceMesssageInfo.Message = msg;
            serviceMesssageInfo.MessageType = msgType;
            serviceMesssageInfo.InstructmentId = DeviceMgrZebrav2.Get.InstrumentName;
            serviceMesssageInfo.IsPublic = false;
            serviceMesssageInfo.UserName = DeviceMgrZebrav2.Get.CurrentUser;
            serviceMesssageInfo.SoftwareVersion = DeviceMgrZebrav2.Get.SoftVersion;
            serviceMesssageInfo.Errorcode = errCode;
            return serviceMesssageInfo;
        }

        private ServiceMessageInfo CreateDeviceMessage(Exception exception,MessageType phase,MachineStatus machineStatus)
        {
            string tempMsg = exception.Message;
            string strDevice = string.Empty;
            string errCode = string.Empty;
            int deviceCode = 0;
            int deviceErrNo = 99;
            bool isDeviceException = true;
            if (exception is BGIDeviceException)
            {
                BGIDeviceException tmpException = (BGIDeviceException)exception;
                tempMsg = tmpException.Message;
                if(tmpException.DeviceCode >0)
                    strDevice = EnumUtils.GetDescriptionByName<DeviceCodeEnum>((DeviceCodeEnum)tmpException.DeviceCode);
                else
                    strDevice = "S";
                deviceCode = tmpException.DeviceCode;
                deviceErrNo = tmpException.ErrorNo + _index;
            }
            else
            {
                isDeviceException = false;
                tempMsg = exception.ToString();
            }
            _Logger.Log()(LogSeverityEnum.Error, $"{tempMsg}");
            string errMsg = string.Empty;
            int workFlowCode = 0;
            if (machineStatus == MachineStatus.Wash)
            {
                errMsg = Properties.Resource.strWashError;
                if (phaseType == MessageType.Wash || phaseType == MessageType.Wash_Cleantubing)
                {
                    workFlowCode = (int)WorkFlowCodeEnum.Wash;
                }
                else if (phaseType == MessageType.WashNaOH)
                    workFlowCode = (int)WorkFlowCodeEnum.DeepWash;
                else if (phaseType == MessageType.Cleantubing)
                    workFlowCode = (int)WorkFlowCodeEnum.CleanTubing;
            }
            else if (machineStatus == MachineStatus.Incorporation)
            {
                errMsg = Properties.Resource.strBIOError;
                workFlowCode = (int)WorkFlowCodeEnum.SequenceBIO;
            }
            else if(machineStatus == MachineStatus.Image)
            {
                errMsg = Properties.Resource.strImgError;
                workFlowCode = (int)WorkFlowCodeEnum.SequenceImg;
            }

            errCode = $"{workFlowCode}-{strDevice}{deviceCode}-{deviceErrNo}";
            errMsg = $"{errMsg}{errCode}";
            if (isDeviceException)
            {
                string[] temp = tempMsg.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (temp != null && temp.Length > 1)
                {
                    foreach(string s in temp)
                    {
                        if (s.StartsWith("E:["))
                        {
                            errMsg += $"-{s.Substring(3,s.Length-4)}";
                            errCode += $"-{s.Substring(3, s.Length - 4)}";
                            break;
                        }
                    }
                    //errMsg += $"-{temp[0]}";
                    //errCode += $"-{temp[0]}";
                }
            }
            return  CreateMessage(ScriptMessageSeverityEnum.Error, errMsg, errCode);
        }

        private string[] SplitOperationKey(string stepKey)
        {
            if (string.IsNullOrEmpty(stepKey) || string.IsNullOrWhiteSpace(stepKey))
            {
                _Logger.Log()(LogSeverityEnum.Error, "Empty StepKey");
                ScriptRunException innerException = new ScriptRunException((int)ScriptErrorEnum.EmptyParaError + _index);
                throw new BioException("Empty StepKey", innerException);
            }
            if (!string.IsNullOrEmpty(stepKey) && !string.IsNullOrWhiteSpace(stepKey))
            {
                string[] rsult = stepKey.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (rsult != null && rsult.Length == 3)
                {
                    return rsult;
                }
                else
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"Invalid StepKey {stepKey}");
                    ScriptRunException innerException = new ScriptRunException((int)ScriptErrorEnum.InvalidParaError + _index);
                    //throw new BGIException($"Invalid StepKey {stepKey}", innerException);
                    throw new BioException($"Invalid StepKey {stepKey}", innerException);
                }

            }
            return null;
        }

        private void SetMachineStatusByPhaseType(MessageType phaseType)
        {
            if (phaseType == MessageType.Wash || phaseType == MessageType.WashNaOH || phaseType == MessageType.Wash_Cleantubing || phaseType == MessageType.Cleantubing)
            {
                machineStatus = MachineStatus.Wash;
            }
            else if ((int)phaseType < 11)
            {
                if ((phaseType == MessageType.Sequencing && operationArray[1] == "2") ||
                    (phaseType == MessageType.Sequencing_Read1 && operationArray[1] == "2") ||
                    (phaseType == MessageType.Sequencing_Read2 && operationArray[1] == "2") ||
                    (phaseType == MessageType.BarcodeSequencing && operationArray[1] == "2"))
                {
                    machineStatus = MachineStatus.Image;
                }
                else
                {
                    machineStatus = MachineStatus.Incorporation;
                }
            }
        }
        #endregion

        #region IEngineerOperations

        public IReadOnlyDictionary<string, object> EnumeratePluginHardware()
        {
            return DeviceMgrZebrav2.Get.EnumeratePluginHardware(_RunName) as Dictionary<string, object>;
        }

        public object GetDeviceObject(string objName)
        {
            if (EnumeratePluginHardware().ContainsKey(objName))
            {
                return EnumeratePluginHardware()[objName];
            }
            return null;
        }

        public void SetCameraExposureTime(float exposureTime)
        {
            DeviceMgrZebrav2.Get.SetCameraExposureTime(exposureTime);
        }

        public void SetCameraExposureTimes(float green_laser_exposure_time, float red_laser_exposure_time)
        {
            throw new NotImplementedException();
        }

        public void SetLaserPowers(float green_laser_power, float red_laser_power)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
