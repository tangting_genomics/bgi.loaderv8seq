﻿//-----------------------------------------------------------------------------
// Copyright 2016 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BGI.Common.Logging;
using BGI.Common.Utils.ExceptionService;

namespace BGI.ZebraV2Seq.Service
{
    static class Program
    {
        private static Logger _StaticLogger = LogMgr.GetLogger();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(String[] args)
        {

            RegisterRuntimeErrorRecorder();

            // Setup unhandled exception handlers
            AppDomain.CurrentDomain.UnhandledException += // CLR exceptions
               new UnhandledExceptionEventHandler(OnUnhandledException);

            // Our main Service
            ServiceEntry svc = new ServiceEntry();  

            if (args.Length > 0)
            {
                if (args[0].Equals("/C"))
                {
                    svc.StartConsole(args);

                    Console.WriteLine("Running in Console Mode");
                    Console.WriteLine("Press any key to stop program");
                    int retval = Console.Read();

                    svc.StopConsole();
                }
                else
                {
                    _StaticLogger.Log()(LogSeverityEnum.Error, "Invalid command line argument passed in.  Use '/C' to run in Console mode.");
                }
            }
            else
            {
                try
                {
                    ServiceBase.Run(svc);
                }
                catch (Exception e)
                {
                    HandleUnhandledException(e);
                }
            }
        }
        /// <summary>
        /// CLR Unhandled Exception delegate
        /// </summary>
        /// <param name="sender">Sender of the call</param>
        /// <param name="e">UnhandledExceptionEventArgs object</param>
        private static void OnUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            HandleUnhandledException(e.ExceptionObject);
        }

        public static void OnUnhandledThreadException(Object sender, ThreadExceptionEventArgs args)
        {
            HandleUnhandledException(args.Exception);
        }

        /// <summary>
        /// Print an exception message to the Log file and exit the application.
        /// </summary>
        /// <param name="o">Exception object</param>
        private static void HandleUnhandledException(Object o)
        {
            try
            {
                // First of all deallocate the exception handler so recursion does not occur.  Also avoids
                // a memory leak.
                AppDomain.CurrentDomain.UnhandledException -=
                   new UnhandledExceptionEventHandler(OnUnhandledException);

                Exception e = o as Exception;
                _StaticLogger.Log()(LogSeverityEnum.Error, "Handling unhandled exception: {0}.", e.Message);
                _StaticLogger.Log("Exception", e);

                //RuntimeErrorRecorder use case.
                //record humanable log to current logfile
                DumpReadableErrors(e, txt => _StaticLogger.Log()(LogSeverityEnum.Error, txt));

            }
            catch (Exception)
            {
                // Do nothing since we can't recurse

            }
            finally
            {
                Environment.Exit(1); // Shutting down
            }
        }

        /// <summary>
        /// Used appdomain to store RumtimeErrorRecord instance.
        /// </summary>
        private static void RegisterRuntimeErrorRecorder()
        {
            var recorder = new RuntimeErrorRecorder(256);
            AppDomain.CurrentDomain.SetData("InstruemntErrorRecorder", recorder);

            Func<Exception, Action<string>, Action<string>, Task[]> RuntimeErroHandle =
                    (ex, howToTellLims, howToTellNewUI) =>
                    {
                        var tells = AppDomain.CurrentDomain.GetData("InstruemntErrorRecorder")
                                            as RuntimeErrorRecorder;
                        Task t1, t2;
                        tells.Append(ex)
                                .TellLimsAsync(ex, howToTellLims, out t1)
                                .TellNewUiAsync(ex, howToTellNewUI, out t2);
                        return new[] { t1, t2 };
                    };
            AppDomain.CurrentDomain.SetData("RuntimeErroHandle", RuntimeErroHandle);
        }

        private static void DumpReadableErrors(Exception ex, Action<string> dump)
        {
            dump.Invoke(ex.LocalizedFormat());
        }
    }

}
