﻿//-----------------------------------------------------------------------------
// Copyright (c) 2016 BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

using BGILog = BGI.Common.Logging;
using BGI.RPC;
using BGI.Common.Config;

namespace BGI.ZebraV2Seq.Service
{
    public class StageRunMgrIPCService
    {
        private BGILog.Logger _Logger = BGILog.LogMgr.GetLogger("StageRunMgrIPC");
        
        Ice.Communicator _ICEComm = null;
        StageRunMgrIPCI _StageRunMgrIPCI = null;
        StageRunMgrIPCCallbackI _StageRunMgrIPCCallbackI = null;

        private static StageRunMgrIPCService _Instance = null;

        private string _ServiceName;
        private Ice.ObjectAdapter iceAdapter = null;
        private Ice.ObjectAdapter adaptorCB = null;

        public StageRunMgrIPCService(string serviceName)
        {
            this._ServiceName = serviceName;
        }

        public int MainICEPort
        {
            get
            {
                int rtnVal = ConfigMgr.Get.GetCurValue<int>(_ServiceName, "MainICEPort");
                return rtnVal;
            }
            set { }
        }

        public int CallbackICEPort
        {
            get
            {
                int rtnVal = ConfigMgr.Get.GetCurValue<int>(_ServiceName, "CallbackICEPort");
                return rtnVal;
            }
            set { }
        }

        /// <summary>
        /// Disabled default constructor to enforce singleton
        /// </summary>
        private StageRunMgrIPCService() { }

        public void Init(IStageRunMgrIPC ic)
        {
            try
            {
                // set up ICE communications

                _Logger.Log()(BGILog.LogSeverityEnum.Debug, "Initializing ICE Comm Using ports {0} and {1}.", MainICEPort, CallbackICEPort);
                _ICEComm = Ice.Util.initialize();
                Ice.Communicator iceComm = Ice.Util.initialize();

                // Create adaptor for commands and requests from client

                _StageRunMgrIPCI = new StageRunMgrIPCI(ic);
                iceAdapter = iceComm.createObjectAdapterWithEndpoints("StageRunMgrIPC", "tcp -p " + MainICEPort.ToString());
                iceAdapter.add(_StageRunMgrIPCI, iceComm.stringToIdentity("StageRunMgrIPC"));
                iceAdapter.activate();

                // OK, now create the Callback adaptor and activate it.
                
                adaptorCB = iceComm.createObjectAdapterWithEndpoints("StageRunMgrIPCCallback", "tcp -p " + CallbackICEPort.ToString());
                _StageRunMgrIPCCallbackI = new StageRunMgrIPCCallbackI();
                adaptorCB.add(_StageRunMgrIPCCallbackI, iceComm.stringToIdentity("StageRunMgrIPCCallback"));
                adaptorCB.activate();
            }
            catch (Ice.Exception ie)
            {
                string errorMsg = String.Format("ICE Exception {0} caught during Init of Imager IPC.", ie.Message);
                _Logger.Log(BGILog.LogSeverityEnum.Error, errorMsg);
                _Logger.Log(errorMsg, ie);
                throw;
            }
            catch (SystemException se)
            {
                string errorMsg = String.Format("Exception {0} caught during Init of Imager IPC.", se.Message);
                _Logger.Log(BGILog.LogSeverityEnum.Error, errorMsg);
                _Logger.Log(errorMsg, se);
                throw;
            }
        }

        /// <summary>
        /// Initiate shutdwon of ICE Communications.
        /// Wait till shutdown completes.
        /// Cleanup all ICE objects.
        /// </summary>
        public void Shutdown()
        {
            if (_ICEComm != null)   // we don't like null reference exceptions
            {
                if (iceAdapter != null)
                {
                    iceAdapter.deactivate();
                    iceAdapter.destroy();
                }
                if (adaptorCB != null)
                {
                    adaptorCB.deactivate();
                    adaptorCB.destroy();
                }
                _ICEComm.shutdown();
                _ICEComm.waitForShutdown();
                _ICEComm.destroy();
            }
        }

        public void SendScriptStatus(ScriptStateEnum stat)
        {
            try
            {
                
                _StageRunMgrIPCCallbackI.SendScriptStatus(stat);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendScriptMessageId(int messageId)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendScriptMessageId(messageId);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendAlarmMessageId(int messageId)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendAlarmMessageId(messageId);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendDataList(List<string> dataList)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendDataList(dataList);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendLogMessageId(int messageId)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendLogMessageId(messageId);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendStageRunMessage(string type, string message)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendStageRunMessage(type, message);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }


        public void SendQCValue(QCDataValue qcValue)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendQCValue(qcValue);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendQCValues(QCDataValue[] qcValues)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendQCValues(qcValues);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }
        }

        public void SendRemainingTime(double remainingTime)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendRemainingTime(remainingTime);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Send Remaining Time error,Ice Excpetion.", ie);
            }
        }

        public void CleanSpaceDataList()
        {
            try
            {
                if (_StageRunMgrIPCCallbackI.DataList != null)
                {
                    _StageRunMgrIPCCallbackI.DataList.Clear();
                }
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Ice Excpetion.", ie);
            }

        }

        public void SendFlowcellBarcode(string flowcell)
        {
            try
            {
                _StageRunMgrIPCCallbackI.SendFlowcellBarcode(flowcell);
            }
            catch (Ice.ConnectionLostException ie)
            {
                _Logger.Log("Send Remaining Time error,Ice Excpetion.", ie);
            }
        }

    }
}
