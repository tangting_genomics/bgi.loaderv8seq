﻿//-----------------------------------------------------------------------------
// Copyright 2016 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.Utils;
using BGI.Control;
using BGI.RPC;

namespace BGI.ZebraV2Seq.Service
{
    class ZebraScheduler : IZebraSchedulerIPC, IDisposable
    {
        private Logger _Logger = LogMgr.GetLogger("Scheduler");
        private static Logger _StaticLogger = LogMgr.GetLogger("Scheduler");
        private const int MAIN_LOOP_SLEEP_MS = 500;

        private ThreadState _CurState = ThreadState.Sleeping;
        private readonly object stateLock_ = new object();

        private StageRunMgr _StageRunMgrA = null;
        private StageRunMgr _StageRunMgrB = null;
        private Thread _StageRunMgrAThread = null;
        private Thread _StageRunMgrBThread = null;
        private SystemMonitor _SysMon = null;
        private Thread _SysMonThread = null;
        private List<Thread> _ScriptThreadL = new List<Thread>();
        private int _HeartbeatCount = 0;
        private string language = "en-US";

        private enum ThreadState
        {
            Stopped,
            Stopping,
            Sleeping,
            Working
        }

        private enum LanguageType
        {
            English = 0,
            Chinese = 1
        }

        public string ScriptHomePath
        {
            get
            {
                string rtnVal = ConfigMgr.Get.GetCurValue<string>("System", "ScriptHomePath");
                if (!rtnVal.EndsWith("\\"))
                {
                    throw new BGIException("ScriptHomePath value must end in backslash to be valid path.");
                }
                return rtnVal;
            }
            set { }
        }

        public void OverrideKitRTBarcode(string barcode)
        {
            throw new NotImplementedException();
        }

        public void OverrideKitCBarcode(string barcode)
        {
            throw new NotImplementedException();
        }

        public void MainLoop()
        {
            bool exit = false;
            DateTime wakeTime = DateTime.UtcNow.AddMinutes(-1);

            _Logger.Log()(LogSeverityEnum.Debug, "Setting up and starting System Monitor thread.");

            // Set up Remote Config.  Note: May move this to higher in the stack, but I didn't want to affect
            // service code with application level stuff.
            bool rcEnabled = ConfigMgr.Get.Enabled;
            if (!rcEnabled)
            {
                throw new BGIException("Cannot run without Remote Scheduler enabled.");
            }

            Stat stat = ConfigMgr.Get.Load(false);
            if (stat.IsErr)
            {
                _Logger.Log()(LogSeverityEnum.Error, "Could not load Config.");
            }

            // Set up callback delegates before loading and starting monitoring
            ConfigMgr.Get.RunOffline += Config_RunOffline;

            LogSeverityEnum sev = ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel");
            _Logger.SetMinLogLevel(sev);
            ConfigMgr.Get.LogLevel = sev; // Need to set Remote Config log level here because it can't call itself.
            ConfigMgr.Get.RegisterLogLevelUpdate(_Logger, "System", "LogLevel");

            LanguageType langType = ConfigMgr.Get.GetCurValue<LanguageType>("System", "Language");
            if (langType == LanguageType.English)
            {
                language = "en-US";
            }
            else if (langType == LanguageType.Chinese)
            {
                language = "zh-CN";
            }
            
            // Set up System Monitor thread
            

            // Set up Stage Script Manager thread
            _StageRunMgrA = new StageRunMgr("StageRunMgrA");
            _StageRunMgrA.SetCurrentCulture(language);
            _StageRunMgrA.Init();

            _StageRunMgrB = new StageRunMgr("StageRunMgrB");
            _StageRunMgrB.SetCurrentCulture(language);
            _StageRunMgrB.Init();

            PrepareForMainloop();

            _SysMon = new SystemMonitor();
            _SysMonThread = new Thread(new ThreadStart(_SysMon.SafeMainLoop));
            _SysMonThread.Name = "SysMon";
            _SysMonThread.Start();

            _StageRunMgrAThread = new Thread(new ThreadStart(_StageRunMgrA.SafeMainLoop));
            _StageRunMgrAThread.Name = "StageRunMgrA";
            _StageRunMgrAThread.Start();

            _StageRunMgrBThread = new Thread(new ThreadStart(_StageRunMgrB.SafeMainLoop));
            _StageRunMgrBThread.Name = "StageRunMgrB";
            _StageRunMgrBThread.Start();
            
            _Logger.Log()(LogSeverityEnum.Debug, "Top of Loop.");

            // OK, now loop while waiting for commands.
            do
            {
                _HeartbeatCount++;
                //Console.WriteLine("HeartBeat {0}", loopCount);

                bool doWork = false;

                // Main loop sleep to avoid cpu-bound process
                Thread.Sleep(100);

                // Should we do work now or wait more
                lock (stateLock_)
                {
                    if (_CurState == ThreadState.Sleeping
                        || _CurState == ThreadState.Working)
                    {
                        if (wakeTime < DateTime.UtcNow)
                        {
                            doWork = true;
                        }
                    }
                }

                // Time to wake up and do something

                if (doWork)
                {
                    lock (stateLock_)
                    {
                        _CurState = ThreadState.Working;
                    }

                    // Do the work and handle certain types of exceptions (BGIException).  All other exceptions should
                    // fall back up the stack and be caught by the global unhandled exception handler.
                    try
                    {
                        // TODO do real work here if any needed

                    }
                    catch (BGIException e)
                    {
                        // Handle BGI Exceptions here if possible.
                        _Logger.Log("Exception", e);
                    }

                    int msToWait = MAIN_LOOP_SLEEP_MS;

                    wakeTime = DateTime.UtcNow.AddMilliseconds(msToWait);
                    _Logger.Log()(LogSeverityEnum.Debug, "Sleeping for {0} ms.", msToWait);

                }
                
                // Check state and change if needed

                lock (stateLock_)
                {
                    // Check if this thread is trying to stop
                    if (_CurState == ThreadState.Stopping)
                    {
                        _Logger.Log()(LogSeverityEnum.Info, "Stopping");

                        // Stop the background threads.
                        _StageRunMgrA.Stop();

                        // Merge the threads to make sure they are stopped.
                        _StageRunMgrAThread.Join();

                        _StageRunMgrB.Stop();

                        _StageRunMgrBThread.Join();

                        _SysMon.Stop();

                        // Merge the threads to make sure they are stopped.
                        _SysMonThread.Join();

                        // TODO Handle joining threads better.

                        // TODO Use an IPC Callback to tell GUI's we are shutting down.
                        //      then wait till the IPC clients reply?  Don't know.

                        // Stop the Device Manager
                        DeviceMgrZebrav2.GetBase.Shutdown();

                        // Stop the IPC
                        ZebraSchedulerIPCService.Get.Shutdown();

                        _StageRunMgrA?.StageService?.Shutdown();

                        _StageRunMgrB?.StageService?.Shutdown();

                        SystemMonitorIPCService.Get.Shutdown();

                        ConfigMgr.Get.StopMonitor();

                        _CurState = ThreadState.Stopped;
                    }

                    // Exit the loop if we are marked stopped
                    if (_CurState == ThreadState.Stopped)
                    {
                        exit = true;
                    }
                }

            } while (!exit);

            // Fell out of main loop.  Exiting.up

            _Logger.Log()(LogSeverityEnum.Warning, "Exiting main processing loop");
        }

        private void PrepareForMainloop()
        {
            try
            {
                // Setup IPC listener for ZebraScheduler
                ZebraSchedulerIPCService.Get.Init(this);
                
                DeviceMgrZebrav2.Get.StageRunMgrMap = new Dictionary<string, StageRunMgr>();
                if (_StageRunMgrA != null)
                    DeviceMgrZebrav2.Get.StageRunMgrMap.Add(_StageRunMgrA.StageRunName, _StageRunMgrA);
                if (_StageRunMgrB != null)
                    DeviceMgrZebrav2.Get.StageRunMgrMap.Add(_StageRunMgrB.StageRunName, _StageRunMgrB);
                DeviceMgrZebrav2.GetBase.Init();
                //SoftwareVer(version.Major, version.Minor, version.Revision, version.Build);
                SoftwareVer ver = GetVersion();
                DeviceMgrZebrav2.Get.SoftVersion = $"{ver.Major}.{ver.Minor}.{ver.Revision}.{ver.Build}";//GetVersion().ToString();
            }
            catch (Exception ex)
            {
                DeviceMgrZebrav2.Get.IsInitDone = true;
                _Logger.Log(LogSeverityEnum.Warning, $"Instrument initialized fail.{ex.Message}");
            }
        }

        /// <summary>
        /// Stop the current process
        /// </summary>
        public void Stop()
        {
            lock (stateLock_)
            {
                if (_CurState == ThreadState.Working)
                {
                    _Logger.Log()(LogSeverityEnum.Info, "Signal to stop working thread received.  Setting state to Stopping.");
                }
                _CurState = ThreadState.Stopping;
            }
        }

        public int GetHeartbeat()
        {
            return _HeartbeatCount;
        }

        /// <summary>
        /// Processes the request to stop
        /// </summary>
        public void RequestStop()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Service is requested to stop");
            // TODO: implement desired functionality for stop on demand
        }

        /// <summary>
        /// Check if the service can be stopped
        /// </summary>
        /// <returns></returns>
        public bool ReadyToStop()
        {
            _Logger.Log()(LogSeverityEnum.Info, "Service is checking if is ready to stop");
            // TODO: implement desired functionality for checking if we are ready to stop on demand

            return true;    // we are now ready to stop the service
        }


        public SoftwareVer GetVersion()
        {
            SoftwareVer rslt = new SoftwareVer(0, 0, 0, 99);
            string strVersion = AssemblyUtils.ApplicationVersion;
            if (!string.IsNullOrEmpty(strVersion))
            {
                Version version = new Version();
                if (Version.TryParse(strVersion, out version))
                {
                    rslt = new SoftwareVer(version.Major, version.Minor, version.Revision, version.Build);
                }
            }
            DeviceMgrZebrav2.Get.GetComponentsVersion();
            return rslt;
        }

        public List<string> GetScriptNames()
        {
            DirectoryInfo di = new DirectoryInfo(ScriptHomePath);
            if (!di.Exists)
            {
                _Logger.Log()(LogSeverityEnum.Warning, "Creating missing script path directory: {0}", di.FullName);
                di.Create();
            }

            //Get all files under the home path SCRIPT_HOME_PATH
            string[] fileArray = Directory.GetFiles(ScriptHomePath, "*.py", SearchOption.AllDirectories);
            List<string> tempList = new List<string>();
            foreach (string filepath in fileArray)
            {
                // TODO remove home directory from path
                tempList.Add(filepath.Replace(ScriptHomePath, ""));
            }
            
            return tempList;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public string GetInstrumentName()
        {
            return Environment.MachineName;
        }

        #region Remote Config Callbacks

        private static void Config_RunOffline(Stat stat, CancelEventArgs e)
        {
            _StaticLogger.Log()(LogSeverityEnum.Info, "Remote Configuration went offline");
        }

        #endregion

    }
}
