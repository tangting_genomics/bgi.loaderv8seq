﻿//-----------------------------------------------------------------------------
// Copyright (c) 2016 BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using BGI.Common.Logging;
using BGI.Common.Config;
using BGI.Common.Utils;
using BGI.Control;
using BGI.RPC;

namespace BGI.ZebraV2Seq.Service
{
    class SystemMonitor : ISystemMonitorIPC
    {
        private Logger _Logger = LogMgr.GetLogger("SysMon");

        private const int MAIN_LOOP_SLEEP_MS = 1000;
        private ThreadState _CurState = ThreadState.Sleeping;
        private readonly object stateLock_ = new object();
        private int _HeartbeatCount = 0;

        private enum ThreadState
        {
            Stopped,
            Stopping,
            Sleeping,
            Working
        }

        public void SafeMainLoop()
        {
            try
            {
                MainLoop();
            }
            finally  // All exceptions' messages & stack traces had been recorded in MainLoop.
            {
                ;
            }
        }

        public void MainLoop()
        {
            bool exit = false;
            int iResult = 0;
            DateTime wakeTime = DateTime.UtcNow.AddMinutes(-1);

            LogSeverityEnum sev = ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel");
            _Logger.SetMinLogLevel(sev);
            ConfigMgr.Get.RegisterLogLevelUpdate(_Logger, "System", "LogLevel");

            _Logger.Log()(LogSeverityEnum.Debug, "Top of Loop.");

            SystemMonitorIPCService.Get.Init(this);
            DeviceMgrZebrav2.GetBase.AtomicValueChanged += GetBase_AtomicValueChanged;

            // OK, now loop to do any status polling
            do
            {
                bool doWork = false;

                // Main loop sleep to avoid cpu-bound process
                Thread.Sleep(100);

                // Should we do work now or wait more
                lock (stateLock_)
                {
                    if (_CurState == ThreadState.Sleeping
                        || _CurState == ThreadState.Working)
                    {
                        if (wakeTime < DateTime.UtcNow)
                        {
                            doWork = true;
                        }
                    }
                }

                if (doWork)
                {

                    _HeartbeatCount++;

                    lock (stateLock_)
                    {
                        _CurState = ThreadState.Working;
                    }

                    // Do the work and handle certain types of exceptions.  All other exceptions should
                    // fall back up the stack and be caught by the global unhandled exception handler.
                    try
                    {
                        // Check for request to stop processing
                        lock (stateLock_)
                        {
                            if (_CurState == ThreadState.Stopping)
                            {
                                //break;
                            }
                        }

                        // DO work here
                        //uint level0 = DeviceMgrZebrav2.Get.GetGPIOLevel(0);
                        //_Logger.Log()(LogSeverityEnum.Info, "Test Level for Pin 0 = {0}", level0);

                        //double cpuTemp = DeviceMgrZebrav2.Get.GetBoardCPUTemp();
                        //_Logger.Log()(LogSeverityEnum.Info, "Test CPUTemp = {0}", cpuTemp);

                        //// Now send as Atomic Value from driver
                        //DeviceMgrZebrav2.Get.SendBoardCPUTemp();

                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "IsBaseCallOnline", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.IsBasecallOnline() }));
                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "IsZlimsOnline", ValueType= AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.IsZlimsOnline() }));
                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "IsImaging", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.IsImaging() }));
                        DeviceMgrZebrav2.Get.OnAtomValueEventHandle(new AtomicValueEventArgs(new AtomicValue() { ValueName = "DiskStatus", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = DeviceMgrZebrav2.Get.IsSpaceEnough }));
                        _Logger.Log()(LogSeverityEnum.Debug, "System Monitor doing something");
                    }
                    catch (BGIException e)
                    {
                        // Handle BGI Exceptions here if possible.
                        _Logger.Log("Exc", e);
                    }

                    int msToWait = MAIN_LOOP_SLEEP_MS;

                    wakeTime = DateTime.UtcNow.AddMilliseconds(msToWait);
                    _Logger.Log()(LogSeverityEnum.Debug, "Sleeping for {0} ms.", msToWait);

                }

                lock (stateLock_)
                {
                    // Check if this thread is trying to stop
                    if (_CurState == ThreadState.Stopping)
                    {
                        _Logger.Log()(LogSeverityEnum.Info, "Stopping");

                        _CurState = ThreadState.Stopped;
                    }

                    // Exit the loop if we are marked stopped
                    if (_CurState == ThreadState.Stopped)
                    {
                        exit = true;
                    }
                }

            } while (!exit);

            _Logger.Log()(LogSeverityEnum.Info, "Exiting");
            iResult = 0;

            if (iResult != 0)
            {
                //throw new BGIException(String.Format("Error returned from Disconnect: {0}", PlateGripperObj.GetErrorMessage()));
            }

            _Logger.Log()(LogSeverityEnum.Warning, "Exiting main processing loop");
        }

        public void Stop()
        {
            lock (stateLock_)
            {
                if (_CurState == ThreadState.Working)
                {
                    _Logger.Log()(LogSeverityEnum.Info, "Signal to stop working thread received.  Setting state to Stopping.");
                }
                _CurState = ThreadState.Stopping;
            }
        }

        public int GetHeartbeat()
        {
            return _HeartbeatCount;
        }

        /// <summary>
        /// Publish new AtomicValue to all active clients.
        /// </summary>
        /// <param name="args"></param>
        private void GetBase_AtomicValueChanged(AtomicValueEventArgs args)
        {
            SystemMonitorIPCService.Get.SendAtomicValue(args.av);
        }

        public void PowerControl(PowerOptionsEnum powerOptions)
        {
            if(powerOptions == PowerOptionsEnum.Reboot)
            {
                _Logger.Log()(LogSeverityEnum.Info, "Reboot");
                DeviceMgrZebrav2.Get.PowerControl(true);
            }
            else if(powerOptions == PowerOptionsEnum.ShutDown)
            {
                _Logger.Log()(LogSeverityEnum.Info, "ShutDown");
                DeviceMgrZebrav2.Get.PowerControl(false);
            }
        }
    }
}
