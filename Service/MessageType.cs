﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.ZebraV2Seq.Service
{
    public enum MessageType
    {
        LoadDNB = 1,
        SeqPrime = 2,

        #region include read1 & read2
        Sequencing_Read1 = 3,
        PESynthesis = 4,
        Sequencing_Read2 = 5,
        BarcodeSynthesis = 6,
        BarcodeSequencing = 7,
        BarcodeSynthesisSE=9,
        #endregion

        Sequencing = 8,

        Wash = 11,
        Cleantubing = 12,
        Wash_Cleantubing = 13,
        
        
        SelfCheck = 14,
        Alarm = 15,
        QCInfo = 16,
        PostLoad = 17,

        Version = 20,
        WashNaOH = 97,
        Complete = 98,
        None = 99
    }

    public enum SequenceStepsEnum
    {
        Incorporation = 213,
        Image = 223,
        Cleavage = 233
    }
}
