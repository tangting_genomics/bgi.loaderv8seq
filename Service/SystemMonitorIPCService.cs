﻿//-----------------------------------------------------------------------------
// Copyright (c) 2016 BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

using BGILog = BGI.Common.Logging;
using BGI.RPC;

namespace BGI.ZebraV2Seq.Service
{
    public class SystemMonitorIPCService
    {
        private BGILog.Logger _Logger = BGILog.LogMgr.GetLogger("SystemMonitorIPC");

        // TODO get from config
        public const int MAIN_INTERFACE_PORT = 5557;
        public const int CALLBACK_INTERFACE_PORT = 5558;

        Ice.Communicator _ICEComm = null;
        SystemMonitorIPCI _SystemMonitorIPCI = null;
        SystemMonitorIPCCallbackI _SystemMonitorIPCCallbackI = null;

        private Ice.ObjectAdapter iceAdapter = null;
        private Ice.ObjectAdapter adaptorCB = null;

        private static SystemMonitorIPCService _Instance = null;

        public static SystemMonitorIPCService Get
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SystemMonitorIPCService();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Disabled default constructor to enforce singleton
        /// </summary>
        private SystemMonitorIPCService() { }

        public void Init(ISystemMonitorIPC ic)
        {
            try
            {

                _Logger.Log()(BGILog.LogSeverityEnum.Debug, "Initializing ICE Comm Using ports {0} and {1}.", MAIN_INTERFACE_PORT, CALLBACK_INTERFACE_PORT);
                _ICEComm = Ice.Util.initialize();
                Ice.Communicator iceComm = Ice.Util.initialize();

                // Create adaptor for commands and requests from client

                _SystemMonitorIPCI = new SystemMonitorIPCI(ic);
                iceAdapter = iceComm.createObjectAdapterWithEndpoints("SystemMonitorIPC", "tcp -p " + MAIN_INTERFACE_PORT.ToString());
                iceAdapter.add(_SystemMonitorIPCI, iceComm.stringToIdentity("SystemMonitorIPC"));
                iceAdapter.activate();

                // OK, now create the Callback adaptor and activate it.

                adaptorCB = iceComm.createObjectAdapterWithEndpoints("SystemMonitorIPCCallback", "tcp -p " + CALLBACK_INTERFACE_PORT.ToString());
                _SystemMonitorIPCCallbackI = new SystemMonitorIPCCallbackI();
                adaptorCB.add(_SystemMonitorIPCCallbackI, iceComm.stringToIdentity("SystemMonitorIPCCallback"));
                adaptorCB.activate();
            }
            catch (Ice.Exception ie)
            {
                string errorMsg = String.Format("ICE Exception {0} caught during Init of Imager IPC.", ie.Message);
                _Logger.Log(BGILog.LogSeverityEnum.Error, errorMsg);
                _Logger.Log(errorMsg, ie);
                throw;
            }
            catch (SystemException se)
            {
                string errorMsg = String.Format("Exception {0} caught during Init of Imager IPC.", se.Message);
                _Logger.Log(BGILog.LogSeverityEnum.Error, errorMsg);
                _Logger.Log(errorMsg, se);
                throw;
            }
        }

        /// <summary>
        /// Initiate shutdwon of ICE Communications.
        /// Wait till shutdown completes.
        /// Cleanup all ICE objects.
        /// </summary>
        public void Shutdown()
        {
            if (_ICEComm != null)   // we don't like null reference exceptions
            {
                if (iceAdapter != null)
                {
                    iceAdapter.deactivate();
                    iceAdapter.destroy();
                }
                if (adaptorCB != null)
                {
                    adaptorCB.deactivate();
                    adaptorCB.destroy();
                }

                _ICEComm.shutdown();
                _ICEComm.waitForShutdown();
                _ICEComm.destroy();
            }
        }

        public void SendAtomicValue(AtomicValue val)
        {
            try
            {
                _SystemMonitorIPCCallbackI.SendAtomicValue(val);
            }
            catch (Ice.ConnectionLostException ie)
            {
                // TODO: not really needed.  Already caught.
                _Logger.Log("Ice Excpetion.", ie);
            }
        }
    }
}
