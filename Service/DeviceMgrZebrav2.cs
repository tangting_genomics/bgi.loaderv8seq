//-----------------------------------------------------------------------------
// Copyright 2016 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;
using System.IO;

using BGI.Common.Logging;
using BGI.Control.Device;
using BGI.Control.Device.Physical;
using BGI.Control.Device.Simulated; // TODO simon will remove
using BGI.Control.Data;
using BGI.Control.Slide;
using BGI.Control.Scanner;
using BGI.Common.Utils;
using BGI.Scripting;
using BGI.Common.Config;
using BGI.Control;
using BGI.Common.Utils.ImageUtil;
using BGI.Scripting.Reagent;
using BGI.RPC;
using System.Diagnostics;

using System.Collections.Concurrent;
using BGI.RPC.Data;
using Newtonsoft.Json;
using System.Collections.Specialized;
using BGI.Common.Utils.Stats;
using BGI.Control.Metrics;
using BGI.OSIIP;
using System.Xml;
using Autofac;
using BGI.Common.ZLimsAPI;

namespace BGI.ZebraV2Seq.Service
{
    /// <summary>
    /// Singleton Device Manager class for Zebra V2
    /// 
    /// This defines the list of devices in the instrument, handles all communications with them, handles sharing of them, and also
    /// provides information back to the System Monitor as needed.  Basically, this class handles all low level operations like
    /// Dispense, Incubate, Image, but also many low level commands.  All calls to this reference Hardware devices, not virtual
    /// things like Reagent Types, Flowcell Types, etc.
    /// 
    /// Put another way, this is the traffic cop for all Device interation.
    /// </summary>
    /// 

    public delegate void ServiceMessageHandler(string name, ServiceMessageInfo msg);

    public class DeviceMgrZebrav2 : DeviceMgrBase
    {
        #region Fields
        private Logger _Logger = LogMgr.GetLogger("DeviceMgr");
        private Logger _QCLogger = LogMgr.GetLogger("QCData");
        private readonly object _LockObj = new object();
        public static Dictionary<StageRunMgr, DeviceMgrAccess> _AccessL = new Dictionary<StageRunMgr, DeviceMgrAccess>();
        private static DeviceMgrZebrav2 _Instance = null;
        private bool _isZlimsSimulated = true;
        
        private bool isUsedChipA = false;
        private bool isUsedChipB = false;
        private ISlide _Slide = null;
        private IScanner _Scanner = null;
        //private ReagentKit _ReagentKit = null;
        private AutoResetEvent _WaitImageEvent;
        private Dictionary<string, ManualResetEvent> _WaitImagePauseEventMap = new Dictionary<string, ManualResetEvent>();
        private Dictionary<string, ManualResetEvent> _WaitImageResumeEventMap = new Dictionary<string, ManualResetEvent>();
        private Dictionary<string, ManualResetEvent> _WaitImageStopEventMap = new Dictionary<string, ManualResetEvent>();


        private Dictionary<string, IReagentNeedle> _ReagentNeedleMap = new Dictionary<string, IReagentNeedle>();
        private Dictionary<string, ISelectionValve> _SelectorValveMap = new Dictionary<string, ISelectionValve>();
        private Dictionary<string, ISyringePump> _SyringePumpMap = new Dictionary<string, ISyringePump>();
        private Dictionary<string, object> _IOBoardMap = new Dictionary<string, object>();
        private Dictionary<string, string> _FlowcellCodeMap = new Dictionary<string, string>();
        private Dictionary<string, ReagentKit> _ReagentMap = new Dictionary<string, ReagentKit>();
        private Dictionary<string, string> _ReagentIdMap = new Dictionary<string, string>();
        private Dictionary<string, ImagingCycle> _ScannerCycle = new Dictionary<string, ImagingCycle>();
        private Dictionary<string, RunInfo> _ScannerExperimentInfo = new Dictionary<string, RunInfo>();
        private Dictionary<string, IIncubator> _IncubatorMap = new Dictionary<string, IIncubator>();

       
        private RunInfo _RunInfo = null;
        private Point3[] _OriginalPoints = new Point3[2];
        private PumpTypeEnum _CurrentPumpType = PumpTypeEnum.Tecan;
        private BoardTypeEnum _CurrentIOBoardType = BoardTypeEnum.V2;
        private Dictionary<string, List<ServiceMessageInfo>> _InitMessageInfoMap = new Dictionary<string, List<ServiceMessageInfo>>();
        private Dictionary<string, bool> _StopRecordMap = new Dictionary<string, bool>();
        private ConcurrentDictionary<string, bool> _WriteFQStatus = new ConcurrentDictionary<string, bool>();
        private AutoResetEvent _WaitWriteFQEvent;
        private bool isInitSuccess = false;
        
        private string _ImageSavePath = string.Empty;
        private int _MinDiskSpaceForRun = 0;
        private int _MinDiskSpaceForCycle = 0;


        private Dictionary<string, string> _OperationMaps = new Dictionary<string, string>();
        private Dictionary<string, int[]> _OperationArrayMaps = new Dictionary<string, int[]>();
        private Dictionary<string, string> _OperationKeyMaps = new Dictionary<string, string>();
        private Dictionary<string, MessageType> _PhaseTypeMaps = new Dictionary<string, MessageType>();

        private int metricsCycle = -1;

        private StringCollection _loadDnbTimeMinute=null;
        private StringCollection _sequenceTimeMinute=null;
        private StringCollection _barcodeTimeMinute=null;
        private StringCollection _PESynthesisTimeMinute = null;
        private StringCollection _SeqPrimTimeMinute = null;
        private double _washTimeMinute =-1;
        private double _washWithNaOHTimeMinute = -1;
        private double _cleanTubeMinute = -1;
        private double _washWithCleantubeTimeMinute = -1;
        private Dictionary<string, StopwatchStats[]> _SequenceWatchStatsMap = null;
        private Dictionary<string, double> _ThetaMap = null;

        private double loadDNBTime = 0;
        private double seqPrimTime = 0;
        private double oneCycleTime = 0;
        private double peSynthesisTime = 0;
        private double barcodePrimTime = 0;

        private Dictionary<string, double> oneCycleRealTime = new Dictionary<string, double>();
        private Dictionary<string, double> realCleavageTimeMap = new Dictionary<string, double>();

        private StopwatchStats washCleantubeStats = new StopwatchStats("wash cleantube stats");
        private Dictionary<string, CycleMetrics> _CycleMetricsMap = new Dictionary<string, CycleMetrics>();
        private Dictionary<string, double> _TotalTimesMap = new Dictionary<string, double>();
        private object lockObj = new object();
        private int _barcodeReaderScanTime = 60;
        private double[] _barcodeReaderPosX = new double[2];
        private double[] _barcodeReaderPosY = new double[2];
        private bool _IsImaging = false;
        private bool _IsScanningBarcode = false;
        private string[] _scannerParameters = new string[2];
        private bool _IsSetBasecallOnline = true;
        private LedStatus _CurrentLedStatus = LedStatus.Ready;
        private bool _IsSetBuzzerValue = false;
        private string _zlimsBaseUrl = string.Empty;
        private StringCollection _FastChipFlag = null;
        private bool[] _FlowcellType = new bool[2];

        private Task _WriteFqTask = null;

        #region user
        private string _ProductUser = string.Empty;
        private string _ProductPwd = string.Empty;

        private string _ResearchUser = string.Empty;
        private string _ResearchPwd = string.Empty;

        private string _GuestUser = string.Empty;
        private string _GuestUserPwd = string.Empty;

        #endregion

        #region Alarm
        private Dictionary<string, Dictionary<int, string>> alarmIDic;
        private Dictionary<string, Dictionary<bool, string>> alarmBDic;
        private Dictionary<string, string> clearAlarmIDic;
        private Dictionary<string, string> clearAlarmBDic;

        private Dictionary<string, List<MachineStatus>> showAlarmDescriptionDic;
        private Dictionary<string, List<MachineStatus>> pauseInMachineStatusDic;
        private Dictionary<string, List<MachineStatus>> pauseAllInMachineStatusDic;
        private Dictionary<string, List<MachineStatus>> stopInMachineStatusDic;
        #endregion

        #region Sensor
        private bool _IsReportChipDoor = true;
        private bool _IsReportDNBKit = true;
        private bool _IsReportFridgeDoor = true;
        private bool _IsReportLeftCover = true;
        private bool _IsReportLiquid = true;
        private bool _IsReportReagentKit = true;
        private bool _IsReportRightCover = true;
        private bool _IsReportSMCStatus = true;
        private bool _IsReportTopCover = true;
        private bool _IsReportVacuoStatus = true;
        private bool _IsReportAllSensor = true;
        #endregion
        private Dictionary<string, BGIDeviceException> _SequenceExceptionMap = new Dictionary<string, BGIDeviceException>();
        private Dictionary<string, BGIDeviceException> _WashExceptionMap = new Dictionary<string, BGIDeviceException>();
        private object _fqLock = new object();

        private Dictionary<string, string> _ComponentVersionMap = new Dictionary<string, string>();
        private Dictionary<string, string> _LastSequenceInfoMap = new Dictionary<string, string>();
        private Dictionary<string, string> _LastSequencePhaseMap = new Dictionary<string, string>();
        private Dictionary<string, double> _LastRemainTimeMap = new Dictionary<string, double>();
        private Dictionary<string, Field> _LastFieldMap = new Dictionary<string, Field>();
        private Dictionary<string, ScanParams> _ScanParamsMap = new Dictionary<string, ScanParams>();
        private Dictionary<string, bool> _SetRunInfoMap = new Dictionary<string, bool>();

        private bool _IsWriteFQ = false;
        private bool _IsWriteFqAsync = false;
        private int _FinishedValvePos = 24;
        private Dictionary<string, string> _SampleCodeMap = new Dictionary<string, string>();

        private Dictionary<string, bool?> _IsSendTotalTime = new Dictionary<string, bool?>();
        private StringCollection _EndCycProcessModeCollection = null;

        #region ZLIMS
        private Autofac.IContainer _container = null;
        private Autofac.ContainerBuilder _builder = null;
        private bool _IsUploadFq = false;
        private bool _IsUploadImg = false;
        private StringCollection _UploadServer = null;
        private StringCollection _UploadUser = null;
        private StringCollection _UploadPwd = null;
        private StringCollection _UploadServerGroup = null;
        private Task _UploadFqTask = null;
        private ConcurrentDictionary<string, string> _UploadFqFileMap = new ConcurrentDictionary<string, string>();
        private ConcurrentDictionary<string, string> _UploadMetricsFileMap = new ConcurrentDictionary<string, string>();

        private ConcurrentDictionary<string, string> _UploadImgFileMap = new ConcurrentDictionary<string, string>();
        private ConcurrentDictionary<string, string> _UploadImgMetricsFileMap = new ConcurrentDictionary<string, string>();
        #endregion

        #region Temperature Metrics
        private double _LastTemperatureA = double.NaN;
        private double _LastTemperatureB = double.NaN;
        private double _LastFridgeTemperature = double.NaN;
        private double _LastMachineTemperature = double.NaN;
        #endregion
        #endregion

        #region Constants
        private const string KEY_FLOWCELL_A = "StageRunMgrA";
        private const string KEY_FLOWCELL_B = "StageRunMgrB";
        private const string DATETIME_FORMAT_FULL = "yyyy-MM-ddTHH:mm:ss.fffZ";
        private const string CATEGORY_IO_BOARD = "IOBoard";
        private const string CATEGORY_CAMERA = "Camera";
        private const string CATEGORY_SYSTEM = "System";
        private const string CATEGORY_SCANNER = "Scanner";
        private const string CATEGORY_ZLIMS = "ZLIMS";
        private const string CATEGORY_READER = "BarcodeReader";
        private const int PUMP_DISPENSE_RATE = 200;
        #endregion

        #region event
        public event ServiceMessageHandler OnServiceMessageEvent;
        #endregion

        #region Properties
        public Dictionary<string,MessageType> CurrentMessageStatus
        {
            get;private set;
        }

        public string CurrentUser { get; private set; }
        public string CurrentUserPwd { get; private set; }

        public string CurrentUserRole { get; private set; }

        public string SelfCheckStatus { get; private set; } = string.Empty;

        public bool IsUsedChipA
        {
            get { return ConfigMgr.Get.GetCurValue<bool>("System", "IsUsedChipA"); }
        }

        public bool IsUsedChipB
        {
            get { return ConfigMgr.Get.GetCurValue<bool>("System", "IsUsedChipB"); }
        }

        private bool IsStopWhenSensorError
        {
            get { return ConfigMgr.Get.GetCurValue<bool>("System", "IsStopWhenSensorError"); }
        }

        private bool IsPauseWhenSensorError
        {
            get { return ConfigMgr.Get.GetCurValue<bool>("System", "IsPauseWhenSensorError"); }
        }

        public bool IsInitDone { get; set; } = false;

        public Dictionary<string, StageRunMgr> StageRunMgrMap { get; set; } = null;

        public Dictionary<string,MachineStatus> MachineStatusMap
        {
            get;set;
        }

        public bool IsSpaceEnough
        {
            get;private set;
        }

        
        #region config time
        private StringCollection LoadDnbTimeMinute
        {
            get
            {
                try
                {
                    if (_loadDnbTimeMinute == null)
                    {
                        _loadDnbTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "LoadDnbTimeMinute");
                    }
                    return _loadDnbTimeMinute;
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _loadDnbTimeMinute = new StringCollection();
                    _loadDnbTimeMinute.Add("85");
                    return _loadDnbTimeMinute;
                }

            }
        }
        private StringCollection SequenceTimeMinute
        {
            get
            {
                try
                {
                    if (_sequenceTimeMinute == null)
                    {
                        _sequenceTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "SequenceTimeMinute");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _sequenceTimeMinute = new StringCollection();
                    _sequenceTimeMinute.Add("3.2");
                    _sequenceTimeMinute.Add("7");
                    _sequenceTimeMinute.Add("3.3");
                }

                return _sequenceTimeMinute;
            }
        }
        private StringCollection BarcodePrimTimeMinute
        {
            get
            {
                try
                {
                    if (_barcodeTimeMinute == null)
                    {
                        _barcodeTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "BarcodePrimTimeMinute");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _barcodeTimeMinute = new StringCollection();
                    _barcodeTimeMinute.Add("40");
                }

                return _barcodeTimeMinute;
            }
        }

        private StringCollection SeqPrimTimeMinute
        {
            get
            {
                try
                {
                    if (_SeqPrimTimeMinute == null)
                    {
                        _SeqPrimTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "SeqPrimTimeMinute");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _SeqPrimTimeMinute = new StringCollection();
                    _SeqPrimTimeMinute.Add("1.1");
                    _SeqPrimTimeMinute.Add("6.8");
                }

                return _SeqPrimTimeMinute;
            }
        }

        private StringCollection PESynthesisTimeMinute
        {
            get
            {
                try
                {
                    if (_PESynthesisTimeMinute == null)
                    {
                        _PESynthesisTimeMinute = ConfigMgr.Get.GetCurValue<StringCollection>("System", "PESynthesisTimeMinute");
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _PESynthesisTimeMinute = new StringCollection();
                    _PESynthesisTimeMinute.Add("0.8");
                    _PESynthesisTimeMinute.Add("9.1");
                    _PESynthesisTimeMinute.Add("14");
                    _PESynthesisTimeMinute.Add("47");
                    _PESynthesisTimeMinute.Add("9.8");
                    _PESynthesisTimeMinute.Add("10.5");
                    _PESynthesisTimeMinute.Add("16");
                }

                return _PESynthesisTimeMinute;
            }
        }

        private double WashTimeMinute
        {
            get
            {
                try
                {
                    if(_washTimeMinute < 0)
                        _washTimeMinute = ConfigMgr.Get.GetCurValue<double>("System", "WashTimeMinute");
                    
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _washTimeMinute = 18;
                }
                return _washTimeMinute;

            }
        }

        

        private double WashWithNaOHTimeMinute
        {
            get
            {
                try
                {
                    if(_washWithNaOHTimeMinute < 0 )
                        _washWithNaOHTimeMinute =  ConfigMgr.Get.GetCurValue<double>("System", "WashWithNaOHTimeMinute");
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _washWithNaOHTimeMinute = 13;
                }
                return _washWithNaOHTimeMinute;

            }
        }

        private double CleanTubeTimeMinute
        {
            get
            {
                try
                {
                    if(_cleanTubeMinute < 0)
                        _cleanTubeMinute = ConfigMgr.Get.GetCurValue<double>("System", "CleanTubeTimeMinute");
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _cleanTubeMinute = 25;
                }
                return _cleanTubeMinute;

            }
        }
        private double WashWithCleantubeTimeMinute
        {
            get
            {
                try
                {
                    if(_washWithCleantubeTimeMinute < 0)
                        _washWithCleantubeTimeMinute =  ConfigMgr.Get.GetCurValue<double>("System", "WashWithCleantubeTimeMinute");
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"{ex.ToString()}");
                    _washWithCleantubeTimeMinute = 70;
                }
                return _washWithCleantubeTimeMinute;
            }
        }
        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Disabled default constructor
        /// </summary>
        private DeviceMgrZebrav2()
        {
            string machineName = Environment.MachineName;
            this.isUsedChipA = IsUsedChipA;
            this.isUsedChipB = IsUsedChipB;
            _WaitImageEvent = new AutoResetEvent(true);
            _WaitWriteFQEvent = new AutoResetEvent(true);
            _Logger.Log()(LogSeverityEnum.Info, "Constructing Device Manager class for Zebra V2");
            _ScannerCycle.Add(KEY_FLOWCELL_A, new ImagingCycle());
            _ScannerCycle.Add(KEY_FLOWCELL_B, new ImagingCycle());
            
            StageRunMgrMap = new Dictionary<string, StageRunMgr>();
            CurrentMessageStatus = new Dictionary<string, MessageType>();
            MachineStatusMap = new Dictionary<string, MachineStatus>();
            MachineStatusMap.Add(KEY_FLOWCELL_A, MachineStatus.Idle);
            MachineStatusMap.Add(KEY_FLOWCELL_B, MachineStatus.Idle);
            this._FlowcellType = new bool[2];
            _FlowcellType[0] = false;
            _FlowcellType[1] = false;

            _SequenceWatchStatsMap = new Dictionary<string, StopwatchStats[]>();
            _SequenceWatchStatsMap.Add(KEY_FLOWCELL_A, null);
            _SequenceWatchStatsMap.Add(KEY_FLOWCELL_B, null);

            _WaitImagePauseEventMap.Add(KEY_FLOWCELL_A, new ManualResetEvent(false));
            _WaitImagePauseEventMap.Add(KEY_FLOWCELL_B, new ManualResetEvent(false));

            _WaitImageResumeEventMap.Add(KEY_FLOWCELL_A, new ManualResetEvent(false));
            _WaitImageResumeEventMap.Add(KEY_FLOWCELL_B, new ManualResetEvent(false));

            _WaitImageStopEventMap.Add(KEY_FLOWCELL_A, new ManualResetEvent(false));
            _WaitImageStopEventMap.Add(KEY_FLOWCELL_B, new ManualResetEvent(false));

            _ThetaMap = new Dictionary<string, double>();
            _ThetaMap.Add(KEY_FLOWCELL_A, 0);
            _ThetaMap.Add(KEY_FLOWCELL_B, 0);

            _SetRunInfoMap = new Dictionary<string, bool>();
            _SetRunInfoMap.Add(KEY_FLOWCELL_A, false);
            _SetRunInfoMap.Add(KEY_FLOWCELL_B, false);

        }

        /// <summary>
        /// Get the instance of this child class.
        /// Note: Should not ever have to access this at this level.  All communication should be done via the Base
        /// class by GetBase.
        /// TODO: This rule may not survive.  Just trying to enforce while we can.  Simon
        /// </summary>
        public static DeviceMgrZebrav2 Get
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DeviceMgrZebrav2();
                }
                return _Instance;
            }
        }

        public static DeviceMgrBase GetBase
        {
            get
            {
                return Get as DeviceMgrBase;
            }
        }

        public static DeviceMgrAccess GetAccess(StageRunMgr stageRunMgr)
        {
            if (!_AccessL.ContainsKey(stageRunMgr))
            {
                _AccessL.Add(stageRunMgr, new DeviceMgrAccess(stageRunMgr));
            }
            //if (!_StageRunMgrMap.ContainsKey(stageRunMgr.StageRunName))
            //    _StageRunMgrMap.Add(stageRunMgr.StageRunName, stageRunMgr);
            return _AccessL[stageRunMgr];
        }

        #endregion

        #region Device definition
        #region For Power Board - One Board
        #endregion

        #region  Incubator
        private DeviceProxy _IncubatorChipA;
        private IIncubator IncubatorChipAInt
        {
            get
            {
                return _IncubatorChipA?.Get<IIncubator>();
            }
        }

        private DeviceProxy _IncubatorChipB;
        private IIncubator IncubatorChipBInt
        {
            get
            {
                return _IncubatorChipB?.Get<IIncubator>();
            }
        }
        #endregion

        #region For Barcode Scanner
        private DeviceProxy _BarcodeReader = null;
        private IBarcodeReader BarcodeReaderInt
        {
            get
            {
                return _BarcodeReader?.Get<IBarcodeReader>();
            }
        }
        #endregion

        #region For IOBoard - One Board
        private DeviceProxy _IoBoard = null;
        private IIOBoardv1 IoBoardInt
        {
            get
            {
                return _IoBoard?.Get<IIOBoardv1>((int)IOIndexEnum.IOChipA);
            }
        }

        private DeviceProxy _IoBoard2 = null;
        private IIOBoardv1 IoBoard2Int
        {
            get
            {
                return _IoBoard2?.Get<IIOBoardv1>((int)IOIndexEnum.IOChipB);
            }
        }
        #endregion

        #region For Temperature Board - One Board
        private DeviceProxy _ChipTempBoard = null;
        private ITempControl ChipTempBoardInt
        {
            get
            {
                return _ChipTempBoard?.Get<ITempControl>();
            }
        }
        #endregion

        #region For Selecotr Valve-Two Valve
        private DeviceProxy _MainSelectorValve1 = null;
        private ISelectionValve MainSelectorValve1Int
        {
            get
            {
                ISelectionValve temp = _MainSelectorValve1?.Get<ISelectionValve>((int)ValveIndexEnum.ValveChipA);

                return temp;
            }
        }

        private DeviceProxy _MainSelectorValve2 = null;
        private ISelectionValve MainSelectorValve2Int
        {
            get
            {
                return _MainSelectorValve2?.Get<ISelectionValve>((int)ValveIndexEnum.ValveChipB);
            }
        }
        #endregion

        #region For Syringe Pump - Four Pump
        private DeviceProxy _SyringePump1 = null;
        private ISyringePump SyringePump1Int
        {
            get
            {
                return _SyringePump1?.Get<ISyringePump>((int)PumpIndexEnum.ChipA);
            }
        }

        private DeviceProxy _SyringePumpByPass1 = null;

        private ISyringePump SyringePumpByPass1Int
        {
            get
            {
                return _SyringePumpByPass1?.Get<ISyringePump>((int)PumpIndexEnum.BypassChipA);
            }
        }

        private DeviceProxy _SyringePump2 = null;
        private ISyringePump SyringePump2Int
        {
            get
            {
                return _SyringePump2?.Get<ISyringePump>((int)PumpIndexEnum.ChipB);
            }
        }

        private DeviceProxy _SyringePumpByPass2 = null;

        private ISyringePump SyringePumpByPass2Int
        {
            get
            {
                return _SyringePumpByPass2?.Get<ISyringePump>((int)PumpIndexEnum.BypassChipB);
            }
        }
        #endregion

        #region For Reagent Needle - Two Needle
        private DeviceProxy _ReagentNeedle1 = null;

        private IReagentNeedle ReagentNeedle1Int
        {
            get
            {
                return _ReagentNeedle1?.Get<IReagentNeedle>((int)ReagentIndexEnum.ChipA);
            }
        }

        private DeviceProxy _ReagentNeedle2 = null;

        private IReagentNeedle ReagentNeedle2Int
        {
            get
            {
                return _ReagentNeedle2?.Get<IReagentNeedle>((int)ReagentIndexEnum.ChipB);
            }
        }
        #endregion

        #region For Camera - Two Camera
        private DeviceProxy _CameraA = null;
        private ICamera CameraIntA
        {
            get
            {
                return _CameraA?.Get<ICamera>(0);
            }
        }

        private DeviceProxy _CameraB = null;

        private ICamera CameraIntB
        {
            get
            {
                return _CameraB?.Get<ICamera>(1);
            }
        }
        #endregion

        #region For AutoFocus Model
        private DeviceProxy _AFControl = null;
        private IAFControl AFControl
        {
            get { return _AFControl?.Get<IAFControl>(); }
        }
        #endregion

        #region For Move Stage
        private DeviceProxy _XYStage = null;
        private IXYStage XYStageInt
        {
            get
            {
                return _XYStage?.Get<IXYStage>();
            }
        }
        #endregion

        #region For Led Lamp belt
        private ILEDLampBelt LEDLampBeltInt
        {
            get
            {
                return _IoBoard?.Get<ILEDLampBelt>();
            }
        }
        #endregion

        #region For Buzzer
        private IBuzzerControl BuzzerControlInt
        {
            get { return _IoBoard?.Get<IBuzzerControl>(); }
        }
        #endregion
        #endregion

        private T GetValueFromConfig<T>(string category,string para, T defaultValue)
        {
            T rslt = defaultValue;
            try
            {
                
                rslt = ConfigMgr.Get.GetCurValue<T>(category, para);
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"GetPara {para} from {category} failed {ex.ToString()}");
            }
            return rslt;
            
        }

        /// <summary>
        /// Initialization called very early, before any ICE IPC's set up, so can be guaranteed it is called before any access.
        /// </summary>
        public override void Init()
        {
            InitAlarmDic();

            this._OperationArrayMaps = new Dictionary<string, int[]>();
            this._OperationKeyMaps = new Dictionary<string, string>();
            this._OperationMaps = new Dictionary<string, string>();
            this._PhaseTypeMaps = new Dictionary<string, MessageType>();

            #region Config 
            BoardTypeEnum ioBoardType = BoardTypeEnum.V2;
            PumpTypeEnum pumpType = PumpTypeEnum.Thomas;
            CameraTypeEnum cameraType = CameraTypeEnum.Andor;
            BoardTypeEnum tcBoardType = BoardTypeEnum.V2;
            SelectorValveTypeEnum valveType = SelectorValveTypeEnum.Idex;
            AFTypeEnum afType = AFTypeEnum.ACS;
            try
            {
                pumpType = ConfigMgr.Get.GetCurValue<PumpTypeEnum>(CATEGORY_SYSTEM, "PumpType");
                cameraType = ConfigMgr.Get.GetCurValue<CameraTypeEnum>(CATEGORY_SYSTEM, "CameraType");
                ioBoardType = ConfigMgr.Get.GetCurValue<BoardTypeEnum>(CATEGORY_SYSTEM, "IOBoardType");
                tcBoardType = ConfigMgr.Get.GetCurValue<BoardTypeEnum>(CATEGORY_SYSTEM, "TemperatureBoardType");
                valveType = ConfigMgr.Get.GetCurValue<SelectorValveTypeEnum>(CATEGORY_SYSTEM, "ValveType");
                afType = ConfigMgr.Get.GetCurValue<AFTypeEnum>(CATEGORY_SYSTEM, "AFType");

                this._IsSetBasecallOnline = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_SYSTEM, "IsBaseCallOnLine");
                this._isZlimsSimulated = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_ZLIMS, "IsSimulated");
                this._zlimsBaseUrl = string.Format("http://{0}:{1}/", ConfigMgr.Get.GetCurValue<string>(CATEGORY_ZLIMS, "BaseUrl"), ConfigMgr.Get.GetCurValue<string>(CATEGORY_ZLIMS, "Port"));

                this._ProductUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ProductUserName");
                this._ProductPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ProductUserPwd");

                this._ResearchUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ResearchUserName");
                this._ResearchPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ResearchUserPwd");

                this._GuestUser = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "GuestUserName");
                this._GuestUserPwd = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "GuestUserPwd");

                this._FastChipFlag = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_SYSTEM, "FastChipFlag");

                this._ImageSavePath = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ImageSavePath");
                this._MinDiskSpaceForRun = ConfigMgr.Get.GetCurValue<int>(CATEGORY_SYSTEM, "MinSpace_GB");
                this._MinDiskSpaceForCycle = ConfigMgr.Get.GetCurValue<int>(CATEGORY_SYSTEM, "MinSpaceForCycle_GB");

                this._scannerParameters[0] = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ScanParams");
                this._scannerParameters[1] = ConfigMgr.Get.GetCurValue<string>(CATEGORY_SYSTEM, "ScanParamsB");

                this._barcodeReaderScanTime = ConfigMgr.Get.GetCurValue<int>(CATEGORY_READER, "ScanTimeout_ms") / 1000;

                this._barcodeReaderPosX[0] = ConfigMgr.Get.GetCurValue<double>(CATEGORY_READER, "QRCodeX_um");
                this._barcodeReaderPosX[1] = ConfigMgr.Get.GetCurValue<double>(CATEGORY_READER, "QRCodeBX_um");

                this._barcodeReaderPosY[0] = ConfigMgr.Get.GetCurValue<double>(CATEGORY_READER, "QRCodeY_um");
                this._barcodeReaderPosY[1] = ConfigMgr.Get.GetCurValue<double>(CATEGORY_READER, "QRCodeBY_um");

                double _originalA_X = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalX_um");
                double _originalA_Y = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalY_um");
                double _originalA_Z = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalZ_um");

                double _originalB_X = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalBX_um");
                double _originalB_Y = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalBY_um");
                double _originalB_Z = ConfigMgr.Get.GetCurValue<double>(CATEGORY_SCANNER, "OriginalBZ_um");

                this._IsReportAllSensor = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportAllSensor");
                this._IsReportChipDoor = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportChipDoor");
                this._IsReportDNBKit = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportDNBKit");
                this._IsReportFridgeDoor = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportFridgeDoor");
                this._IsReportLeftCover = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportLeftCover");
                this._IsReportLiquid = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportLiquid");
                this._IsReportReagentKit = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportReagentKit");
                this._IsReportRightCover = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportRightCover");
                this._IsReportSMCStatus = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportSMCStatus");
                this._IsReportTopCover = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportTopCover");
                this._IsReportVacuoStatus = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_IO_BOARD, "IsReportVacuoStatus");

                this._IsWriteFqAsync = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_SYSTEM, "WriteFqAsync");
                this._FinishedValvePos = ConfigMgr.Get.GetCurValue<int>(CATEGORY_SYSTEM, "FinishedValvePos");

                this._CurrentPumpType = pumpType;
                this._CurrentIOBoardType = ioBoardType;


                this._OriginalPoints = new Point3[2];
                this._OriginalPoints[0] = new Point3() { x = _originalA_X, y = _originalA_Y, z = _originalA_Z };
                this._OriginalPoints[1] = new Point3() { x = _originalB_X, y = _originalB_Y, z = _originalB_Z };

                this._EndCycProcessModeCollection = ConfigMgr.Get.GetCurValue<StringCollection>("BaseCall", "EndCycProcessMode");
                this._IsUploadFq = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_SYSTEM, "IsUploadFq");
                this._IsUploadImg = ConfigMgr.Get.GetCurValue<bool>(CATEGORY_SYSTEM, "IsUploadImg");
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"Get Config Failed {ex.ToString()}");
            }

            #endregion

            if (this._IsUploadFq || this._IsUploadImg)
            {
                _builder = new Autofac.ContainerBuilder();
                _builder.RegisterInstance(new ZLimsApiService(_zlimsBaseUrl)).As<IZLimsApiService>();
                _container = _builder.Build();

                this._UploadServer = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_SYSTEM, "FqServerAddress");
                this._UploadUser = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_SYSTEM, "FqServerUserName");
                this._UploadPwd = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_SYSTEM, "FqServerPwd");
                this._UploadServerGroup = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_SYSTEM, "FqServerGroup");
            }

            _IncubatorMap = new Dictionary<string, IIncubator>();

            _InitMessageInfoMap = new Dictionary<string, List<ServiceMessageInfo>>();

            _WriteFQStatus = new ConcurrentDictionary<string, bool>();

            

            if (isUsedChipA)
            {
                _IncubatorChipA = new DeviceProxy("Incubator", typeof(VIncubator), typeof(SIncubator));
                _IncubatorChipA.Initialize();
                _IncubatorChipA.Connect();
                _IncubatorMap.Add(KEY_FLOWCELL_A, IncubatorChipAInt);
            }
            if (isUsedChipB)
            {
                _IncubatorChipB = new DeviceProxy("Incubator", typeof(VIncubator), typeof(SIncubator));
                _IncubatorChipB.Initialize();
                _IncubatorChipB.Connect();
                _IncubatorMap.Add(KEY_FLOWCELL_B, IncubatorChipBInt);
            }
            
            #region Init IoBoard
            if (ioBoardType == BoardTypeEnum.V01)
            {
                _IoBoard = new DeviceProxy("IOBoard", typeof(VIOBoardV01), typeof(SIOBoardv1));
                if (CheckDeviceInit(_IoBoard, (int)IOIndexEnum.IOChipA))
                {
                    _IOBoardMap.Add(KEY_FLOWCELL_A, IoBoardInt);
                    _IoBoard.AtomicValueChanged -= OnAtomValueEventHandle;
                    _IoBoard.AtomicValueChanged += OnAtomValueEventHandle;
                }
                _IoBoard2 = new DeviceProxy("IOBoard", typeof(VIOBoardV01), typeof(SIOBoardv1));
                if (CheckDeviceInit(_IoBoard2, (int)IOIndexEnum.IOChipB))
                {
                    _IOBoardMap.Add(KEY_FLOWCELL_B, IoBoard2Int);
                    _IoBoard2.AtomicValueChanged -= OnAtomValueEventHandle;
                    _IoBoard2.AtomicValueChanged += OnAtomValueEventHandle;
                }
            }
            else if (ioBoardType == BoardTypeEnum.V2)
            {
                _IoBoard = new DeviceProxy("IOBoard", typeof(VIOBoardV2), typeof(SIOBoardv1));
                if (CheckDeviceInit(_IoBoard))
                {
                    _IOBoardMap.Add(KEY_FLOWCELL_A, IoBoardInt);
                    _IOBoardMap.Add(KEY_FLOWCELL_B, IoBoardInt);
                    _IoBoard.AtomicValueChanged -= OnAtomValueEventHandle;
                    _IoBoard.AtomicValueChanged += OnAtomValueEventHandle;
                    _Logger.Log()(LogSeverityEnum.Info, $"IOBoard init success");
                }
                else
                {
                    _IoBoard = null;
                    _Logger.Log()(LogSeverityEnum.Error, $"IOBoard init failed.");
                }
            }
            #endregion

            #region Init Selector Valve
            if (isUsedChipA)
            {
                if (valveType == SelectorValveTypeEnum.Idex)
                {
                    _MainSelectorValve1 = new DeviceProxy("SelectorValve", typeof(VIdexUartValve), typeof(SSelectionValve),new object[] { (int)ValveIndexEnum.ValveChipA });
                }
                else if (valveType == SelectorValveTypeEnum.Vici)
                {
                    _MainSelectorValve1 = new DeviceProxy("SelectorValve", typeof(VViciEUTAValve), typeof(SSelectionValve), new object[] { (int)ValveIndexEnum.ValveChipA });
                }
                else if (valveType == SelectorValveTypeEnum.Tecan)
                {
                    _MainSelectorValve1 = new DeviceProxy("SelectorValve", typeof(VTecanSmartValve), typeof(SSelectionValve), new object[] { (int)ValveIndexEnum.ValveChipA });
                }
                if (CheckDeviceInit(_MainSelectorValve1, (int)ValveIndexEnum.ValveChipA))
                {
                    _SelectorValveMap.Add(KEY_FLOWCELL_A, MainSelectorValve1Int);
                    _Logger.Log()(LogSeverityEnum.Info, $"SelectorValve for ChipA init success.");
                }
                else
                {
                    _MainSelectorValve1 = null;
                    _Logger.Log()(LogSeverityEnum.Error, $"SelectorValve for ChipA init failed.");
                }
            }
            if (isUsedChipB)
            {
                if (valveType == SelectorValveTypeEnum.Idex)
                {
                    _MainSelectorValve2 = new DeviceProxy("SelectorValve", typeof(VIdexUartValve), typeof(SSelectionValve), new object[] { (int)ValveIndexEnum.ValveChipB });
                }
                else if (valveType == SelectorValveTypeEnum.Vici)
                {
                    _MainSelectorValve2 = new DeviceProxy("SelectorValve", typeof(VViciEUTAValve), typeof(SSelectionValve), new object[] { (int)ValveIndexEnum.ValveChipB });
                }
                else if (valveType == SelectorValveTypeEnum.Tecan)
                {
                    _MainSelectorValve2 = new DeviceProxy("SelectorValve", typeof(VTecanSmartValve), typeof(SSelectionValve), new object[] { (int)ValveIndexEnum.ValveChipB });
                }
                if (CheckDeviceInit(_MainSelectorValve2, (int)ValveIndexEnum.ValveChipB))
                {
                    _SelectorValveMap.Add(KEY_FLOWCELL_B, MainSelectorValve2Int);
                    _Logger.Log()(LogSeverityEnum.Info, $"SelectorValve for ChipB init success.");
                }
                else
                {
                    _MainSelectorValve2 = null;
                    _Logger.Log()(LogSeverityEnum.Error, $"SelectorValve for ChipB init failed.");
                }
            }
            #endregion

            #region Init Syringe Pump
            if (isUsedChipA)
            {
                if (pumpType == PumpTypeEnum.Tecan)
                {
                    _SyringePump1 = new DeviceProxy("SyringePump", typeof(VCavroXMP6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipA });
                    _SyringePumpByPass1 = new DeviceProxy("SyringePump", typeof(VCavroXLP6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.BypassChipA });
                    if (CheckDeviceInit(_SyringePump1, (int)PumpIndexEnum.ChipA))
                    {
                        _SyringePumpMap.Add(KEY_FLOWCELL_A, SyringePump1Int);
                    }
                    if (CheckDeviceInit(_SyringePumpByPass1, (int)PumpIndexEnum.BypassChipA))
                    {
                        _SyringePumpMap.Add($"{KEY_FLOWCELL_A}_ByPass", SyringePumpByPass1Int);
                    }
                }
                else
                {
                    if (pumpType == PumpTypeEnum.Thomas)
                    {
                        _SyringePump1 = new DeviceProxy("SyringePump", typeof(VThomasMC6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipA });
                    }
                    else if (pumpType == PumpTypeEnum.Tecan3Plus)
                    {
                        _SyringePump1 = new DeviceProxy("SyringePump", typeof(VCavroXMP6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipA });
                    }

                    if (CheckDeviceInit(_SyringePump1, (int)PumpIndexEnum.ChipA))
                    {
                        _SyringePumpMap.Add(KEY_FLOWCELL_A, SyringePump1Int);
                        _SyringePumpMap.Add($"{KEY_FLOWCELL_A}_ByPass", SyringePump1Int);
                        _Logger.Log()(LogSeverityEnum.Info, $"SyringePump for ChipA init success.");
                    }
                    else
                    {
                        _SyringePump1 = null;
                        _Logger.Log()(LogSeverityEnum.Error, $"SyringePump for ChipA init failed.");
                    }
                }

            }
            if (isUsedChipB)
            {
                if (pumpType == PumpTypeEnum.Tecan)
                {
                    _SyringePump2 = new DeviceProxy("SyringePump", typeof(VCavroXMP6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipB });
                    _SyringePumpByPass2 = new DeviceProxy("SyringePump", typeof(VCavroXLP6KPump), typeof(SSyringePump),new object[] { (int)PumpIndexEnum.BypassChipB });
                    if (CheckDeviceInit(_SyringePump2, (int)PumpIndexEnum.ChipB))
                    {
                        _SyringePumpMap.Add(KEY_FLOWCELL_B, SyringePump2Int);
                    }
                    if (CheckDeviceInit(_SyringePumpByPass2, (int)PumpIndexEnum.BypassChipB))
                    {
                        _SyringePumpMap.Add($"{KEY_FLOWCELL_B}_ByPass", SyringePumpByPass2Int);
                    }
                }
                else
                {
                    if (pumpType == PumpTypeEnum.Thomas)
                    {
                        _SyringePump2 = new DeviceProxy("SyringePump", typeof(VThomasMC6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipB });
                    }
                    else if (pumpType == PumpTypeEnum.Tecan3Plus)
                    {
                        _SyringePump2 = new DeviceProxy("SyringePump", typeof(VCavroXMP6KPump), typeof(SSyringePump), new object[] { (int)PumpIndexEnum.ChipB });
                    }
                    if (CheckDeviceInit(_SyringePump2, (int)PumpIndexEnum.ChipB))
                    {
                        _SyringePumpMap.Add(KEY_FLOWCELL_B, SyringePump2Int);
                        _SyringePumpMap.Add($"{KEY_FLOWCELL_B}_ByPass", SyringePump2Int);
                        _Logger.Log()(LogSeverityEnum.Info, $"SyringePump for ChipB init success.");
                    }
                    else
                    {
                        _SyringePump2 = null;
                        _Logger.Log()(LogSeverityEnum.Error, $"SyringePump for ChipB init failed.");
                    }
                }
            }
            #endregion

            #region Init ReagentNeedle
            if (isUsedChipA)
            {
                _ReagentNeedle1 = new DeviceProxy("ReagentNeedle", typeof(VReagentNeedle), typeof(SReagentNeedle),new object[] { (int)ReagentIndexEnum.ChipA });
                if (CheckDeviceInit(_ReagentNeedle1, (int)ReagentIndexEnum.ChipA))
                {
                    _ReagentNeedleMap.Add(KEY_FLOWCELL_A, ReagentNeedle1Int);
                    _Logger.Log()(LogSeverityEnum.Info, "ReagentNeedle for ChipA init success");
                }
                else
                {
                    _ReagentNeedle1 = null;
                    _Logger.Log()(LogSeverityEnum.Error, "ReagentNeedle for ChipA init Failed");
                }
            }
            if (isUsedChipB)
            {
                _ReagentNeedle2 = new DeviceProxy("ReagentNeedle", typeof(VReagentNeedle), typeof(SReagentNeedle), new object[] { (int)ReagentIndexEnum.ChipB });
                if (CheckDeviceInit(_ReagentNeedle2, (int)ReagentIndexEnum.ChipB))
                {
                    _ReagentNeedleMap.Add(KEY_FLOWCELL_B, ReagentNeedle2Int);
                    _Logger.Log()(LogSeverityEnum.Info, "ReagentNeedle for ChipB init success");
                }
                else
                {
                    _ReagentNeedle2 = null;
                    _Logger.Log()(LogSeverityEnum.Error, "ReagentNeedle for ChipB init Failed");
                }
            }

            #endregion

            #region Init Temperature Board
            if (tcBoardType == BoardTypeEnum.V01)
            {
                _ChipTempBoard = new DeviceProxy("TemperatureBoard", typeof(VTemperatureBoardV01), typeof(SChipTempBoard));
            }
            else if (tcBoardType == BoardTypeEnum.V2)
            {
                _ChipTempBoard = new DeviceProxy("TemperatureBoard", typeof(VTemperatureBoardV2), typeof(SChipTempBoard));
            }
            if (CheckDeviceInit(_ChipTempBoard))
            {
                _ChipTempBoard.AtomicValueChanged -= OnAtomValueEventHandle;
                _ChipTempBoard.AtomicValueChanged += OnAtomValueEventHandle;
                _Logger.Log()(LogSeverityEnum.Info, "TemperatureBoard init success");
            }
            else
            {
                _ChipTempBoard = null;
                _Logger.Log()(LogSeverityEnum.Error, "TemperatureBoard init Failed");
            }

            #endregion

            #region Init AutoFocus Model
            if (afType == AFTypeEnum.ASI)
                _AFControl = new DeviceProxy("AFControl", typeof(VAsiAFControl), typeof(SAsiAFControl));
            else if (afType == AFTypeEnum.WDI)
                _AFControl = new DeviceProxy("AFControl", typeof(VWdiAFControl), typeof(SAsiAFControl));
            else if (afType == AFTypeEnum.ACS)
                _AFControl = new DeviceProxy("AFControl", typeof(VACSAFControl), typeof(SAsiAFControl));
            if (CheckDeviceInit(_AFControl))
            {
                _Logger.Log()(LogSeverityEnum.Info, "AFControl init success");
            }
            else
            {
                _AFControl = null;
                _Logger.Log()(LogSeverityEnum.Error, "AFControl init Failed");
            }
            #endregion

            #region Init Move Stage
            _XYStage = new DeviceProxy("XYStage", typeof(VACSStage), typeof(SACSStage));
            if(CheckDeviceInit(_XYStage))
            {
                _Logger.Log()(LogSeverityEnum.Info, "XYStage init success");
            }
            else
            {
                _XYStage = null;
                _Logger.Log()(LogSeverityEnum.Error, "XYStage init Failed");
            }
            #endregion

            #region Init BarcodeScanner
            _BarcodeReader = new DeviceProxy("BarcodeReader", typeof(VCognexBarcode), typeof(SCognexBarcode));
            if (!CheckDeviceInit(_BarcodeReader))
            {
                _BarcodeReader = null;
                _Logger.Log()(LogSeverityEnum.Error, $"BarcodeReader init failed");
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Info, $"BarcodeReader init success");
            }

            #endregion

            #region Init Camera
            if (cameraType == CameraTypeEnum.Lumenera)
            {
                _CameraA = new DeviceProxy("Camera", typeof(VLumeneraCamera), typeof(SCamera),new object[] { 0 });
                _CameraB = new DeviceProxy("Camera", typeof(VLumeneraCamera), typeof(SCamera), new object[] { 1 });
            }
            else if (cameraType == CameraTypeEnum.PcoEdge)
            {
                _CameraA = new DeviceProxy("Camera", typeof(VPcoEdgeCamera), typeof(SCamera), new object[] { 0 });
                _CameraB = new DeviceProxy("Camera", typeof(VPcoEdgeCamera), typeof(SCamera), new object[] { 1 });
            }
            else if (cameraType == CameraTypeEnum.Andor)
            {
                _CameraA = new DeviceProxy("Camera", typeof(VAndorCamera), typeof(SCamera), new object[] { 0 });
                _CameraB = new DeviceProxy("Camera", typeof(VAndorCamera), typeof(SCamera), new object[] { 1 });
            }
            if(CheckDeviceInit(_CameraA, 0))
            {
                _Logger.Log()(LogSeverityEnum.Info, "CameraA init success");
            }
            else
            {
                _CameraA = null;
                _Logger.Log()(LogSeverityEnum.Error, "CameraA init Failed");
            }
            Thread.Sleep(1000);
            if(CheckDeviceInit(_CameraB, 1))
            {
                _Logger.Log()(LogSeverityEnum.Info, "CameraB init success");
            }
            else
            {
                _CameraB = null;
                _Logger.Log()(LogSeverityEnum.Error, "CameraB init Failed");
            }

            #endregion
            _Slide = new SlideV2();
            try
            {
                _Scanner = new ScannerV2(new ICamera[] { CameraIntA, CameraIntB }, XYStageInt, AFControl, IoBoardInt, _IoBoard.Get<ICameraControl>());
                _Scanner.SendImageInfoHandle += Scanner_SendImageHandle;
                ImageBufferExt.Get.QCDataEventHandleByLane += SendQCInfoToUIAndLimis;
            }
            catch (NullReferenceException ex)
            {
                if (CameraIntA == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"CameraA is null");
                }
                if (CameraIntB == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"CameraB is null");
                }
                if (XYStageInt == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"XYStage is null");
                }
                if (AFControl == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"AFControl is null");
                }
                if (IoBoardInt == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"IoBoard is null");
                }
                _Scanner = null;
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"Init Scanner Failed,Err:{ex.ToString()}");
                _Scanner = null;
            }
            if (_Scanner == null)
            {
                string resourceMsg = GetResourceMsg(Properties.Resource.ResourceManager.GetString("strScannerInitFailed"), new string[] { "" });
                ServiceMessageInfo msgInfo = new ServiceMessageInfo() { MessageType = ScriptMessageSeverityEnum.Error, Datetime = DateTime.UtcNow.ToLocalTime().ToString(), Message = resourceMsg };
                if (_InitMessageInfoMap.ContainsKey(KEY_FLOWCELL_A)) _InitMessageInfoMap[KEY_FLOWCELL_A].Add(msgInfo);
                else _InitMessageInfoMap.Add(KEY_FLOWCELL_A, new List<ServiceMessageInfo>() { msgInfo });
            }
            
            this.IsInitDone = true;

            if (_InitMessageInfoMap != null && _InitMessageInfoMap.Count > 0)
            {
                isInitSuccess = false;
                ConsoleColor firstColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                foreach (string key in _InitMessageInfoMap.Keys)
                {
                    List<ServiceMessageInfo> lst = _InitMessageInfoMap[key];
                    foreach (ServiceMessageInfo msg in lst)
                    {
                        //SendServiceMessage(msg);
                        Console.WriteLine(msg.Message);
                    }
                }
                Console.ForegroundColor = firstColor;
            }
            else
                isInitSuccess = true;

            Console.WriteLine("Done");
        }

        public string InstrumentName
        {
            get { return Environment.MachineName; }
        }

        public string SoftVersion
        {
            get;set;
        }

        private void Scanner_SendImageHandle(object cycle, int index, object args)
        {
            if (_ScannerExperimentInfo == null || _ScannerExperimentInfo.Count == 0) return;
            RunInfo currentInfo = null;
            string _operation = string.Empty;
            string _operationKey = string.Empty;
            string key = string.Empty;
            if (index == 0) key = KEY_FLOWCELL_A;
            else key = KEY_FLOWCELL_B;
            if (_ScannerExperimentInfo.ContainsKey(key)) currentInfo = _ScannerExperimentInfo[key];
            if (currentInfo == null) return;
            if (_OperationMaps != null && _OperationMaps.ContainsKey(key))
                _operation = _OperationMaps[key];
            if (_OperationKeyMaps != null && _OperationKeyMaps.ContainsKey(key))
                _operationKey = _OperationKeyMaps[key];
            MessageType phaseType = MessageType.None;
            if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(key)) phaseType = _PhaseTypeMaps[key];
            ImagingCycle _CurrentCycle = cycle as ImagingCycle;
            Field field = args as Field;
            if (field != null && _CurrentCycle != null)
            {
                if (_LastFieldMap == null) _LastFieldMap = new Dictionary<string, Field>();
                if (_LastFieldMap.ContainsKey(key)) _LastFieldMap[key] = field;
                else _LastFieldMap.Add(key, field);

                Sequence sequence = new Sequence();
                sequence.OperationKey = _operationKey;
                sequence.Operation = _operation;

                sequence.CurrentFovCol = field.Col;
                sequence.CurrentFovRow = field.Row;
                sequence.CurrentLane = field.Lane;
                ScanParams scanparams = _ScanParamsMap != null && _ScanParamsMap.ContainsKey(key) ? _ScanParamsMap[key] : null;
                if(scanparams != null)
                {
                    sequence.TotalLane = scanparams.ScanAreas.Count;
                    sequence.TotalColumn = scanparams.ScanAreas[field.LaneIndex].MaxColNum;//field.ColNum;
                    sequence.TotalRow = scanparams.ScanAreas[field.LaneIndex].MaxRowNum;//field.RowNum;
                }

                if (phaseType == MessageType.Sequencing || phaseType == MessageType.Sequencing_Read1)
                {
                    sequence.CurrentCycle = _CurrentCycle == 0 ? 1 : _CurrentCycle;
                    sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.Read1Len;
                }
                else if(phaseType == MessageType.Sequencing_Read2)
                {
                    sequence.CurrentCycle = (_CurrentCycle == 0 || (_CurrentCycle - currentInfo.Read1Len)<=0) ? 1 : (_CurrentCycle-currentInfo.Read1Len);
                    sequence.TotalCycle = currentInfo == null ? 0 :  currentInfo.Read2Len;
                }
                else if (phaseType == MessageType.BarcodeSequencing)
                {
                    sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.BarcodeLen;
                    sequence.CurrentCycle = (_CurrentCycle == 0 || (_CurrentCycle - currentInfo.Read1Len - currentInfo.Read2Len) <= 0) ? 1 : (_CurrentCycle - currentInfo.Read1Len - currentInfo.Read2Len);
                }

                string message = JsonConvert.SerializeObject(sequence);
                SendStageRunMessage(message, key);
            }
        }

        public void OnAtomValueEventHandle(AtomicValueEventArgs args)
        {
            lock (lockObj)
            {
                bool isUsedBase = true;
                AtomicValueEventArgs rsltArgs = args;
                try
                {
                    ServiceMessageInfo msg = null;
                    string normalTipStr = string.Empty;
                    string errorcode = string.Empty;
                    List<string> removeAlarmList = new List<string>();
                    Dictionary<int, string> dicI = new Dictionary<int, string>();
                    Dictionary<bool, string> dicB = new Dictionary<bool, string>();
                    switch (args.av.ValueName)
                    {
                        case "TemperatureBoard_SlideTemperatureStatusA":
                            dicI.Clear();
                            if (alarmIDic.Keys.Contains(args.av.ValueName))
                            {
                                dicI = alarmIDic[args.av.ValueName];
                                GetAlarmMessageByValue<int>(args.av.ValueName, args.av.ValueI, dicI, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmIDic, KEY_FLOWCELL_A,false);
                            }
                            break;
                        case "TemperatureBoard_SlideTemperatureStatusB":
                            dicI.Clear();
                            if (alarmIDic.Keys.Contains(args.av.ValueName))
                            {
                                dicI = alarmIDic[args.av.ValueName];
                                GetAlarmMessageByValue<int>(args.av.ValueName, args.av.ValueI, dicI, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmIDic, KEY_FLOWCELL_B,false);
                            }
                            break;
                        case "TemperatureBoard_MachineTemperatureStatus":
                            dicI.Clear();
                            if (alarmIDic.Keys.Contains(args.av.ValueName))
                            {
                                dicI = alarmIDic[args.av.ValueName];
                                GetAlarmMessageByValue<int>(args.av.ValueName, args.av.ValueI, dicI, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmIDic, KEY_FLOWCELL_A,true);
                            }
                            break;
                        case "TemperatureBoard_TemperatureA":
                            this._LastTemperatureA = args.av.ValueD;
                            break;
                        case "TemperatureBoard_TemperatureB":
                            this._LastTemperatureB = args.av.ValueD;
                            break;
                        case "TemperatureBoard_RefrigeratorTemperature":
                            this._LastFridgeTemperature = args.av.ValueD;
                            break;
                        case "TemperatureBoard_MachineTemperature":
                            this._LastMachineTemperature = args.av.ValueD;
                            break;
                        case "IOBoard_Status":
                            isUsedBase = false;
                            int value = args.av.ValueI;
                            bool statusSMC = GetIOBoardStatus(value, 0);
                            if (!_IsReportSMCStatus || !_IsReportAllSensor) statusSMC = true;
                            CreateAtomicValueEventArgs("IOBoard_SMCStatusA", statusSMC, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,false);
                            bool statusSMCB = GetIOBoardStatus(value, 4);
                            if (!_IsReportSMCStatus || !_IsReportAllSensor) statusSMCB = true;
                            CreateAtomicValueEventArgs("IOBoard_SMCStatusB", statusSMCB, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_B, false);
                            bool statusChip = GetIOBoardStatus(value, 8);
                            if (!_IsReportChipDoor || !_IsReportAllSensor) statusChip = true;
                            CreateAtomicValueEventArgs("IOBoard_ChipDoor", statusChip, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);
                            bool statusFridge = GetIOBoardStatus(value, 9);
                            if (!_IsReportFridgeDoor || !_IsReportAllSensor) statusFridge = true;
                            CreateAtomicValueEventArgs("IOBoard_FridgeDoor", statusFridge, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);
                            bool statusKitA = GetIOBoardStatus(value, 10);
                            if (!_IsReportReagentKit || !_IsReportAllSensor) statusKitA = false;
                            CreateAtomicValueEventArgs("IOBoard_KitStatusA", !statusKitA, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A, false);
                            bool statusKitB = GetIOBoardStatus(value, 11);
                            if (!_IsReportReagentKit || !_IsReportAllSensor) statusKitB = false;
                            CreateAtomicValueEventArgs("IOBoard_KitStatusB", !statusKitB, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_B, false);

                            bool statusVacuoA = GetIOBoardStatus(value, 12);
                            if (!_IsReportVacuoStatus || !_IsReportAllSensor) statusVacuoA = true;
                            CreateAtomicValueEventArgs("IOBoard_VacuoButtonStatusA", statusVacuoA, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A, false);

                            bool statusVacuoB = GetIOBoardStatus(value, 13);
                            if (!_IsReportVacuoStatus || !_IsReportAllSensor) statusVacuoB = true;
                            CreateAtomicValueEventArgs("IOBoard_VacuoButtonStatusB", statusVacuoB, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_B, false);

                            bool statusTop = GetIOBoardStatus(value, 14);
                            if (!_IsReportTopCover || !_IsReportAllSensor) statusTop = true;
                            CreateAtomicValueEventArgs("IOBoard_TopCover", statusTop, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);

                            bool statusLeft = GetIOBoardStatus(value, 15);
                            if (!_IsReportLeftCover || !_IsReportAllSensor) statusLeft = true;
                            CreateAtomicValueEventArgs("IOBoard_LeftSideCover", statusLeft, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);

                            bool statusRight = GetIOBoardStatus(value, 16);
                            if (!_IsReportRightCover || !_IsReportAllSensor) statusRight = true;
                            CreateAtomicValueEventArgs("IOBoard_RightSideCover", statusRight, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);

                            bool statusDnbA = GetIOBoardStatus(value, 17);
                            if (!_IsReportDNBKit || !_IsReportAllSensor) statusDnbA = false;
                            CreateAtomicValueEventArgs("IOBoard_DNBKitA", !statusDnbA, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A, false);

                            bool statusDnbB = GetIOBoardStatus(value, 18);
                            if (!_IsReportDNBKit || !_IsReportAllSensor) statusDnbB = false;
                            CreateAtomicValueEventArgs("IOBoard_DNBKitB", !statusDnbB, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_B, false);

                            bool statusLiquid = GetIOBoardStatus(value, 19);
                            if (!_IsReportLiquid || !_IsReportAllSensor) statusLiquid = true;
                            CreateAtomicValueEventArgs("IOBoard_LiquidStatus", statusLiquid, ref msg, ref removeAlarmList, ref normalTipStr, KEY_FLOWCELL_A,true);
                            break;
                        case "DiskStatus":
                            dicB.Clear();
                            if (alarmBDic.Keys.Contains(args.av.ValueName))
                            {
                                dicB = alarmBDic[args.av.ValueName];
                                GetAlarmMessageByValue<bool>(args.av.ValueName, args.av.ValueB, dicB, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmBDic,KEY_FLOWCELL_A,true);
                            }
                            break;
                        case "IsBaseCallOnline":
                            dicB.Clear();
                            if (alarmBDic.Keys.Contains(args.av.ValueName))
                            {
                                dicB = alarmBDic[args.av.ValueName];
                                GetAlarmMessageByValue<bool>(args.av.ValueName, args.av.ValueB, dicB, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmBDic, KEY_FLOWCELL_A,true);
                            }
                            break;
                        case "IsZlimsOnline":
                            dicB.Clear();
                            if (alarmBDic.Keys.Contains(args.av.ValueName))
                            {
                                dicB = alarmBDic[args.av.ValueName];
                                GetAlarmMessageByValue<bool>(args.av.ValueName, args.av.ValueB, dicB, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmBDic, KEY_FLOWCELL_A,true);
                            }
                            break;
                    }
                    if (isUsedBase)
                    {
                        if (msg != null)
                        {
                            msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
                            if (StageRunMgrMap != null)
                            {
                                var m = StageRunMgrMap.Values.FirstOrDefault(a => (a.ScriptState > ScriptStateEnum.ReadyToRun && a.ScriptState < ScriptStateEnum.Unknown));
                                if (m != null) msg.MessageType = ScriptMessageSeverityEnum.Error;
                                else msg.MessageType = ScriptMessageSeverityEnum.Warning;
                            }
                            switch (args.av.ValueName)
                            {
                                case "TemperatureBoard_SlideTemperatureStatusA":
                                case "TemperatureBoard_SlideTemperatureStatusB":
                                case "TemperatureBoard_MachineTemperatureStatus":
                                    msg.MessageType = ScriptMessageSeverityEnum.Error;
                                    break;
                                case "IsBaseCallOnline":
                                case "IsZlimsOnline":
                                    msg.MessageType = ScriptMessageSeverityEnum.Error;
                                    msg.IsPublic = true;
                                    break;
                                case "DiskStatus":
                                    msg.IsPublic = true;
                                    break;
                            }
                            if(args.av.ValueName == "TemperatureBoard_SlideTemperatureStatusB")
                            {
                                SendServiceMessage(msg,KEY_FLOWCELL_B);
                            }
                            else SendServiceMessage(msg,KEY_FLOWCELL_A);
                        }
                    }
                    
                    if (isUsedBase)
                        base.OnAtomicValueChanged(rsltArgs);
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                }
                finally
                {
                    if (isUsedBase)
                        base.OnAtomicValueChanged(rsltArgs);
                }
                
            }
        }

        public bool IsBasecallOnline()
        {
            try
            {
                if (!_IsSetBasecallOnline) return true;
                //if(MachineStatusMap.Values.Contains(MachineStatus.Idle) || MachineStatusMap.Values.Contains(MachineStatus.Idle) || MachineStatus)
                if (MachineStatusMap.Values.Contains(MachineStatus.WaitImage) ||
                    MachineStatusMap.Values.Contains(MachineStatus.Incorporation) || MachineStatusMap.Values.Contains(MachineStatus.Image) ||
                    MachineStatusMap.Values.Contains(MachineStatus.Stopping) || MachineStatusMap.Values.Contains(MachineStatus.DataHandle))
                {
                    return ImageBufferExt.Get.IsOnlineBaseCall;
                }
                else
                {
                    if (ImageBufferExt.Get.Clients != null && ImageBufferExt.Get.Clients[0] != null)
                        return ImageBufferExt.Get.Clients[0].IsOnline;
                    else return false;
                }

                //if(ImageBufferExt.Get.Clients != null && ImageBufferExt.Get.Clients.Length>0 && ImageBufferExt.Get.Clients[0] != null)
                //    return ImageBufferExt.Get.Clients[0].IsOnline;
                //return false;
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                return false;
            }
        }

        public bool IsZlimsSimulated
        {
            get
            {
                return _isZlimsSimulated;
            }
        }

        public bool IsZlimsOnline()
        {
            bool result = false;

            if (_isZlimsSimulated)
                return true;

            try
            {
                #region Check zlims running status
                //using (var scope = _container.BeginLifetimeScope())
                //{
                //    ResultInfo resultInfo = scope.Resolve<IZLimsApiService>().CheckRunningStatus();

                //    if (!resultInfo.IsSuccess)
                //    {
                //        this._Logger.Log(LogSeverityEnum.Debug, "ZLIMS API FAILURE, The CheckRunningStatus method of ZLimsApiService called failure, the failure message is " + resultInfo.ErrorMessage.ToString() + ".");
                //    }

                //    result = resultInfo.IsSuccess;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                this._Logger.Log()(LogSeverityEnum.Error, "ZLIMS API EXCEPTION, " + ex.ToString());
            }

            return result;
        }

        public bool IsImaging()
        {
            if (!_IsImaging)
            {
                return _IsScanningBarcode;
            }
            return _IsImaging;
        }

        public void ShowAlarmIndecator(LedStatus status)
        {
            try
            {
                if (status == LedStatus.Error)
                {
                    //LEDLampBeltInt?.SetLedColor(LedStatus.Error);
                    SetLedIndicatorColor(LedStatus.Error);
                    SetBuzzerByInterval(100, 50, 10);
                }
                else if (status == LedStatus.Warning)
                {

                    SetLedIndicatorColor(LedStatus.Warning);
                    //SetBuzzerByInterval(100, 200, 10);
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "show alarm led indecator error:status={0},ex={1}", status, ex);
            }
        }

        public void StartSelfCheck(DeviceCheckTypeEnum checkType, string key)
        {
            switch (checkType)
            {
                case DeviceCheckTypeEnum.InitCheck:
                    BeforeRunningCheck(key, DeviceCheckTypeEnum.BeforeRunningCheck);
                    DeviceInitCheck(KEY_FLOWCELL_A);
                    SendWriteFqStatus(KEY_FLOWCELL_A,this._IsWriteFqAsync);
                    SendWriteFqStatus(KEY_FLOWCELL_B, this._IsWriteFqAsync);
                    break;
                case DeviceCheckTypeEnum.BeforeRunningCheck:
                    BeforeRunningCheck(key, DeviceCheckTypeEnum.BeforeRunningCheck);
                    break;
                case DeviceCheckTypeEnum.OtherCheck:
                    BeforeRunningCheck(key, DeviceCheckTypeEnum.OtherCheck);
                    break;
                case DeviceCheckTypeEnum.RunningCheck:
                    if (_OperationMaps.ContainsKey(key) && _OperationKeyMaps.ContainsKey(key) && !string.IsNullOrEmpty(_OperationKeyMaps[key]))
                    {
                        SendLastMsg(key);
                    }
                    break;
                default:
                    break;
            }
            if (checkType == DeviceCheckTypeEnum.InitCheck)
            {
                StageRunMgr _stageRunMgr = StageRunMgrMap[KEY_FLOWCELL_A];
                if (_stageRunMgr.GetAlarmMessageCount() > 0)
                {
                    if (_stageRunMgr.IsExistAlarmMessageType(ScriptMessageSeverityEnum.Error))
                        LEDLampBeltInt.SetLedColor(LedStatus.Error);
                    else
                        LEDLampBeltInt.SetLedColor(LedStatus.Warning);
                }
            }

        }
        
        public void SendDataList(StageRunMgr stageRunMgr, string key,bool isEmpty = false)
        {
            string rootPath =  _ImageSavePath;
            if (!string.IsNullOrEmpty(rootPath))
            {
                DirectoryInfo dir = new DirectoryInfo(rootPath);
                DirectoryInfo[] subFolder = dir.GetDirectories();
                if (subFolder != null && subFolder.Length > 0)
                {
                    SortAsFolderCreationTime(ref subFolder);
                    List<string> folder = new List<string>();
                    for (int i = 0; i < subFolder.Length - 1; i++)
                    {
                        folder.Add(subFolder[i].Name);
                    }
                    if(!isEmpty)
                        stageRunMgr.StageService.SendDataList(folder);
                    else stageRunMgr.StageService.SendDataList(new List<string>());
                }
                else
                {   // Empty
                    stageRunMgr.StageService.SendDataList(new List<string>());
                }
            }

        }
        
        public void SetLedIndicatorColor(LedStatus status)
        {
            if(this.IsInitDone)
                LEDLampBeltInt?.SetLedColor(status);
        }

        public bool IsFastFlowcell(string key)
        {
            if(_FlowcellType != null && _FlowcellType.Length > 1)
            {
                if (key == KEY_FLOWCELL_A)
                {
                    return _FlowcellType[0];
                }
                else return _FlowcellType[1];
            }
            return false;
        }

        public override void Shutdown()
        {
            this.IsInitDone = false;
            _CameraA?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Camera A Disconnect success");
            _CameraB?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Camera B Disconnect success");
            _ReagentNeedle1?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Reagent Needle 1 Disconnect success");
            _ReagentNeedle2?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Reagent Needle 2 Disconnect success");
            _ChipTempBoard?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Temperature Board Disconnect success");
            _IoBoard?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"IO Board Disconnect success");
            try
            {
                _MainSelectorValve1?.Disconnect();
                _Logger.Log()(LogSeverityEnum.Info, $"Selector Valve 1 Disconnect success");
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Info, $"Selector Valve 1 Disconnect Failed {ex.ToString()}");
            }

            try
            {
                _MainSelectorValve2?.Disconnect();
                _Logger.Log()(LogSeverityEnum.Info, $"Selector Valve 2 Disconnect success");
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Info, $"Selector Valve 2 Disconnect Failed {ex.ToString()}");
            }
            _SyringePump1?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Syringe Pump 1 Disconnect success");
            _SyringePump2?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Syringe Pump 2 Disconnect success");
            _SyringePumpByPass1?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Syringe Pump for bypass 1 Disconnect success");
            _SyringePumpByPass2?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"Syringe Pump for bypass 2 Disconnect success");
            _XYStage?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"XYStage Disconnect success");
            _AFControl?.Disconnect();
            _Logger.Log()(LogSeverityEnum.Info, $"AFControl Disconnect success");
        }

        public void PowerControl(bool isReboot)
        {
            if(isReboot) Process.Start("shutdown.exe", "-f -r -t 0");
            else Process.Start("shutdown.exe", "-f -s -t 0");
        }

        #region BIO Operations Commands IBioOperations


        public void Reagent(string reagentName,
                            float reagentVolumn_uL,
                            float flowRate  /* uL/s */,
                            string key,
                            int postDelay_sec = 0)
        {
            _Logger.Log()(LogSeverityEnum.Debug, "Reagent() Entry: reagentName: '{0}', reagentVolume_uL: {1}, flowRate: {2}, postDelay_sec: {3}",
                reagentName, reagentVolumn_uL, flowRate, postDelay_sec);

            // Get a lock on all equiment needed to perform the action.
            // This operation needs the MainSelectionValveInt and SyringePumpInt
            // TODO this
            ISelectionValve selectorValve = null;
            ISyringePump syringePump = null;
            IDevicePowerOnOff powerDevice = null;
            if (_SelectorValveMap.ContainsKey(key) && _SyringePumpMap.ContainsKey(key) && _IOBoardMap.ContainsKey(key))
            {
                selectorValve = _SelectorValveMap[key];
                syringePump = _SyringePumpMap[key];
                powerDevice = _IOBoardMap[key] as IDevicePowerOnOff;
                if (powerDevice == null)
                {
                    throw new BGIException("Invalid Device Type");
                }
                powerDevice.PowerOnOff(DeviceType.SMCLiquid, false);
            }
            else
            {
                throw new BGIException("Invalid Selector Valve or Syringe Pump");
            }
            int pos = GetReagentPosFromName(reagentName);

            //For selection valve solution            
            selectorValve.GotoPosition(pos);

            syringePump.AspirateSafety(reagentVolumn_uL, flowRate);

            if (postDelay_sec > 0)
            {
                Thread.Sleep(postDelay_sec * 1000);
            }

            syringePump.Dispense(reagentVolumn_uL, PUMP_DISPENSE_RATE);
        }

        /// <summary>
        /// Handle flowing a specified Reagent through the Flowcell.  This method does the complex air gap and
        /// prime operations, including any delays during this operation to allow for pressure changes.
        /// 
        /// This action will not be paused by operator commands.
        /// </summary>
        /// <param name="reagentPos"></param>
        /// <param name="volume"></param>
        /// <param name="rate"></param>
        public void DoReagentFlow(string reagentName, float volume, float rate, string key)
        {
            // Get a lock on all equiment needed to perform the action.
            // This operation needs the MainSelectionValveInt and SyringePumpInt
            // TODO this
            ISelectionValve selectorValve = null;
            ISyringePump syringePump = null;
            ISyringePump syringePumpBypass = null;
            if (_SelectorValveMap.ContainsKey(key) && _SyringePumpMap.ContainsKey(key))
            {
                selectorValve = _SelectorValveMap[key];
                syringePump = _SyringePumpMap[key];
                syringePumpBypass = _SyringePumpMap[$"{key}_ByPass"];
            }
            else
            {
                throw new BGIException("Invalid Selector Valve or Syringe Pump");
            }
            // Put syringe pump at home
            syringePump.Dispense(0, PUMP_DISPENSE_RATE);

            // First put air gap into line
            int airPos = GetReagentPosFromName("AIR"); // TODO config location?
            selectorValve.GotoPosition(airPos);
            syringePumpBypass.Aspirate(10, 600, true); // TODO config value for voluem and rate

            int reagentPos = GetReagentPosFromName(reagentName);

            // Switch to reagent being flowed
            selectorValve.GotoPosition(reagentPos);

            // Now prime line up to bypass line
            syringePumpBypass.Aspirate(65, 600, true); // TODO config value for volume and rate

            // delay ?
            Thread.Sleep(2000); // TODO delay time remote config

            // Now flow reagent through flowcell
            syringePump.Aspirate(volume, rate, false);

            // Done reagent flow, now move pump valve to output to isolate flowcell
            syringePump.SwitchValve((int)SyringePumpEnums.ValvePosition3portEnum.Output);
        }

        /// <summary>
        /// Sets the Target Temperature of the FlowCell(Slide)/Chuck
        /// </summary>
        /// <param name="tempSetVal">Set temperature [°C]</param>
        /// <param name="location">Location of FlowCell(Slide)/Chuck</param>
        public void SetTemp(double tempSetVal, string chip)
        {
            //RunnerStatus[key] = "BIO";
            if (chip == KEY_FLOWCELL_A) ChipTempBoardInt.SetATargetTemperature(tempSetVal);
            else if (chip == KEY_FLOWCELL_B) ChipTempBoardInt.SetBTargetTemperature(tempSetVal);
        }


        /// <summary>
        /// Returns the current Target Temperature of the FlowCell(Slide)/Chuck
        /// </summary>
        public double GetTempSetpoint(string chip)
        {
            if (chip == KEY_FLOWCELL_A)
                return ChipTempBoardInt.GetATargetTemprature();
            return ChipTempBoardInt.GetBTargetTemprature();
        }

        /// <summary>
        /// Retrieves Current Temperature of the FlowCell(Slide)/Chuck
        /// </summary>
        /// <param name="location">Location of FlowCell(Slide)</param>
        /// <returns>Current temperature of the FlowCell(Slide)/Chuck [°C]</returns>
        /// <remarks>Zebra v0.1 has only one FC so the location is not defined, therefore always 0</remarks>
        public double GetCurrentTemp(string chip)
        {
            if (chip == KEY_FLOWCELL_A)
                return ChipTempBoardInt.GetACurrentTemperature();
            return ChipTempBoardInt.GetBCurrentTemperature();
        }

        /// <summary>
        /// Aspirate and dispense reagent using syringe pump, a negative pressure system, 
        /// which means aspirate the reagent into slide and dispense waste to waste container.
        /// </summary>
        /// <param name="pos">reagent position, using position not using reagent name seems to be better, here we don't need to know the relationships between reagent and positions.</param>
        /// <param name="volume">ul</param>
        /// <param name="aspirateRate">ul/min</param>
        /// <param name="dispenseRate">ul/min</param>
        /// <param name="delayMs">delay time between aspirate and dispense, in negative pressure system, it need time to release the pressure</param>
        public void Reagent(int pos, double volume, double aspirateRate, float dispenseRate, int delayMs, string key, bool FromBypass = false)
        {
            // Get the reagent location for volume needed.  Consume volume if volume tracking implemented.
            try
            {
                //if (_WashExceptionMap.ContainsKey(key))
                //{
                //    if (_WashExceptionMap[key] != null)
                //        throw _WashExceptionMap[key];
                //}
                ISelectionValve selectorValve = null;
                ISyringePump syringePump = null;
                ISyringePump syringePumpBypass = null;
                IDiaphragmPump powerDevice = null;
                ReagentKit reagentKit = null;
                double aRate = aspirateRate / 60;
                double dRate = dispenseRate / 60;
                int liquidSolenoidIndex = 0;
                
                if (_SelectorValveMap.ContainsKey(key) && _SyringePumpMap.ContainsKey(key) && _ReagentMap.ContainsKey(key) && _IOBoardMap.ContainsKey(key))
                {
                    selectorValve = _SelectorValveMap[key];
                    syringePump = _SyringePumpMap[key];
                    syringePumpBypass = _SyringePumpMap[$"{key}_ByPass"];
                    reagentKit = _ReagentMap[key];
                    powerDevice = (_IOBoardMap[key]) as IDiaphragmPump;
                    if (powerDevice == null)
                    {
                        //throw new BGIException("Invalid Device Type");
                        throw new BGIDeviceException((int)DeviceCodeEnum.IOBoard, (int)IOErrorEnum.DeviceTypeError, "Invalid Device Type");
                    }
                    liquidSolenoidIndex = GetLiquidSolenoid(_CurrentIOBoardType, key == KEY_FLOWCELL_A ? true : false);
                    powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                }
                else
                {
                    if (!_SelectorValveMap.ContainsKey(key))
                        throw new BGIDeviceException((int)DeviceCodeEnum.SelectorValve, (int)ValveErrorCodeEnum.UnkownDevice, "Invalid Seletor Valve");
                    if (!_SyringePumpMap.ContainsKey(key))
                        throw new BGIDeviceException((int)DeviceCodeEnum.SyringPump, (int)PumpErrCodeEnum.UnkownDevice, "Invalid Seletor Valve");
                }
                if(pos == _FinishedValvePos)
                {
                    selectorValve.GotoPosition(pos);
                }
                else
                {
                    reagentKit.Consume(pos, volume);
                    SendReagentMessage(string.Empty, pos, key);
                    selectorValve.GotoPosition(pos);
                    int aspiratePos = GetSyringePumpPos(_CurrentPumpType, FromBypass, true, key);
                    if (!FromBypass)
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, true);
                        syringePump.SwitchValve(aspiratePos);
                        syringePump.Aspirate(volume, aRate);
                    }

                    else
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                        syringePumpBypass.SwitchValve(aspiratePos);
                        syringePumpBypass.Aspirate(volume, aRate);
                    }

                    Thread.Sleep(delayMs);
                    int dispensePos = GetSyringePumpPos(_CurrentPumpType, FromBypass, false, key);
                    if (!FromBypass)
                    {
                        syringePump.SwitchValve(dispensePos);
                        syringePump.Dispense(volume, dRate);
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                    }
                    else
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                        syringePumpBypass.SwitchValve(dispensePos);
                        syringePumpBypass.Dispense(volume, dRate);
                    }
                    if (_WashExceptionMap.ContainsKey(key)) _WashExceptionMap[key] = null;
                    else _WashExceptionMap.Add(key, null);
                }
                
            }
            catch(Exception ex)
            {
                if(ex is BGIDeviceException)
                {
                    BGIDeviceException inner = ex as BGIDeviceException;
                    if (_WashExceptionMap.ContainsKey(key)) _WashExceptionMap[key] = inner;
                    else _WashExceptionMap.Add(key, inner);
                }
                throw;
            }
            

        }

        public int GetReagentPosFromName(string reagentName)
        {
            return 0;
        }

        public void Reagent(int aspiratePos, double aspirateVolume, double aspirateRate, bool aspirateFromBypass,
                            int dispensePos, double dispenseRate, bool dispenseFromBypass, int delayMs, string key)
        {
            ISelectionValve selectorValve = null;
            ISyringePump syringePump = null;
            ISyringePump syringePumpBypass = null;
            IDiaphragmPump powerDevice = null;
            double aRate = aspirateRate / 60;
            double dRate = dispenseRate / 60;
            int liquidSolenoidIndex = 0;
            try
            {
                if (_SelectorValveMap.ContainsKey(key) && _SyringePumpMap.ContainsKey(key) && _IOBoardMap.ContainsKey(key))
                {
                    selectorValve = _SelectorValveMap[key];
                    syringePump = _SyringePumpMap[key];
                    syringePumpBypass = _SyringePumpMap[$"{key}_ByPass"];
                    powerDevice = _IOBoardMap[key] as IDiaphragmPump;
                    if (powerDevice == null)
                    {
                        throw new BGIDeviceException((int)DeviceCodeEnum.IOBoard, (int)IOErrorEnum.DeviceTypeError, "Invalid Device Type");
                    }
                    liquidSolenoidIndex = GetLiquidSolenoid(_CurrentIOBoardType, key == KEY_FLOWCELL_A ? true : false);
                    powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                }
                else
                {
                    if (!_SelectorValveMap.ContainsKey(key))
                        throw new BGIDeviceException((int)DeviceCodeEnum.SelectorValve, (int)ValveErrorCodeEnum.UnkownDevice, "Invalid Seletor Valve");
                    if (!_SyringePumpMap.ContainsKey(key))
                        throw new BGIDeviceException((int)DeviceCodeEnum.SyringPump, (int)PumpErrCodeEnum.UnkownDevice, "Invalid Seletor Valve");
                }
                if (aspiratePos == _FinishedValvePos || dispensePos == _FinishedValvePos)
                {
                    selectorValve.GotoPosition(aspiratePos);
                }
                else
                {
                    selectorValve.GotoPosition(aspiratePos);
                    if (!aspirateFromBypass)
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, true);
                        syringePump.Aspirate(aspirateVolume, aspirateRate, aspirateFromBypass);

                    }
                    else
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                        syringePumpBypass.Aspirate(aspirateVolume, aspirateRate, aspirateFromBypass);
                    }
                    Thread.Sleep(delayMs);
                    selectorValve.GotoPosition(dispensePos);
                    if (dispenseFromBypass)
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                    }
                    syringePump.Dispense(0, dispenseRate, dispenseFromBypass);
                    if (!dispenseFromBypass)
                    {
                        powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                    }
                }
                
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                if (powerDevice != null)
                {
                    powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                }
                SetTemp(20, key);
                throw;
            }

        }

        public void Reagent(string reagentName, double volume, double aspirateRate, float dispenseRate, int delayMs, string key, bool FromBypass = false)
        {
            if (!_ReagentMap.ContainsKey(key))
            {
                throw new BGIException("Invalid ReagentKit");
            }
            int pos = _ReagentMap[key].GetLocationAndConsume(reagentName, volume);
            Reagent(pos, volume, aspirateRate, dispenseRate, delayMs, key, FromBypass);
        }

        public void SetBuzzerPWM(UInt16 frequency)
        {
            if (_IsSetBuzzerValue) return;
            Task.Run(() =>
            {
                _IsSetBuzzerValue = true;
                try
                {
                    BuzzerControlInt?.SetBuzzerPWM(frequency);
                    BuzzerControlInt?.PowerOnOffBuzzer(true);
                    Thread.Sleep(2000);
                    BuzzerControlInt?.PowerOnOffBuzzer(false);
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, "Set Buzzer frenquency error, " + ex.ToString());
                }
                finally
                {
                    _IsSetBuzzerValue = false;
                }
            });
        }
        public void ZLIMSIPAddrReload()
        {
            _zlimsBaseUrl = string.Format("http://{0}:{1}/", ConfigMgr.Get.GetCurValue<string>("ZLIMS", "BaseUrl"), ConfigMgr.Get.GetCurValue<string>("ZLIMS", "Port"));
        }
        #endregion

        #region IMG Operations Commands IImageOperations

        //Reset cycle number when a new script is started
        public void ResetCylceNumber(string key)
        {
            if (_ScannerCycle.ContainsKey(key))
            {
                _ScannerCycle[key] = new ImagingCycle();
            }
        }

        public void SetSampleId(string key,string sampleid)
        {
            if(_SampleCodeMap.Values.Contains(sampleid) && !string.IsNullOrEmpty(sampleid))
            {
                CreateAndSendMessage(Properties.Resource.strDuplicateSampleId, ScriptMessageSeverityEnum.Error, false, "", "", key);
                if (_SampleCodeMap.ContainsKey(key)) _SampleCodeMap.Remove(key);
                throw new BGIRemoteException($"Duplicate Sample Id {sampleid}");
            }
            else
            {
                RemoveAlarmMessage(Properties.Resource.strDuplicateSampleId, "", key, false);
            }

            if (_SampleCodeMap.ContainsKey(key)) _SampleCodeMap[key] = sampleid;
            else _SampleCodeMap.Add(key, sampleid);
        }

        public void ClearSampleId(string key)
        {
            if (_SampleCodeMap != null && _SampleCodeMap.ContainsKey(key))
            {
                _SampleCodeMap[key] = string.Empty;
            }
        }
        public void SetSlide(string key, string barcode,bool isRemove)
        {
            if (isRemove)
            {
                if (_FlowcellCodeMap.ContainsKey(key)) _FlowcellCodeMap.Remove(key);
                return;
            }
            if (_FlowcellCodeMap.Values.Contains(barcode))
            {
                CreateAndSendMessage(Properties.Resource.strDuplicateFlowcellId, ScriptMessageSeverityEnum.Error, false, "", "", key);
                if (_FlowcellCodeMap.ContainsKey(key)) _FlowcellCodeMap.Remove(key);
                throw new BGIRemoteException($"Duplicate Flowcell Id {barcode}");
            }
            else
            {
                RemoveAlarmMessage(Properties.Resource.strDuplicateFlowcellId, "", key, false);
            }
            if (!_FlowcellCodeMap.ContainsKey(key))
            {
                _FlowcellCodeMap.Add(key, barcode);
            }
            else
            {
                _FlowcellCodeMap[key] = barcode;
            }
            if(key == KEY_FLOWCELL_A)
            {
                _FlowcellType[0] = false;
                if (_FastChipFlag != null)
                {
                    foreach (string s in _FastChipFlag)
                    {
                        if (barcode.StartsWith(s, true, null))
                        {
                            _FlowcellType[0] = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                _FlowcellType[1] = false;
                if (_FastChipFlag != null)
                {
                    foreach (string s in _FastChipFlag)
                    {
                        if (barcode.StartsWith(s, true, null))
                        {
                            _FlowcellType[1] = true;
                            break;
                        }
                    }
                }
            }
        }

        public void ClearSlide(string key)
        {
            if (_FlowcellCodeMap != null && _FlowcellCodeMap.ContainsKey(key))
            {
                _FlowcellCodeMap[key] = string.Empty;
            }
        }
        public void SetReagent(string key, string barcode)
        {
            //TODO create reagent kit by barcode
            try
            {

                if (_ReagentIdMap.Values.Contains(barcode) && !string.IsNullOrEmpty(barcode))
                {
                    CreateAndSendMessage(Properties.Resource.strDuplicateReagentKitId, ScriptMessageSeverityEnum.Error, false, "", "", key);
                    if (_ReagentMap.ContainsKey(key)) _ReagentMap.Remove(key);
                    throw new BGIRemoteException($"Duplicate ReagentKit Id {barcode}");
                }
                else
                {
                    RemoveAlarmMessage(Properties.Resource.strDuplicateReagentKitId, "", key, false);
                }

                if (_ReagentIdMap.ContainsKey(key)) _ReagentIdMap[key] = barcode;
                else _ReagentIdMap.Add(key, barcode);
                //if(_ReagentMap.Values.FirstOrDefault(a=>string.Compare(a.Name,barcode,true) == 0) != null && !string.IsNullOrEmpty(barcode))
                ////if (_ReagentMap.Values.Select(a => string.Compare(a.Name, barcode, true) == 0).Count() > 0)
                //{
                //    ServiceMessageInfo msg = new ServiceMessageInfo();
                //    msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
                //    msg.MessageType = ScriptMessageSeverityEnum.Error;
                //    msg.InstructmentId = Environment.MachineName;
                //    msg.UserName = Environment.UserName;
                //    msg.FlowCellId = _Slide?.Barcode;
                //    msg.Message = Properties.Resource.strDuplicateReagentKitId;
                //    SendServiceMessage(msg, key);
                //    if (_ReagentMap.ContainsKey(key)) _ReagentMap.Remove(key);
                //    throw new BGIRemoteException($"Duplicate ReagentKit Id {barcode}");
                //}
                //else
                //{
                //    ServiceMessageInfo msg = StageRunMgrMap[key]?.RemoveAlarmMessage(Properties.Resource.strDuplicateReagentKitId);
                //    if (msg != null)
                //    {
                //        StageRunMgrMap[key]?.AddLogMessage(msg);
                //    }
                //}

                string filePath = @"C:\BGI\ReagentKey\reagentKey.json";
                ReagentKit reagentKit = new ReagentKit();
                reagentKit.LoadFromFile(filePath);
                reagentKit.Name = barcode;

                if (!_ReagentMap.ContainsKey(key))
                {
                    _ReagentMap.Add(key, reagentKit);
                }
                else
                {
                    _ReagentMap[key] = reagentKit;
                }
            }
            catch(BGIRemoteException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.Message);
            }
        }

        public void ClearReagent(string key)
        {
            if (_ReagentIdMap != null && _ReagentIdMap.ContainsKey(key))
            {
                _ReagentIdMap[key] = string.Empty;
            }
        }
        public void SetExperimentInfo(string key, RunInfo info)
        {
            info.FlowcellPosition = key == KEY_FLOWCELL_A ? "A" : "B";
            if (_ScannerExperimentInfo.ContainsKey(key))
                _ScannerExperimentInfo[key] = info;
            else _ScannerExperimentInfo.Add(key, info);
            if (_TotalTimesMap.ContainsKey(key))
            {
                _TotalTimesMap[key] = GetTotalTime(info);
            }
            else
            {
                _TotalTimesMap.Add(key, GetTotalTime(info));
            }
            _Scanner.SetExperimentInfo(info);
            if (_SetRunInfoMap == null) _SetRunInfoMap = new Dictionary<string, bool>();
            if (_SetRunInfoMap.ContainsKey(key)) _SetRunInfoMap[key] = true;
            else _SetRunInfoMap.Add(key, true);
            
        }


        /// <summary>
        /// Take field without L&T board for alpha
        /// </summary>
        /// <param name="field">the field we want to take images</param>
        /// <param name="CheckLastField">if check last field image is received. When scanning, between each field, there has a stage movement and autofocus time, we can check at this point and waste no time.</param>
        public void WaitForImagingReady(string key)
        {
            this.MachineStatusMap[key] = MachineStatus.WaitImage;
            
            _WaitImageEvent.WaitOne();
            if (_WaitImagePauseEventMap[key].WaitOne(0) == true)
            {
                _WaitImageEvent.Set();
                _WaitImageResumeEventMap[key].WaitOne();
                _WaitImagePauseEventMap[key].Reset();
                _WaitImageResumeEventMap[key].Reset();
                //_WaitImageStopEventMap[key].Reset();
                _WaitImageEvent.WaitOne();
            }
            if(_WaitImageStopEventMap[key].WaitOne(0)== true)
            {
                _WaitImageEvent.Set();
                _WaitImagePauseEventMap[key].Reset();
                _WaitImageResumeEventMap[key].Reset();
                _WaitImageStopEventMap[key].Reset();
                throw new AbortedByUserException("stop by user");
            }
            
            if ((_SequenceExceptionMap.ContainsKey(key) && _SequenceExceptionMap[key] != null))
            {
                throw _SequenceExceptionMap[key];
            }
            if(_WashExceptionMap.ContainsKey(key) && _WashExceptionMap[key] != null)
            {
                throw _WashExceptionMap[key];
            }
        }

        public void PrepareImage(string key)
        {

            if (_ScanParamsMap == null) _ScanParamsMap = new Dictionary<string, ScanParams>();
            ScanParams scanParams = null;
            if (key == KEY_FLOWCELL_A)
            {
                scanParams = new ScanParams();
                scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[0], _FlowcellType[0]);
            }
            else
            {
                scanParams = new ScanParams();
                scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[1], _FlowcellType[1]);
            }
            if (_ScanParamsMap.ContainsKey(key)) _ScanParamsMap[key] = scanParams;
            else _ScanParamsMap.Add(key, scanParams);
        }

        public void WaitDiskEnough(string key)
        {
            if (_WaitImagePauseEventMap[key].WaitOne(0) == true)
            {
                _WaitImageResumeEventMap[key].WaitOne();
                _WaitImagePauseEventMap[key].Reset();
                _WaitImageResumeEventMap[key].Reset();
            }
            if (_WaitImageStopEventMap[key].WaitOne(0) == true)
            {
                _WaitImageEvent.Set();
                _WaitImagePauseEventMap[key].Reset();
                _WaitImageResumeEventMap[key].Reset();
                _WaitImageStopEventMap[key].Reset();
                throw new AbortedByUserException("stop by user");
            }
        }

        public void Image(string key)
        {
            try
            {
                while (true)
                {
                    if (!_IsScanningBarcode) break;
                    Thread.Sleep(50);
                }
                this._IsImaging = true;

                if (!CheckDiskSpaceForCycle(key))
                {
                    string flowcellId = GetFlowcellId(key);
                    int errCode = (int)ScriptErrorEnum.DiskNotEnough;
                    if (key == KEY_FLOWCELL_B) errCode = (int)ScriptErrorEnum.DiskNotEnough + 1;
                    string errMsg = $"{Properties.Resource.strImgError}{(int)WorkFlowCodeEnum.SequenceImg}-S{(int)DeviceCodeEnum.Others}-{errCode}";
                    CreateAndSendMessage(errMsg, ScriptMessageSeverityEnum.Warning, true, flowcellId, $"{WorkFlowCodeEnum.SequenceImg}-S{DeviceCodeEnum.Others}-{errCode}", key);
                    WaitDiskEnough(key);
                    while (!CheckDiskSpaceForCycle(key))
                    {
                        StageRunMgrMap[key]?.PauseScript();
                        WaitDiskEnough(key);
                    }
                    RemoveAlarmMessage(errMsg, "", key, true);
                }
                MachineStatusMap[key] = MachineStatus.Image;
                
                if (!_WriteFQStatus.ContainsKey(key))
                {
                    _WriteFQStatus.TryAdd(key, false);
                }
                else _WriteFQStatus[key] = false;

                //Switch Original

                ImagingCycle imageCycle = _ScannerCycle[key];
                _Slide.Barcode = _FlowcellCodeMap[key];
                _Scanner.SetSlide(_Slide);

                
                if (key == KEY_FLOWCELL_A)
                {
                    _Scanner.SetOriginalPoint(_OriginalPoints[0].x, _OriginalPoints[0].y, _OriginalPoints[0].z);
                    ScanParams scanParams = new ScanParams();
                    scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[0], _FlowcellType[0]);
                    //if (_FlowcellType[0]) scanParams.ScanType = ScanDirection.MShape;
                    _Scanner.SetScanParams(scanParams);
                }
                else
                {
                    _Scanner.SetOriginalPoint(_OriginalPoints[1].x, _OriginalPoints[1].y, _OriginalPoints[1].z);
                    ScanParams scanParams = new ScanParams();
                    scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[1], _FlowcellType[1]);
                    //if (_FlowcellType[1]) scanParams.ScanType = ScanDirection.MShape;
                    _Scanner.SetScanParams(scanParams);
                }

                if (_ScanParamsMap == null) _ScanParamsMap = new Dictionary<string, ScanParams>();
                if (_ScanParamsMap.ContainsKey(key)) _ScanParamsMap[key] = _Scanner.ScanParams;
                else _ScanParamsMap.Add(key, _Scanner.ScanParams);

                
                _Scanner.CreateThumbNail();
                if (_ScannerExperimentInfo.ContainsKey(key))
                {
                    RunInfo runInfo = _ScannerExperimentInfo[key];
                    _Scanner.SetExperimentInfo(runInfo);
                    if (_UploadImgFileMap == null) _UploadImgFileMap = new ConcurrentDictionary<string, string>();

                    if (!_UploadImgFileMap.ContainsKey(key))
                    {
                        string tempPath = _FlowcellCodeMap[key];
                        if (runInfo.StartSequenceDateTime.HasValue)
                        {
                            tempPath = $"{runInfo.StartSequenceDateTime.Value.ToString("yyyyMMdd")}_{Environment.MachineName}_{runInfo.FlowcellPosition}_{tempPath}";
                        }
                        _UploadImgFileMap.TryAdd(key, $@"{Path.Combine(_ImageSavePath, tempPath)}");
                    }
                }
                    
                _Scanner.CycleNumber = imageCycle;
                _Scanner.IncorporationImage();
                _ThetaMap[key] = _Scanner.Theta;
                _ScannerCycle[key] = _Scanner.CycleNumber;
                if (_SequenceExceptionMap.ContainsKey(key))
                    _SequenceExceptionMap[key] = null;
                else
                {
                    _SequenceExceptionMap.Add(key, null);
                }
                _WaitImageEvent.Set();
                this._IsImaging = false;
            }
            catch(Exception ex)
            {
                if(ex is BGIDeviceException)
                {
                    BGIDeviceException inner = ex as BGIDeviceException;
                    if(inner.DeviceCode > 0)
                    {
                        if (_SequenceExceptionMap.ContainsKey(key)) _SequenceExceptionMap[key] = inner;
                        else _SequenceExceptionMap.Add(key, inner);
                    }
                }
                throw;
            }
            //catch(SoftwareBugException ex)
            //{
            //    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
            //    ServiceMessageInfo msg = new ServiceMessageInfo();
            //    msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            //    msg.MessageType = ScriptMessageSeverityEnum.Error;
            //    msg.Message = Properties.Resource.strImageAlgorithmFailed;
            //    SendServiceMessage(msg, key);
            //    if(StageRunMgrMap != null && StageRunMgrMap.ContainsKey(key))
            //    {
            //        MachineStatusMap[key] = MachineStatus.Stopping;
            //        StageRunMgrMap[key].StopScript();
            //    }
            //}
            //catch(NetworkOrServerConnectionException ex)
            //{
            //    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
            //    ServiceMessageInfo msg = new ServiceMessageInfo();
            //    msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            //    msg.MessageType = ScriptMessageSeverityEnum.Error;
            //    msg.Message = Properties.Resource.strBufferTimeout;
            //    SendServiceMessage(msg, key);
            //    if (StageRunMgrMap != null && StageRunMgrMap.ContainsKey(key))
            //    {
            //        MachineStatusMap[key] = MachineStatus.Stopping;
            //        StageRunMgrMap[key].StopScript();
            //    }
            //}
            //catch(AbortedByUserException ex)
            //{
            //    if (StageRunMgrMap != null && StageRunMgrMap.ContainsKey(key))
            //    {
            //        _Logger.Log()(LogSeverityEnum.Info, ex.Message);
            //        MachineStatusMap[key] = MachineStatus.Stopping;
            //        StageRunMgrMap[key].StopScript();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _WaitImageEvent.Set();
            //    this.isFullError = true; 
            //    this._IsImaging = false;

            //    ServiceMessageInfo msg = new ServiceMessageInfo();
            //    msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            //    msg.MessageType = ScriptMessageSeverityEnum.Error;
            //    msg.Message = Properties.Resource.strImgError;
            //    SendServiceMessage(msg, key);

            //    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
            //    if(StageRunMgrMap != null)
            //    {
            //        foreach(StageRunMgr srm in StageRunMgrMap.Values)
            //        {
            //            if(srm.CurrentScriptType.HasValue && srm.CurrentScriptType.Value == ScriptTypeEnum.WF)
            //            {
            //                MachineStatusMap[srm.StageRunName] = MachineStatus.Stopping;
            //                srm.StopScript();
            //            }
                            
            //        }
            //    }
            //    throw new BGIException($"Image with error {ex.ToString()}");
            //}
        }

        public void ResetImageEvent()
        {
            _WaitImageEvent.Set();
            this._IsImaging = false;
        }

        public void PauseImageWait(string key)
        {
            _WaitImagePauseEventMap[key].Set();
        }

        public void ResumeImageWait(string key)
        {
            _WaitImageResumeEventMap[key].Set();
        }

        public void StopImageWait(string key)
        {
            _WaitImageResumeEventMap[key].Set();
            _WaitImageStopEventMap[key].Set();
        }

        public void CleavageImage(string key)
        {
            try
            {
                this._IsImaging = true;
                _Slide.Barcode = _FlowcellCodeMap[key];
                _Scanner.SetSlide(_Slide);
                if (key == KEY_FLOWCELL_A)
                    _Scanner.SetOriginalPoint(_OriginalPoints[0].x,_OriginalPoints[0].y,_OriginalPoints[0].z);
                else _Scanner.SetOriginalPoint(_OriginalPoints[1].x, _OriginalPoints[1].y, _OriginalPoints[1].z);
                _Scanner.CleavageImage();
                _WaitImageEvent.Set();
                this._IsImaging = false;
            }
            catch (Exception ex)
            {
                _WaitImageEvent.Set();
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                this._IsImaging = false;
                if (StageRunMgrMap != null)
                {
                    foreach (StageRunMgr srm in StageRunMgrMap.Values)
                    {
                        srm.StopScript();
                    }
                }
                throw new BGIException($"CleavageImage with error {ex.ToString()}");
            }

        }

        public void BypassWash(string key, double volume)
        {

        }

        public void Registration()
        {
            // TODO: registration code goes here
        }

        public void Location(int retical)
        {
            _Scanner.Location(retical);
        }

        public void ThetaCorrection()
        {
            _Scanner.ThetaCorrection();
        }

        public void XAxisCalibration()
        {
            double factor, angle;
            _Scanner.XAxisCalibration(out factor, out angle);
        }

        public void YAxisCalibration()
        {
            double factor, angle;
            _Scanner.YAxisCalibration(out factor, out angle);
        }

        public void XYAxisCalibration()
        {
            double xFactor, yFactor, orthogonality;
            _Scanner.XYAxisCalibration(out xFactor, out yFactor, out orthogonality);
        }

        public Field GoToField(int lane, int row, int col)
        {
            return _Scanner.GoToField(lane, row, col);
        }

        public void AutoFocusSweep(Field field, int count, double step, out IDictionary<string, object> curve)
        {
            _Scanner.AutoFocusSweep(field, count, step, out curve);
        }

        public void TakePicture(Field field)
        {
            _Scanner.TakePicture(field);
        }

        public void SetLaserPower(LaserColor color, float power)
        {
            IoBoardInt.SetLaserPower(color, power);
        }

        public void OpenLaser(LaserColor color, bool isOpen)
        {
            IoBoardInt.OpenLaser(color, isOpen);
        }


        #endregion

        #region Other Operations

        public void SetRunInfo(RunInfo runInfo)
        {
            _RunInfo = runInfo;
        }

        public void ReagentNeedleUp(string key)
        {
            string errMsg = string.Empty;
            if (_ReagentNeedleMap != null && _ReagentNeedleMap.ContainsKey(key))
            {
                try
                {
                    _ReagentNeedleMap[key].Up();
                    string alarmMsg = String.Format(Properties.Resource.strReagentDownError, key == KEY_FLOWCELL_A ? "A" : "B");
                    string alarmMsgUp = String.Format(Properties.Resource.strReagentUpError, key == KEY_FLOWCELL_A ? "A" : "B");
                    string normalMeg = String.Format(Properties.Resource.strReagentNormal, key == KEY_FLOWCELL_A ? "A" : "B");
                    RemoveAlarmMessage(alarmMsg, normalMeg, key,false);
                    RemoveAlarmMessage(alarmMsgUp, normalMeg, key,false);
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"needle up faild {ex.ToString()}");
                    errMsg = String.Format(Properties.Resource.strReagentUpError, key == KEY_FLOWCELL_A ? "A" : "B");
                }
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Invalid Reagent Needle For Move Up");
                errMsg = string.Format(Properties.Resource.strDetectNeedleNotFound, key == KEY_FLOWCELL_A ? "A" : "B");
            }
            if (!string.IsNullOrEmpty(errMsg))
            {
                string flowcellId = GetFlowcellId(key);
                CreateAndSendMessage(errMsg, ScriptMessageSeverityEnum.Error, false, flowcellId, "", key);
                throw new BGIException(errMsg);
            }
        }

        public void ReagentNeedleDown(string key)
        {
            string errMsg = string.Empty;
            if (_ReagentNeedleMap != null && _ReagentNeedleMap.ContainsKey(key))
            {
                try
                {
                    _ReagentNeedleMap[key].Down();
                    string alarmMsg = String.Format(Properties.Resource.strReagentDownError, key == KEY_FLOWCELL_A ? "A" : "B");
                    string alarmMsgUp = String.Format(Properties.Resource.strReagentUpError, key == KEY_FLOWCELL_A ? "A" : "B");
                    string normalMeg = String.Format(Properties.Resource.strReagentNormal, key == KEY_FLOWCELL_A ? "A" : "B");
                    RemoveAlarmMessage(alarmMsg, normalMeg, key,false);
                    RemoveAlarmMessage(alarmMsgUp, normalMeg, key,false);
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"needle down faild {ex.ToString()}");
                    errMsg = String.Format(Properties.Resource.strReagentDownError, key == KEY_FLOWCELL_A ? "A" : "B");
                }
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Invalid Reagent Needle For Move Down");
                errMsg = string.Format(Properties.Resource.strDetectNeedleNotFound, key == KEY_FLOWCELL_A ? "A" : "B");
            }
            if (!string.IsNullOrEmpty(errMsg))
            {
                string flowcellId = GetFlowcellId(key);
                CreateAndSendMessage(errMsg, ScriptMessageSeverityEnum.Error, false, flowcellId, "", key);
                throw new BGIException(errMsg);
            }
        }

        public void SetActionName(string actionName)
        {
            throw new NotImplementedException();
        }

        public int GetCurrentPositionNumber(string key)
        {
            // TODO is this right?
            if (_ScannerCycle != null && _ScannerCycle.ContainsKey(key))
                return _ScannerCycle[key]+1;
            return 1;
        }

        public void SetCurrentPositionNumber(int currPos)
        {
            // TODO don't know how to do this with the ImagingCycle object.
            // I don't really see the value of this ImagingCycle yet, because it is simply an integer.
            // Also, I am not using this convention in V8 yet.  Will consider later.
            // Feel free to use this or not.
            //_Scanner.CycleNumber = currPos;
        }


        #endregion

        #region SBC Commands


        #endregion

        #region for engineer script IEngineerOperations

        /// <summary>
        /// Exposred all hardware instance to engineer scirpt.
        /// If encounter null element, drop it or not handle it. 
        /// </summary>
        /// <returns>If some device object is null ,then the value is new object instance.</returns>
        public IReadOnlyDictionary<string, object> EnumeratePluginHardware(string chipKey)
        {
            try
            {
                var t = new Dictionary<string, object>();
                t.Add("TemperatureBoard", ChipTempBoardInt);
                t.Add("Slide", _Slide);
                t.Add("Scanner", _Scanner);
                t.Add("Stage", XYStageInt);
                t.Add("AF", AFControl);
                t.Add("Reader", BarcodeReaderInt);
                t.Add("IOBoard", IoBoardInt);
                t.Add("CameraA", CameraIntA);
                t.Add("CameraB", CameraIntB);
                if (chipKey == KEY_FLOWCELL_A)
                {
                    t.Add("SelectorValve", MainSelectorValve1Int);
                    t.Add("Needle", ReagentNeedle1Int);
                    t.Add("SyringePump", SyringePump1Int);
                    t.Add("SyringePumpBypass", SyringePumpByPass1Int);
                }
                else if(chipKey == KEY_FLOWCELL_B)
                {
                    t.Add("SelectorValve", MainSelectorValve2Int);
                    t.Add("Needle", ReagentNeedle2Int);
                    t.Add("SyringePump", SyringePump2Int);
                    t.Add("SyringePumpBypass", SyringePumpByPass2Int);
                }
                return t;
            }
            catch (Exception ex)
            {
                _Logger.Log(LogSeverityEnum.Error, "Can't enumerate plugin hardware to engineer scriot" + ex.Message);
                throw ex;
            }
        }

        public void StartRecord()
        {
            CameraIntA.StartRecord();
        }

        public void StopRecord()
        {
            CameraIntA.StopRecord();
        }


        public TimeSpan Incubation(string key, TimeSpan span)
        {
            return _IncubatorMap[key].Incubation(span);
        }

        
        public void FinishScript(string chipKey, string scriptType)
        {
            if (string.IsNullOrEmpty(scriptType) || string.IsNullOrWhiteSpace(scriptType))
            {
                _Logger.Log()(LogSeverityEnum.Error, $"step key is empty");
                return;
            }
            int[] operationArray = SplitOperationKey(scriptType);
            if (operationArray == null || operationArray.Length != 3)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"step key has invalid length {scriptType}");
                return;
            }

            if (_OperationArrayMaps != null && _OperationArrayMaps.ContainsKey(chipKey))
                _OperationArrayMaps[chipKey] = operationArray;
            else _OperationArrayMaps.Add(chipKey, operationArray);
            MessageType operationType = (MessageType)operationArray[0];
            CaculateRemainingTime(scriptType, operationType, chipKey);
            string _operationKey = string.Empty;
            if (_OperationKeyMaps != null && _OperationKeyMaps.ContainsKey(chipKey))
                _operationKey = _OperationKeyMaps[chipKey];
            if (operationArray[1] == 0 && operationArray[2] == 0)
            {
                if (_IsWriteFqAsync)
                {
                    _WriteFQStatus[chipKey] = true;
                    SendWriteFqStatus(chipKey, this._IsWriteFqAsync);
                    SendInfoAlarm(Properties.Resource.strDataHandling, "", chipKey, false);
                    if (!_IsWriteFQ)
                    {
                        _IsWriteFQ = true;
                        Task.Run(() =>
                        {
                            WriteFQAysn();
                        });

                    }
                }
                else
                {
                    _WriteFQStatus[chipKey] = true;
                    SendWriteFqStatus(chipKey, this._IsWriteFqAsync);
                    SendInfoAlarm(Properties.Resource.strDataHandling, "", chipKey, false);
                    if (_WriteFqTask == null || _WriteFqTask.Status != TaskStatus.Running)
                    {
                        if (_WriteFqTask != null)
                        {
                            _Logger.Log()(LogSeverityEnum.Info, $"Task Status {_WriteFqTask.Status}");
                        }
                        _WriteFqTask = Task.Run(() =>
                        {
                            while (true)
                            {
                                if (_WriteFQStatus.Values.Contains(false))
                                {
                                    Thread.Sleep(500);
                                    continue;
                                }
                                break;
                            }
                            if (!_WriteFQStatus.Values.Contains(false))
                            {
                                _Logger.Log()(LogSeverityEnum.Info, $"all slide image done,should write fq");
                                lock (_fqLock)
                                {
                                    //SendWriteFqStatus(KEY_FLOWCELL_A);
                                    //SendWriteFqStatus(KEY_FLOWCELL_B);
                                    foreach (string key in _FlowcellCodeMap.Keys)
                                    {
                                        try
                                        {
                                            string slideId = _FlowcellCodeMap[key];
                                            _Logger.Log()(LogSeverityEnum.Info, $"Start Fq for Slide {slideId}");
                                            WriteFQ(key);
                                            _WriteFQStatus[key] = false;
                                            //SendInfoAlarm(Properties.Resource.strDataHandled, "", key, false);
                                            RemoveInfoAlarm(Properties.Resource.strDataHandling, Properties.Resource.strDataHandled, "", key, false);
                                            _Logger.Log()(LogSeverityEnum.Info, $"Finished Fq for Slide {slideId}");

                                        }
                                        catch (Exception ex)
                                        {
                                            _Logger.Log()(LogSeverityEnum.Error, $"WriteFq with error {ex.ToString()}");
                                        }
                                        finally
                                        {
                                            _WriteFQStatus[key] = false;
                                        }
                                    }
                                    #region Start Upload Fq
                                    try
                                    {
                                        if (_IsUploadFq) UploadFq();
                                    }
                                    catch(Exception ex)
                                    {
                                        _Logger.Log()(LogSeverityEnum.Error, $"Upload Fq with error {ex.ToString()}");
                                    }
                                    #endregion

                                    #region Start Upload Image
                                    try
                                    {
                                        if (_IsUploadImg) UploadImage();
                                    }
                                    catch (Exception ex)
                                    {
                                        _Logger.Log()(LogSeverityEnum.Error, $"Upload Image with error {ex.ToString()}");
                                    }
                                    #endregion
                                    foreach (string key in _WriteFQStatus.Keys)
                                    {
                                        try
                                        {
                                            SendWriteFqStatus(key, this._IsWriteFqAsync);
                                            if (_FlowcellCodeMap.ContainsKey(key))
                                                _FlowcellCodeMap[key] = string.Empty;
                                        }
                                        catch (Exception ex)
                                        {
                                            _Logger.Log()(LogSeverityEnum.Error, $"Send Finished with error {ex.ToString()}");
                                        }

                                    }
                                    _WriteFQStatus.Clear();
                                }
                            }
                        });
                    }
                }

                foreach (string key in _FlowcellCodeMap.Keys)
                {
                    if (key != chipKey) continue;
                    Sequence sequence = new Sequence();
                    sequence.OperationKey = _operationKey;
                    sequence.IsFinished = true;
                    RunInfo runInfo = _ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(key) ?_ScannerExperimentInfo[key]:null;
                    if(runInfo != null)
                    {
                        if (runInfo.BarcodeLen > 0)
                        {
                            sequence.CurrentCycle = runInfo.BarcodeLen;
                            sequence.TotalCycle = runInfo.BarcodeLen;
                        }
                        else if(runInfo.Read2Len > 0)
                        {
                            sequence.CurrentCycle = runInfo.Read2Len;
                            sequence.TotalCycle = runInfo.Read2Len;
                        }
                        else if (runInfo.Read1Len > 0)
                        {
                            sequence.CurrentCycle = runInfo.Read1Len;
                            sequence.TotalCycle = runInfo.Read1Len;
                        }
                        
                        sequence.CurrentFovCol = 0;
                        if(_LastFieldMap != null && _LastFieldMap.ContainsKey(key))
                        {
                            Field field = _LastFieldMap[key];
                            if(field != null)
                            {
                                sequence.CurrentFovCol = field.Col;
                                sequence.CurrentFovRow = field.Row;
                                sequence.CurrentLane = field.Lane;

                                ScanParams scanparams = _ScanParamsMap != null && _ScanParamsMap.ContainsKey(key) ? _ScanParamsMap[key] : null;
                                if (scanparams != null)
                                {
                                    sequence.TotalLane = scanparams.ScanAreas.Count;
                                    sequence.TotalColumn = scanparams.ScanAreas[field.LaneIndex].MaxColNum;//field.ColNum;
                                    sequence.TotalRow = scanparams.ScanAreas[field.LaneIndex].MaxRowNum;//field.RowNum;
                                }
                            }
                        }
                    }
                    //sequence.CurrentCycle = _ScannerExperimentInfo[key]
                    string message = JsonConvert.SerializeObject(sequence);
                    SendStageRunMessage(message, key);
                    if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(key))
                        _PhaseTypeMaps[chipKey] = MessageType.None;
                    else _PhaseTypeMaps.Add(key, MessageType.None);

                    if (CurrentMessageStatus.ContainsKey(key)) CurrentMessageStatus[key] = MessageType.None;
                    else
                    {
                        CurrentMessageStatus.Add(key, MessageType.None);
                    }
                }
                
            }
            if (operationType == MessageType.Wash || operationType == MessageType.WashNaOH || operationType == MessageType.Wash_Cleantubing || operationType == MessageType.Cleantubing)
            {
                Wash wash = new Wash();
                wash.IsFinished = true;
                string mesageWash = JsonConvert.SerializeObject(wash);
                SendStageRunMessage(mesageWash, chipKey);
                if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(chipKey))
                    _PhaseTypeMaps[chipKey] = MessageType.None;
                else _PhaseTypeMaps.Add(chipKey, MessageType.None);
                if (CurrentMessageStatus.ContainsKey(chipKey)) CurrentMessageStatus[chipKey] = MessageType.None;
                else
                {
                    CurrentMessageStatus.Add(chipKey, MessageType.None);
                }
            }
        }
        
        public void SetExperimentTypePhaseAndOperation(string chipKey, string operation, string stepKey = "")
        {
            try
            {
                if (string.IsNullOrEmpty(stepKey) || string.IsNullOrWhiteSpace(stepKey))
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"step key is empty");
                    return;
                }
                int[] operationArray = SplitOperationKey(stepKey);
                if (operationArray == null)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"step key has invalid length {stepKey}");
                    return;
                }
                try
                {
                    if (_OperationKeyMaps.ContainsKey(chipKey))
                        _OperationKeyMaps[chipKey] = stepKey;
                    else _OperationKeyMaps.Add(chipKey, stepKey);
                    if (_OperationMaps.ContainsKey(chipKey))
                        _OperationMaps[chipKey] = operation;
                    else _OperationMaps.Add(chipKey, operation);
                    if (_OperationArrayMaps != null && _OperationArrayMaps.ContainsKey(chipKey))
                        _OperationArrayMaps[chipKey] = operationArray;
                    else _OperationArrayMaps.Add(chipKey, operationArray);
                    MessageType phaseType = (MessageType)operationArray[0];
                    if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(chipKey)) _PhaseTypeMaps[chipKey] = phaseType;
                    else _PhaseTypeMaps.Add(chipKey, phaseType);

                    if (!_IsSendTotalTime.ContainsKey(chipKey))
                    {
                        _IsSendTotalTime.Add(chipKey, null);
                    }
                    SendTotalTime(stepKey, chipKey);
                    _IsSendTotalTime[chipKey] = true;
                    //set machine status
                    SetMachineStatusByPhaseType(phaseType, chipKey);
                    if (!((phaseType == MessageType.Sequencing || phaseType == MessageType.BarcodeSequencing || 
                        phaseType == MessageType.Sequencing_Read1 || phaseType == MessageType.Sequencing_Read2)
                        && operationArray != null && operationArray[1] == 2))
                    {
                        SendReagentMessage(string.Empty, 0, chipKey);
                    }
                    else if((phaseType == MessageType.Sequencing || phaseType == MessageType.Sequencing_Read1 ||
                        phaseType == MessageType.Sequencing_Read2 || phaseType == MessageType.BarcodeSequencing) && 
                        operationArray != null && operationArray[1] == 2)
                    {
                        RunInfo currentInfo = null;
                        ImagingCycle _CurrentCycle = null;

                        if (_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(chipKey)) currentInfo = _ScannerExperimentInfo[chipKey];
                        if (currentInfo == null) return;
                        if (_ScannerCycle != null && _ScannerCycle.ContainsKey(chipKey)) _CurrentCycle = _ScannerCycle[chipKey];
                        if (_CurrentCycle == null) return;
                        Sequence sequence = new Sequence();
                        sequence.OperationKey = stepKey;
                        sequence.Operation = operation;

                        sequence.CurrentFovCol = 1;
                        sequence.CurrentFovRow = 1;
                        sequence.CurrentLane = 1;

                        ScanParams scanparams = _ScanParamsMap != null && _ScanParamsMap.ContainsKey(chipKey) ? _ScanParamsMap[chipKey] : null;
                        if(scanparams != null)
                        {
                            sequence.TotalLane = scanparams.ScanAreas.Count;
                            sequence.TotalColumn = scanparams.ScanAreas[0].MaxColNum;//field.ColNum;
                            sequence.TotalRow = scanparams.ScanAreas[0].MaxRowNum;//field.RowNum;
                        }
                        else
                        {
                            sequence.TotalLane = 4;
                            sequence.TotalColumn = 6;
                            sequence.TotalRow = 72;
                        }

                        if (phaseType == MessageType.Sequencing || phaseType == MessageType.Sequencing_Read1)
                        {
                            sequence.CurrentCycle = _CurrentCycle == 0 ? 1 : _CurrentCycle + 1;
                            //sequence.CurrentCycle += 1;
                            sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.Read1Len;
                            
                        }
                        else if (phaseType == MessageType.Sequencing_Read2)
                        {
                            sequence.CurrentCycle = (_CurrentCycle == 0 || (_CurrentCycle - currentInfo.Read1Len) <= 0) ? 1 : (_CurrentCycle - currentInfo.Read1Len) + 1;
                            //sequence.CurrentCycle += 1;
                            sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.Read2Len;
                        }
                        else if (phaseType == MessageType.BarcodeSequencing)
                        {
                            sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.BarcodeLen;
                            sequence.CurrentCycle = (_CurrentCycle == 0 || (_CurrentCycle - currentInfo.Read1Len - currentInfo.Read2Len) <= 0) ? 1 : (_CurrentCycle - currentInfo.Read1Len - currentInfo.Read2Len) + 1;
                            //sequence.CurrentCycle += 1;
                        }
                        if (sequence.CurrentCycle > sequence.TotalCycle)
                        {
                            _Logger.Log()(LogSeverityEnum.Warning, $"Invalid CurretyCyle {sequence.CurrentCycle} For Wait");
                            sequence.CurrentCycle = sequence.TotalCycle;
                        }
                        string message = JsonConvert.SerializeObject(sequence);
                        SendStageRunMessage(message, chipKey);
                    } 
                    if (CurrentMessageStatus.ContainsKey(chipKey)) CurrentMessageStatus[chipKey] = phaseType;
                    else
                    {
                        CurrentMessageStatus.Add(chipKey, phaseType);
                    }
                    
                    //Create limis task 
                    //SendCreateTaskToLimis(phaseType);
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, $"Set experiment type and phase error,operation={operation},stepKey={stepKey},ex={ex}");
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, Str.SafeFmt("Set experiment type and phase error,operation={0},stepKey={1},ex={2}", operation, stepKey, ex));
            }
        }

        public void Reagent(bool fromByAir, string key)
        {
            if (fromByAir)
            {
                if (_ReagentNeedleMap.ContainsKey(key))
                {
                    //_ReagentNeedleMap[key].Up();
                    ReagentNeedleUp(key);
                }
            }
            else
            {
                if (_ReagentNeedleMap.ContainsKey(key))
                {
                    //_ReagentNeedleMap[key].Down();
                    ReagentNeedleDown(key);
                }
            }
        }

        public string[] GetSequenceType(string chipKey)
        {
            var types = new List<string>();
            try
            {
                var ScriptPath = @"C:\BGI\Scripts";
                DirectoryInfo dir = new DirectoryInfo(ScriptPath);
                var subdirs = dir.GetDirectories();
                foreach (var subdir in subdirs)
                {
                    if (subdir.Name == "common" || subdir.Name == ".vscode")
                        continue;
                    var files = subdir.GetFiles();
                    if (files.Any(p1 => p1.Name.Equals("bio.py", StringComparison.OrdinalIgnoreCase))
                    && files.Any(p2 => p2.Name.Equals("img.py", StringComparison.OrdinalIgnoreCase))
                    && files.Any(p3 => p3.Name.Equals("workflow.py", StringComparison.OrdinalIgnoreCase)))
                    {
                        types.Add(subdir.Name);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                string sampleId = "";
                _Logger.Log()(LogSeverityEnum.Error, "sampleId:{0},GetSequenceTypes Error,{1} ", sampleId, ex.ToString());
            }
            return types.ToArray();
        }

        public void WaitForFluidicsAvailable()
        {
            throw new NotImplementedException();
        }

        public void WaitForFluidicsReady()
        {
            throw new NotImplementedException();
        }

        public void WaitForImagingAvailable()
        {
            throw new NotImplementedException();
        }

        public object GetDeviceObject(string objName)
        {
            throw new NotImplementedException();
        }

        public string SendBarcode(string key)
        {
            this._IsScanningBarcode = true;

            base.OnAtomicValueChanged(new AtomicValueEventArgs(new AtomicValue() { ValueName = "IsImaging", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = true }));

            try
            {
                if (BarcodeReaderInt != null)
                {
                    if (key == KEY_FLOWCELL_A)
                    {
                        if (!_BarcodeReader.IsSimulated)
                            XYStageInt?.MoveXY(_barcodeReaderPosX[0], _barcodeReaderPosY[0]);
                    }
                    else
                    {
                        if (!_BarcodeReader.IsSimulated)
                            XYStageInt?.MoveXY(_barcodeReaderPosX[1], _barcodeReaderPosY[1]);
                    }
                    string rslt = BarcodeReaderInt.Scan(this._barcodeReaderScanTime);
                    XYStageInt?.MoveXY(0, 0);
                    this._IsScanningBarcode = false;
                    return rslt;
                }
            }
            catch(Exception ex)
            {
                this._IsScanningBarcode = false;
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
            }
            finally
            {
                this._IsScanningBarcode = false;
                base.OnAtomicValueChanged(new AtomicValueEventArgs(new AtomicValue() { ValueName = "IsImaging", ValueType = AtomicValueTypeEnum.TypeBool, ValueB = false }));
            }
            return string.Empty;
        }

        public void SetCameraExposureTime(float exposureTime)
        {
            if(exposureTime <= 0)
            {
                CameraIntA?.SetExposureTime((float)CameraIntA?.InitialExposureTime_ms);
                CameraIntB?.SetExposureTime((float)CameraIntB?.InitialExposureTime_ms);
            }
            else
            {
                CameraIntA?.SetExposureTime(exposureTime);
                CameraIntB?.SetExposureTime(exposureTime);
            }
            
        }

        public void SetTempAndWait(double tempSetVal)
        {
            throw new NotImplementedException();
        }

        public void WaitForTemp()
        {
            throw new NotImplementedException();
        }

        public RPC.LoginedInfo Authenicate(string userName, string password)
        {
            string user_role = string.Empty;

            try
            {
                bool isCheckSuccess = false;
                #region Authenticate
                if (this._isZlimsSimulated)
                {
                    if (userName.Equals(this._ProductUser) && password.Equals(this._ProductPwd))
                    {
                        user_role = "porduction_user";
                        isCheckSuccess = true;
                    }
                    else if (userName.Equals(this._ResearchUser) && password.Equals(this._ResearchPwd))
                    {
                        user_role = "research_user";
                        isCheckSuccess = true;
                    }
                    else if (userName.Equals("admin") && password.Equals("password"))
                    {
                        user_role = "OnlyExitScreen";
                        isCheckSuccess = true;
                    }
                    else if(userName.Equals(this._GuestUser) && password.Equals(this._GuestUserPwd))
                    {
                        user_role = "guest_user";
                        isCheckSuccess = true;
                    }
                }

                CurrentUser = userName;
                CurrentUserPwd = password;
                CurrentUserRole = user_role;

                return new RPC.LoginedInfo()
                {
                    IsSuccess = isCheckSuccess,
                    // The UserRole may be "porduction_user"/"research_user"
                    UserRole = user_role
                };
                #endregion
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                
                return new LoginedInfo() { IsSuccess = false, UserRole = string.Empty };
            }
        }
        #endregion


        #region Process Metrics
        public void StartRecordMetric(string key)
        {
            if (_FlowcellCodeMap != null && _FlowcellCodeMap.ContainsKey(key))
            {
                if (_StopRecordMap.ContainsKey(key)) _StopRecordMap[key] = true;
                else _StopRecordMap.Add(key, true);
                Task.Run(() =>
                {
                    while (_StopRecordMap[key])
                    {
                        bool isFlowcellA = false;
                        if (key == KEY_FLOWCELL_A) isFlowcellA = true;
                        if (!_FlowcellCodeMap.ContainsKey(key) || string.IsNullOrEmpty(_FlowcellCodeMap[key]))
                            break;
                        if (_SetRunInfoMap == null || !_SetRunInfoMap.ContainsKey(key) || !_SetRunInfoMap[key]) continue;
                        if (_ScannerExperimentInfo == null || !_ScannerExperimentInfo.ContainsKey(key) || _ScannerExperimentInfo[key] == null) continue;
                        RunInfo runInf = _ScannerExperimentInfo[key];
                        string slideId = _FlowcellCodeMap[key];
                        if (runInf.StartSequenceDateTime.HasValue)
                            slideId = $"{runInf.StartSequenceDateTime.Value.ToLocalTime().ToString("yyyyMMdd")}_{Environment.MachineName}_{runInf.FlowcellPosition}_{_FlowcellCodeMap[key]}";
                        string filePath = string.Format(@"{0}{1}\Metrics\", ImageCatalogs.SAVE_DATA_ROOT, slideId);
                        string fileName = "Temperature.csv";
                        string fullFilePath = Str.SafeFmt("{0}{1}", filePath, fileName);
                        var csv = new SmartCsvOutputFile(fileName);
                        csv.AutoFlush = true;

                        if (File.Exists(fullFilePath))
                        {
                            csv.OpenForAppend(filePath, fileName);
                        }
                        else
                        {
                            csv.Open(filePath, fileName);
                            csv.AddHeaderField("DateTime");
                            csv.AddHeaderField("Phase");
                            csv.AddHeaderField("Cycle");
                            csv.AddHeaderField("Slide");
                            csv.AddHeaderField("Machine");
                            csv.AddHeaderField("Fridge");
                        }

                        csv.AddField(DateTime.UtcNow.ToLocalTime().ToString(" yyyy-MM-dd HH:mm:ss.fff"));
                        //csv.AddField(curr)
                        if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(key))
                        {
                            csv.AddField(_PhaseTypeMaps[key].ToString());
                        }
                        else
                            csv.AddField("");
                        if (_ScannerCycle != null && _ScannerCycle.ContainsKey(key))
                        {
                            csv.AddField(_ScannerCycle[key].ToString());
                        }
                        else csv.AddField("S000");
                        if (isFlowcellA)
                        {
                            csv.AddField(_LastTemperatureA.ToString("F1"));
                            csv.AddField(_LastMachineTemperature.ToString("F1"));
                            csv.AddField(_LastFridgeTemperature.ToString("F1"));
                        }
                        else
                        {
                            csv.AddField(_LastTemperatureB.ToString("F1"));
                            csv.AddField(_LastMachineTemperature.ToString("F1"));
                            csv.AddField(_LastFridgeTemperature.ToString("F1"));
                        }
                        csv.WriteDataRow();
                        csv.Close();
                        ThreadUtils.Delay(3000);
                    }
                });


            }
        }

        public void StopRecordMetric(string key)
        {
            if (_StopRecordMap.ContainsKey(key)) _StopRecordMap[key] = false;
            else _StopRecordMap.Add(key, false);
        }

        public List<BGIDeviceException> ResetBIO(string key)
        {
            List<BGIDeviceException> lst = new List<BGIDeviceException>();
            try
            {
                SetTemp(20, key);
            }
            catch(BGIDeviceException ex)
            {
                lst.Add(ex);
            }
            catch(Exception ex)
            {
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.TemperatureBoard, (int)TemperatureErrorEnum.SetTempError, ex.Message);
                lst.Add(inner);
            }

            try
            {
                IDiaphragmPump powerDevice = (_IOBoardMap[key]) as IDiaphragmPump;
                if (powerDevice != null)
                {
                    int liquidSolenoidIndex = GetLiquidSolenoid(_CurrentIOBoardType, key == KEY_FLOWCELL_A ? true : false);
                    powerDevice.PowerOnOffDiaphragmPump((DiaphragmPumpType)liquidSolenoidIndex, false);
                }
            }
            catch(BGIDeviceException ex)
            {
                lst.Add(ex);
            }
            catch(Exception ex)
            {
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.IOBoard, (int)IOErrorEnum.PowerLiquidError, ex.Message);
                lst.Add(inner);
            }
            try
            {
                if (_ReagentNeedleMap != null && _ReagentNeedleMap.ContainsKey(key))
                {
                    _ReagentNeedleMap[key].Up();
                }
            }
            catch(BGIDeviceException ex)
            {
                lst.Add(ex);
            }
            catch(Exception ex)
            {
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.ReagentNeedle, (int)ReagentNeedleErrCodeEnum.UpError, ex.Message);
                lst.Add(inner);
            }

            try
            {
                (IoBoardInt as ISafeLock).UnLock(false);
            }
            catch(BGIDeviceException ex)
            {
                lst.Add(ex);
            }
            catch(Exception ex)
            {
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.IOBoard, (int)IOErrorEnum.LockError, ex.Message);
                lst.Add(inner);
            }

            return lst;
        }

        public void ResetStatus(string key, bool withError)
        {
            if(!withError)
                this._CurrentLedStatus = LedStatus.Ready;
            if (_PhaseTypeMaps.ContainsKey(key))
                _PhaseTypeMaps[key] = MessageType.None;
            this._OperationMaps[key] = string.Empty;
            this._OperationKeyMaps[key] = string.Empty;
            if (_CycleMetricsMap.ContainsKey(key)) _CycleMetricsMap[key] = null;
            if (_SequenceWatchStatsMap.ContainsKey(key)) _SequenceWatchStatsMap[key] = null;
            if (_ReagentMap.ContainsKey(key)) _ReagentMap.Remove(key);
            if (_SampleCodeMap.ContainsKey(key)) _SampleCodeMap.Remove(key);
            if (_SetRunInfoMap.ContainsKey(key)) _SetRunInfoMap[key] = false;
            if (withError)
            {
                if (_WriteFQStatus.ContainsKey(key))
                {
                    if (!_WriteFQStatus[key])
                    {
                        bool isRemove = false;
                        bool rslt = _WriteFQStatus.TryRemove(key, out isRemove);
                        _Logger.Log()(LogSeverityEnum.Info, $"Remove Status for {key} is {rslt}");
                        ImageBufferExt.Get.Stop(_FlowcellCodeMap[key]);
                        if (_FlowcellCodeMap.ContainsKey(key)) _FlowcellCodeMap.Remove(key);
                    }
                }
            }
            else
            {
                if(_FlowcellCodeMap.ContainsKey(key))
                    ImageBufferExt.Get.RemoveBatch(_FlowcellCodeMap[key]);
            }
            if (_IsSendTotalTime.ContainsKey(key)) _IsSendTotalTime[key] = null;
        }

        public void InitSequenceMap(string key)
        {
            if (_ScanParamsMap != null && _ScanParamsMap.ContainsKey(key)) _ScanParamsMap[key] = null;
            if (_LastFieldMap != null && _LastFieldMap.ContainsKey(key)) _LastFieldMap[key] = null;
            if (_LastRemainTimeMap != null && _LastRemainTimeMap.ContainsKey(key)) _LastRemainTimeMap[key] = 0;
            if (_LastSequenceInfoMap != null && _LastSequenceInfoMap.ContainsKey(key)) _LastSequenceInfoMap[key] = null;
            if (_LastSequencePhaseMap != null && _LastSequencePhaseMap.ContainsKey(key)) _LastSequencePhaseMap[key] = null;
        }
        #endregion

        //public void StopImageB

        #region  Pause Resume & Stop Image

        public void ScannerStop(string key)
        {
            ((IScannerOperation)_Scanner).ScannerOperation(ScannerOperationEnum.Stop);
        }

        public void ScannerPause(string key)
        {
            ((IScannerOperation)_Scanner).ScannerOperation(ScannerOperationEnum.Pause);
        }

        public void ScannerResume(string key)
        {
            ((IScannerOperation)_Scanner).ScannerOperation(ScannerOperationEnum.Resume);
        }

        public List<BGIDeviceException> ScannerReset(string key)
        {
            List<BGIDeviceException> lst = new List<BGIDeviceException>();
            try
            {
                (IoBoardInt as ISafeLock).UnLock(false);
            }
            catch (BGIDeviceException ex)
            {
                lst.Add(ex);
            }
            catch (Exception ex)
            {
                BGIDeviceException inner = new BGIDeviceException((int)DeviceCodeEnum.IOBoard, (int)IOErrorEnum.LockError, ex.Message);
                lst.Add(inner);
            }
            _WaitImageEvent.Set();
            ((IScannerOperation)_Scanner).ScannerOperation(ScannerOperationEnum.Finished);
            this._IsImaging = false;
            return lst;
        }

        public void UnLock(bool isOpen,string key)
        {
            try
            {
                (IoBoardInt as ISafeLock).UnLock(isOpen);
                string openError = Properties.Resource.strOpenLockFailed;
                string closeError = Properties.Resource.strCloseLockFailed;
                string normal = Properties.Resource.strSafeLockNormal;
                RemoveAlarmMessage(openError, normal, key,true);
                RemoveAlarmMessage(closeError, normal, key,true);
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                string flowcellId = GetFlowcellId(key);
                CreateAndSendMessage(isOpen ? Properties.Resource.strOpenLockFailed : Properties.Resource.strCloseLockFailed, ScriptMessageSeverityEnum.Error, false, flowcellId, "", key);
            }
            
        }
        #endregion

        #region Private Method

        #region Device Check
        private bool CheckPump(int index, SelfCheck selfCheck, SelfChecker checker, string checkKey = "")
        {
            bool hasError = false;
            string errorcode = "";
            string key = string.Empty;
            //if (!string.IsNullOrEmpty(checkKey)) key = checkKey;
            //else
            //{
            //    if (index == 0) key = KEY_FLOWCELL_A;
            //    else key = KEY_FLOWCELL_B;
            //}
            if (index == 0) key = KEY_FLOWCELL_A;
            else key = KEY_FLOWCELL_B;
            ISyringePump pump = null;
            if (_SyringePumpMap.ContainsKey(key))
                pump = _SyringePumpMap[key];
            else if (_SyringePumpMap.ContainsKey($"{key}_ByPass"))
                pump = _SyringePumpMap[$"{key}_ByPass"];

            if (index == 0)
            {
                if (_SyringePumpMap.ContainsKey(KEY_FLOWCELL_A))
                    pump = _SyringePumpMap[KEY_FLOWCELL_A];
                if (_SyringePumpMap.ContainsKey($"{KEY_FLOWCELL_A}_ByPass"))
                {
                    pump = _SyringePumpMap[$"{KEY_FLOWCELL_A}_ByPass"];
                }
            }
            else
            {
                if (_SyringePumpMap.ContainsKey(KEY_FLOWCELL_B))
                    pump = _SyringePumpMap[KEY_FLOWCELL_B];
                if (_SyringePumpMap.ContainsKey($"{KEY_FLOWCELL_B}_ByPass"))
                {
                    pump = _SyringePumpMap[$"{KEY_FLOWCELL_B}_ByPass"];
                }
            }
            selfCheck.DeviceType = SelfDevice.SyringPump.ToString();
            string[] para = new string[] { "A" };
            if (index == 1) para = new string[] { "B" };
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check Pump {para[0]}");
            if (pump != null)
            {
                try
                {
                    pump.SwitchValve(3);
                    checker.CheckSyringePump(pump,false);
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectPumpNormal, para);
                    string msg = GetResourceMsg(Properties.Resource.strDetectPumpFailed, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo, key,false);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,false);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectPumpFailed, para);
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);

                }
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Pump null");
                selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectPumpNotFound, para);
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);
            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check Pump {para[0]} with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check Pump {para[0]}");
            return hasError;
        }
        private bool CheckValve(int index, SelfCheck selfCheck, SelfChecker checker, string checkKey = "")
        {
            bool hasError = false;
            string errorcode = "";
            string key = string.Empty;
            //if (!string.IsNullOrEmpty(checkKey)) key = checkKey;
            //else
            //{
            //    if (index == 0) key = KEY_FLOWCELL_A;
            //    else key = KEY_FLOWCELL_B;
            //}
            if (index == 0) key = KEY_FLOWCELL_A;
            else key = KEY_FLOWCELL_B;
            ISelectionValve valve = null;
            if (index == 0 && _SelectorValveMap.ContainsKey(KEY_FLOWCELL_A))
            {
                valve = _SelectorValveMap[KEY_FLOWCELL_A];
            }
            else if (index != 0 && _SelectorValveMap.ContainsKey(KEY_FLOWCELL_B))
            {
                valve = _SelectorValveMap[KEY_FLOWCELL_B];
            }
            selfCheck.DeviceType = SelfDevice.SelectorValve.ToString();
            string[] para = new string[] { "A" };
            if (index == 1) para = new string[] { "B" };
            if (valve != null)
            {
                try
                {
                    _Logger.Log()(LogSeverityEnum.Info, $"Start Check Valve {para[0]}");
                    checker.CheckSelectionValve(valve);

                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectValveNormal, para);
                    string msg = GetResourceMsg(Properties.Resource.strDetectValveFailed, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo,key,false);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectValveFailed, para);
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);

                }
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Valve null");
                selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectValveNotFound, para);
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);

            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check Valve {para[0]} with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check Valve {para[0]}");
            return hasError;
        }
        private bool CheckReagentNeedle(int index, SelfCheck selfCheck, SelfChecker checker, string checkKey = "")
        {
            bool hasError = false;
            string errorcode = "";
            string key = string.Empty;
            //if (!string.IsNullOrEmpty(checkKey)) key = checkKey;
            //else
            //{
            //    if (index == 0) key = KEY_FLOWCELL_A;
            //    else key = KEY_FLOWCELL_B;
            //}
            if (index == 0) key = KEY_FLOWCELL_A;
            else key = KEY_FLOWCELL_B;
            IReagentNeedle needle = null;
            if (_ReagentNeedleMap.ContainsKey(KEY_FLOWCELL_A))
            {
                if (index == 0) needle = _ReagentNeedleMap[KEY_FLOWCELL_A];
            }
            if (_ReagentNeedleMap.ContainsKey(KEY_FLOWCELL_B))
            {
                if (index != 0) needle = _ReagentNeedleMap[KEY_FLOWCELL_B];
            }
            selfCheck.DeviceType = SelfDevice.ReagentNeedle.ToString();
            string[] para = new string[] { "A" };
            if (index == 1) para = new string[] { "B" };
            if (needle != null)
            {
                try
                {
                    _Logger.Log()(LogSeverityEnum.Info, $"Start check ReagentNeedle {para[0]}");
                    checker.CheckReagentNeedle(needle);

                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectNeedleNormal, para);
                    string msg = GetResourceMsg(Properties.Resource.strDetectNeedleFailed, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo, key,false);
                    msg = GetResourceMsg(Properties.Resource.strReagentUpError, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo, key,false);
                    msg = GetResourceMsg(Properties.Resource.strReagentDownError, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo, key,false);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key);
                }
                catch (Exception ex)
                {
                    hasError = true;
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectNeedleFailed, para);
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);

                }
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, "Needle null");
                selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectNeedleNotFound, para);
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key);

            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check ReagentNeedle {para[0]} with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check ReagentNeedle {para[0]}");
            return hasError;

        }
        private bool CheckTemperature(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.Temperature.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check TemperatureBoard");
            if (ChipTempBoardInt != null)
            {
                try
                {
                    //checker.CheckTemperatureBoard(ChipTempBoardInt);
                    selfCheck.CheckInfo = Properties.Resource.strDetectTemperNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectTemperFailed, selfCheck.CheckInfo,key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectTemperFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectTemperNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, "Finished check TemperatureBoard with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check TemperatureBoard");
            return hasError;
        }
        private bool CheckStage(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.Stage.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check Stage");
            if (XYStageInt != null)
            {
                try
                {
                    //checker.CheckTemperatureBoard(ChipTempBoardInt);
                    checker.CheckXYStage(XYStageInt, _OriginalPoints[0].x, _OriginalPoints[0].y);
                    checker.CheckXYStage(XYStageInt, _OriginalPoints[1].x, _OriginalPoints[1].y);
                    selfCheck.CheckInfo = Properties.Resource.strDetectStageNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectStageFailed, selfCheck.CheckInfo, key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectStageFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectStageNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check Stage with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check Stage");
            return hasError;
        }
        private bool CheckAutoFocus(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.AutoFocus.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check AutoFocus");
            if (AFControl != null)
            {
                try
                {
                    checker.CheckAFControl(AFControl, _OriginalPoints[0].z);
                    selfCheck.CheckInfo = Properties.Resource.strDetectAFNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectAFFailed, selfCheck.CheckInfo, key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectAFFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectAFNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check AutoFocus with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check AutoFoucs");
            return hasError;
        }
        private bool CheckIoBoard(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.IOBoard.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check IOBoard");
            if (IoBoardInt != null)
            {
                try
                {
                    selfCheck.CheckInfo = Properties.Resource.strDetectIOBoardNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectIOBoardFailed, selfCheck.CheckInfo, key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectIOBoardFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectIOBoardNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if (hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished check IOBoard with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check IOBoard");
            return hasError;
        }
        private bool CheckLed(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.LedIndicator.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check LedLamp");
            if (LEDLampBeltInt != null)
            {
                try
                {
                    checker.CheckLedIndicator(LEDLampBeltInt);
                    selfCheck.CheckInfo = Properties.Resource.strDetectLedNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectLedFailed, selfCheck.CheckInfo, key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectLedFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectLedNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if (hasError)
                _Logger.Log()(LogSeverityEnum.Error, $"Finished check Led with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished check Led");
            return hasError;
        }
        private bool CheckBarcodeReader(SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.BarcodeReader.ToString();
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check BarcodeReader");
            if (BarcodeReaderInt != null)
            {
                try
                {
                    checker.CheckBarcodeReader(BarcodeReaderInt);
                    selfCheck.CheckInfo = Properties.Resource.strDetectReaderNormal;
                    RemoveAlarmMessage(Properties.Resource.strDetectReaderFailed, selfCheck.CheckInfo, key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = Properties.Resource.strDetectReaderFailed;
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = Properties.Resource.strDetectReaderNotFound;
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            _Logger.Log()(LogSeverityEnum.Info, $"Finished Check Reader");
            return hasError;
        }
        private bool CheckCamera(int index, SelfCheck selfCheck, SelfChecker checker)
        {
            bool hasError = false;
            string key = KEY_FLOWCELL_A;
            string errorcode = "";
            selfCheck.DeviceType = SelfDevice.Camera.ToString();
            ICamera camera = null;
            string[] para = new string[] { "A" };
            
            if (index == 0)
            {
                camera = CameraIntA;
            }
            else
            {
                camera = CameraIntB;
                para = new string[] { "B" };
            }
            _Logger.Log()(LogSeverityEnum.Info, $"Start Check Camera {para[0]}");
            if (camera != null)
            {
                try
                {
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectCameraNormal, para);
                    string msg = GetResourceMsg(Properties.Resource.strDetectCameraFailed, para);
                    RemoveAlarmMessage(msg, selfCheck.CheckInfo,key,true);
                    SendSelfCheckNormalMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
                catch (BGIException ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                    selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectCameraFailed, para);
                    hasError = true;
                    SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
                }
            }
            else
            {
                selfCheck.CheckInfo = GetResourceMsg(Properties.Resource.strDetectCameraNotFound, para);
                hasError = true;
                SendSelfCheckAlarmMsg(selfCheck.CheckInfo, errorcode, key,true);
            }
            if(hasError) _Logger.Log()(LogSeverityEnum.Error, $"Finished Check Camera {para[0]} with error");
            else _Logger.Log()(LogSeverityEnum.Info, $"Finished Check Camera {para[0]}");
            return hasError;
        }

        #endregion

        private void SortAsFolderCreationTime(ref DirectoryInfo[] arrFi, bool isAsc = true)
        {
            Array.Sort(arrFi, delegate (DirectoryInfo x, DirectoryInfo y)
            {
                if (isAsc)
                    return x.CreationTime.CompareTo(y.CreationTime);
                else
                    return y.CreationTime.CompareTo(x.CreationTime);
            });
        }

        private bool CheckDiskSpaceForCycle(string key)
        {
            string diskPath = this._ImageSavePath;
            
            DirectoryInfo dir = new DirectoryInfo(diskPath);
            if (!dir.Exists)
            {
                try
                {
                    dir = Directory.CreateDirectory(diskPath);
                }
                catch(Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                }
                
            }
            if (!dir.Exists)
            {
                return false;
            }
            diskPath = dir.Root.Name;
            string diskName = diskPath.Remove(diskPath.Length - 2);
            int space = OSUtils.GetDiskFreeSpace(diskName);
            int minSpace = this._MinDiskSpaceForCycle;
            if (space <= minSpace)
            {
                IsSpaceEnough = false;
                return false;
            }
            else
            {
                IsSpaceEnough = true;
                RemoveAlarmMessage(Properties.Resource.strDiskError, Properties.Resource.strDiskStatusNormal, key, true);
                return true;
            }
        }

        private bool CheckDiskSpaceForRun(string key,DeviceCheckTypeEnum checkType)
        {
            if (StageRunMgrMap.ContainsKey(key))
            {
                StageRunMgr _StageRunMgr = StageRunMgrMap[key];
                _StageRunMgr.StageService.CleanSpaceDataList();
                string diskPath = this._ImageSavePath;
                RemoveAlarmMessage(Properties.Resource.strDiskNotConfig, Properties.Resource.strDiskStatusNormal, key, true);
                if (string.IsNullOrEmpty(diskPath))
                {
                    string flowcellId = GetFlowcellId(key);
                    CreateAndSendMessage(Properties.Resource.strDiskNotConfig, ScriptMessageSeverityEnum.Error, true, flowcellId, "",key);
                    return false;
                }
                DirectoryInfo dir = new DirectoryInfo(diskPath);
                DirectoryInfo root = dir.Root;
                RemoveAlarmMessage(Str.SafeFmt(Properties.Resource.strDiskNotFind,root.Name), Properties.Resource.strDiskStatusNormal, key, true);
                if (!root.Exists)
                {
                    string flowcellId = GetFlowcellId(key);
                    CreateAndSendMessage(Str.SafeFmt(Properties.Resource.strDiskNotFind, root.Name), ScriptMessageSeverityEnum.Error, true, flowcellId, "", key);
                    return false;
                }

                RemoveAlarmMessage(Str.SafeFmt(Properties.Resource.strDiskCreateFailed, diskPath), Properties.Resource.strDiskStatusNormal, key, true);
                if (!dir.Exists)
                {
                    try
                    {
                        dir = Directory.CreateDirectory(diskPath);
                    }
                    catch(Exception ex)
                    {
                        _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                        string flowcellId = GetFlowcellId(key);
                        CreateAndSendMessage(Str.SafeFmt(Properties.Resource.strDiskCreateFailed, diskPath), ScriptMessageSeverityEnum.Error, true, flowcellId, "", key);
                        return false;
                    }
                }
                if (!dir.Exists)
                {
                    string flowcellId = GetFlowcellId(key);
                    CreateAndSendMessage(Str.SafeFmt(Properties.Resource.strDiskCreateFailed, diskPath), ScriptMessageSeverityEnum.Error, true, flowcellId, "", key);
                    return false;
                }
                diskPath = dir.Root.Name;
                string diskName = diskPath.Remove(diskPath.Length - 2);
                int space = OSUtils.GetDiskFreeSpace(diskName);
                int minSpace = this._MinDiskSpaceForRun;
                if (space <= minSpace)
                {
                    string flowcellId = GetFlowcellId(key);
                    CreateAndSendMessage(Properties.Resource.strDiskError, ScriptMessageSeverityEnum.Error, true, flowcellId, "", key);
                    SendDataList(_StageRunMgr, key);
                    IsSpaceEnough = false;
                    return false;
                }
                else
                {
                    if(checkType == DeviceCheckTypeEnum.OtherCheck)
                    {
                        SendDataList(_StageRunMgr, key);
                    }
                    else
                    {
                        SendDataList(_StageRunMgr, key,true);
                    }
                    RemoveAlarmMessage(Properties.Resource.strDiskError, Properties.Resource.strDiskStatusNormal, key,true);
                    IsSpaceEnough = true;
                    return true;
                }
            }
            return true;
        }

        private void SetBuzzerByInterval(int soundTimeMS, int stopTimeMS, int intervalCount)
        {
            if (!this.IsInitDone) return;
            try
            {
                for (int i = 0; i < intervalCount; i++)
                {
                    BuzzerControlInt?.PowerOnOffBuzzer(true);
                    ThreadUtils.Delay(soundTimeMS);
                    BuzzerControlInt?.PowerOnOffBuzzer(false);
                    ThreadUtils.Delay(stopTimeMS);
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "Set buzzer by interval error,sound Time={0},sleep Time={1},interval time={2},ex={3}",
                    soundTimeMS, stopTimeMS, intervalCount, ex);
            }
        }

        private bool CheckDeviceInit(DeviceProxy proxy, int deviceIndex = -1)
        {
            bool rslt = false;
            string deviceName = string.Empty;
            try
            {
                if (proxy != null) deviceName = proxy.DeviceName;
                proxy.Initialize();
                proxy.Connect();
                //if (deviceIndex < 0)
                //    proxy.Initialize();
                //else proxy.Initialize(deviceIndex);
                rslt = true;
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"Init {deviceName} For Chip{deviceIndex} Failed,Err:{ex.ToString()}");
                string resourceKey = $"str{deviceName}InitFailed";
                resourceKey = Properties.Resource.ResourceManager.GetString(resourceKey);
                ServiceMessageInfo msgInfo = null;
                if (deviceIndex == 0 || deviceIndex == 2)//for ChipA
                {
                    string resourceMsg = GetResourceMsg(resourceKey, new string[] { "A" });
                    msgInfo = new ServiceMessageInfo() { MessageType = ScriptMessageSeverityEnum.Error, Datetime = DateTime.UtcNow.ToLocalTime().ToString(), Message = resourceMsg };
                    if (_InitMessageInfoMap.ContainsKey(KEY_FLOWCELL_A))
                    {
                        _InitMessageInfoMap[KEY_FLOWCELL_A].Add(msgInfo);

                    }
                    else
                    {
                        List<ServiceMessageInfo> msgList = new List<ServiceMessageInfo>();
                        msgList.Add(msgInfo);
                        _InitMessageInfoMap[KEY_FLOWCELL_A] = msgList;
                    }
                }
                else if (deviceIndex == 1 || deviceIndex == 3)//for ChipB
                {
                    string resourceMsg = GetResourceMsg(resourceKey, new string[] { "B" });
                    msgInfo = new ServiceMessageInfo() { MessageType = ScriptMessageSeverityEnum.Error, Datetime = DateTime.UtcNow.ToLocalTime().ToString(), Message = resourceMsg };
                    if (_InitMessageInfoMap.ContainsKey(KEY_FLOWCELL_B))
                    {
                        _InitMessageInfoMap[KEY_FLOWCELL_B].Add(msgInfo);

                    }
                    else
                    {
                        List<ServiceMessageInfo> msgList = new List<ServiceMessageInfo>();
                        msgList.Add(msgInfo);
                        _InitMessageInfoMap[KEY_FLOWCELL_B] = msgList;
                    }
                }
                else//for ChipA & ChipB
                {
                    string resourceMsg = GetResourceMsg(resourceKey, new string[] { "" });
                    msgInfo = new ServiceMessageInfo() { MessageType = ScriptMessageSeverityEnum.Error, Datetime = DateTime.UtcNow.ToLocalTime().ToString(), Message = resourceMsg };
                    if (_InitMessageInfoMap.ContainsKey(KEY_FLOWCELL_A))
                    {
                        _InitMessageInfoMap[KEY_FLOWCELL_A].Add(msgInfo);

                    }
                    else
                    {
                        List<ServiceMessageInfo> msgList = new List<ServiceMessageInfo>();
                        msgList.Add(msgInfo);
                        _InitMessageInfoMap[KEY_FLOWCELL_A] = msgList;
                    }
                }
            }
            return rslt;
        }

        private string GetResourceMsg(string strMsg, params string[] para)
        {
            return Str.SafeFmt(strMsg, para);
        }

        private void DeviceInitCheck(string key)
        {
            
            while (!IsInitDone)
            {
                //Wait Device Init Done
                Thread.Sleep(1000);
            }
            if (MachineStatusMap[KEY_FLOWCELL_A] == MachineStatus.SelfCheck ||
                MachineStatusMap[KEY_FLOWCELL_B] == MachineStatus.SelfCheck)
                return;
            int sleepTimeForSimulated = 200;
            MachineStatusMap[KEY_FLOWCELL_A] = MachineStatus.SelfCheck;
            MachineStatusMap[KEY_FLOWCELL_B] = MachineStatus.SelfCheck;
            double totalCheck = 14;
            double startIndex = 1;
            bool hasError = false;
            string errorcode = string.Empty;
            SelfCheck selfCheck = new SelfCheck();
            SelfChecker checker = new SelfChecker();
            List<bool> errorListA = new List<bool>();
            List<bool> errorListB = new List<bool>();
            #region Check ReagentNeedle
            hasError = CheckReagentNeedle(0, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_ReagentNeedle1 != null && _ReagentNeedle1.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;

            errorListA.Add(hasError);

            hasError = CheckReagentNeedle(1, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if (_ReagentNeedle2 != null && _ReagentNeedle2.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListB.Add(hasError);
            #endregion

            #region Check Pump
            hasError = CheckPump(0, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            
            Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            hasError = CheckPump(1, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListB.Add(hasError);
            #endregion

            #region Check Valve
            hasError = CheckValve(0, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            startIndex++;
            errorListA.Add(hasError);
            hasError = CheckValve(1, selfCheck, checker, key);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_SyringePump1 != null && _SyringePump1.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListB.Add(hasError);
            #endregion

            #region Check Temperature
            hasError = CheckTemperature(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_ChipTempBoard != null && _ChipTempBoard.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            #endregion

            #region Check Camera
            hasError = CheckCamera(0, selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_CameraA != null && _CameraA.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            hasError = CheckCamera(1, selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_CameraB != null && _CameraB.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            #endregion

            #region Check Auto Focus
            hasError = CheckAutoFocus(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_AFControl != null && _AFControl.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            #endregion

            #region Check Stage
            hasError = CheckStage(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_XYStage != null && _XYStage.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            #endregion


            #region Check Io
            hasError = CheckIoBoard(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_IoBoard != null && _IoBoard.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            errorListA.Add(hasError);
            errorListB.Add(hasError);
            #endregion

            #region Check Led
            hasError = CheckLed(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            if(_IoBoard != null && _IoBoard.IsSimulated)
                Thread.Sleep(sleepTimeForSimulated);
            startIndex++;
            //errorListA.Add(hasError);
            //errorListB.Add(hasError);
            #endregion

            #region Check BarcodeReader
            hasError = CheckBarcodeReader(selfCheck, checker);
            selfCheck.Progress = startIndex / totalCheck;
            SendShelfInfo(selfCheck);
            startIndex++;
            #endregion
            selfCheck.DeviceType = "Done";
            if(errorListA.Exists(p=>p==true) && errorListB.Exists(p => p == true))
            {
                selfCheck.CheckInfo = "Fail";
            }
            else if (errorListA.Exists(p => p == true))
            {
                selfCheck.CheckInfo = "FailA";
            }
            else if (errorListB.Exists(p => p == true))
            {
                selfCheck.CheckInfo = "FailB";
            }
            else
                selfCheck.CheckInfo = "Success";
            SelfCheckStatus = selfCheck.CheckInfo;
            Thread.Sleep(100);
            SendShelfInfo(selfCheck);
            MachineStatusMap[KEY_FLOWCELL_A] = MachineStatus.Idle;
            MachineStatusMap[KEY_FLOWCELL_B] = MachineStatus.Idle;
        }

        private void SendSelfCheckAlarmMsg(string errMsg, string errorcode, string key,bool isPublic = false)
        {
            ServiceMessageInfo msg = new ServiceMessageInfo();
            msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            msg.MessageType = ScriptMessageSeverityEnum.Error;
            msg.InstructmentId = Environment.MachineName;
            msg.UserName = !string.IsNullOrEmpty(CurrentUser) ? CurrentUser : Environment.UserName;
            msg.FlowCellId = GetFlowcellId(key);
            msg.Message = errMsg;
            msg.Errorcode = errorcode;
            msg.IsPublic = isPublic;
            SendServiceMessage(msg, key);
        }

        private void SendSelfCheckNormalMsg(string errMsg, string errorcode, string key,bool isPublic=false)
        {
            ServiceMessageInfo msg = new ServiceMessageInfo();
            msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            msg.MessageType = ScriptMessageSeverityEnum.Info;
            msg.InstructmentId = Environment.MachineName;
            msg.UserName = !string.IsNullOrEmpty(CurrentUser) ? CurrentUser : Environment.UserName;
            msg.FlowCellId = GetFlowcellId(key);
            msg.Message = errMsg;
            msg.Errorcode = errorcode;
            msg.IsPublic = isPublic;
            if (StageRunMgrMap != null && StageRunMgrMap.ContainsKey(key))
                StageRunMgrMap[key].AddLogMessage(msg);
            //SendServiceMessage(msg, key);
        }

        private void SendInfoAlarm(string info,string errCode,string key,bool isPublic)
        {
            string flowcellId = GetFlowcellId(key);
            string msg = string.Empty;
            if (key == KEY_FLOWCELL_A) msg = Str.SafeFmt(info, "A");
            else msg = Str.SafeFmt(info, "B");
            CreateAndSendMessage(msg, ScriptMessageSeverityEnum.Info, isPublic, flowcellId, errCode, key, true);
        }

        private void RemoveInfoAlarm(string alarmInfo,string normalInfo,string errCode,string key,bool isPublic)
        {
            if (StageRunMgrMap != null && StageRunMgrMap.ContainsKey(key))
            {
                string tempMsg = alarmInfo;
                string normalMsg = normalInfo;
                if (key == KEY_FLOWCELL_A)
                {
                    tempMsg = Str.SafeFmt(alarmInfo, "A");
                    normalMsg = Str.SafeFmt(normalInfo, "A");
                }
                else
                {
                    tempMsg = Str.SafeFmt(alarmInfo, "B");
                    normalMsg = Str.SafeFmt(normalInfo, "B");
                }
                RemoveAlarmMessage(tempMsg, normalMsg, key, isPublic);
            }
        }

        public void SendServiceMessage(ServiceMessageInfo serviceMessageInfo, string key,bool isIncludeInfoAlarm=false)
        {
            lock (lockObj)
            {
                StageRunMgr _StageRunMgr = null;
                if (StageRunMgrMap != null && StageRunMgrMap.Count > 0)
                {
                    string tempKey = key;
                    if (string.IsNullOrEmpty(tempKey) || string.IsNullOrWhiteSpace(tempKey))
                        tempKey = KEY_FLOWCELL_A;
                    if (!StageRunMgrMap.ContainsKey(tempKey)) return;
                    _StageRunMgr = StageRunMgrMap[tempKey];
                    if (_StageRunMgr != null)
                    {
                        Task setBuzzerTask = null;
                        if (serviceMessageInfo.MessageType == ScriptMessageSeverityEnum.Error)
                        {
                            if (_StageRunMgr.IsExistAlarmMessage(serviceMessageInfo.Message) && !_StageRunMgr.IsExistAlarmMessageType(ScriptMessageSeverityEnum.Error))
                            {
                                if (_StageRunMgr.IsExistAlarmMessage(serviceMessageInfo.Message))
                                {
                                    ServiceMessageInfo logMsg = _StageRunMgr.GetAlarmMessage(serviceMessageInfo.Message);
                                    if(logMsg != null)
                                    {
                                        _StageRunMgr.AddLogMessage(logMsg);
                                    }
                                }
                                _StageRunMgr.RemoveAlarmMessage(serviceMessageInfo.Message);
                                
                            }
                            //filter same alarm message
                            if (!_StageRunMgr.IsExistAlarmMessage(serviceMessageInfo.Message))
                            {
                                setBuzzerTask = Task.Run(() => ShowAlarmIndecator(LedStatus.Error));
                            }
                            //SendeMessageToZLims(serviceMessageInfo.Message, "error");
                            _StageRunMgr.AddAlarmMessage(serviceMessageInfo);
                            
                        }
                        else if (serviceMessageInfo.MessageType == ScriptMessageSeverityEnum.Warning)
                        {
                            if (!_StageRunMgr.IsExistAlarmMessage(serviceMessageInfo.Message) && !_StageRunMgr.IsExistAlarmMessageType(ScriptMessageSeverityEnum.Error))
                            {
                                setBuzzerTask = Task.Run(() => ShowAlarmIndecator(LedStatus.Warning));
                            }
                            // SendeMessageToZLims(serviceMessageInfo.Message, "warning");
                            _StageRunMgr.AddAlarmMessage(serviceMessageInfo);
                        }
                        else if(serviceMessageInfo.MessageType == ScriptMessageSeverityEnum.Info)
                        {
                            if(isIncludeInfoAlarm)
                                _StageRunMgr.AddAlarmMessage(serviceMessageInfo);
                        }
                        
                        int iAlarmCount = _StageRunMgr.GetAlarmMessageCount();
                        if (iAlarmCount > 0)
                        {
                            if (_StageRunMgr.IsExistAlarmMessageType(ScriptMessageSeverityEnum.Error))
                            {
                                if (_CurrentLedStatus != LedStatus.Error)
                                {
                                    _CurrentLedStatus = LedStatus.Error;
                                    SetLedIndicatorColor(LedStatus.Error);
                                }
                            }
                            else if(_StageRunMgr.IsExistAlarmMessageType(ScriptMessageSeverityEnum.Warning))
                            {
                                if (_CurrentLedStatus != LedStatus.Warning)
                                {
                                    _CurrentLedStatus = LedStatus.Warning;
                                    SetLedIndicatorColor(LedStatus.Warning);
                                }
                            }
                                
                        }
                        if (setBuzzerTask != null)
                        {
                            setBuzzerTask.Wait();
                        }
                    }
                }
            }
        }

        private void SendShelfInfo(SelfCheck checkMsg)
        {
            string message = JsonConvert.SerializeObject(checkMsg);
            StageRunMgrMap[KEY_FLOWCELL_A].SendStageRunMessage(MessageType.SelfCheck.ToString(), message);
        }

        private void SendStageRunMessage(string message, string key)
        {
            if (StageRunMgrMap != null && StageRunMgrMap.Count > 0 && StageRunMgrMap.ContainsKey(key))
            {
                MessageType phaseType = MessageType.None;
                if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(key))
                {
                    phaseType = _PhaseTypeMaps[key];
                    if (_LastSequencePhaseMap == null) _LastSequencePhaseMap = new Dictionary<string, string>();
                    if (_LastSequenceInfoMap == null) _LastSequenceInfoMap = new Dictionary<string, string>();
                    if(phaseType != MessageType.Version)
                    {
                        if (_LastSequencePhaseMap.ContainsKey(key)) _LastSequencePhaseMap[key] = phaseType.ToString();
                        else _LastSequencePhaseMap.Add(key, phaseType.ToString());

                        if (_LastSequenceInfoMap.ContainsKey(key)) _LastSequenceInfoMap[key] = message;
                        else _LastSequenceInfoMap.Add(key, message);
                    }
                    StageRunMgrMap[key].StageService.SendStageRunMessage(phaseType.ToString(), message);
                }
            }
        }

        private void SendStageRemainingTime(double remainTime,string key)
        {
            if(StageRunMgrMap != null && StageRunMgrMap.Count > 0 && StageRunMgrMap.ContainsKey(key))
            {
                if (_LastRemainTimeMap == null) _LastRemainTimeMap = new Dictionary<string, double>();
                if (_LastRemainTimeMap.ContainsKey(key)) _LastRemainTimeMap[key] = remainTime;
                else _LastRemainTimeMap.Add(key, remainTime);
                StageRunMgrMap[key].StageService.SendRemainingTime(remainTime);
            }
        }

        /// <summary>
        /// Get alarm mesage or get remove alarm message
        /// </summary>
        /// <param name="value"></param>
        /// <param name="alarmDic"></param>
        /// <param name="alarmMsg"></param>
        /// <param name="removeAlarmList"></param>
        private void GetAlarmMessageByValue<T>(string alarmTypeStr, T value, Dictionary<T, string> alarmDic, ref ServiceMessageInfo alarmMsg,
            ref List<string> removeAlarmList, ref string normalTipStr, Dictionary<string, string> normalTipDic,string chipKey,bool isPublic) where T : IComparable
        {
            try
            {
                StageRunMgr _stageRunMgr = null;
                if (StageRunMgrMap == null || StageRunMgrMap.Count == 0) return;
                if (string.IsNullOrEmpty(chipKey)) _stageRunMgr = StageRunMgrMap[KEY_FLOWCELL_A];
                else if (StageRunMgrMap.ContainsKey(chipKey)) _stageRunMgr = StageRunMgrMap[chipKey];
                if (_stageRunMgr == null) return;
                if (alarmDic.Keys.Contains(value))
                {
                    if (showAlarmDescriptionDic.Keys.Contains(alarmTypeStr) && showAlarmDescriptionDic[alarmTypeStr].Exists(p => p == MachineStatusMap[chipKey]))
                    {
                        foreach (KeyValuePair<T, string> kvp in alarmDic)
                        {
                            if (value.CompareTo(kvp.Key) == 0)
                            {
                                alarmMsg = new ServiceMessageInfo();
                                alarmMsg.Message = kvp.Value;
                            }
                        }
                    }

                    //pause script
                    if (IsPauseWhenSensorError && pauseInMachineStatusDic.Keys.Contains(alarmTypeStr)
                        && pauseInMachineStatusDic[alarmTypeStr].Exists(p => p == MachineStatusMap[chipKey]))
                    {
                        if (_stageRunMgr.ScriptState == ScriptStateEnum.ScriptRunning)
                        {
                            _Logger.Log()(LogSeverityEnum.Warning, $"pause script start alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                            _stageRunMgr.PauseScript();
                            _Logger.Log()(LogSeverityEnum.Warning, $"pause script end alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                        }
                    }

                    //pause all script
                    if (IsPauseWhenSensorError && pauseAllInMachineStatusDic.Keys.Contains(alarmTypeStr)
                        && pauseAllInMachineStatusDic[alarmTypeStr].Exists(p => p == MachineStatusMap[chipKey]))
                    {
                        
                        if (StageRunMgrMap != null)
                        {
                            _Logger.Log()(LogSeverityEnum.Warning, $"pause all script start alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                            foreach (StageRunMgr srm in StageRunMgrMap.Values)
                            {
                                if (srm != null && srm.ScriptState == ScriptStateEnum.ScriptRunning)
                                    srm.PauseScript();
                            }
                            _Logger.Log()(LogSeverityEnum.Warning, $"pause all script end alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                        }
                    }

                    //stop script
                    if (IsStopWhenSensorError && stopInMachineStatusDic.Keys.Contains(alarmTypeStr)
                        && stopInMachineStatusDic[alarmTypeStr].Exists(p => p == MachineStatusMap[chipKey]))
                    {

                        if (_stageRunMgr.ScriptState == ScriptStateEnum.ScriptRunning || _stageRunMgr.ScriptState == ScriptStateEnum.ScriptPaused
                            || _stageRunMgr.ScriptState == ScriptStateEnum.ScriptPausing)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"stop script start alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                            _stageRunMgr.StopScript();
                            ResetStatus(chipKey, true);
                            _Logger.Log()(LogSeverityEnum.Error, $"stop script end alarmTypeStr={alarmTypeStr} machineStatus = {MachineStatusMap[chipKey]} _stageRunMgr.ScriptState={_stageRunMgr.ScriptState}");
                        }
                    }
                }
                else
                {
                    if (normalTipDic.Keys.Contains(alarmTypeStr))
                    {
                        normalTipStr = normalTipDic[alarmTypeStr];
                    }
                    foreach (KeyValuePair<T, string> kvp in alarmDic)
                    {
                        RemoveAlarmMessage(kvp.Value,normalTipStr,chipKey,isPublic);
                    }
                }
            }
            catch(Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
            }
        }


        private void InitAlarmDic()
        {
            alarmIDic = new Dictionary<string, Dictionary<int, string>>()
                        {
                            { "TemperatureBoard_SlideTemperatureStatusA", new Dictionary<int, string>()
                            { { 1, Properties.Resource.strSlideTemperatureHightA },{ 2, Properties.Resource.strSlideTemperatureLowA}} },
                            { "TemperatureBoard_SlideTemperatureStatusB", new Dictionary<int, string>()
                            { { 1, Properties.Resource.strSlideTemperatureHightB },{ 2, Properties.Resource.strSlideTemperatureLowB}} },
                            { "TemperatureBoard_MachineTemperatureStatus", new Dictionary<int, string>()
                            { { 1, Properties.Resource.strMachineTemperatureHight },{ 2, Properties.Resource.strMachineTemperatureLow}} }
                        };

            clearAlarmIDic = new Dictionary<string, string>()
                            {
                               { "TemperatureBoard_SlideTemperatureStatusA",Properties.Resource.strSlideTemperatureNormalA },
                               { "TemperatureBoard_SlideTemperatureStatusB",Properties.Resource.strSlideTemperatureNormalB },
                               { "TemperatureBoard_MachineTemperatureStatus",Properties.Resource.strMachineTemperatureNormal }
                            };

            alarmBDic = new Dictionary<string, Dictionary<bool, string>>()
                        {
                            { "IOBoard_LiquidStatus", new Dictionary<bool, string>() { { false, Properties.Resource.strLiquidStatusError } } },
                            { "IOBoard_SMCStatusA", new Dictionary<bool, string>() { { false, Properties.Resource.strSMCStatusErrorA } } },
                            { "IOBoard_SMCStatusB", new Dictionary<bool, string>() { { false, Properties.Resource.strSMCStatusErrorB } } },
                            { "IOBoard_ChipDoor", new Dictionary<bool, string>() { { false, Properties.Resource.strChipDoorError } } },
                            { "IOBoard_FridgeDoor", new Dictionary<bool, string>() { { false, Properties.Resource.strFridgeDoorError } } },
                            { "IOBoard_KitStatusA", new Dictionary<bool, string>() { { false, Properties.Resource.strKitStatusErrorA } } },
                            { "IOBoard_KitStatusB", new Dictionary<bool, string>() { { false, Properties.Resource.strKitStatusErrorB } } },
                            { "IOBoard_TopCover", new Dictionary<bool, string>() { { false, Properties.Resource.strTopCoverError } } },
                            { "IOBoard_LeftSideCover", new Dictionary<bool, string>() { { false, Properties.Resource.strLeftSideCoverError } } },
                            { "IOBoard_RightSideCover", new Dictionary<bool, string>() { { false, Properties.Resource.strRightSideCoverError } } },
                            { "IOBoard_DNBKitA", new Dictionary<bool, string>() { { false, Properties.Resource.strDnbKitStatusErrorA } } },
                            { "IOBoard_DNBKitB", new Dictionary<bool, string>() { { false, Properties.Resource.strDnbKitStatusErrorB } } },
                            { "DiskStatus", new Dictionary<bool, string>() { { false, Properties.Resource.strDiskError } } },
                            { "IsBaseCallOnline", new Dictionary<bool, string>() { { false, Properties.Resource.strBasecallConnectError } } },
                            { "IsZlimsOnline", new Dictionary<bool, string>() { { false, Properties.Resource.strZlimsConnectError } } }
                        };

            clearAlarmBDic = new Dictionary<string, string>()
                        {
                            { "IOBoard_LiquidStatus",Properties.Resource.strIOBoard_LiquidStatusNormal },
                            { "IOBoard_SMCStatusA" ,Properties.Resource.strIOBoard_SMCStatusNormalA },
                            { "IOBoard_SMCStatusB" ,Properties.Resource.strIOBoard_SMCStatusNormalB },
                            { "IOBoard_ChipDoor",Properties.Resource.strIOBoard_ChipDoorNormal},
                            { "IOBoard_FridgeDoor" ,Properties.Resource.strIOBoard_FridgeDoorNormal },
                            { "IOBoard_KitStatusA" ,Properties.Resource.strIOBoard_KitStatusNormalA },
                            { "IOBoard_KitStatusB" ,Properties.Resource.strIOBoard_KitStatusNormalB },
                            { "IOBoard_TopCover" ,Properties.Resource.strIOBoard_TopCoverNormal },
                            { "IOBoard_LeftSideCover", Properties.Resource.strIOBoard_LeftSideCoverNormal },
                            { "IOBoard_RightSideCover", Properties.Resource.strIOBoard_RightSideCoverNormal },

                            { "IOBoard_DNBKitA", Properties.Resource.strIOBoard_DNBKitStatusNormalA },
                            { "IOBoard_DNBKitB", Properties.Resource.strIOBoard_DNBKitStatusNormalB },
                            { "DiskStatus", Properties.Resource.strDiskStatusNormal },
                            { "IsBaseCallOnline", Properties.Resource.strBaseCallNormal},
                            { "IsZlimsOnline", Properties.Resource.strZlimsNormal }
                        };

            showAlarmDescriptionDic = new Dictionary<string, List<MachineStatus>>()
            {
                { "IOBoard_LiquidStatus",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation, MachineStatus.Image } },
                { "IOBoard_ChipDoor",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_TopCover",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_LeftSideCover",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_RightSideCover",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_SMCStatusA",new List<MachineStatus>() {  MachineStatus.Init,MachineStatus.Idle,MachineStatus.Wash, MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_SMCStatusB",new List<MachineStatus>() {  MachineStatus.Init,MachineStatus.Idle,MachineStatus.Wash, MachineStatus.Incorporation,MachineStatus.Image}},
                { "IOBoard_FridgeDoor",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash, MachineStatus.Incorporation,MachineStatus.Image }},
                { "IOBoard_KitStatusA",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck, MachineStatus.Wash,MachineStatus.Incorporation, MachineStatus.Image } },
                { "IOBoard_KitStatusB",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck, MachineStatus.Wash,MachineStatus.Incorporation, MachineStatus.Image } },
                { "IOBoard_DNBKitA",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck, MachineStatus.Wash,MachineStatus.Incorporation, MachineStatus.Image } },
                { "IOBoard_DNBKitB",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck, MachineStatus.Wash,MachineStatus.Incorporation, MachineStatus.Image } },

                { "TemperatureBoard_SlideTemperatureStatusA",new List<MachineStatus>() { MachineStatus.Init ,MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image } },
                { "TemperatureBoard_SlideTemperatureStatusB",new List<MachineStatus>() { MachineStatus.Init ,MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image } },
                { "TemperatureBoard_MachineTemperatureStatus",new List<MachineStatus>() { MachineStatus.Init ,MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image } },

                { "IsBaseCallOnline",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image,MachineStatus.DataHandle} },
                { "IsZlimsOnline",new List<MachineStatus>() { MachineStatus.Init, MachineStatus.Idle, MachineStatus.SelfCheck,MachineStatus.Wash,MachineStatus.Incorporation,MachineStatus.Image,MachineStatus.DataHandle } }

            };

            pauseAllInMachineStatusDic = new Dictionary<string, List<MachineStatus>>()
            {
                //{ "IOBoard_FridgeDoor",new List<MachineStatus>() { MachineStatus.Wash,MachineStatus.Incorporation } },
                { "IOBoard_ChipDoor",new List<MachineStatus>() { MachineStatus.Incorporation} }
            };

            pauseInMachineStatusDic = new Dictionary<string, List<MachineStatus>>()
            {
                { "IOBoard_ChipDoor",new List<MachineStatus>() { MachineStatus.Incorporation} },
                //{ "IOBoard_FridgeDoor",new List<MachineStatus>() { MachineStatus.Wash,MachineStatus.Incorporation } },
                { "IOBoard_KitStatusA",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation } },
                { "IOBoard_KitStatusB",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation } },
                { "IOBoard_DNBKitA",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation } },
                { "IOBoard_DNBKitB",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation } }
            };

            stopInMachineStatusDic = new Dictionary<string, List<MachineStatus>>()
            {
                { "IOBoard_SMCStatusA",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation} },
                { "IOBoard_SMCStatusB",new List<MachineStatus>() { MachineStatus.Wash, MachineStatus.Incorporation} },
                {"TemperatureBoard_SlideTemperatureStatusA",new List<MachineStatus>() { MachineStatus.Incorporation } },
                {"TemperatureBoard_SlideTemperatureStatusB",new List<MachineStatus>() { MachineStatus.Incorporation } }
            };
        }

        private void RemoveAlarmMessage(string alarmStr,string normalMsg, string chipKey,bool isPublic = false)
        {
            StageRunMgr _stageRunMgr = null;
            if (StageRunMgrMap == null || StageRunMgrMap.Count == 0) return;
            if (!string.IsNullOrEmpty(chipKey) && StageRunMgrMap.ContainsKey(chipKey)) _stageRunMgr = StageRunMgrMap[chipKey];
            else _stageRunMgr = StageRunMgrMap[KEY_FLOWCELL_A];
            if (_stageRunMgr == null) return;
            ServiceMessageInfo alarm = _stageRunMgr.RemoveAlarmMessage(alarmStr);
            if (alarm != null)
            {
                try
                {
                    _stageRunMgr.AddLogMessage(alarm);
                    if (!string.IsNullOrEmpty(normalMsg))
                    {
                        ServiceMessageInfo normal = CreateMessage(normalMsg, ScriptMessageSeverityEnum.Info, isPublic, GetFlowcellId(chipKey), "");
                        _stageRunMgr.AddLogMessage(normal);
                    }

                    _stageRunMgr.StageService.SendAlarmMessageId(0);
                    if (_stageRunMgr.GetAlarmMessageCount() == 0)
                    {
                        if ((int)_stageRunMgr.ScriptState <= (int)ScriptStateEnum.ReadyToRun)
                        {
                            if(_CurrentLedStatus != LedStatus.Ready)
                            {
                                SetLedIndicatorColor(LedStatus.Ready);
                                _CurrentLedStatus = LedStatus.Ready;
                            }
                            
                        }
                        else
                        {
                            if(_CurrentLedStatus != LedStatus.Running)
                            {
                                SetLedIndicatorColor(LedStatus.Running);
                                _CurrentLedStatus = LedStatus.Running;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _Logger.Log()(LogSeverityEnum.Error, "Remove alarm message error:alarmStr={0},ex={1}", alarmStr, ex);
                }
            }
        }

        private void CreateAtomicValueEventArgs(string valueName,object curVal,ref ServiceMessageInfo msg,ref List<string> removeAlarmList, ref string normalTipStr,string chipKey,bool isPublic)
        {
            AtomicValue av = new AtomicValue();
            msg = null;
            av.IsNull = false;
            av.ValueName = valueName;
            av.MeasurementDT = DateTime.UtcNow.ToString(DATETIME_FORMAT_FULL);
            if (curVal == null)
            {
                av.IsNull = true;
            }
            else if (curVal.GetType().Equals(typeof(double)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeDouble;
                av.ValueD = (double)curVal;
            }
            else if (curVal.GetType().Equals(typeof(float)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeFloat;
                av.ValueF = (float)curVal;
            }
            else if (curVal.GetType().Equals(typeof(int)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeInt;
                av.ValueI = (int)curVal;
            }
            else if (curVal.GetType().Equals(typeof(long)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeLong;
                av.ValueL = (long)curVal;
            }
            else if (curVal.GetType().Equals(typeof(bool)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeBool;
                av.ValueB = (bool)curVal;
            }
            else if (curVal.GetType().Equals(typeof(string)))
            {
                av.ValueType = AtomicValueTypeEnum.TypeString;
                av.ValueS = (string)curVal;
            }
            else
            {
                av.IsNull = true;
            }
            if (!av.IsNull)
            {
                Dictionary<bool, string> dicB = new Dictionary<bool, string>();
                if (alarmBDic.Keys.Contains(av.ValueName))
                {
                    dicB = alarmBDic[av.ValueName];
                    GetAlarmMessageByValue<bool>(valueName, av.ValueB, dicB, ref msg, ref removeAlarmList, ref normalTipStr, clearAlarmBDic,chipKey,isPublic);

                    if (msg != null)
                    {
                        msg.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
                        if(StageRunMgrMap != null)
                        {
                            switch (av.ValueName)
                            {
                                case "IOBoard_ChipDoor":
                                case "IOBoard_FridgeDoor":
                                case "IOBoard_TopCover":
                                case "IOBoard_LeftSideCover":
                                case "IOBoard_RightSideCover":
                                    var m = StageRunMgrMap.Values.FirstOrDefault(a => (a.ScriptState > ScriptStateEnum.ReadyToRun && a.ScriptState < ScriptStateEnum.Unknown));
                                    if (m != null) msg.MessageType = ScriptMessageSeverityEnum.Error;
                                    else msg.MessageType = ScriptMessageSeverityEnum.Warning;
                                    msg.IsPublic = true;
                                    break;
                                case "IOBoard_LiquidStatus":
                                    msg.MessageType = ScriptMessageSeverityEnum.Error;
                                    msg.IsPublic = true;
                                    break;
                                default:
                                    if (StageRunMgrMap.ContainsKey(chipKey))
                                    {
                                        StageRunMgr srm = StageRunMgrMap[chipKey];
                                        if (srm.ScriptState > ScriptStateEnum.ReadyToRun && srm.ScriptState < ScriptStateEnum.Unknown)
                                            msg.MessageType = ScriptMessageSeverityEnum.Error;
                                        else msg.MessageType = ScriptMessageSeverityEnum.Warning;
                                    }
                                    break;

                            }

                            
                        }
                        SendServiceMessage(msg,chipKey);
                    }

                }
            }
            base.OnAtomicValueChanged(new AtomicValueEventArgs(av));
        }

        private bool GetIOBoardStatus(int value, int index)
        {
            if (index > 7)
            {
                byte result = (byte)((value & (0x01 << index)) >> index);
                return result > 0;
            }
            else if (index == 0)
            {
                BGI.Control.Device.SMCStatus result = (BGI.Control.Device.SMCStatus)(value & 0x0F);
                if (result == BGI.Control.Device.SMCStatus.Normal) return true;
                else return false;
            }
            else if (index == 4)
            {
                BGI.Control.Device.SMCStatus result = (BGI.Control.Device.SMCStatus)((value & 0xF0) >> 4);
                if (result == BGI.Control.Device.SMCStatus.Normal) return true;
                else return false;
            }

            return true;
        }

        private void BeforeRunningCheck(string key, DeviceCheckTypeEnum checkType)
        {
            while (!IsInitDone)
            {
                Thread.Sleep(1000);
            }
            MachineStatusMap[key] = MachineStatus.SelfCheck;
            bool result = CheckDiskSpaceForRun(key,checkType);
            AtomicValue av = new AtomicValue();
            av.ValueName = "DiskStatus";
            av.ValueB = result;
            av.ValueType = AtomicValueTypeEnum.TypeBool;
            MachineStatusMap[key] = MachineStatus.Idle;
            //base.OnAtomicValueChanged(new AtomicValueEventArgs(av));
            OnAtomValueEventHandle(new AtomicValueEventArgs(av));
        }

        private void SendWriteFqStatus(string key, bool isAsync = false)
        {
            if (!isAsync)
            {
                bool isWrite = _WriteFQStatus.Values.Contains(true);
                CreateWriteFqStatus("IsWriteFqA", isWrite);
                CreateWriteFqStatus("IsWriteFqB", isWrite);
            }
            else
            {
                string name = "IsWriteFqA";
                bool isWrite = false;
                if (_WriteFQStatus != null && _WriteFQStatus.ContainsKey(key))
                    isWrite = _WriteFQStatus[key];
                if (key != KEY_FLOWCELL_A) name = "IsWriteFqB";
                AtomicValue av = new AtomicValue();
                av.ValueName = name;
                av.ValueB = isWrite;
                av.ValueType = AtomicValueTypeEnum.TypeBool;
                OnAtomValueEventHandle(new AtomicValueEventArgs(av));
            }

        }

        private void CreateWriteFqStatus(string name, bool isWrite)
        {
            AtomicValue av = new AtomicValue();
            av.ValueName = name;
            av.ValueB = isWrite;
            av.ValueType = AtomicValueTypeEnum.TypeBool;
            OnAtomValueEventHandle(new AtomicValueEventArgs(av));
        }

        public bool CheckWashError(string key)
        {
            if (_WashExceptionMap.ContainsKey(key) && _WashExceptionMap[key] != null)
            {
                return true;
            }
            return false;
        }

        public bool CheckSequenceError(string key)
        {
            if (_SequenceExceptionMap.ContainsKey(key) && _SequenceExceptionMap[key] != null)
            {
                return true;
            }
            return false;
        }

        private void SendQCInfoToUIAndLimis(object sender, Dictionary<string, Dictionary<string, Dictionary<QCInfoName, float>>> qcInfoDic)
        {
            if (_FlowcellCodeMap == null) return;
            bool isSuccess = (bool)sender;
            string slideNo = string.Empty;
            int lane = 0;
            string chipKey = string.Empty;
            Dictionary<string, Dictionary<QCInfoName, float>> qcInfo = null;
            foreach (string key in qcInfoDic.Keys)
            {
                lane = Convert.ToInt32(key.Substring(key.Length - 1, 1));
                slideNo = key.Substring(0, key.Length - 2).Trim();
                qcInfo = qcInfoDic[key];
            }
            foreach (KeyValuePair<string, string> kv in _FlowcellCodeMap)
            {
                if (kv.Value == slideNo)
                {
                    chipKey = kv.Key;
                    break;
                }
            }
            StageRunMgr _StageRunMgr = null;
            if (!string.IsNullOrEmpty(chipKey) && StageRunMgrMap != null && StageRunMgrMap.ContainsKey(chipKey))
            {
                _StageRunMgr = StageRunMgrMap[chipKey];
            }
            if (isSuccess && qcInfo != null)
            {
                List<QCDataValue> qcDatas = StatisticsQCDatas(qcInfo, lane, slideNo);
                _StageRunMgr?.AddQCValues(qcDatas);
                SendQCDatasTOUI(qcDatas, _StageRunMgr);
                SendQcDatasToLimis(qcDatas);
            }
            else
            {
                _Logger.Log()(LogSeverityEnum.Error, $"Get qc data error,not send qc to UI and zlims,Slide = {slideNo} Lane={lane} Cycle={_Scanner?.CycleNumber}");
            }

        }

        private void SendQCDatasTOUI(List<QCDataValue> qcDatas, StageRunMgr stageRunMgr)
        {
            stageRunMgr?.StageService.SendQCValues(qcDatas.ToArray());
        }

        private void SendQcDatasToLimis(List<QCDataValue> qcDatas)
        {

        }
        private List<QCDataValue> StatisticsQCDatas(Dictionary<string, Dictionary<QCInfoName, float>> qcInfoDic, int lane,string slide)
        {
            List<QCDataValue> qcDataValues = new List<QCDataValue>();
            List<Dictionary<QCInfoName, float>> qcValues = qcInfoDic.Select(p => p.Value).ToList();

            foreach (Dictionary<QCInfoName, float> qcValueDic in qcValues)
            {
                foreach (KeyValuePair<QCInfoName, float> kvp in qcValueDic)
                {
                    string tempValueName = kvp.Key.valueName.ToUpper();
                    if (kvp.Key.valueName.ToUpper().Contains("Q30"))
                        tempValueName = "Q30";
                    QCDataValue qcData = qcDataValues.Find(p => p.cycleNum == kvp.Key.cycle && string.Compare(p.QCMetricName.Substring(2, p.QCMetricName.Length - 2), tempValueName, true) == 0);
                    if (qcData != null)
                    {

                        if (!Double.IsNaN(kvp.Value))
                        {
                            if (string.Compare("Q30", tempValueName, true) == 0 || 
                                string.Compare("fit", tempValueName, true) == 0 || 
                                string.Compare("bic", tempValueName, true) == 0 ||
                                string.Compare("ESR",tempValueName,true) ==0)
                            {
                                qcData.QCValue += (kvp.Value * 100);
                            }
                            else
                                qcData.QCValue += kvp.Value;
                        }

                    }
                    else
                    {
                        string metricsName = kvp.Key.valueName.ToUpper();
                        //
                        if (!Double.IsNaN(kvp.Value))
                        {
                            //if (metricsName.Contains("Q30") || metricsName.Contains("ESR") || metricsName.Contains("BIC") || metricsName.Contains("FIT"))
                            if (string.Compare("CycQ30", metricsName, true) == 0 || 
                                string.Compare("fit", metricsName, true) == 0 || 
                                string.Compare("bic", metricsName, true) == 0 ||
                                string.Compare("ESR", metricsName, true) == 0)
                            {
                                if (string.Compare("CycQ30", metricsName, true) == 0) metricsName = "Q30";
                                qcDataValues.Add(new QCDataValue() { cycleNum = kvp.Key.cycle, QCMetricName = $"{lane}-{metricsName}", QCValue = kvp.Value * 100 });
                            }
                            else
                                qcDataValues.Add(new QCDataValue() { cycleNum = kvp.Key.cycle, QCMetricName = $"{lane}-{metricsName}", QCValue = kvp.Value });
                        }

                        else
                            qcDataValues.Add(new QCDataValue() { cycleNum = kvp.Key.cycle, QCMetricName = $"{lane}-{metricsName}", QCValue = 0 });
                    }
                }
            }

            if (qcDataValues.Count > 0)
            {
                qcDataValues.Sort((a, b) => { return a.cycleNum.CompareTo(b.cycleNum); });
                qcDataValues.ForEach(p => p.QCValue = p.QCValue / qcInfoDic.Count);
            }
            Task.Run(() =>
            {
                List<QCDataValue> qcList = qcDataValues.FindAll(a =>
                    string.Compare(a.QCMetricName, $"{lane}-Signal_A", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-Signal_G", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-Signal_C", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-Signal_T", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-SNR_A", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-SNR_G", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-SNR_C", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-SNR_T", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-bic", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-fit", true) == 0 ||
                    string.Compare(a.QCMetricName, $"{lane}-Q30", true) == 0);
                if (qcList != null)
                {
                    foreach (QCDataValue qc in qcList)
                    {
                        _QCLogger.Log()(LogSeverityEnum.Info, $"Slide ={slide} Lane={lane} Cycle={qc.cycleNum} MetricsName={qc.QCMetricName} Value = {qc.QCValue}");
                    }
                }
            });

            return qcDataValues;
        }

        private void WriteFQAysn()
        {
            while (true)
            {
                if (!_WriteFQStatus.Values.Contains(true))
                {
                    Thread.Sleep(100);
                    continue;
                }
                foreach (string key in _WriteFQStatus.Keys)
                {
                    if (_WriteFQStatus[key])
                    {
                        if (_FlowcellCodeMap.ContainsKey(key))
                        {
                            SendWriteFqStatus(key, this._IsWriteFqAsync);
                            bool tempRemove = false;
                            SendInfoAlarm(Properties.Resource.strDataHandling, "", key, false);

                            string slideId = _FlowcellCodeMap[key];
                            if (!string.IsNullOrEmpty(slideId) && !string.IsNullOrWhiteSpace(slideId))
                            {
                                _Logger.Log()(LogSeverityEnum.Info, $"Start write fq for slide {slideId}");

                                RunInfo tempRuninfo = null;
                                if (!_ScannerExperimentInfo.ContainsKey(key))
                                {
                                    _Logger.Log()(LogSeverityEnum.Error, $"ExperimentInfo is null for key={key}");
                                    continue;
                                }
                                tempRuninfo = _ScannerExperimentInfo[key];
                                try
                                {
                                    if (ImageBufferExt.Get.IsOnlineBaseCall)
                                    {
                                        #region write fastq files
                                        bool isWriteFastqSuccess = true;
                                        this._Logger.Log(LogSeverityEnum.Info, $"START, Start writing fastq file for Slide {slideId} ...");

                                        Dictionary<int, bool> _laneResultMap = new Dictionary<int, bool>();
                                        Dictionary<int, BaseCallClient> _FQBasecallMap = new Dictionary<int, BaseCallClient>();
                                        int slideIndex = 0;
                                        ScanParams scanParams = new ScanParams();
                                        if (key == KEY_FLOWCELL_A)
                                        {
                                            scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[0], _FlowcellType[0]);
                                            slideIndex = 0;
                                        }
                                        else
                                        {
                                            scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[1], _FlowcellType[1]);
                                            slideIndex = 1;
                                        }

                                        for (int lane = 0; lane < _Slide.NumLanes; lane++)
                                        {
                                            if (scanParams.ScanAreas[lane].MaxColNum == 0 || scanParams.ScanAreas[lane].MaxRowNum == 0)
                                            {
                                                continue;
                                            }
                                            _laneResultMap.Add(lane, false);
                                            _FQBasecallMap.Add(lane, GetClient(lane));
                                        }
                                        _Logger.Log()(LogSeverityEnum.Info, $"FQBasecallMap Count is {_FQBasecallMap.Count} & Result Map count={_laneResultMap.Count}");
                                        for (int lane = 0; lane < _Slide.NumLanes; lane++)
                                        {
                                            if (scanParams.ScanAreas[lane].MaxColNum == 0 || scanParams.ScanAreas[lane].MaxRowNum == 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                int laneIndex = lane;
                                                _Logger.Log()(LogSeverityEnum.Info, $"Write FQ with lane Index = {laneIndex}");
                                                if (_laneResultMap.ContainsKey(laneIndex)) _laneResultMap[laneIndex] = false;
                                                else _laneResultMap.Add(laneIndex, false);
                                                int maxRow = 0;
                                                int maxCol = 0;
                                                if (!scanParams.ScanAreas.ContainsKey(laneIndex))
                                                {
                                                    _Logger.Log()(LogSeverityEnum.Error, $"ScannerParams not contains lane index={laneIndex}");
                                                    _WriteFQStatus.TryRemove(key, out tempRemove);
                                                    return;
                                                }
                                                maxRow = scanParams.ScanAreas[laneIndex].MaxRowNum;
                                                maxCol = scanParams.ScanAreas[laneIndex].MaxColNum;
                                                if (!_FQBasecallMap.ContainsKey(laneIndex))
                                                {
                                                    _laneResultMap[laneIndex] = true;
                                                    _Logger.Log()(LogSeverityEnum.Error, $"FQBasecallMap not contains lane index={laneIndex}");
                                                    _WriteFQStatus.TryRemove(key, out tempRemove);
                                                    return;
                                                }
                                                if (_FQBasecallMap[laneIndex] == null)
                                                {
                                                    _Logger.Log()(LogSeverityEnum.Error, $"FQBasecallMap lane index={laneIndex} is null");
                                                    _WriteFQStatus.TryRemove(key, out tempRemove);
                                                    return;
                                                }
                                                while (!_FQBasecallMap[laneIndex].IsOnline)
                                                {
                                                    continue;
                                                }
                                                BatchParameters para = new BatchParameters();
                                                para.read1Length = tempRuninfo.Read1Len;
                                                para.read2Length = tempRuninfo.Read2Len;
                                                para.barcodeLength = tempRuninfo.BarcodeLen;
                                                para.barcodePos = BarcodePosition.BarcodePosEnd;
                                                EndCycProcessMode endProcessMode = GetEndCycProcessMode(key);
                                                Stopwatch timer = new Stopwatch();
                                                timer.Start();
                                                int fastqValue = _FQBasecallMap[laneIndex].WriteFQ(slideId, laneIndex + 1, maxRow, maxCol,slideIndex, para,endProcessMode);
                                                timer.Stop();
                                                if (fastqValue != -1)
                                                {
                                                    Dictionary<string, string[]> fastqFiles = _FQBasecallMap[laneIndex].GetFastqFiles(slideId, laneIndex + 1, tempRuninfo.TotalReads);
                                                    string local_fq_path = string.Empty;
                                                    foreach (string[] valueItem in fastqFiles.Values)
                                                    {
                                                        if (valueItem != null && valueItem.Length > 0)
                                                        {
                                                            string firstFileName = valueItem.First();
                                                            local_fq_path = Path.Combine(firstFileName.Substring(0, firstFileName.IndexOf(slideId)), slideId, $"L{(laneIndex + 1).ToString().PadLeft(2, '0')}").Replace("\\", "/");
                                                            if (_UploadFqFileMap == null) _UploadFqFileMap = new ConcurrentDictionary<string, string>();
                                                            if (!_UploadFqFileMap.ContainsKey(local_fq_path)) _UploadFqFileMap.TryAdd(local_fq_path, slideId);
                                                        }
                                                        if (_IsUploadFq)
                                                        {
                                                            if (!string.IsNullOrEmpty(local_fq_path))
                                                            {
                                                                if (_UploadFqTask == null || _UploadFqTask.Status != TaskStatus.Running)
                                                                {
                                                                    _UploadFqTask = Task.Run(() =>
                                                                    {
                                                                        UploadFq();
                                                                    });
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    _laneResultMap[laneIndex] = true;
                                                    this._Logger.Log(LogSeverityEnum.Info, "SUCCESS, Write fastq file successfully about lane " + (laneIndex + 1).ToString() + ", it spent about " + timer.Elapsed.ToString() + ".");
                                                }
                                                else
                                                {
                                                    _laneResultMap[laneIndex] = false;
                                                    this._Logger.Log(LogSeverityEnum.Error, "FAILURE, Write fastq file failure about lane " + (laneIndex + 1).ToString() + ", it spent about " + timer.Elapsed.ToString() + ".");
                                                }
                                            }

                                        }
                                        if (_laneResultMap.ContainsValue(false)) isWriteFastqSuccess = false;
                                        else isWriteFastqSuccess = true;
                                        #endregion

                                        if (isWriteFastqSuccess)
                                        {
                                            this._Logger.Log(LogSeverityEnum.Info, "SUCCESS, Write all fastq file success.");
                                        }
                                        else
                                        {
                                            this._Logger.Log(LogSeverityEnum.Error, "ERROR, Write fastq file failure.");
                                        }
                                    }
                                    _WriteFQStatus.TryRemove(key, out tempRemove);
                                }
                                catch (Exception ex)
                                {
                                    _Logger.Log()(LogSeverityEnum.Error, "write fastq and send fastq ex={0}", ex);
                                }
                                finally
                                {
                                    _WriteFQStatus[key] = false;
                                    _FlowcellCodeMap[key] = string.Empty;
                                    //SendInfoAlarm(Properties.Resource.strDataHandled, "", key, false);
                                    RemoveInfoAlarm(Properties.Resource.strDataHandling, Properties.Resource.strDataHandled, "", key, false);
                                    SendWriteFqStatus(key, this._IsWriteFqAsync);
                                }
                            }
                            _Logger.Log()(LogSeverityEnum.Info, $"Finished write fq for {slideId}");

                        }

                    }
                }
                if (!_WriteFQStatus.Values.Contains(true))
                {
                    _IsWriteFQ = false;
                    _Logger.Log()(LogSeverityEnum.Info, $"All slide finished should set IsWrietFq with false");
                    if (_IsUploadImg)
                    {
                        UploadImage();
                    }
                    break;
                }
            }
        }

        private void WriteFQ(string key)
        {
            if (!_WriteFQStatus.ContainsKey(key) || !_WriteFQStatus[key])
            {
                _Logger.Log()(LogSeverityEnum.Info, $"For {key} already write fq");
                return;
            }

            string slideId = _FlowcellCodeMap[key];
            if (string.IsNullOrEmpty(slideId) || string.IsNullOrWhiteSpace(slideId))
            {
                _Logger.Log()(LogSeverityEnum.Warning, $"Slide is null for key={key}");
                return;
            }
            RunInfo tempRuninfo = null;
            if (!_ScannerExperimentInfo.ContainsKey(key))
            {
                _Logger.Log()(LogSeverityEnum.Error, $"ExperimentInfo is null for key={key}");
                return;
            }
            tempRuninfo = _ScannerExperimentInfo[key];
            try
            {
                if (ImageBufferExt.Get.IsOnlineBaseCall)
                {
                    #region write fastq files
                    bool isWriteFastqSuccess = true;
                    this._Logger.Log(LogSeverityEnum.Info, $"START, Start writing fastq file for Slide {slideId} ...");

                    Dictionary<int, bool> _laneResultMap = new Dictionary<int, bool>();
                    Dictionary<int, BaseCallClient> _FQBasecallMap = new Dictionary<int, BaseCallClient>();
                    int slideIndex = 0;
                    ScanParams scanParams = new ScanParams();
                    if (key == KEY_FLOWCELL_A)
                    {
                        scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[0], _FlowcellType[0]);
                        slideIndex = 0;
                    }
                    else
                    {
                        scanParams.ScanAreasParam = GetScanParameters(_scannerParameters[1], _FlowcellType[1]);
                        slideIndex = 1;
                    }

                    for (int lane = 0; lane < _Slide.NumLanes; lane++)
                    {
                        if (scanParams.ScanAreas[lane].MaxColNum == 0 || scanParams.ScanAreas[lane].MaxRowNum == 0)
                        {
                            continue;
                        }
                        _laneResultMap.Add(lane, false);
                        _FQBasecallMap.Add(lane, GetClient(lane));
                    }
                    _Logger.Log()(LogSeverityEnum.Info, $"FQBasecallMap Count is {_FQBasecallMap.Count} & Result Map count={_laneResultMap.Count}");
                    for (int lane = 0; lane < _Slide.NumLanes; lane++)
                    {
                        if (scanParams.ScanAreas[lane].MaxColNum == 0 || scanParams.ScanAreas[lane].MaxRowNum == 0)
                        {
                            continue;
                        }
                        else
                        {
                            int laneIndex = lane;
                            _Logger.Log()(LogSeverityEnum.Info, $"Write FQ with lane Index = {laneIndex}");
                            if (_laneResultMap.ContainsKey(laneIndex)) _laneResultMap[laneIndex] = false;
                            else _laneResultMap.Add(laneIndex, false);
                            int maxRow = 0;
                            int maxCol = 0;
                            if (!scanParams.ScanAreas.ContainsKey(laneIndex))
                            {
                                _Logger.Log()(LogSeverityEnum.Error, $"ScannerParams not contains lane index={laneIndex}");
                                return;
                            }
                            maxRow = scanParams.ScanAreas[laneIndex].MaxRowNum;
                            maxCol = scanParams.ScanAreas[laneIndex].MaxColNum;
                            bool isFastChip = false;
                            if (key == KEY_FLOWCELL_A)
                            {
                                isFastChip = _FlowcellType[0];
                            }
                            else isFastChip = _FlowcellType[1];
                            if (isFastChip)
                            {
                                if (laneIndex > 1) continue;
                                if (scanParams.ScanAreas[laneIndex].MaxColNum > 3)
                                    maxCol = 3;
                            }
                            if (!_FQBasecallMap.ContainsKey(laneIndex))
                            {
                                _laneResultMap[laneIndex] = true;
                                _Logger.Log()(LogSeverityEnum.Error, $"FQBasecallMap not contains lane index={laneIndex}");
                                return;
                            }
                            if (_FQBasecallMap[laneIndex] == null)
                            {
                                _Logger.Log()(LogSeverityEnum.Error, $"FQBasecallMap lane index={laneIndex} is null");
                                return;
                            }
                            while (!_FQBasecallMap[laneIndex].IsOnline)
                            {
                                continue;
                            }
                            BatchParameters para = new BatchParameters();
                            para.read1Length = tempRuninfo.Read1Len;
                            para.read2Length = tempRuninfo.Read2Len;
                            para.barcodeLength = tempRuninfo.BarcodeLen;
                            para.barcodePos = BarcodePosition.BarcodePosEnd;
                            EndCycProcessMode endProcessMode = GetEndCycProcessMode(key);
                            Stopwatch timer = new Stopwatch();
                            timer.Start();
                            int fastqValue = _FQBasecallMap[laneIndex].WriteFQ(slideId, laneIndex + 1, maxRow, maxCol,slideIndex, para,endProcessMode);
                            timer.Stop();
                            if (fastqValue != -1)
                            {
                                //Dictionary<string, string[]> fastqFiles = _FQBasecallMap[laneIndex].GetFastqFiles();
                                Dictionary<string, string[]> fastqFiles = _FQBasecallMap[laneIndex].GetFastqFiles(slideId, laneIndex + 1, tempRuninfo.TotalReads);
                                string local_fq_path = string.Empty;
                                foreach (string[] valueItem in fastqFiles.Values)
                                {
                                    if (valueItem != null && valueItem.Length > 0)
                                    {
                                        string firstFileName = valueItem.First();
                                        //local_fastq_path = Path.Combine(firstFileName.Substring(0, firstFileName.IndexOf(_Slide.Barcode)), _Slide.Barcode);
                                        //string local_fastq_path = firstFileName.Substring(0, firstFileName.IndexOf("OutputFq") - 1);

                                        //local_fq_path = firstFileName.Substring(0, firstFileName.IndexOf("Outputfq", StringComparison.OrdinalIgnoreCase) - 1);

                                        local_fq_path = Path.Combine(firstFileName.Substring(0, firstFileName.IndexOf(slideId)), slideId, $"L{(laneIndex + 1).ToString().PadLeft(2, '0')}").Replace("\\", "/");
                                        if (_UploadFqFileMap == null) _UploadFqFileMap = new ConcurrentDictionary<string, string>();
                                        if (!_UploadFqFileMap.ContainsKey(local_fq_path)) _UploadFqFileMap.TryAdd(local_fq_path, slideId);

                                        //string tempPath = firstFileName.Substring(0, firstFileName.IndexOf(slideId)).Replace("OutputFq", "workspace");
                                        //local_fq_path = Path.Combine(tempPath, slideId, $"L{(laneIndex + 1).ToString().PadLeft(2, '0')}").Replace("\\", "/");
                                        //if (_UploadMetricsFileMap == null) _UploadMetricsFileMap = new ConcurrentDictionary<string, string>();
                                        //if (!_UploadMetricsFileMap.ContainsKey(local_fq_path)) _UploadMetricsFileMap.TryAdd(local_fq_path, slideId);
                                    }
                                    //if (_IsUploadFq)
                                    //{
                                    //    if (!string.IsNullOrEmpty(local_fq_path))
                                    //    {
                                    //        if (_UploadFqTask == null || _UploadFqTask.Status != TaskStatus.Running)
                                    //        {
                                    //            _UploadFqTask = Task.Run(() =>
                                    //            {
                                    //                UploadFq();
                                    //            });
                                    //        }
                                    //        break;
                                    //    }
                                    //}
                                    
                                }
                                _laneResultMap[laneIndex] = true;
                                this._Logger.Log(LogSeverityEnum.Info, "SUCCESS, Write fastq file successfully about lane " + (laneIndex + 1).ToString() + ", it spent about " + timer.Elapsed.ToString() + ".");
                            }
                            else
                            {
                                _laneResultMap[laneIndex] = false;
                                this._Logger.Log(LogSeverityEnum.Error, "FAILURE, Write fastq file failure about lane " + (laneIndex + 1).ToString() + ", it spent about " + timer.Elapsed.ToString() + ".");
                            }
                        }

                    }
                    if (_laneResultMap.ContainsValue(false)) isWriteFastqSuccess = false;
                    else isWriteFastqSuccess = true;
                    #endregion

                    if (isWriteFastqSuccess)
                    {
                        this._Logger.Log(LogSeverityEnum.Info, "SUCCESS, Write all fastq file success.");
                    }
                    else
                    {
                        this._Logger.Log(LogSeverityEnum.Error, "ERROR, Write fastq file failure.");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, "write fastq and send fastq ex={0}", ex);
            }
        }

        private EndCycProcessMode GetEndCycProcessMode(string key)
        {
            if(_EndCycProcessModeCollection != null && _EndCycProcessModeCollection.Count >0)
            {
                if(_EndCycProcessModeCollection.Count > 1)
                {
                    if(key == KEY_FLOWCELL_A)
                    {
                        int temp = 0;
                        if(int.TryParse(_EndCycProcessModeCollection[0],out temp))
                        {
                            if (temp >= (int)EndCycProcessMode.R1R2None && temp <= (int)EndCycProcessMode.R1R2Both)
                                return (EndCycProcessMode)temp;
                        }
                    }
                    else if(key == KEY_FLOWCELL_B)
                    {
                        int temp = 0;
                        if (int.TryParse(_EndCycProcessModeCollection[1], out temp))
                        {
                            if (temp >= (int)EndCycProcessMode.R1R2None && temp <= (int)EndCycProcessMode.R1R2Both)
                                return (EndCycProcessMode)temp;
                        }
                    }
                }
                else
                {
                    int temp = 0;
                    if (int.TryParse(_EndCycProcessModeCollection[0], out temp))
                    {
                        if (temp >= (int)EndCycProcessMode.R1R2None && temp <= (int)EndCycProcessMode.R1R2Both)
                            return (EndCycProcessMode)temp;
                    }
                }
            }
            return EndCycProcessMode.R1R2None;
        }

        private void UploadImage()
        {
            while (_IsUploadImg)
            {
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    foreach (string s in _UploadImgFileMap.Values)
                    {
                        DirectoryInfo imgDir = new DirectoryInfo(s);
                        if (!imgDir.Exists)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"this folder {s} not exists");
                            continue;
                        }
                        DirectoryInfo[] subDir = imgDir.GetDirectories();
                        if(subDir == null || subDir.Length == 0)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"this floder {s} is empty");
                            continue;
                        }
                        foreach(DirectoryInfo folder in subDir)
                        {
                            for (int i = 0; i < _UploadServer.Count; i++)
                            {
                                int uploadTimes = 0;
                                bool isUploadFastqFilesSuccess = false;
                                Stopwatch timer = new Stopwatch();
                                timer.Start();
                                while (!isUploadFastqFilesSuccess && uploadTimes < 3)
                                {
                                    try
                                    {
                                        string local = folder.FullName.Replace(":", "").Replace(@"\", @"/");
                                        // upload fastq files
                                        isUploadFastqFilesSuccess = zlimsApiService.UploadFastqFiles(
                                            _UploadServer[i],
                                            _UploadUser[i],
                                            _UploadPwd[i],
                                            local,
                                            Path.Combine(_UploadServerGroup[i], imgDir.Name).Replace(@"\", @"/"));
                                    }
                                    catch (Exception e)
                                    {
                                        _Logger.Log()(LogSeverityEnum.Error, "Exception, Upload fastq file exception, the message is: {0}", e.ToString());
                                    }

                                    uploadTimes++;
                                    if (!isUploadFastqFilesSuccess)
                                    {
                                        Thread.Sleep(60 * 1000);
                                    }
                                }
                                timer.Stop();
                                _Logger.Log()(LogSeverityEnum.Info, $"Upload folder {s} elapsed {timer.ElapsedMilliseconds} ms");
                                if (isUploadFastqFilesSuccess || uploadTimes >= 3)
                                {
                                    if (uploadTimes >= 3 && !isUploadFastqFilesSuccess)
                                    {
                                        _Logger.Log()(LogSeverityEnum.Error, $"upload folder {s} failed after retry {uploadTimes}");
                                    }
                                    string temp = string.Empty;
                                    bool isRemoved = _UploadImgFileMap.TryRemove(s, out temp);
                                    _Logger.Log()(LogSeverityEnum.Info, $"Removed {s} result is {isRemoved}");
                                }
                            }
                        }
                        
                    }
                }

            }
        }

        private void UploadFq()
        {
            while (_IsUploadFq)
            {
                if(_UploadFqFileMap == null || _UploadFqFileMap.Count == 0)
                {
                    _Logger.Log()(LogSeverityEnum.Info, $"no folder for upload");
                    break;
                }
                using (var scope = _container.BeginLifetimeScope())
                {
                    var zlimsApiService = scope.Resolve<IZLimsApiService>();
                    foreach (string s in _UploadFqFileMap.Keys)
                    {
                        for(int i =0;i < _UploadServer.Count; i++)
                        {
                            int uploadTimes = 0;
                            bool isUploadFastqFilesSuccess = false;
                            Stopwatch timer = new Stopwatch();
                            timer.Start();
                            while(!isUploadFastqFilesSuccess && uploadTimes < 3)
                            {
                                try
                                {
                                    string local = s.Replace(":", "").Replace(@"\", @"/");
                                    // upload fastq files
                                    isUploadFastqFilesSuccess = zlimsApiService.UploadFastqFiles(
                                        _UploadServer[i],
                                        _UploadUser[i],
                                        _UploadPwd[i],
                                        local,
                                        Path.Combine(_UploadServerGroup[i], _UploadFqFileMap[s]).Replace(@"\", @"/"));

                                    string[] parseLocal = local.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                                    string laneNo = string.Empty;
                                    string metricsPath = string.Empty;
                                    foreach(string p in parseLocal)
                                    {
                                        string strTemp = p;
                                        if (string.Compare(p, "outputfq", true) == 0)
                                            strTemp = "workspace";
                                        metricsPath += strTemp + "/";
                                        laneNo = p;
                                    }
                                    metricsPath += "metrics";

                                    //upload metrics files
                                    isUploadFastqFilesSuccess = zlimsApiService.UploadFastqFiles(
                                        _UploadServer[i],
                                        _UploadUser[i],
                                        _UploadPwd[i],
                                        metricsPath,
                                        Path.Combine(_UploadServerGroup[i], _UploadFqFileMap[s],laneNo).Replace(@"\", @"/"));
                                }
                                catch (Exception e)
                                {
                                    _Logger.Log()(LogSeverityEnum.Error, "Exception, Upload fastq file exception, the message is: {0}", e.ToString());
                                }

                                uploadTimes++;
                                if (!isUploadFastqFilesSuccess)
                                {
                                    Thread.Sleep(60 * 1000);
                                }
                            }
                            timer.Stop();
                            _Logger.Log()(LogSeverityEnum.Info, $"Upload folder {s} elapsed {timer.ElapsedMilliseconds} ms");
                            if (isUploadFastqFilesSuccess || uploadTimes>= 3)
                            {
                                if(uploadTimes >= 3 && !isUploadFastqFilesSuccess)
                                {
                                    _Logger.Log()(LogSeverityEnum.Error, $"upload folder {s} failed after retry {uploadTimes}");
                                }
                                string temp = string.Empty;
                                bool isRemoved = _UploadFqFileMap.TryRemove(s, out temp);
                                _Logger.Log()(LogSeverityEnum.Info, $"Removed {s} result is {isRemoved}");
                            }
                        }
                    }
                }
                
            }
        }

        /// <summary>
        /// Used at Phase Finished
        /// </summary>
        /// <param name="stepKey"></param>
        /// <param name="phaseType"></param>
        /// <param name="chipKey"></param>
        private void CaculateRemainingTime(string stepKey, MessageType phaseType, string chipKey)
        {
            try
            {
                RunInfo CurrentInfo = null;
                if (_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(chipKey))
                    CurrentInfo = _ScannerExperimentInfo[chipKey];

                int[] tmpKey = SplitOperationKey(stepKey);
                if (tmpKey == null)
                {
                    _Logger.Log()(LogSeverityEnum.Warning, $"With Invalid Step {stepKey}");
                    return;
                }
                int operationInt = tmpKey[1];
                switch (phaseType)
                {
                    case MessageType.Wash:
                    case MessageType.Cleantubing:
                    case MessageType.Wash_Cleantubing:
                    case MessageType.WashNaOH:
                        SendStageRemainingTime(0, chipKey);
                        washCleantubeStats.Stop();
                        double caculateTime = Convert.ToDouble((washCleantubeStats.Avg / 60000).ToString("F2"));
                        string paraName = "WashTimeMinute";
                        _washTimeMinute = caculateTime;
                        if(phaseType == MessageType.WashNaOH)
                        {
                            paraName = "WashWithNaOHTimeMinute";
                            _washWithNaOHTimeMinute = caculateTime;
                        }
                        else if(phaseType == MessageType.Wash_Cleantubing)
                        {
                            paraName = "WashWithCleantubeTimeMinute";
                            _washWithCleantubeTimeMinute = caculateTime;
                        }
                        else if(phaseType == MessageType.Cleantubing)
                        {
                            paraName = "CleanTubeTimeMinute";
                            _cleanTubeMinute = caculateTime;
                        }
                        bool isConfigOnline = ConfigMgr.Get.IsOnline;
                        try
                        {
                            SaveConfigPara("System", paraName, caculateTime, isConfigOnline);
                        }
                        catch(Exception ex)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"Update {paraName} as {caculateTime} isOnline={isConfigOnline} failed {ex.ToString()}");
                        }
#if DEBUG
                        _Logger.Log()(LogSeverityEnum.Info, $"CaculateRemainingTime,scriptType={stepKey},remaining time={caculateTime} minutes");
#endif
                        if (_IsSendTotalTime.ContainsKey(chipKey)) _IsSendTotalTime[chipKey] = null;
                        else _IsSendTotalTime.Add(chipKey, null);
                        break;
                    case MessageType.Sequencing:
                    case MessageType.BarcodeSequencing:
                    case MessageType.Sequencing_Read1:
                    case MessageType.Sequencing_Read2:
                        if (CurrentInfo == null) return;
                        if (tmpKey[1] == 0 && tmpKey[2] == 0)
                        {
                            SendStageRemainingTime(0, chipKey);
                            if (_IsSendTotalTime.ContainsKey(chipKey)) _IsSendTotalTime[chipKey] = null;
                            else _IsSendTotalTime.Add(chipKey, null);
                        }
                        else
                        {
                            CaculateTimeByCycle(operationInt - 1, stepKey, chipKey);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"caculate remaining time error,ex= {ex}");
            }
        }

        private void CaculateTimeByCycle(int index, string stepKey, string chipKey)
        {
            StageRunMgr _StageRunMgr = null;
            RunInfo _currentInfo = null;
            if (StageRunMgrMap != null && StageRunMgrMap.ContainsKey(chipKey))
            {
                _StageRunMgr = StageRunMgrMap[chipKey];
            }
            if (_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(chipKey))
                _currentInfo = _ScannerExperimentInfo[chipKey];

            ImagingCycle _currentCycle = null;
            if (_ScannerCycle != null && _ScannerCycle.ContainsKey(chipKey)) _currentCycle = _ScannerCycle[chipKey];
            if (_currentInfo == null || _currentCycle == null) return;
            double time = 0;

            StopwatchStats[] _sequenceStats = null;
            if (_SequenceWatchStatsMap.ContainsKey(chipKey))
            {
                _sequenceStats = _SequenceWatchStatsMap[chipKey];
            }
            if (_sequenceStats == null) return;
            if (_sequenceStats.Length > index)
            {
                _sequenceStats[index].Stop();
#if DEBUG
                _Logger.Log()(LogSeverityEnum.Info, $"sequenceWatchStats[{index}] stop,stepKey={stepKey},time={_sequenceStats[index].Last}");
#endif

                //one cycle is complete
                if (index == _sequenceStats.Length - 1)
                {
                    double realCleavageTime = 0;
                    time = WriteCycleTimeMetrics(chipKey, false, out realCleavageTime);
                    if (!oneCycleRealTime.ContainsKey(chipKey)) oneCycleRealTime.Add(chipKey, oneCycleTime);
                    if(_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(chipKey) && _ScannerExperimentInfo[chipKey] != null)
                    {

                        if (!realCleavageTimeMap.ContainsKey(chipKey))
                        {
                            double cleavageTime = 3.5;
                            if (_currentCycle <= 1)
                            {
                                double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                            }
                            else cleavageTime = realCleavageTime;
                            realCleavageTimeMap.Add(chipKey, cleavageTime);
                        }
                        UpdateTotalTime(chipKey, (_ScannerExperimentInfo[chipKey].TotalReads) * oneCycleRealTime[chipKey] - realCleavageTimeMap[chipKey], (_ScannerExperimentInfo[chipKey].TotalReads) * time - realCleavageTime);
                        //UpdateTotalTime(chipKey, (_ScannerExperimentInfo[chipKey].TotalReads) * oneCycleRealTime[chipKey], (_ScannerExperimentInfo[chipKey].TotalReads) * time);
                    }
                    oneCycleRealTime[chipKey] = time;
                    realCleavageTimeMap[chipKey] = realCleavageTime;
                }
                else if (_currentInfo.TotalReads == _currentCycle
                    && index == _sequenceStats.Length - 2)
                {
                    //last cycle,no cleavage
                    double realCleavageTime = 0;
                    WriteCycleTimeMetrics(chipKey,true,out realCleavageTime);
                }
            }
        }

        private void SendTotalTime(string stepKey, string chipKey)
        {
            if (StageRunMgrMap == null || !StageRunMgrMap.ContainsKey(chipKey))
                return;
            StageRunMgr _StageRunMgr = StageRunMgrMap[chipKey];
            if (_StageRunMgr == null) return;
            RunInfo currentInfo = null;
            if (_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(chipKey))
                currentInfo = _ScannerExperimentInfo[chipKey];
            double _TotalTimes = 0;
            if (_TotalTimesMap != null && _TotalTimesMap.ContainsKey(chipKey))
                _TotalTimes = _TotalTimesMap[chipKey];
            if (string.IsNullOrEmpty(stepKey) || string.IsNullOrWhiteSpace(stepKey))
            {
                _Logger.Log()(LogSeverityEnum.Error, $"send total time error,step key is null");
                return;
            }
            int[] tempKey = SplitOperationKey(stepKey);
            if (tempKey == null)
            {
                _Logger.Log()(LogSeverityEnum.Error, $"send total time has invalid value {stepKey}");
                return;
            }
            MessageType phaseType = MessageType.None;
            if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(chipKey)) phaseType = _PhaseTypeMaps[chipKey];
            switch (phaseType)
            {
                case MessageType.Wash:
                    washCleantubeStats.Start();
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                        SendStageRemainingTime(WashTimeMinute, chipKey);
                    break;
                case MessageType.Cleantubing:
                    washCleantubeStats.Start();
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                        SendStageRemainingTime(CleanTubeTimeMinute, chipKey);
                    break;
                case MessageType.WashNaOH:
                    washCleantubeStats.Start();
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                        SendStageRemainingTime(WashWithNaOHTimeMinute, chipKey);
                    break;
                case MessageType.Wash_Cleantubing:
                    washCleantubeStats.Start();
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                        SendStageRemainingTime(WashWithCleantubeTimeMinute, chipKey);
                    break;
                case MessageType.LoadDNB:
                case MessageType.PostLoad:
                    //if (tempKey[1] == "1" && (currentInfo != null))
                    if ((currentInfo != null))
                    {
                        if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                        {
                            int totalSteps = Convert.ToInt32(tempKey[2]);
                            int configCount = LoadDnbTimeMinute.Count;
                            double temploadTime = 0;
                            if (totalSteps < configCount)
                            {
                                for (int i = 0; i < configCount - totalSteps; i++)
                                {
                                    double tempTime = 0;
                                    double.TryParse(LoadDnbTimeMinute[i], out tempTime);
                                    temploadTime += tempTime;
                                }
                            }
                            else temploadTime = loadDNBTime;

                            double cleavageTime = 0;
                            double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                            double totalTime = _TotalTimesMap[chipKey] - temploadTime - cleavageTime;
                            UpdateTotalTime(chipKey, loadDNBTime + cleavageTime, temploadTime);
                            SendStageRemainingTime(totalTime, chipKey);
                            loadDNBTime = temploadTime;
                        }
                    }
                    break;
                case MessageType.SeqPrime:
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                    {
                        double seqPrimeTime = GetSeqPrimTime();
                        double cleavageTime = 0;
                        double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                        double totalTime = _TotalTimesMap[chipKey] -loadDNBTime - cleavageTime;
                        UpdateTotalTime(chipKey, loadDNBTime+ cleavageTime, 0);
                        SendStageRemainingTime(totalTime, chipKey);
                    }
                    break;
                case MessageType.PESynthesis:
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                    {
                        if(currentInfo != null)
                        {
                            double cleavageTime = 0;
                            double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                            double unusedTime = loadDNBTime +seqPrimTime + cleavageTime;
                            double totalTime = _TotalTimesMap[chipKey] - unusedTime;
                            UpdateTotalTime(chipKey, unusedTime, 0);
                            SendStageRemainingTime(totalTime, chipKey);
                        }
                    }
                    break;
                case MessageType.BarcodeSynthesis:
                case MessageType.BarcodeSynthesisSE:
                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                    {
                        if (currentInfo != null)
                        {
                            double cleavageTime = 0;
                            double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                            double unusedTime = loadDNBTime + seqPrimTime + cleavageTime + peSynthesisTime;
                            double totalTime = _TotalTimesMap[chipKey] - unusedTime;
                            UpdateTotalTime(chipKey, unusedTime, 0);
                            SendStageRemainingTime(totalTime, chipKey);
                        }
                    }
                    break;
                case MessageType.Sequencing:
                case MessageType.Sequencing_Read1:
                case MessageType.Sequencing_Read2:
                case MessageType.BarcodeSequencing:
                    if (currentInfo == null) return;

                    ImagingCycle _CurrentCycle = null;
                    if (_ScannerCycle != null && _ScannerCycle.ContainsKey(chipKey)) _CurrentCycle = _ScannerCycle[chipKey];

                    if (_IsSendTotalTime != null && _IsSendTotalTime.ContainsKey(chipKey) && !_IsSendTotalTime[chipKey].HasValue)
                    {
                        double cleavageTime = 0;
                        double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                        double unusedTime = loadDNBTime + seqPrimTime + cleavageTime;
                        UpdateTotalTime(chipKey, unusedTime, 0);
                    }
                    else
                    {
                        if(_CurrentCycle < 1)
                        {
                            if(tempKey[1] == 1)
                            {
                                double cleavageTime = 0;
                                double.TryParse(SequenceTimeMinute[SequenceTimeMinute.Count - 1], out cleavageTime);
                                if (!realCleavageTimeMap.ContainsKey(chipKey)) realCleavageTimeMap.Add(chipKey, cleavageTime);
                                double unusedTime = seqPrimTime;
                                UpdateTotalTime(chipKey, unusedTime, 0);
                            }
                        }
                    }
                    StopwatchStats[] _sequenceStats = null;
                    if(_SequenceWatchStatsMap.ContainsKey(chipKey))
                    {
                        _sequenceStats = _SequenceWatchStatsMap[chipKey];
                    }
                    int watchStatsIndex = Convert.ToInt32(tempKey[1]) - 1;

                    //start stopwatch stat to caculate average time of sequence
                    if (_sequenceStats == null || _sequenceStats.Length == 0)
                    {
                        //create stop watch of sequence cycle
                        string[] sequenceSteps = Enum.GetNames(typeof(SequenceStepsEnum));
                        _sequenceStats = new StopwatchStats[sequenceSteps.Length];
                        for (int i = 0; i < sequenceSteps.Length; i++)
                        {
                            _sequenceStats[i] = new StopwatchStats(sequenceSteps[i]);
#if DEBUG
                            _Logger.Log()(LogSeverityEnum.Info, $"Create Watch for {sequenceSteps[i]}");
#endif
                        }
                        _SequenceWatchStatsMap[chipKey] = _sequenceStats;
                        //create cycle metrics 
                        //cycleMetrics = new CycleMetrics(_StageRunMgr.FlowcellBarcode, 0);
                        string customFolder = string.Empty;
                        if (currentInfo.StartSequenceDateTime.HasValue) customFolder = $"{currentInfo.StartSequenceDateTime.Value.ToLocalTime().ToString("yyyyMMdd")}_{Environment.MachineName}_{currentInfo.FlowcellPosition}_{_FlowcellCodeMap[chipKey]}";
                        CycleMetrics _cycleMetrics = new CycleMetrics(_FlowcellCodeMap[chipKey], 0, customFolder);
                        if (!_CycleMetricsMap.ContainsKey(chipKey))
                            _CycleMetricsMap.Add(chipKey, _cycleMetrics);
                        else
                            _CycleMetricsMap[chipKey] = _cycleMetrics;
                    }

                    if (watchStatsIndex > -1 && watchStatsIndex < _sequenceStats.Length)
                    {
                        _SequenceWatchStatsMap[chipKey][watchStatsIndex].Start();
#if DEBUG
                        _Logger.Log()(LogSeverityEnum.Info, $"start watch[{_sequenceStats[watchStatsIndex].Description}], watchStatsIndex={watchStatsIndex},stepKey={stepKey}");
#endif
                    }
                    
                    
                    if(_CurrentCycle != null)
                    {
                        if(_TotalTimesMap != null && _TotalTimesMap.ContainsKey(chipKey))
                        {
                            if( tempKey[1] == 1)
                            {
                                double tempTime = _TotalTimesMap[chipKey] - _CurrentCycle * (oneCycleRealTime.ContainsKey(chipKey) ? oneCycleRealTime[chipKey] : oneCycleTime);
                                //if(realCleavageTimeMap.ContainsKey(chipKey))
                                //    tempTime -= realCleavageTimeMap[chipKey];
                                if (tempTime <= 0)
                                {
                                    tempTime = 1;
                                }
                                SendStageRemainingTime(tempTime, chipKey);
                            }
                        }
                    }
                    break;
            }
        }

        private void SendReagentMessage(string reagentName, int reagentPos, string key, int progress = 0)
        {
            string message = string.Empty;
            RunInfo currentInfo = null;
            if (_ScannerExperimentInfo != null && _ScannerExperimentInfo.ContainsKey(key))
                currentInfo = _ScannerExperimentInfo[key];
            string _operation = string.Empty;
            string _operationKey = string.Empty;
            if (_OperationMaps != null && _OperationMaps.ContainsKey(key)) _operation = _OperationMaps[key];
            if (_OperationKeyMaps != null && _OperationKeyMaps.ContainsKey(key)) _operationKey = _OperationKeyMaps[key];
            MessageType phaseType = MessageType.None;
            if (_PhaseTypeMaps != null && _PhaseTypeMaps.ContainsKey(key)) phaseType = _PhaseTypeMaps[key];

            int[] tempKeyArray = null;
            if (_OperationArrayMaps != null && _OperationArrayMaps.ContainsKey(key)) tempKeyArray = _OperationArrayMaps[key];
            ImagingCycle _currentCycle = null;
            if (_ScannerCycle != null && _ScannerCycle.ContainsKey(key)) _currentCycle = _ScannerCycle[key];
            switch (phaseType)
            {
                case MessageType.Sequencing:
                case MessageType.LoadDNB:
                case MessageType.PostLoad:
                case MessageType.BarcodeSequencing:
                case MessageType.BarcodeSynthesis:
                case MessageType.BarcodeSynthesisSE:
                case MessageType.PESynthesis:
                case MessageType.Sequencing_Read1:
                case MessageType.Sequencing_Read2:
                case MessageType.SeqPrime:
                    if (currentInfo == null) return;
                    Sequence sequence = new Sequence();
                    sequence.OperationKey = _operationKey;
                    sequence.Operation = _operation;
                    sequence.ReagentName = reagentName;
                    sequence.ReagentPos = reagentPos;

                    if (phaseType == MessageType.LoadDNB || phaseType == MessageType.SeqPrime || phaseType == MessageType.PostLoad)
                    {
                        sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.TotalReads;
                        sequence.CurrentCycle = 0;
                    }
                    else if (phaseType == MessageType.Sequencing || phaseType == MessageType.Sequencing_Read1)
                    {
                        sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.Read1Len;
                        int cycleNum = 0;
                        if(tempKeyArray != null && tempKeyArray[1] == 1)
                        {
                            cycleNum = (_currentCycle == null || _currentCycle <= 0) ? 1 : _currentCycle + 1;
                        }
                        else cycleNum = (_currentCycle == null || _currentCycle <= 0) ? 1 : _currentCycle;
                        sequence.CurrentCycle = cycleNum;
                    }
                    else if(phaseType == MessageType.Sequencing_Read2)
                    {
                        sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.Read2Len;
                        int cycleNum = 0;
                        if (tempKeyArray != null && tempKeyArray[1] == 1)
                        {
                            cycleNum = (_currentCycle == null || _currentCycle <= 0 || (_currentCycle - currentInfo.Read1Len) <= 0) ? 1 : (_currentCycle - currentInfo.Read1Len) + 1;
                        }
                        else cycleNum = (_currentCycle == null || _currentCycle <= 0 || (_currentCycle - currentInfo.Read1Len) <= 0) ? 1 : (_currentCycle - currentInfo.Read1Len);
                        sequence.CurrentCycle = cycleNum;
                    }
                    else if (phaseType == MessageType.PESynthesis)
                    {
                        sequence.TotalCycle = currentInfo == null ? 0 :currentInfo.Read2Len;
                        sequence.CurrentCycle = 0;// cycleNum;
                    }
                    else if (phaseType == MessageType.BarcodeSequencing || phaseType == MessageType.BarcodeSynthesis || phaseType == MessageType.BarcodeSynthesisSE)
                    {
                        sequence.TotalCycle = currentInfo == null ? 0 : currentInfo.BarcodeLen;
                        if (phaseType == MessageType.BarcodeSynthesis || phaseType == MessageType.BarcodeSynthesisSE)
                        {
                            sequence.CurrentCycle = 0;
                        }
                        else
                        {
                            int cycleNum = 0;
                            if (tempKeyArray != null && tempKeyArray[1] == 1)
                            {
                                cycleNum = (_currentCycle == null || _currentCycle <= 0 || (_currentCycle - currentInfo.Read2Len - currentInfo.Read1Len) <= 0) ? 1 : (_currentCycle - currentInfo.Read2Len - currentInfo.Read1Len) + 1;
                            }
                            else cycleNum = (_currentCycle == null || _currentCycle <= 0 || (_currentCycle - currentInfo.Read2Len - currentInfo.Read1Len) <= 0) ? 1 : (_currentCycle - currentInfo.Read2Len - currentInfo.Read1Len);
                            sequence.CurrentCycle = cycleNum;
                        }
                    }
                    if (sequence.CurrentCycle > sequence.TotalCycle)
                    {
                        _Logger.Log()(LogSeverityEnum.Warning, $"Invalid CurretyCyle {sequence.CurrentCycle }");
                        sequence.CurrentCycle = sequence.TotalCycle;
                    }
                    metricsCycle = sequence.CurrentCycle;
                    if(_LastFieldMap != null && _LastFieldMap.ContainsKey(key) && _LastFieldMap[key] != null)
                    {
                        Field field = _LastFieldMap[key];
                        sequence.CurrentLane = field.Lane;
                        sequence.CurrentFovCol = field.Col;
                        sequence.CurrentFovRow = field.Row;
                        sequence.TotalColumn = field.ColNum;
                        sequence.TotalRow = field.RowNum;
                        if(_ScanParamsMap != null && _ScanParamsMap.ContainsKey(key) && _ScanParamsMap[key] != null)
                            sequence.TotalLane = _ScanParamsMap[key].ScanAreas.Count;
                    }
                    message = JsonConvert.SerializeObject(sequence);
                    break;
                case MessageType.Wash_Cleantubing:
                case MessageType.Cleantubing:
                case MessageType.WashNaOH:
                    Wash wash = new Wash();
                    wash.ReagentName = reagentName;
                    wash.ReagentPos = reagentPos;
                    message = JsonConvert.SerializeObject(wash);
                    break;
            }
            SendStageRunMessage(message, key);
        }

        private void SetMachineStatusByPhaseType(MessageType phaseType, string chipKey)
        {
            if(phaseType == MessageType.Wash || phaseType == MessageType.WashNaOH || phaseType == MessageType.Wash_Cleantubing || phaseType == MessageType.Cleantubing)
            {
                MachineStatusMap[chipKey] = MachineStatus.Wash;
            }
            else if((int)phaseType < 11)
            {
                int[] operationArray = null;
                if (_OperationArrayMaps != null && _OperationArrayMaps.ContainsKey(chipKey))
                    operationArray = _OperationArrayMaps[chipKey];
                if (operationArray == null || operationArray.Length != 3) return;
                if ((phaseType == MessageType.Sequencing && operationArray[1] == 2) || 
                    (phaseType == MessageType.Sequencing_Read1 && operationArray[1] == 2) ||
                    (phaseType == MessageType.Sequencing_Read2 && operationArray[1] == 2) ||
                    (phaseType == MessageType.BarcodeSequencing && operationArray[1] == 2))
                {
                    //MachineStatusMap[chipKey] = MachineStatus.Image;
                }
                else
                {
                    MachineStatusMap[chipKey] = MachineStatus.Incorporation;
                }
            }
        }

        private int[] SplitOperationKey(string stepKey)
        {
            if (!string.IsNullOrEmpty(stepKey) && !string.IsNullOrWhiteSpace(stepKey))
            {
                string[] rsult = stepKey.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (rsult != null && rsult.Length == 3)
                {
                    int temp = 0;
                    int[] rslt = new int[3];
                    bool isInvalid = false;
                    for(int i = 0;i < 3; i++)
                    {
                        if (int.TryParse(rsult[i], out temp))
                        {
                            rslt[i] = temp;
                        }
                        else
                        {
                            isInvalid = true;
                            break;
                        }
                    }
                    if (isInvalid) return null;
                    else return rslt;
                }
            }
            return null;
        }


        private int GetLiquidSolenoid(BoardTypeEnum boardType, bool isChipA)
        {
            if (boardType == BoardTypeEnum.V2)
            {
                if (!isChipA) return (int)DiaphragmPumpType.LiquidSolenoidvalve + 1;
            }
            return (int)DiaphragmPumpType.LiquidSolenoidvalve;
        }

        private int GetSyringePumpPos(PumpTypeEnum pumpType, bool fromBypass, bool isAspirate,string key)
        {
            bool isFastChip = false;
            if (key == KEY_FLOWCELL_A) isFastChip = _FlowcellType[0];
            else isFastChip = _FlowcellType[1];
            if (pumpType == PumpTypeEnum.Tecan)
            {
                if (fromBypass)
                {
                    if (isAspirate) return 0;
                    else return 1;
                }
                else
                {
                    if (isAspirate) return 1;
                    else return 3;
                }
            }
            else if (pumpType == PumpTypeEnum.Tecan3Plus)
            {
                if (fromBypass)
                {
                    if (isAspirate)
                    {
                        if (isFastChip) return 5;
                        return 1;
                    }
                    else return 3;
                }
                else
                {
                    if (isAspirate)
                    {
                        if (isFastChip) return 4;
                        return 2;
                    }
                    else return 3;
                }
            }
            else if (pumpType == PumpTypeEnum.Thomas)
            {
                if (fromBypass)
                {
                    if (isAspirate)
                    {
                        if (isFastChip) return 5;
                        return 2;
                    }
                    else return 3;
                }
                else
                {
                    if (isAspirate)
                    {
                        if (isFastChip) return 4;
                        return 1;
                    }
                    else return 3;
                }
            }
            return 0;
        }
        private BaseCallClient GetClient(int index)
        {

            if (ImageBufferExt.Get.IsOnlineBaseCall && ImageBufferExt.Get.Clients != null && ImageBufferExt.Get.Clients.Length > 0)
            {
                int len = ImageBufferExt.Get.Clients.Length;
                return ImageBufferExt.Get.Clients[len-1];
            }
            _Logger.Log()(LogSeverityEnum.Error, $"Client is empty");
            return null;
        }

        private double WriteCycleTimeMetrics(string chipKey,bool isNoCleavage,out double clevageTime)
        {
            clevageTime = 0;
            CycleMetrics _cycleMetrics = null;
            if (_CycleMetricsMap.ContainsKey(chipKey))
                _cycleMetrics = _CycleMetricsMap[chipKey];
            if (_cycleMetrics == null)
            {
                return 0;
            }
            StopwatchStats[] _sequenceWatchStats = null;
            if (_SequenceWatchStatsMap.ContainsKey(chipKey))
            {
                _sequenceWatchStats = _SequenceWatchStatsMap[chipKey];
            }
            if (_sequenceWatchStats == null) return 0;

            
            double time = 0;
            CycleFieldData data = new CycleFieldData();
            //send remaining time by cycle
            foreach (StopwatchStats watch in _sequenceWatchStats)
            {
                time += watch.Avg / 1000 / 60;
                if (string.Compare(watch.Description, SequenceStepsEnum.Incorporation.ToString("g")) == 0)
                {
                    data.IncorporationTimeMS = watch.Last;
#if DEBUG
                    _Logger.Log()(LogSeverityEnum.Info, $"Watch {watch.Description} time is {data.IncorporationTimeMS}");
#endif
                }
                else if (string.Compare(watch.Description, SequenceStepsEnum.Image.ToString("g")) == 0)
                {
                    data.ImageTimeMS = watch.Last;
#if DEBUG
                    _Logger.Log()(LogSeverityEnum.Info, $"Watch {watch.Description} time is {data.ImageTimeMS}");
#endif
                }
                else if (!isNoCleavage && string.Compare(watch.Description, SequenceStepsEnum.Cleavage.ToString("g")) == 0)
                {
                    data.CleavageTimeMS = watch.Last;
                    clevageTime = watch.Last /1000/60;
#if DEBUG
                    _Logger.Log()(LogSeverityEnum.Info, $"Watch {watch.Description} time is {data.CleavageTimeMS}");
#endif
                }
            }

            //write cycle metrics
            data.CycleNo = _ScannerCycle[chipKey];
            if (_ThetaMap != null && _ThetaMap.ContainsKey(chipKey))
                data.Theta = _ThetaMap[chipKey];
            else data.Theta = 0;
            data.TotalTimeMS = data.IncorporationTimeMS + data.ImageTimeMS + data.CleavageTimeMS;
            data.IsGetQCDataSucccess = true;
            _cycleMetrics.WriteOrAppendCsv(data);
            return time;
        }

        private void SendLastMsg(string chipKey)
        {
            if(StageRunMgrMap != null && StageRunMgrMap.ContainsKey(chipKey))
            {
                StageRunMgr srm = StageRunMgrMap[chipKey]; if (srm == null) return;
                if (_LastRemainTimeMap != null  && _LastRemainTimeMap.ContainsKey(chipKey))
                {
                    srm.StageService?.SendRemainingTime(_LastRemainTimeMap[chipKey]);
                }

                if (_LastSequencePhaseMap != null && _LastSequencePhaseMap.ContainsKey(chipKey) &&
                    _LastSequenceInfoMap != null && _LastSequenceInfoMap.ContainsKey(chipKey))
                {
                    srm.StageService?.SendStageRunMessage(_LastSequencePhaseMap[chipKey], _LastSequenceInfoMap[chipKey]);
                }
            }
        }

        private string GetScanParameters(string para,bool isFastChip)
        {
            //_scannerParameters
            //6 - 72,6 - 72,6 - 72,6 - 72
            if (!isFastChip) return para;
            if (!string.IsNullOrEmpty(para))
            {
                string rslt = string.Empty;
                string[] laneArray = para.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                for(int i = 0;i < 2; i++)
                {
                    string[] colArray = laneArray[i].Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    if (colArray != null && colArray.Length == 2)
                    {
                        int tempCol = 0;
                        if (int.TryParse(colArray[0], out tempCol))
                        {
                            if (tempCol > 3) tempCol = 3;
                            rslt += $"{tempCol}-{colArray[1]},";
                        }
                    }
                }
                rslt = rslt.Substring(0, rslt.Length - 1);
                return rslt;
            }
            return para;
        }

        public void GetComponentsVersion()
        {
            if (!IsInitDone) return;
            //return;
            string emptyVersion = "NaN";
            if (_ComponentVersionMap == null) _ComponentVersionMap = new Dictionary<string, string>();
            string componentKey = Properties.Resource.strVersionIOBoard;
            string version = string.Empty;
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = IoBoardInt?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = IoBoardInt?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = Properties.Resource.strVersionTemperatur;
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = ChipTempBoardInt?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = ChipTempBoardInt?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            if (ImageBufferExt.Get.IsOnlineBaseCall)
            {
                componentKey = Properties.Resource.strVersionBasecall;
                if (_ComponentVersionMap.ContainsKey(componentKey))
                {
                    if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                    {
                        version = ImageBufferExt.Get.Clients[0]?.Version;
                        _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                    }
                }
                else
                {
                    version = ImageBufferExt.Get.Clients[0]?.Version;
                    _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
                }
            }

            componentKey = Properties.Resource.strVersionStage;
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = XYStageInt?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = XYStageInt?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }


            componentKey = Properties.Resource.strVersionAF;
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = AFControl?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = AFControl?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = string.Format(Properties.Resource.strVersionCamera, "A");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = CameraIntA?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = CameraIntA?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }
            componentKey = string.Format(Properties.Resource.strVersionCamera, "B");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = CameraIntB?.GetVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = CameraIntB?.GetVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = string.Format(Properties.Resource.strVersionPump, "A");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = SyringePump1Int?.GetFirmwareVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = SyringePump1Int?.GetFirmwareVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = string.Format(Properties.Resource.strVersionPump, "B");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = SyringePump2Int?.GetFirmwareVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = SyringePump2Int?.GetFirmwareVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = string.Format(Properties.Resource.strVersionValve, "A");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = MainSelectorValve1Int?.GetFirmwareVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = MainSelectorValve1Int?.GetFirmwareVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            componentKey = string.Format(Properties.Resource.strVersionValve, "B");
            if (_ComponentVersionMap.ContainsKey(componentKey))
            {
                if (string.Compare(_ComponentVersionMap[componentKey], emptyVersion, true) == 0)
                {
                    version = MainSelectorValve2Int?.GetFirmwareVersion();
                    _ComponentVersionMap[componentKey] = string.IsNullOrEmpty(version) ? emptyVersion : version.Trim();
                }
            }
            else
            {
                version = MainSelectorValve2Int.GetFirmwareVersion();
                _ComponentVersionMap.Add(componentKey, string.IsNullOrEmpty(version) ? emptyVersion : version.Trim());
            }

            MessageType type = MessageType.Version;
            string msg = JsonConvert.SerializeObject(_ComponentVersionMap);
            if(StageRunMgrMap != null && StageRunMgrMap.ContainsKey(KEY_FLOWCELL_A) && StageRunMgrMap[KEY_FLOWCELL_A] != null)
                StageRunMgrMap[KEY_FLOWCELL_A].StageService.SendStageRunMessage(type.ToString(), msg);
        }

        /// <summary>
        /// save data to xml file
        /// </summary>
        /// <param name="category"></param>
        /// <param name="param"></param>
        /// <param name="value"></param>
        private void SaveConfigPara(string category, string param, object value,bool isOnline)
        {
            if (!isOnline)
            {
                string configPath = @"C:\BGI\Config\BGI.ZebraV2Seq.Service.xml";
                XmlDocument doc = new XmlDocument();
                doc.Load(configPath);
                XmlNode node = doc.SelectSingleNode("Configs/InstrumentConfig[Para_Set_Name='" + category + "' and Para_Name='" + param + "']").SelectSingleNode("Cur_Value").FirstChild;
                if (node != null) node.Value = value.ToString();
                doc.Save(configPath);
            }
            else
            {
                ConfigMgr.Get.SetCurValue(category, param, value, string.Empty);
                ConfigMgr.Get.SaveParameter(category, param);
            }
        }
        private void UpdateTotalTime(string key,double oldTime,double newTime)
        {
            if (_TotalTimesMap.ContainsKey(key))
            {
                double totalTime = _TotalTimesMap[key] - oldTime;
                _TotalTimesMap[key] = totalTime + newTime;
            }
        }

        private double GetTotalTime(RunInfo runInfo)
        {
            if(runInfo != null)
            {
                loadDNBTime = GetLoadDnbTime();
                seqPrimTime = GetSeqPrimTime();
                oneCycleTime = GetSequenceTime();
                peSynthesisTime = GetPeSynthesisTime();
                barcodePrimTime = GetBarcodePrimTime();
                double rslt = loadDNBTime + seqPrimTime;
                if (runInfo.Read2Len > 0)
                    rslt += peSynthesisTime;
                if (runInfo.BarcodeLen > 0)
                    rslt += barcodePrimTime;
                rslt += runInfo.TotalReads * oneCycleTime;
                return rslt;
            }
            return 48*60;
        }

        private double GetLoadDnbTime()
        {
            double time = GetTimeFromStringCollectiong(LoadDnbTimeMinute);
            if (time <= 0) time = 82;
            return time;
        }

        private double GetSequenceTime()
        {
            double time = GetTimeFromStringCollectiong(SequenceTimeMinute);
            if (time <= 0) time = 14;
            return time;
        }

        private double GetPeSynthesisTime()
        {
            double time = GetTimeFromStringCollectiong(PESynthesisTimeMinute);
            if (time <= 0) time = 108;
            return time;
        }

        private double GetBarcodePrimTime()
        {
            double time= GetTimeFromStringCollectiong(BarcodePrimTimeMinute);
            if (time <= 0) time = 31;
            return time;
        }

        private double GetSeqPrimTime()
        {
            double time= GetTimeFromStringCollectiong(SeqPrimTimeMinute);
            if (time <= 0) time = 8;
            return time;
        }

        private double GetTimeFromStringCollectiong(StringCollection sc,int index = -1)
        {
            if(sc != null && sc.Count > index)
            {
                double time = 0;
                if(index == -1)
                {
                    foreach (string s in sc)
                    {
                        double temp = 0;
                        double.TryParse(s, out temp);
                        time += temp;
                    }
                }
                else
                {
                    string s = sc[index];
                    double.TryParse(s, out time);
                }
                return time;
            }
            return 0;
        }

        private double GetCleavageTime(string collection)
        {
            return 0;
        }

        private ServiceMessageInfo CreateMessage(string msg,ScriptMessageSeverityEnum type, bool isPublic,string flowcellId,string errCode)
        {
            ServiceMessageInfo rslt = new ServiceMessageInfo();
            rslt.MessageType = type;
            rslt.UserName =  !string.IsNullOrEmpty(CurrentUser)?CurrentUser: Environment.UserName;
            rslt.InstructmentId = Environment.MachineName;
            rslt.IsPublic = isPublic;
            rslt.SoftwareVersion = SoftVersion;
            rslt.Datetime = DateTime.UtcNow.ToLocalTime().ToString();
            rslt.Message = msg;
            rslt.FlowCellId = flowcellId;
            rslt.Errorcode = errCode;
            return rslt;
        }

        private void CreateAndSendMessage(string msg,ScriptMessageSeverityEnum type,bool isPublic,string flowcellId,string errCode,string chipKey,bool isIncludeInfo = false)
        {
            ServiceMessageInfo serviceMsg = CreateMessage(msg, type, isPublic, flowcellId, errCode);
            SendServiceMessage(serviceMsg, chipKey,isIncludeInfo);
        }

        public string GetFlowcellId(string key)
        {
            if(_FlowcellCodeMap != null && _FlowcellCodeMap.ContainsKey(key) && !string.IsNullOrEmpty(_FlowcellCodeMap[key]))
            {
                return _FlowcellCodeMap[key];
            }
            return string.Empty;
        }

        public void SendScriptError(string key,string msg,string flowcellId, string errCode)
        {
            CreateAndSendMessage(msg, ScriptMessageSeverityEnum.Error, false, "", errCode, key);
        }

        public void RemoveScriptError(string key,string msg)
        {
            RemoveAlarmMessage(msg, "", key);
        }
        #endregion


    }
}
