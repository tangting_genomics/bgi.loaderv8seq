﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves = V8Loader.RotaryValves

TempBoardA=V8Loader.TemperatureBoardA
TempBoardB=V8Loader.TemperatureBoardB
TempBoardC=V8Loader.TemperatureBoardC

# reagents default map dictionary
reagentsMap = {'Hybridization':1, 'WB1':2,'CMR':3,'MDAPrimer':4, 'MDAmix':5,'WB2':7 ,'MDABuffer':6,'Blockmix':8,'2ndinsertprimer':9,'Air':24}

IsRun=True
OperationName='MDAPostLoading'
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]


def main(tunnel):
	try:
		TempBoard=None
		tempchannel=1
		if (tunnel-1)/2==0:
			TempBoard=TempBoardA
		elif (tunnel-1)/2==1:
			TempBoard=TempBoardB
		elif (tunnel-1)/2==2:
			TempBoard=TempBoardC
		if tunnel%2==1:
			tempchannel=1
		else:
			tempchannel=2
			
		
		shared.RotaryValves[tunnel-1].GotoPosition(reagentsMap['Air'])#旋转阀选通24号口
		shared.WashSyringepump(tunnel,200)
		#Hybridization
		
		operation.UpdateCurrentAction("Hybridization Loading")
		shared.engOp.SendStatusMessageInfo("Tunnel"+str(tunnel)+"channel:"+str(tempchannel)+"Temperature")
		TempBoard.SetTargetTemperature(25, tempchannel)
		#time.sleep(120)
		#PostLoadingAspirate(tunnel,RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
		shared.PostLoadingAspirate(tunnel,reagentsMap['Hybridization'],3000,100,80)
		#PostLoadingDispense(tunnel,volumn1,volumn2,volumn3,volumn4,rate)
		shared.PostLoadingDispense(tunnel,150,200,2300,200,40)
		#shared.WashSyringepump(tunnel,200)
		
		#Hybridization
		operation.UpdateCurrentAction("Hybridization Loading")
		TempBoard.SetTargetTemperature(25, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap['Hybridization'],3000,100,80)
		shared.PostLoadingDispense(tunnel,150,200,2300,200,40)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		operation.UpdateCurrentAction("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB1'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#CMR
		operation.UpdateCurrentAction("CMR Loading")
		TempBoard.SetTargetTemperature(57, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['CMR'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(180)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		operation.UpdateCurrentAction("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB1'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#WB2
		operation.UpdateCurrentAction("WB2 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB2'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#Hybridization
		#operation.UpdateCurrentAction("Hybridization Loading")
		#TempBoard.SetTargetTemperature(45, tempchannel)
		#shared.PostLoadingAspirate(tunnel,reagentsMap['Hybridization'],2000,100,80)
		#shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#MDAPrimer
		operation.UpdateCurrentAction("MDAPrimer Loading")
		TempBoard.SetTargetTemperature(55, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['MDAPrimer'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(300)
		TempBoard.SetTargetTemperature(40, tempchannel)
		time.sleep(300)
		TempBoard.SetTargetTemperature(28, tempchannel)
		time.sleep(300)
		shared.WashSyringepump(tunnel,200)
		
		#WB2
		operation.UpdateCurrentAction("WB2 Loading")
		TempBoard.SetTargetTemperature(37, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB2'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#MDAmix
		operation.UpdateCurrentAction("MDAmix Loading")
		TempBoard.SetTargetTemperature(37, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['MDAmix'],2600,100,40)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(900)
		shared.WashSyringepump(tunnel,200)
		shared.WashSyringepump(tunnel,200)
		
		#MDAbuffer
		operation.UpdateCurrentAction("MDABuffer Loading")
		TempBoard.SetTargetTemperature(37, tempchannel)
		#time.sleep(20)
		shared.PostLoadingAspirate(tunnel,reagentsMap['MDABuffer'],2000,100,40)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(2100)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		operation.UpdateCurrentAction("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB1'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#time.sleep(300)
		#shared.WashSyringepump(tunnel,200)
		
		#WB2
		operation.UpdateCurrentAction("WB2 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB2'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#time.sleep(300)
		#shared.WashSyringepump(tunnel,200)
		
		#Blockmix
		operation.UpdateCurrentAction("Blockmix Loading")
		TempBoard.SetTargetTemperature(55, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['Blockmix'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(600)
		shared.WashSyringepump(tunnel,200)
		#shared.WashSyringepump(tunnel,200)
		
		#WB1
		operation.UpdateCurrentAction("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB1'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#WB2
		operation.UpdateCurrentAction("WB2 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB2'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		#Hybridization
		operation.UpdateCurrentAction("Hybridization Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap['Hybridization'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		shared.WashSyringepump(tunnel,200)
		
		#2ndinsertprimer
		operation.UpdateCurrentAction("2ndinsertprimer Loading")
		TempBoard.SetTargetTemperature(55, tempchannel)
		#time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap['2ndinsertprimer'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		time.sleep(300)
		TempBoard.SetTargetTemperature(40, tempchannel)
		time.sleep(300)
		TempBoard.SetTargetTemperature(28, tempchannel)
		time.sleep(300)
		#shared.WashSyringepump(tunnel,200)
		shared.WashSyringepump(tunnel,200)
		
		#WB2
		operation.UpdateCurrentAction("WB2 Loading")
		TempBoard.SetTargetTemperature(28, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap['WB2'],2000,100,80)
		shared.PostLoadingDispense(tunnel,150,150,1400,150,10)
		#shared.WashSyringepump(tunnel,200)
		
		operation.UpdateCurrentAction("Finished")
	except Exception as e:
		shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
		operation.UpdateCurrentAction("Error Exit")
	finally:
		shared.engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False

def protect():
	global IsRun
	IsRun=True
	while(IsRun==True):
		state=str(operation.GetScriptState())
		#shared.engOp.SendStatusMessageInfo("start")
		if	state=="Stopping" or state=="Finished" or state=="None":
			for	t in thread_list:
				try:
					t.Abort()
				except Exception as e:
					shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
				finally:
					global IsRun
					IsRun=False
			shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(shared.LoaderIOBoardV8.Connected and shared.SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
	
for	tunnel in tunnels:
	if not shared.RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)

result=shared.LoadWashPre(30,"A")
if result==True:
	protect_thread=threading.Thread(target=protect)
	protect_thread.start()
	
	for	tunnel in tunnels:
		num=int(tunnel.Id)
		tstart=ParameterizedThreadStart(main)
		t=Thread(tstart)
		t.IsBackground=True
		t.Start(num)
		thread_list.append(t)
	
	protect_thread.join()
	
	
	for t in thread_list:
		t.Join()
		engOp.SendStatusMessage(str(time.time()))
		sys.exit(0)