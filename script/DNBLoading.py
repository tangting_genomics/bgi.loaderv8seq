﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves=V8Loader.RotaryValves

# reagents default map dictionary
reagentsMap = {'Air':24, 'DNB':21,"Water":17,'DRB':11, 'DCB':13,'REB':14 ,'PWB':15,'PAW':16,"PRIMENT":19,"HOT":20}

IsRun=True

OperationName="DNBLoading"
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]



def main(tunnel):
	try:
		shared.RotaryValves[tunnel-1].GotoPosition(reagentsMap["Air"])#旋转阀选通24号口
		shared.WashSyringepump(tunnel,200)
		
		operation.UpdateCurrentAction("DNB Loading")
		#DNBAspirate(rotaryvalve,volumn(DNB量),airrate(吸入Air的速度),dnbrate(吸入DNB的速度))
		shared.DNBAspirate(tunnel,1400,100,20)
		#DNBDispense(rate(排液速度),pumprate(清洗注射泵速度),volumn1,volumn2,volumn3,volumn4,volumn5)
		shared.DNBDispense(tunnel,20,200,100,200,700,60,60)
		time.sleep(3600)
		
		#DRB
		operation.UpdateCurrentAction("DRB Loading")
		#PostLoadingAspirate(tunnel,RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
		shared.PostLoadingAspirate(tunnel,reagentsMap["DRB"],3200,100,50)
		#PostLoadingDispense(tunnel,volumn1,volumn2,volumn3,volumn4,rate)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		
		#DCB
		operation.UpdateCurrentAction("DCB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["DCB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,20)
		time.sleep(300)
		shared.WashSyringepump(tunnel,200)
		
		#REB
		operation.UpdateCurrentAction("REB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["REB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		
		#PWB
		operation.UpdateCurrentAction("DRB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["PWB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		time.sleep(480)
		shared.WashSyringepump(tunnel,200)
		
		#DCB
		operation.UpdateCurrentAction("DCB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["DCB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,20)
		time.sleep(180)
		shared.WashSyringepump(tunnel,200)
		
		#REB
		operation.UpdateCurrentAction("REB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["REB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		
		#PAW
		operation.UpdateCurrentAction("PAW Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["PAW"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		
		#PRIMENT
		operation.UpdateCurrentAction("PRIMENT Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["PRIMENT"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		time.sleep(1200)
		shared.WashSyringepump(tunnel,200)
		
		#REB
		operation.UpdateCurrentAction("REB Loading")
		shared.PostLoadingAspirate(tunnel,reagentsMap["REB"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		
		#HOT
		#shared.PostLoadingAspirate(tunnel,reagentsMap["HOT"],3200,100,50)
		#shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		#time.sleep(600)
		#shared.WashSyringepump(tunnel,200)
		
		#REB
		#shared.PostLoadingAspirate(tunnel,reagentsMap["REB"],3200,100,50)
		#shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		#shared.WashSyringepump(tunnel,200)
		
		operation.UpdateCurrentAction("Finished")
	except Exception as e:
		operation.UpdateCurrentAction("Error Exit")
		shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
	finally:
		shared.engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False

def updatemsg(msg):
	operation.UpdateCurrentAction(msg)

def protect():
	global IsRun
	IsRun=True
	while(IsRun==True):
		state=str(operation.GetScriptState())
		#shared.engOp.SendStatusMessageInfo("start")
		if	state=="Stopping" or state=="Finished" or state=="None":
			for	t in thread_list:
				try:
					t.Abort()
				except Exception as e:
					shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
				finally:
					global IsRun
					IsRun=False
			shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(shared.LoaderIOBoardV8.Connected and shared.SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
	
for	tunnel in tunnels:
	if not shared.RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)		

result=shared.LoadWashPre(20,"B")
if result==True:
	protect_thread=threading.Thread(target=protect)
	protect_thread.start()
	
	for	tunnel in tunnels:
		num=int(tunnel.Id)
		tstart=ParameterizedThreadStart(main)
		t=Thread(tstart)
		t.IsBackground=True
		t.Start(num)
		thread_list.append(t)
	
	protect_thread.join()
	
	for t in thread_list:
		t.Join()
		engOp.SendStatusMessage(str(time.time()))
		sys.exit(0)