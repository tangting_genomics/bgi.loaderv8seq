﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves=V8Loader.RotaryValvess
shared.ConnectIOBoard()


shared.ConnectSyringePump(1)

shared.ConnectSyringePump(2)

shared.ConnectSyringePump(3)


shared.ConnectSyringePump(4)

shared.ConnectSyringePump(5)
#开关电磁阀（number，open{True：打开，False：关闭}）
#shared.LoaderIOBoardV8.ControlRadiotube(5,True)