﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves=V8Loader.RotaryValves
# reagents default map dictionary
reagentsMap = {'Air':24, 'DNB':21,"Water":18,'DRB':11, 'DCB':13,'REB':14 ,'PWB':15,'PAW':16,"PRIMENT":19,"HOT":20}


def connect():
	shared = LoaderV8_FullCmmand
	engOp = globals().get('EngineerOperationsClass')
	V8Loader = engOp.GetDeviceObject("V8MutiLoader")
	shared.engOp=engOp
	shared.V8Loader=V8Loader
	shared.LoaderIOBoardV8 = V8Loader.IOBoard
	shared.SyringePump = V8Loader.Pump
	shared.RotaryValves=V8Loader.RotaryValves
	shared.ConnectIOBoard()
	shared.ConnectSyringePump(1)
	rotaryvalve=shared.ConnectRotaryValve(1)
def run(rotaryvalve,shared):

	#DRB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["DRB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	
	#DCB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["DCB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2000,200,20)
	time.sleep(180)
	shared.WashSyringepump(200)
	
	#REB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["REB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	
	#PWB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["PWB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	time.sleep(480)
	shared.WashSyringepump(200)
	
	
	#DCB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["DCB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2000,200,20)
	time.sleep(180)
	shared.WashSyringepump(200)
	
	#REB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["REB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	
	#PAW
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["PAW"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	
	#PRIMENT
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["PRIMENT"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	time.sleep(1200)
	shared.WashSyringepump(200)
	
	#REB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["REB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	
	#HOT
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["HOT"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	time.sleep(600)
	shared.WashSyringepump(200)
	
	#REB
	#PostLoadingAspirate(RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
	shared.PostLoadingAspirate(reagentsMap["REB"],3200,200,50)
	#PostLoadingDispense(volumn1,volumn2,volumn3,volumn4,rate)
	shared.PostLoadingDispense(100,400,2200,200,20)
	shared.WashSyringepump(200)

def updatemsg(msg):
	operation.UpdateCurrentAction(msg)