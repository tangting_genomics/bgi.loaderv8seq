﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves=V8Loader.RotaryValves

TempBoardA=V8Loader.TemperatureBoardA
TempBoardB=V8Loader.TemperatureBoardB
TempBoardC=V8Loader.TemperatureBoardC

# reagents default map dictionary
rvolumn={1:500,2:500,3:600,4:500,5:20,6:500,7:500,8:500,9:500,10:500,11:500}

OperationName="MDAPerfusion"
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]
IsRun=True

def main(tunnel):
	try:
		for i in range(1,11):
			if (i-0)%3==0:
				updatemsg("Wash Syringe pump")
				shared.WashSyringepump(tunnel,200)
			#i:RotaryValve Position;rvolumn[i]:rotaryvalve volumn;Aspirate Speed:50;Dispense Speed:200
			shared.WashRotaryValve(tunnel,i,rvolumn[i],50,200)
			updatemsg("完成灌注管路："+str(i))
			shared.engOp.SendStatusMessageInfo("完成清洗旋转阀"+str(i))
		#Speed:200
		shared.WashSyringepump(tunnel,200)
		MessageBox.Show("通道"+str(tunnel)+"灌注完成！")
	except Exception as e:
		shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
	finally:
		shared.engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False

		
def updatemsg(msg):
	operation.UpdateCurrentAction(msg)

def protect():
	global IsRun
	IsRun=True
	while(IsRun==True):
		state=str(operation.GetScriptState())
		#shared.engOp.SendStatusMessageInfo("start")
		if	state=="Stopping" or state=="Finished" or state=="None":
			try:
				for	t in thread_list:
					t.Abort()
			except Exception as e:
				print("Exception")
				shared.engOp.SendStatusMessageInfo("Exception")
			finally:
				global IsRun
				IsRun=False
				shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(shared.LoaderIOBoardV8.Connected and shared.SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
	
for	tunnel in tunnels:
	if not shared.RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)

result=shared.LoadWashPre(20,"A")
if result==True:
	protect_thread=threading.Thread(target=protect)
	protect_thread.start()
	
	for	tunnel in tunnels:
		num=int(tunnel.Id)
		tstart=ParameterizedThreadStart(main)
		t=Thread(tstart)
		t.IsBackground=True
		t.Start(num)
		thread_list.append(t)
	
	protect_thread.join()
	
	
	for t in thread_list:
		t.Join()
		engOp.SendStatusMessage(str(time.time()))
		sys.exit(0)
