﻿import System
import System.Windows
import clr
import time

clr.AddReference("BGI.Control.TestApps.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
from System.Windows.Forms import *
from BGI.Control.TestApps import *

import LoaderV8_FullCmmand

shared = LoaderV8_FullCmmand


engOp = globals().get('EngineerOperationsClass')
engOp_DM = engOp.GetDeviceObject("DM")
shared.engOp=engOp
shared.engOp_DM=V8Loader
shared.LoaderIOBoardV8 = V8Loader.GetDevice('LoaderIOBoardV8').Device 
shared.ConnectIOBoard()

#移动电机（1:选择电机（A,B,1,2,3,4,5）,2:(up，down),3:(碰到传感器继续往前走的距离)）
shared.MoveMotor("2","down",3)
