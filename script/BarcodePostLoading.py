﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves = V8Loader.RotaryValves

TempBoardA=V8Loader.TemperatureBoardA
TempBoardB=V8Loader.TemperatureBoardB
TempBoardC=V8Loader.TemperatureBoardC
IsRun=True

OperationName="BarcodePostLoading"
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]


def main(tunnel):
	try:
		shared.RotaryValves[tunnel-1].GotoPosition(reagentsMap["Air"])#旋转阀选通24号口
		shared.WashSyringepump(tunnel,200)
		TempBoard = None
		tempchannel=1
		if tunnel/2==0:
			TempBoard=TempBoardA
		elif tunnel/2=1:
			TempBoard=TempBoardB
		elif tunnel/2=2:
			TempBoard=TempBoardC
		if tunnel%2==1:
			tempchannel=1
		else:
			tempchannel=2
		
		
		#Hybridization
		updatemsg("Hybridization Loading")
		shared.engOp.SendStatusMessageInfo("Tunnel"+str(tunnel)+"channel:"+str(tempchannel)+"Temperature")
		TempBoard.SetTargetTemperature(28, tempchannel)
		time.sleep(120)
		#PostLoadingAspirate(tunnel,RotaryValve Position,Volumn,Air Aspirate Speed,Aspirate Speed)
		shared.PostLoadingAspirate(tunnel,reagentsMap["Hybridization"],3200,100,80)
		#PostLoadingDispense(tunnel,volumn1,volumn2,volumn3,volumn4,rate)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		updatemsg("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB1"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#CMR
		updatemsg("CMR Loading")
		TempBoard.SetTargetTemperature(57, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["CMR"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,50)
		time.sleep(120)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		updatemsg("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB1"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#WB2
		updatemsg("WB2 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB2"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#Blockmix
		updatemsg("Blockmix Loading")
		TempBoard.SetTargetTemperature(55, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["Blockmix"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		time.sleep(900)
		shared.WashSyringepump(tunnel,200)
		shared.WashSyringepump(tunnel,200)
		
		#WB1
		updatemsg("WB1 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB1"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2000,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#WB2
		updatemsg("WB2 Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB2"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#Hybridization
		updatemsg("Hybridization Loading")
		TempBoard.SetTargetTemperature(45, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap["Hybridization"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		shared.WashSyringepump(tunnel,200)
		
		#2ndinsertprimer
		updatemsg("2ndinsertprimer Loading")
		
		TempBoard.SetTargetTemperature(55, tempchannel)
		time.sleep(120)
		shared.PostLoadingAspirate(tunnel,reagentsMap["2ndinsertprimer"],3200,100,50)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,50)
		time.sleep(300)
		TempBoard.SetTargetTemperature(40, tempchannel)
		time.sleep(300)
		TempBoard.SetTargetTemperature(28, tempchannel)
		time.sleep(600)
		shared.WashSyringepump(tunnel,200)
		shared.WashSyringepump(tunnel,200)
		
		#WB2
		updatemsg("WB2 Loading")
		TempBoard.SetTargetTemperature(28, tempchannel)
		shared.PostLoadingAspirate(tunnel,reagentsMap["WB2"],3200,100,80)
		shared.PostLoadingDispense(tunnel,100,400,2200,200,20)
		shared.WashSyringepump(tunnel,200)
		
		updatemsg("Normal Exit")
	except Exception as e:
		shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
		updatemsg("Error Exit")
	finally:
		shared.engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False

def updatemsg(msg):
	operation.UpdateCurrentAction(msg)

def protect():
	global IsRun
	IsRun=True
	while(IsRun==True):
		state=str(operation.GetScriptState())
		#shared.engOp.SendStatusMessageInfo("start")
		if	state=="Stopping" or state=="Finished" or state=="None":
			for	t in thread_list:
				try:
					t.Abort()
				except Exception as e:
					shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
				finally:
					global IsRun
					IsRun=False
			shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(shared.LoaderIOBoardV8.Connected and shared.SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
	
for	tunnel in tunnels:
	if not shared.RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)		

result=shared.LoadWashPre(20,"A")
if result==True:
	protect_thread=threading.Thread(target=protect)
	protect_thread.start()
	
	for	tunnel in tunnels:
		num=int(tunnel.Id)
		tstart=ParameterizedThreadStart(main)
		t=Thread(tstart)
		t.IsBackground=True
		t.Start(num)
		thread_list.append(t)
	
	protect_thread.join()
	
	
	for t in thread_list:
		t.Join()
		engOp.SendStatusMessage(str(time.time()))
		sys.exit(0)