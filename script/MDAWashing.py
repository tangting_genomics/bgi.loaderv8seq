﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

import LoaderV8_FullCmmand
shared = LoaderV8_FullCmmand

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
shared.engOp=engOp
shared.V8Loader=V8Loader
shared.LoaderIOBoardV8 = V8Loader.IOBoard
shared.SyringePump = V8Loader.Pump
shared.RotaryValves=V8Loader.RotaryValves

TempBoardA=V8Loader.TemperatureBoardA
TempBoardB=V8Loader.TemperatureBoardB
TempBoardC=V8Loader.TemperatureBoardC
# reagents default map dictionary
reagentsMap = {'Air':24, 'DNB':23,"Water":17,'DRB':3, 'DCB':4 }

OperationName="MDAWash"
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]
IsRun=True
def updatemsg(msg):
	V8Loader.UpdateStatus(OperationName,msg)

def main(tunnel):
	try:
		updatemsg("Tunnel:"+str(tunnel)+"准备进行MDAWashing")
		
		#清洗次数
		number=2
		while number>0:
			number=number-1
			for i in range(1,11):
				if (i-11)%5==0:
					
					shared.WashSyringepump(tunnel,200)
				#WashRotaryValve(tunnel,pos(选择旋转阀口),volumn(流量),inrate(吸液速度),outrate(排液速度))
				updatemsg("Tunnel:"+str(tunnel)+"Position:"+str(i)+"  清洗旋转阀")
				shared.WashRotaryValve(tunnel,i,2000,200,200)
			result=shared.LoadWashPre(20,"A")
			if result==False:
				break
		shared.WashRotaryValveOver("A")	
		updatemsg("Tunnel:"+str(tunnel)+"  清洗注射泵")
		shared.WashSyringepump(tunnel,200)
		
		updatemsg("Tunnel:"+str(tunnel)+"  清洗盖板")
		#shared.WashCover(tunnel)
		shared.LoaderIOBoardV8.MoveSuctionMotorBToUpper(50,0,0)#移动吸液电机1提升到顶部传感器1的位置
	
		number=1
		while number>0:
			number=number-1
			for i in range(1,11):
				if (i-11)%5==0:
					shared.WashSyringepump(tunnel,200)
					
				#(rotaryvalve,pos(选择旋转阀口),volumn(流量),inrate(吸液速度),outrate(排液速度))
				updatemsg("Tunnel:"+str(tunnel)+"Position:"+str(i)+"排空管路")
				shared.LoadDrainEmpty(tunnel,i,1000,200,200)
		shared.WashSyringepump(tunnel,200)
		MessageBox.Show("Tunnel:"+str(tunnel)+"排空完成！")
	except Exception as e:
		shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
	finally:
		shared.engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False
		
	
def protect():
	global IsRun
	IsRun=True
	while(IsRun):
		state=str(operation.GetScriptState())
		if	state=="Stopping" or state=="Finished" or state=="None":
			for	t in thread_list:
				try:
					t.Abort()
				except Exception as e:
					shared.engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
				finally:
					global IsRun
					IsRun=False
			shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(shared.LoaderIOBoardV8.Connected and shared.SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
if	tunnels is None:
	sys.exit(0)

for	tunnel in tunnels:
	if not shared.RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)

result=shared.LoadWashPre(30,"A")
if result==True:
	protect_thread=threading.Thread(target=protect)
	protect_thread.start()
		
	for	tunnel in tunnels:
		num=int(tunnel.Id)
		tstart=ParameterizedThreadStart(main)
		t=Thread(tstart)
		t.IsBackground=True
		t.Start(num)
		thread_list.append(t)
		
	protect_thread.join()
	
	
	for t in thread_list:
		t.Join()
		engOp.SendStatusMessage(str(time.time()))
		sys.exit(0)
	