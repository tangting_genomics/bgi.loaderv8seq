﻿import System
import System.Windows
import clr
import time
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

from System.Windows.Forms import *

#Motor Opposite Distance
distance1=0
distance2=0
NCposition=1
NOposition=0
waitTime=4

# reagents default map dictionary
reagentsMap = {'Air':24, 'DNB':21,"Water":18,'DRB':11, 'DCB':13,'REB':14 ,'PWB':15,'PAW':16,"PRIMENT":19,"HOT":20}
motorA="A"
motorB="B"
Tunnel1={1:1, 2:2, 3:3, 4:4, 5:5, 6:26}
Tunnel2={1:6, 2:7, 3:8, 4:9, 5:10,6:27}
Tunnel3={1:11,2:12,3:13,4:14,5:15,6:28}
Tunnel4={1:16,2:17,3:18,4:19,5:20,6:29}
Tunnel5={1:21,2:22,3:23,4:24,5:25,6:30}
Tunnels={1:Tunnel1,2:Tunnel2,3:Tunnel3,4:Tunnel4,5:Tunnel5}

DNBvolume=None
PostLoadingvolume=0
engOp = None 
V8Loader = None 
#IO控制板
LoaderIOBoardV8 = None 
#注射泵
SyringePump = None

RotaryValves=None

def HandlePause():
	if engOp.IsPauseRequested() == True:
		engOp.SendStatusMessage("Script Paused. Waiting.")
		engOp.WaitUntilResume()

def ConnectIOBoard():
	engOp.SendStatusMessageInfo("Connect IOBoard")
	if	LoaderIOBoardV8.Connected == False:
		LoaderIOBoardV8.Connect()

def ConnectRotaryValve(tunnelnum):
	engOp.SendStatusMessageInfo("Connect Rotary Valve "+str(device))
	LoaderIOBoardV8.RotaryValve(tunnelnum,True)
	RotaryValve=None
	if	RotaryValves[tunnelnum-1].Connected == False:
		RotaryValves[tunnelnum-1].Connect()
	return	RotaryValves[tunnelnum-1]

def ConnectSyringePump(tunnelnum):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Connect Syringe Pump")
	LoaderIOBoardV8.SyringePump(tunnelnum,True)
	PowerOffSolenoidValve(tunnelnum)
	LoaderIOBoardV8.SolenoidValve(2,True)
	if	SyringePump.Connected == False:
		SyringePump.Connect()

def LoadWashPre(speed,motor=motorA):
	engOp.SendStatusMessageInfo("Install Clean Reagents ,speed:"+str(speed))
	actualdistance = 0
	if motor==motorA:
		LoaderIOBoardV8.MoveSuctionMotorAToUpper(speed,0,actualdistance)
	elif motor==motorB:
		LoaderIOBoardV8.MoveSuctionMotorBToUpper(speed,0,actualdistance)
	result = MessageBox.Show("请安装试剂！", "温馨提示", MessageBoxButtons.OKCancel)
	if result==DialogResult.OK:
		if motor==motorA:
			LoaderIOBoardV8.MoveSuctionMotorAToLower(speed,distance1,actualdistance)
		elif motor==motorB:
			LoaderIOBoardV8.MoveSuctionMotorBToLower(speed,distance1,actualdistance)
		return True
	else:
		return False
	
def WashSyringepump(tunnelnum,speed):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Wash Syringe Pump Start,speed:"+str(speed))
	PowerOffSolenoidValve(tunnelnum)
	SyringePump.Home(tunnelnum)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	LoaderIOBoardV8.SyringePump(True)
	SyringePump.SwitchValve(tunnelnum,NCposition)
	SyringePump.Aspirate(tunnelnum,5000,speed)
	time.sleep(waitTime)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,5000,speed)
	time.sleep(waitTime)
	#LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)#关闭2号电磁阀
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+" Wash Syringe Pump Finish")
	
def WashRotaryValve(tunnelnum,pos,volume,inrate,outrate):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+" Wash Rotary Valve Start,[Pos:"+str(pos)+",volume:"+str(volume)+"]")
	PowerOffSolenoidValve(tunnelnum)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],True)
	RotaryValves[tunnelnum-1].GotoPosition(pos)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,volume,inrate)
	time.sleep(waitTime)
	
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume,outrate)
	time.sleep(waitTime)
	PowerOffSolenoidValve(tunnelnum)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Wash Rotary Valve Finish")
	
def WashRotaryValveOver(motor):
	engOp.SendStatusMessageInfo("Wash Rotary Valve Over,Move Suction Motor B To Upper")
	if motor==motorA:
		LoaderIOBoardV8.MoveSuctionMotorAToUpper(50,0,0)
	elif motor==motorB:
		LoaderIOBoardV8.MoveSuctionMotorBToUpper(50,0,0)
	
def WashCover(tunnelnum):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Wash Cover Start")
	PowerOffSolenoidValve(tunnelnum)
	#第一次
	SyringePump.SwitchValve(tunnelnum,NCposition)
	SyringePump.Aspirate(tunnelnum,5000,150)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],True)
	time.sleep(waitTime)
	
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,5000,150)
	time.sleep(waitTime)
	
	#第二次
	SyringePump.SwitchValve(tunnelnum,NCposition)
	SyringePump.Aspirate(tunnelnum,5000,150)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],True)
	time.sleep(waitTime)
	
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,5000,150)
	time.sleep(waitTime)
	
	#第三次
	SyringePump.SwitchValve(tunnelnum,NCposition)
	SyringePump.Aspirate(tunnelnum,5000,150)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],True)
	time.sleep(waitTime)
	
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,5000,150)
	time.sleep(waitTime)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Wash Cover Finish")
	
#排空
def LoadDrainEmpty(tunnelnum,pos,volume,inrate,outrate):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"RotaryValve Dispence Start,Pos:"+str(pos)+";volume:"+str(volume))
	LoaderIOBoardV8.MoveSuctionMotorBToUpper(50,0,0)
	WashRotaryValve(tunnelnum,pos,volume,inrate,outrate)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"RotaryValve Dispence Stop")
	
def DNBAspirate(tunnelnum,volume,airrate,dnbrate):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+" DNB Aspirate Start")
	PowerOffSolenoidValve(tunnelnum)
	
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 6:ON,Radiotube 2:ON")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],True)
	RotaryValves[tunnelnum-1].GotoPosition(reagentsMap["Air"])
	
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,50,airrate)
	time.sleep(waitTime)
	RotaryValves[tunnelnum-1].GotoPosition(reagentsMap["DNB"])
	
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,volume,dnbrate)
	time.sleep(waitTime)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"DNB Aspirate Finish")
	
def DNBDispense(tunnelnum,rate,washrate,volume1,volume2,volume3,volume4,volume5):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"DNB Dispence Start,rate:"+str(rate)+"ul/s")
	PowerOffSolenoidValve(tunnelnum)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 6:OFF,Radiotube 2:ON,Dispense "+str(volume1)+"ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume1,rate)
	time.sleep(waitTime)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:OFF,Radiotube 1:ON,Radiotube 3:ON,Dispense "+str(volume2)+"ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],True)
	SyringePump.Dispense(tunnelnum,volume2,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 3:OFF,Radiotube 4:ON,Dispense "+str(volume3)+"ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],True)
	time.sleep(waitTime)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume3,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 4:OFF,Radiotube 5:ON,Dispense"+str(volume4)+"ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],True)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume4,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 5:OFF,Radiotube 3:ON,Dispense"+str(volume5)+"ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],True)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume5,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 1:OFF,3:OFF,2:ON,6:ON")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],True)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Aspirate 2ml(NO)+Pos:"+str(reagentsMap["Water"]))
	RotaryValves[tunnelnum-1].GotoPosition(reagentsMap["Water"])
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,2000,200)
	time.sleep(waitTime)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 26:OFF,Dispense 2000 ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,2000,200)
	time.sleep(waitTime)
	
	WashSyringepump(tunnelnum,washrate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:OFF")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"DNB Dispense Finish")
	

def PostLoadingAspirate(tunnelnum,pos,volume,airrate,rate):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Post Loading Aspirate Start")
	PowerOffSolenoidValve(tunnelnum)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:ON,6:ON,Aspirate Air 100 ul(NO),Pos:"+str(reagentsMap["Air"]))
	RotaryValves[tunnelnum-1].GotoPosition(reagentsMap["Air"])
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],True)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,50,airrate)
	time.sleep(waitTime)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Rotary Valve Open,Aspirate "+str(volume)+" ul(NO),Pos:"+str(pos))
	RotaryValves[tunnelnum-1].GotoPosition(pos)
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Aspirate(tunnelnum,volume,rate)
	time.sleep(waitTime)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:ON,6:OFF")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)
	engOp.SendStatusMessageInfo("Post Loading Aspirate Finish")
	
def PostLoadingDispense(tunnelnum,volume1,volume2,volume3,volume4,rate):
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Post Loading Dispense Start,rate(ul/s):"+str(rate))
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Dispense "+str(volume1)+" ul(NO)")
	SyringePump.SwitchValve(tunnelnum,NOposition)
	SyringePump.Dispense(tunnelnum,volume1,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:OFF,1:ON,3:ON,Dispense "+str(volume2)+" ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],True)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],True)
	SyringePump.Dispense(tunnelnum,volume2,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 3:OFF,4:ON,Dispense "+str(volume3)+" ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],True)
	time.sleep(waitTime)
	SyringePump.Dispense(tunnelnum,volume3,rate)
	
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 5:ON,4:OFF,Dispense "+str(volume4)+" ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],True)
	SyringePump.Dispense(tunnelnum,volume4,rate)
	
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 1:OFF,5:OFF,2:ON,Dispense All ul(NO)")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],True)
	
	time.sleep(waitTime)
	SyringePump.Dispense(tunnelnum,rate)
	
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Radiotube 2:OFF")
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)
	engOp.SendStatusMessageInfo("Tunnel:"+str(tunnelnum)+"Post Loading Dispense Finish")

def PowerOffSolenoidValve(tunnelnum):
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][1],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][2],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][3],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][4],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][5],False)
	LoaderIOBoardV8.SolenoidValve(Tunnels[tunnelnum][6],False)

def MoveMotor(channal,upordown,over):
	if upordown =='down':
		if channal=='B':
			LoaderIOBoardV8.MoveSuctionMotorBToLower(20,over,0)
		elif channal=='A':
			LoaderIOBoardV8.MoveSuctionMotorAToLower(20,over,0)
		elif channal=='1':
			LoaderIOBoardV8.MoveCapMotor1stToLower(20,over,0)
		elif channal=='2':
			LoaderIOBoardV8.MoveCapMotor2ndToLower(20,over,0)
		elif channal=='3':
			LoaderIOBoardV8.MoveCapMotor3thToLower(20,over,0)
		elif channal=='4':
			LoaderIOBoardV8.MoveCapMotor4thToLower(20,over,0)
		elif channal=='5':
			LoaderIOBoardV8.MoveCapMotor5thToLower(20,over,0)
	elif upordown =='up':
		if channal=='B':
			LoaderIOBoardV8.MoveSuctionMotorBToUpper(20,over,0)
		elif channal=='A':
			LoaderIOBoardV8.MoveSuctionMotorAToUpper(20,over,0)
		elif channal=='1':
			LoaderIOBoardV8.MoveCapMotor1stToUpper(20,over,0)
		elif channal=='2':
			LoaderIOBoardV8.MoveCapMotor2ndToUpper(20,over,0)
		elif channal=='3':
			LoaderIOBoardV8.MoveCapMotor3thToUpper(20,over,0)
		elif channal=='4':
			LoaderIOBoardV8.MoveCapMotor4thToUpper(20,over,0)
		elif channal=='5':
			LoaderIOBoardV8.MoveCapMotor5thToUpper(20,over,0)