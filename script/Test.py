﻿import System
import System.Windows
import clr
import sys
import time
import threading
import ctypes
import inspect

clr.AddReference("BGI.LoaderV8Seq.Engineer.GUI.exe")
clr.AddReferenceByPartialName("System.Windows.Forms")
clr.AddReferenceByPartialName("System.Threading")
from System.Windows.Forms import *
from System.Threading import *
from BGI.LoaderV8Seq.Engineer.GUI import *

# reagents default map dictionary
reagentsMap = {'Air':24, 'DNB':23,"Water":17,'DRB':3, 'DCB':4 }

engOp = globals().get('EngineerOperationsClass')
V8Loader = engOp.GetDeviceObject("V8MutiLoader")
LoaderIOBoardV8 = V8Loader.IOBoard 
SyringePump = V8Loader.Pump
#RotaryValve1 = V8Loader.GetDevice('RotaryValve1').Device
#RotaryValve2 = V8Loader.GetDevice('RotaryValve2').Device
#RotaryValve3 = V8Loader.GetDevice('RotaryValve3').Device
#RotaryValve4 = V8Loader.GetDevice('RotaryValve4').Device
#RotaryValve5 = V8Loader.GetDevice('RotaryValve5').Device

RotaryValves = V8Loader.RotaryValves#{1:RotaryValve1,2:RotaryValve2,3:RotaryValve3,4:RotaryValve4,5:RotaryValve5}

OperationName="LoadingWash"
operation=V8Loader.GetOperation(OperationName)
tunnels=operation.GetTunnel()
thread_list=[]
IsRun=True
engOp.SendStatusMessageInfo("Top Level script Exception message")
def main(tunnel):
	try:
		
		number=1
		while number>0:
			number=number-1
			for i in range(11,22):
				if (i-11)%5==0:
					engOp.SendStatusMessageInfo("string i: {0}", str(i))
					time.sleep(2)
				#(rotaryvalve,pos(选择旋转阀口),volumn(流量),inrate(吸液速度),outrate(排液速度))
				
		
		MessageBox.Show("Tunnel:"+str(tunnel)+"排空完成！")
	except Exception as e:
		engOp.SendStatusMessageInfo("Top Level script Exception message: {0}", e)
	finally:
		engOp.SendStatusMessageInfo("End")
		global IsRun
		IsRun=False

def updatemsg(msg):
	V8Loader.UpdateStatus(OperationName,msg)		
	
def protect():
	global IsRun
	IsRun=True
	while(IsRun):
		state=str(operation.GetScriptState())
		if	state=="Stopping" or state=="Finished" or state=="None":
			try:
				for	t in thread_list:
					t.Abort()
			except Exception as e:
				print("Exception")
				#shared.engOp.SendStatusMessageInfo("Exception")
			finally:
				global IsRun
				IsRun=False
				shared.engOp.SendStatusMessageInfo("End Protect")
		time.sleep(1)

if not(LoaderIOBoardV8.Connected and SyringePump.Connected):
	MessageBox.Show("请先连接串口！")
	sys.exit(0)
	
for	tunnel in tunnels:
	if not RotaryValves[int(tunnel.Id)-1].Connected:
		MessageBox.Show("请先连接旋转阀串口！")
		sys.exit(0)

protect_thread=threading.Thread(target=protect)
protect_thread.start()
	
for	tunnel in tunnels:
	num=int(tunnel.Id)
	tstart=ParameterizedThreadStart(main)
	t=Thread(tstart)
	t.IsBackground=True
	t.Start(num)
	thread_list.append(t)
	
protect_thread.join()


for t in thread_list:
	t.Join()
	engOp.SendStatusMessage(str(time.time()))
	sys.exit(0)
