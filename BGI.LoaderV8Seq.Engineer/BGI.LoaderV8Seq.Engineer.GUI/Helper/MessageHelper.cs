﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BGI.LoaderV8Seq.Engineer.GUI.Helper
{
    public class MessageHelper
    {
        public static void ShowMessage(string msg)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                MessageBox.Show(msg);
            });
        }
    }
}
