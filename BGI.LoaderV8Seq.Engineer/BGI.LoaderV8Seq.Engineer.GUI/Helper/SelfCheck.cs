﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BGI.LoaderV8Seq.Engineer.GUI.Helper
{
    public class SelfCheck
    { 
        public static  List<ServiceController> GetISWService
        {
            get
            {
                List<ServiceController> LServiceController = new List<ServiceController>();
                #region Service detection 
                var serviceControllers = ServiceController.GetServices();
                foreach (var service in serviceControllers)
                {
                    if (service.ServiceName == "BGI.ZebraV01Seq.Service")
                    {
                        LServiceController.Add(service);
                    }
                } 
                #endregion 
                #region Process detection
                //System.Diagnostics.Process[] processList = System.Diagnostics.Process.GetProcesses();
                //foreach (System.Diagnostics.Process process in processList)
                //{
                //    if (process.ProcessName.ToUpper() == "WINRAR")
                //    {
                //        return true;
                //        process.Kill(); 
                //    }
                //}

                #endregion
                return LServiceController;
            }
        } 
    }
}
