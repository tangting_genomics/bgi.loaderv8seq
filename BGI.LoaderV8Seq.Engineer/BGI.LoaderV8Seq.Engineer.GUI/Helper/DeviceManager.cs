﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BGI.Common.Logging;
using BGI.Control.Device;
using BGI.Control.Device.Physical;
using BGI.Control.Device.Simulated;
using BGI.Common.Config;
using BGI.Common.Utils;
using System.Xml;
using System.Threading;
using System.Collections.Specialized;
using System.ServiceProcess;

namespace BGI.LoaderV8Seq.Engineer.GUI.Helper
{
    public enum StageTypeEnum
    {
        V1Stage,
        V0_1Stage,
        V2Stage
    }
    public enum ReagentNeedleTypeEnum
    {
        V1ReagentNeedle,
        V0_1ReagentNeedle,
        V2_ChipAReagentNeedle,
        V2_ChipBReagentNeedle,
    }
    public enum IOBoardTypeEnum
    {
        V1IOBoard = BoardTypeEnum.V1,
        V0_1IOBoard = BoardTypeEnum.V01,
        V2_ChipAIOBoard,
        V2_ChipBIOBoard,
    }
    public enum TemperatureTypeEnum
    {
        V1Temperature,
        V0_1Temperature,
        V2_ChipATemperature,
        V2_ChipBTemperature,
        V0_1TemperatureMin
    }
    public enum SelectionValveTypeEnum
    {
        V1SelectionValve,
        V0_1SelectionValve,
        V2_ChipASelectionValve,
        V2_ChipBSelectionValve,
    }
    public enum PumpSeqTypeEnum
    {
        V1Pump,
        V0_1Pump,
        V2_ChipAPump,
        V2_ChipBPump,
    }
    public enum CameraSeqTypeEnum
    {
        V1Camera,
        V0_1Camera,
        V2_CameraA,
        V2_CameraB,
    }
    public enum EngineerSeqEnum
    {
        V1,
        V0_1,
        V2,
        V8,
        none
    }
    public class DeviceManager
    {
        #region Fileds
        private static DeviceManager _DeviceManager;
        private Logger _logger = LogMgr.GetLogger("DeviceManager");
        private DeviceProxy _XYStage;
        private string appName;
        private ServiceController _CurrentService;
        //private AFTypeEnum afType = AFTypeEnum.ASI;

        private Dictionary<BoardTypeEnum, string> _DicTempTypeKey;
        private DeviceProxy _AFModule;
        private List<string> _IOBoardList = null;
        private Dictionary<string, DeviceProxy> _IOBoardProxyMap;
        private List<string> _TemperatureBoardList = null;
        private Dictionary<string, DeviceProxy> _TemperatureBoardProxyMap;
        private List<string> _PumpNameList = null;
        private Dictionary<string, DeviceProxy> _PumpProxyMap;
        private Dictionary<string, DeviceProxy> _SelectorValveMap;
        private Dictionary<PumpTypeEnum, string> _DicPumpTypeKey;
        private List<string> _ValveList = null;
        private Dictionary<string, DeviceProxy> _ValveProxyMap;

        private bool _IsUsedChipA = false;
        private bool _IsUsedChipB = false;
        #endregion

        #region Ctor
        private DeviceManager()
        {
            this._IOBoardList = new List<string>();
            this._IOBoardProxyMap = new Dictionary<string, DeviceProxy>();

            this._TemperatureBoardList = new List<string>();
            this._TemperatureBoardProxyMap = new Dictionary<string, DeviceProxy>();
            
           
            this._PumpNameList = new List<string>();
            this._PumpProxyMap = new Dictionary<string, DeviceProxy>();

            this._ValveList = new List<string>();
            this._ValveProxyMap = new Dictionary<string, DeviceProxy>();
            
            //_DicIOTypeKey = new Dictionary<BoardTypeEnum, string>();
            //_DicIOTypeKey.Add(BoardTypeEnum.V1, "IO_V1");
            //_DicIOTypeKey.Add(BoardTypeEnum.V01, "IO_V01");
            //_DicIOTypeKey.Add(BoardTypeEnum.V01_1, "IO_Mini");
            //_DicIOTypeKey.Add(BoardTypeEnum.V2, "IO_V2");

            _DicTempTypeKey = new Dictionary<BoardTypeEnum, string>();
            _DicTempTypeKey.Add(BoardTypeEnum.V1, "TC_V1");
            _DicTempTypeKey.Add(BoardTypeEnum.V01, "TC_V01");
            _DicTempTypeKey.Add(BoardTypeEnum.V01_1, "TC_Mini");
            _DicTempTypeKey.Add(BoardTypeEnum.V2, "TC_V2");
        }
        public static DeviceManager Get
        {
            get
            {
                if (_DeviceManager == null)
                {
                    _DeviceManager = new DeviceManager();
                    _DeviceManager.LoadCfg();
                }
                return _DeviceManager;
            }
        }
        #endregion

        #region Constants
        //private const string APP_V01 = "BGI.ZebraV01Seq.Service";
        //private const string APP_V02 = "BGI.ZebraV01Seq.Service";//BGI.ZebraV02Seq.Service
        //private const string APP_V2 = "BGI.ZebraV2Seq.Service";
        //private const string APP_V8 = "BGI.ZebraV8Seq.Service";
        //private const string IOBoardType = "V8IOBoard";// BoardTypeEnum.V01;
        //private const string TCBoardType = "V8TempBoard";
        //private const string SyringPumpType = "V8SyringPump";
        //private const string ValveType = "V8Valve";

        private const string LOADER_V8 = "BGI.LoaderV8Seq.Engineer";
        private const string CATEGORY_IO = "V8IOBoard";
        private const string CATEGORY_TEMPERATURE = "V8TempBoard";
        private const string CATEGORY_PUMP = "SyringePump";
        private const string CATEGORY_VALVE = "SelectorValve";
        private const string CATEGORY_SYSTEM = "System";
        #endregion

        #region Properties
        public string AppName
        {
            get
            {
                return appName;
            }
        }

        public bool IsCfgOnline { get; private set; } = false;
        
      

        

        #region For IO Board
        //public List<string> IOBoardList
        //{
        //    get
        //    {
        //        if (_IOBoardList != null && _IOBoardList.Count > 0)
        //            return _IOBoardList;
        //        _IOBoardList = new List<string>();
        //        if (appName == APP_V01)
        //        {
        //            _IOBoardList.Add(_DicIOTypeKey[this.ioBoardType]);
        //        }
        //        else if (appName == APP_V2)
        //        {
        //            _IOBoardList.Add($"{_DicIOTypeKey[this.ioBoardType]}_A");
        //            _IOBoardList.Add($"{_DicIOTypeKey[this.ioBoardType]}_B");
        //        }
        //        else if (appName == APP_V8)
        //        {

        //        }
        //        return _IOBoardList;
        //    }
        //}
        public Dictionary<string, DeviceProxy> IOBoardProxyMap
        {
            get
            {
                if (_IOBoardProxyMap != null && _IOBoardProxyMap.Count > 0)
                {
                    return _IOBoardProxyMap;
                }
                _IOBoardProxyMap = new Dictionary<string, DeviceProxy>();

                DeviceProxy proxy = null;

                proxy = new DeviceProxy(CATEGORY_IO, typeof(VLoaderBoardV8), null);
                proxy.Initialize();
                _IOBoardProxyMap.Add(CATEGORY_IO, proxy);
                proxy.Connect();
                return _IOBoardProxyMap;
            }
        }
        #endregion

        #region For Temperature Board
        public List<string> TemperatureBoardList
        {
            get
            {

                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_A");
                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_B");
                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_C");

                return _TemperatureBoardList;
            }
        }
        public Dictionary<string, DeviceProxy> TemperatureBoardProxyMap
        {
            get
            {
                if (_TemperatureBoardProxyMap != null && _TemperatureBoardProxyMap.Count > 0)
                {
                    return _TemperatureBoardProxyMap;
                }

                DeviceProxy proxyA = null;
                proxyA = new DeviceProxy($"{CATEGORY_TEMPERATURE}_A", typeof(VTemperatureBoardLoaderV8), null);
                proxyA.Initialize();
                //proxyA.Connect();
                _TemperatureBoardProxyMap.Add($"{CATEGORY_TEMPERATURE}_A", proxyA);
                DeviceProxy proxyB = null;
                proxyB = new DeviceProxy($"{CATEGORY_TEMPERATURE}_B", typeof(VTemperatureBoardLoaderV8), null);
                proxyB.Initialize();
                //proxyB.Connect();
                _TemperatureBoardProxyMap.Add($"{CATEGORY_TEMPERATURE}_B", proxyB);
                DeviceProxy proxyC = null;
                proxyC = new DeviceProxy($"{CATEGORY_TEMPERATURE}_C", typeof(VTemperatureBoardLoaderV8), null);
                proxyC.Initialize();
                //proxyC.Connect();
                //((VTemperatureBoardLoaderV8)proxyC._VDeviceInt).SetTargetTemperature(4, 2);
                _TemperatureBoardProxyMap.Add($"{CATEGORY_TEMPERATURE}_C", proxyC);
                return _TemperatureBoardProxyMap;
            }
        }
        
        /// <summary>
        /// Used to Display Slide Current Temperature
        /// </summary>
        public byte[] DisplayTCChannels
        {
            get
            {
                return new byte[] { 0x01, 0x02 };
            }
        }
       
        /// <summary>
        /// Used to Set Slide Target Temperature
        /// </summary>
        public byte[] TargetTCChannels
        {
            get
            {
                BoardTypeEnum boardType = ConfigMgr.Get.GetCurValue<BoardTypeEnum>(CATEGORY_SYSTEM, "TemperatureBoardType");
                if (boardType == BoardTypeEnum.V2)
                {
                    StringCollection sc = ConfigMgr.Get.GetCurValue<StringCollection>(CATEGORY_TEMPERATURE, "ChannelChipA");
                    return GetTemperatureChannel(sc);
                }
                if (boardType == BoardTypeEnum.V01)
                {
                    return new byte[] { 0x04 };
                }
                if (boardType == BoardTypeEnum.V01_1)
                {
                    return new byte[] { 0x04 };
                }
                if (boardType == BoardTypeEnum.V1)
                {
                    return new byte[] { 0x01 };
                }
                return null;
            }
        }
        
        private byte[] GetTemperatureChannel(StringCollection channelCollection)
        {
            try
            {
                byte[] channel = new byte[channelCollection.Count];
                for (int i = 0; i < channelCollection.Count; i++)
                {
                    channel[i] = Convert.ToByte(channelCollection[i]);
                }
                return channel;
            }
            catch (Exception ex)
            {
                _logger.Log()(LogSeverityEnum.Error, ex.ToString());
                throw new BGIException("Invalid Channel Parameters");
            }
        }
        #endregion
        
        #region For SyringPump
        private bool _CanBypass = true;
        public bool CanBypass
        {
            get { return _CanBypass; }
            set { _CanBypass = value; }
        }
     
        public Dictionary<string, DeviceProxy> SyringPumpProxyMap
        {
            get
            {
                if (_PumpProxyMap != null && _PumpProxyMap.Count > 0) return _PumpProxyMap;
                _PumpProxyMap = new Dictionary<string, DeviceProxy>();
                DeviceProxy proxy = null;
                

                proxy = new DeviceProxy(CATEGORY_PUMP, typeof(VCavroXLP6KPumpV1), null);
                proxy.Initialize();
                proxy.Connect();
                _PumpProxyMap.Add($"{CATEGORY_PUMP}", proxy);
                return _PumpProxyMap;
            }
        }
        #endregion

        #region Selector Valve
        public Dictionary<string, DeviceProxy> SelectorValveMap
        {
            get
            {
                if (_SelectorValveMap != null && _SelectorValveMap.Count > 0)
                    return _SelectorValveMap;
                _SelectorValveMap = new Dictionary<string, DeviceProxy>();

                DeviceProxy proxy1 = null;
                proxy1 = new DeviceProxy($"{CATEGORY_VALVE}_1", typeof(VIdexUartValve2), null);
                proxy1.Initialize();
                //proxy1.Connect();
                _SelectorValveMap.Add($"{CATEGORY_VALVE}_1", proxy1);

                DeviceProxy proxy2 = null;
                proxy2 = new DeviceProxy($"{CATEGORY_VALVE}_2", typeof(VIdexUartValve2), null);
                proxy2.Initialize();
                //proxy2.Connect();
                _SelectorValveMap.Add($"{CATEGORY_VALVE}_2", proxy2);
                DeviceProxy proxy3 = null;
                proxy3 = new DeviceProxy($"{CATEGORY_VALVE}_3", typeof(VIdexUartValve2), null);
                proxy3.Initialize();
                //proxy3.Connect();
                _SelectorValveMap.Add($"{CATEGORY_VALVE}_3", proxy3);
                DeviceProxy proxy4 = null;
                proxy4 = new DeviceProxy($"{CATEGORY_VALVE}_4", typeof(VIdexUartValve2), null);
                proxy4.Initialize();
                //proxy4.Connect();
                _SelectorValveMap.Add($"{CATEGORY_VALVE}_4", proxy4);
                DeviceProxy proxy5 = null;
                proxy5 = new DeviceProxy($"{CATEGORY_VALVE}_5", typeof(VIdexUartValve2), null);
                proxy5.Initialize();
                //proxy5.Connect();
                _SelectorValveMap.Add($"{CATEGORY_VALVE}_5", proxy5);

                return _SelectorValveMap;
            }
        }
        
        #endregion;

        

        #endregion

        #region Private Methods
        private bool isLoadCfg = false;
        private void LoadCfg()
        {
            if (!isLoadCfg)
            {
                bool rcEnabled = ConfigMgr.Get.Enabled;
                if (!rcEnabled)
                {
                    _logger.Log()(LogSeverityEnum.Error, "Remote config not enabled.  Running offline if possible.");
                }
                this.appName = ConfigMgr.Get.ApplicationName;
                
                ConfigMgr.Get.RunOffline += Get_RunOffline;
                Stat stat = ConfigMgr.Get.Load(true);
                if (stat.IsErr)
                {
                    _logger.Log()(LogSeverityEnum.Error, "Could not load Config: {0}", stat.Header);
                    this.isLoadCfg = false;
                    this.IsCfgOnline = false;
                    return;
                }
                this.IsCfgOnline = ConfigMgr.Get.IsOnline;
                LogSeverityEnum sev = ConfigMgr.Get.GetCurValue<LogSeverityEnum>("System", "LogLevel");
                _logger.SetMinLogLevel(sev);
                ConfigMgr.Get.LogLevel = sev;
                ConfigMgr.Get.RegisterLogLevelUpdate(_logger, "System", "LogLevel");
               
                isLoadCfg = true;
            }
        }


        #endregion

        #region Public Methods
        public void SetParameter(string category, string paraName, object value)
        {
            try
            {
                if (this.IsCfgOnline)
                {
                    ConfigMgr.Get.SetCurValue(category, paraName, value, $"Changed {category}-{paraName} by Engineer");
                    ConfigMgr.Get.SaveParameter(category, paraName);
                    
                }
                else
                {
                    string configPath = $@"C:\BGI\Config\{appName}.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.Load(configPath);
                    XmlNode node = doc.SelectSingleNode($"Configs/InstrumentConfig[Para_Set_Name='{category}'][Para_Name='{paraName}']");
                    if (node != null)
                    {
                        XmlNode valueNode = node.SelectSingleNode("Cur_Value");
                        if (valueNode != null && valueNode.FirstChild != null)
                        {
                            valueNode.FirstChild.Value = value.ToString();
                            doc.Save(configPath);
                        }
                        else
                            throw new BGIException($"Not find Catory={category} ParaName={paraName}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Log()(LogSeverityEnum.Error, ex.ToString());
                throw;
            }
        }

        public void Disconnect(EnigneerDeviceType deviceType)
        {
            try
            {
                Dictionary<string, DeviceProxy> tempMap = new Dictionary<string, DeviceProxy>();
                switch (deviceType)
                {
                    case EnigneerDeviceType.IOBoard:
                        tempMap = _IOBoardProxyMap;
                        break;
                    case EnigneerDeviceType.TemperatureBoard:
                        tempMap = _TemperatureBoardProxyMap;
                        break;
                    case EnigneerDeviceType.SelectorValve:
                        tempMap = _SelectorValveMap;
                        break;
                    case EnigneerDeviceType.SyringPump:
                        tempMap = _PumpProxyMap;
                        break;
                  
                }
                foreach (string key in tempMap.Keys)
                {
                    DeviceProxy proxy = tempMap[key];
                    proxy?.Disconnect();
                }
                tempMap.Clear();
            }
            catch(Exception ex)
            {
                _logger.Log()(LogSeverityEnum.Error, $"{deviceType.ToString()} Disconnect Failed {ex.ToString()}");
            }
            
        }
        private int _CurrentChipNo = 0;
        public int CurrentChipNo
        {
            get
            {
                return _CurrentChipNo;
            }
            set
            {
                _CurrentChipNo = value;
            }
        }

        public bool? HasService
        {
            get
            {
                var serviceControllers = ServiceController.GetServices();
                bool isFind = false;
                foreach(var service in serviceControllers)
                {
                    if(service.DisplayName == this.appName)
                    {
                        isFind = true;
                        _CurrentService = service;
                        if(service.Status == ServiceControllerStatus.Running)
                        {
                            return true;
                        }
                    }

                }
                if (isFind) return false;
                return null;
            }
        }

        public void StartService(ref string errMsg)
        {
            try
            {
                if (_CurrentService != null && _CurrentService.Status == ServiceControllerStatus.Stopped)
                {
                    _CurrentService.Start();
                    DateTime finished = DateTime.UtcNow.AddMinutes(3);
                    while (true)
                    {
                        var serviceControllers = ServiceController.GetServices();
                        bool isRunning = false;
                        foreach(var service in serviceControllers)
                        {
                            if (service.DisplayName == this.appName && service.Status == ServiceControllerStatus.Running)
                            {
                                isRunning = true;
                                break;
                            }
                        }
                        if (isRunning) break;
                        if (DateTime.UtcNow > finished)
                        {
                            throw new Exception("Timeout");
                        }
                        Thread.Sleep(500);
                    }
                }
                    
            }
            catch(Exception ex)
            {
                errMsg = ex.Message;
            }
        }

        public void StopService(ref string errMsg)
        {
            try
            {
                if (_CurrentService != null && _CurrentService.Status == ServiceControllerStatus.Running)
                {
                    _CurrentService.Stop();
                    DateTime finished = DateTime.UtcNow.AddMinutes(3);
                    while (true)
                    {
                        var serviceControllers = ServiceController.GetServices();
                        bool isStopped = false;
                        foreach (var service in serviceControllers)
                        {
                            if (service.DisplayName == this.appName && service.Status == ServiceControllerStatus.Stopped)
                            {
                                isStopped = true;
                                break;
                            }
                        }
                        if (isStopped) break;
                        if (DateTime.UtcNow > finished)
                        {
                            throw new Exception("Timeout");
                        }
                        Thread.Sleep(500);
                    }
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
        }

        public void Shutdown()
        {
            Disconnect(EnigneerDeviceType.TemperatureBoard);
            Disconnect(EnigneerDeviceType.IOBoard);
            Disconnect(EnigneerDeviceType.ReagentNeedle);
            Disconnect(EnigneerDeviceType.SelectorValve);
            Disconnect(EnigneerDeviceType.SyringPump);
            Disconnect(EnigneerDeviceType.AF);
            Disconnect(EnigneerDeviceType.Stage);
            Disconnect(EnigneerDeviceType.Camera);
        }

    
        #endregion

        #region Events
        private void Get_RunOffline(Stat t, System.ComponentModel.CancelEventArgs u)
        {
            u.Cancel = true;
            this.IsCfgOnline = false;
        }
        #endregion


        public enum EnigneerDeviceType
        {
            IOBoard=0,
            TemperatureBoard,
            SelectorValve,
            SyringPump,
            LedBelt,
            Camera,
            Stage,
            AF,
            ReagentNeedle,
            Unknown,
        }
    }
}
