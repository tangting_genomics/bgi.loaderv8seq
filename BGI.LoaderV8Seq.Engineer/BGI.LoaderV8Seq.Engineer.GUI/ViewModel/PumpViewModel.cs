﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Input;
using BGI.Control.Device;
using BGI.Control.Device.Physical;
using System.Windows;
using System.Threading;
using BGI.LoaderV8Seq.Engineer.GUI.Helper;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class PumpViewModel : NewViewModelBase
    {
        private const string ValveType = "V8Valve";
        private VLoaderBoardV8 IOBoardv8;
        private ISyringePump sp;
        private ISyringePump spbypass;
        private Task task;
        private bool bStop;

        #region(Property)
        private List<PumpSeqTypeEnum> _TypeSource;
        public List<PumpSeqTypeEnum> TypeSource
        {
            get
            {
                _TypeSource = new List<PumpSeqTypeEnum>();
                foreach (string name in Enum.GetNames(typeof(PumpSeqTypeEnum)))
                {
                    var type = (PumpSeqTypeEnum)Enum.Parse(typeof(PumpSeqTypeEnum), name);
                    _TypeSource.Add(type);
                }
                return _TypeSource;
            }
            set
            {
                _TypeSource = value;
                RaisePropertyChanged("TypeSource");
            }
        }
     
        private bool _bInitEnabled = true;
        public bool bInitEnabled
        {
            get { return _bInitEnabled; }
            set
            {
                _bInitEnabled = value;
                this.RaisePropertyChanged("bInitEnabled");
            }
        }

        public PumpViewModel(DeviceManager dm)
        {
            this.DeviceManagerEntity = DeviceManagerEntity;
            _iASpeed = 0;
            _iDSpeed = 0;
        }
        private int _iCycleCount = 1;
        public int iCycleCount
        {
            get { return _iCycleCount; }
            set
            {
                _iCycleCount = value;
                this.RaisePropertyChanged("iCycleCount");
            }
        }

        private int _iCurCycle = 0;
        public int iCurCycle
        {
            get { return _iCurCycle; }
            set
            {
                _iCurCycle = value;
                this.RaisePropertyChanged("iCurCycle");
            }
        }


        private bool _bPowerOnEnabled = true;
        public bool bPowerOnEnabled
        {
            get { return _bPowerOnEnabled; }
            set
            {
                _bPowerOnEnabled = value;
                this.RaisePropertyChanged("bPowerOnEnabled");
            }
        }

        private bool _bPowerOffEnabled = false;
        public bool bPowerOffEnabled
        {
            get { return _bPowerOffEnabled; }
            set
            {
                _bPowerOffEnabled = value;
                this.RaisePropertyChanged("bPowerOffEnabled");
            }
        }

        private bool _bPHomeEnabled = false;
        public bool bPHomeEnabled
        {
            get { return _bPHomeEnabled; }
            set
            {
                _bPHomeEnabled = value;
                this.RaisePropertyChanged("bPHomeEnabled");
            }
        }

        private bool _bAspirateEnabled = false;
        public bool bAspirateEnabled
        {
            get { return _bAspirateEnabled; }
            set
            {
                _bAspirateEnabled = value;
                this.RaisePropertyChanged("bAspirateEnabled");
            }
        }

        private bool _bDispenseEnabled = false;
        public bool bDispenseEnabled
        {
            get { return _bDispenseEnabled; }
            set
            {
                _bDispenseEnabled = value;
                this.RaisePropertyChanged("bDispenseEnabled");
            }
        }
        private int _iASpeed = 0;
        public int iASpeed
        {
            get { return _iASpeed; }
            set
            {
                _iASpeed = value;
                this.RaisePropertyChanged("iASpeed");
            }
        }
        private int _iAVolume = 0;
        public int iAVolume
        {
            get { return _iAVolume; }
            set
            {
                _iAVolume = value;
                this.RaisePropertyChanged("iAVolume");
            }
        }

        private int _iDSpeed = 0;
        public int iDSpeed
        {
            get { return _iDSpeed; }
            set
            {
                _iDSpeed = value;
                this.RaisePropertyChanged("iDSpeed");
            }
        }
        private int _iDVolume = 0;
        public int iDVolume
        {
            get { return _iDVolume; }
            set
            {
                _iDVolume = value;
                this.RaisePropertyChanged("iDVolume");
            }
        }

        private int _iAPos = 0;
        public int iAPos
        {
            get { return _iAPos; }
            set
            {
                _iAPos = value;
                this.RaisePropertyChanged("iAPos");
            }
        }

        private int _iDPos = 0;
        public int iDPos
        {
            get { return _iDPos; }
            set
            {
                _iDPos = value;
                this.RaisePropertyChanged("iDPos");
            }
        }


        private bool _bStartUpEnabled = false;
        public bool bStartUpEnabled
        {
            get { return _bStartUpEnabled; }
            set
            {
                _bStartUpEnabled = value;
                this.RaisePropertyChanged("bStartUpEnabled");
            }
        }

        private bool _bStopEnabled = false;
        public bool bStopEnabled
        {
            get { return _bStopEnabled; }
            set
            {
                _bStopEnabled = value;
                this.RaisePropertyChanged("bStopEnabled");
            }
        }


        #endregion

        #region(Command)
        private ICommand _initVViewCommand;
        public ICommand InitVViewCommand
        {
            get
            {
                return _initVViewCommand ?? (_initVViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        //var selectionValveType = (SelectionValveTypeEnum)Enum.ToObject(typeof(SelectionValveTypeEnum), ((int)this));
                      
                    
                        //IOBoardv8 = (VIOBoardV8)DeviceManagerEntity.IOBoardProxyMap.First().Value.VDevice;

                        //IOBoardv8.SyringePump(true);
                       
                        ////sp = pumpdic.First().Key;
                        ////spbypass = pumpdic.First().Value;
                        //bPowerOnEnabled = true;
                        //bInitEnabled = false;
                    }
                    catch
                    {

                    }
                    finally
                    {
                        ;
                    }
                }));
            }

        }

        private ICommand _goPHomeViewCommand;
        public ICommand GoPHomeViewCommand
        {
            get
            {
                return _goPHomeViewCommand ?? (_goPHomeViewCommand = new RelayCommand(() =>
                {
                    sp.Home();
                }));
            }

        }

        private ICommand _aspirateViewCommand;
        public ICommand AspirateViewCommand
        {
            get
            {
                return _aspirateViewCommand ?? (_aspirateViewCommand = new RelayCommand(() =>
                {
                    if (_iASpeed == 0)
                    {
                        MessageBox.Show("Please input the speed of diaphragm");
                        return;
                    }
                    if (_iAVolume == 0)
                    {
                        MessageBox.Show("Please input the volume of diaphragm");
                        return;
                    }
                    //sp.AspirateSafety(_iVolume,_iSpeed);
                    for (int i = 0; i < iCycleCount; i++)
                    {
                        //sp.Aspirate(_iAVolume, _iASpeed, _bIsAChecked);
                        //sp.Dispense(_iDVolume, _iDSpeed, _bIsDChecked);
                        iCurCycle++;
                    }

                }));
            }

        }

        private ICommand _dispenseViewCommand;
        public ICommand DispenseViewCommand
        {
            get
            {
                return _dispenseViewCommand ?? (_dispenseViewCommand = new RelayCommand(() =>
                {
                    if (_iDSpeed == 0)
                    {
                        MessageBox.Show("Please input the speed of diaphragm");
                        return;
                    }
                    if (_iDVolume == 0)
                    {
                        MessageBox.Show("Please input the volume of diaphragm");
                        return;
                    }
                    //sp.Dispense(_iDVolume, _iDSpeed,_bDIsChecked);
                }));
            }

        }

        private ICommand _powerOnViewCommand;
        public ICommand PowerOnViewCommand
        {
            get
            {
                return _powerOnViewCommand ?? (_powerOnViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                       
                        IOBoardv8.SyringePump(true);
                        setControlEnabled(true);
                        //MessageBox.Show("operate success");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }

        }

        private ICommand _powerOffViewCommand;
        public ICommand PowerOffViewCommand
        {
            get
            {
                return _powerOffViewCommand ?? (_powerOffViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        IOBoardv8.SyringePump(false);
                        setControlEnabled(false);
                        //MessageBox.Show("operate success");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }

        }

        private ICommand _startViewCommand;
        public ICommand StartViewCommand
        {
            get
            {
                return _startViewCommand ?? (_startViewCommand = new RelayCommand(() =>
                {
                    if (_iASpeed == 0)
                    {
                        MessageBox.Show("Please input the speed of diaphragm");
                        return;
                    }
                    if (_iAVolume == 0)
                    {
                        MessageBox.Show("Please input the volume of diaphragm");
                        return;
                    }
                    if (_iDSpeed == 0)
                    {
                        MessageBox.Show("Please input the speed of diaphragm");
                        return;
                    }
                    if (_iDVolume == 0)
                    {
                        MessageBox.Show("Please input the volume of diaphragm");
                        return;
                    }
                    if (_iDVolume > _iAVolume)
                    {
                        MessageBox.Show("Volume of Dispense volume should be less than Aspirate.");
                        return;
                    }
                    bStop = false;
                    bStartUpEnabled = false;
                    startAction();
                    iCurCycle = 0;
                    task.Start();
                }));
            }

        }
        private ICommand _stopViewCommand;
        public ICommand StopViewCommand
        {
            get
            {
                return _stopViewCommand ?? (_stopViewCommand = new RelayCommand(() =>
                {
                    bStop = true;
                    sp.Home();
                }));
            }

        }

        #endregion
        private void startAction()
        {
            task = new Task(() =>
            {

                for (int i = 0; i < iCycleCount; i++)
                {
                    if (bStop)
                        break;
                   
                    sp.Aspirate(_iAVolume, _iASpeed, false);
                    Thread.Sleep(100);
                    sp.Dispense(_iDVolume, _iDSpeed, false);
                    Thread.Sleep(100);
                    iCurCycle++;
                }
                bStartUpEnabled = true;
            });
        }
        private void setControlEnabled(bool bPowerOn)
        {
            if (bPowerOn)
            {

                //bPHomeEnabled = true;
                //bAspirateEnabled = true;
                //bDispenseEnabled = true;
                bPowerOffEnabled = true;
                bStartUpEnabled = true;
                bStopEnabled = true;
                bPowerOnEnabled = false;
            }
            else
            {
                //bPHomeEnabled = false;
                //bAspirateEnabled = false;
                //bDispenseEnabled = false;
                bPowerOnEnabled = true;
                bStopEnabled = false;
                bStartUpEnabled = false;
                bPowerOffEnabled = false;
            }
        }
    }
}
