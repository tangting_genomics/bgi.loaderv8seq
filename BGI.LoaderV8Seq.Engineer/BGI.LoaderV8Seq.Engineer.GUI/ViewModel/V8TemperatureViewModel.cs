﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BGI.Control.Device;
using BGI.Control.Device.Physical;
using BGI.Control.Device.Simulated;
using System.Windows;
using System.Windows.Controls;
using BGI.Common.Logging;
using BGI.Common.Config;
using System.Collections.Specialized;
using BGI.Common.Utils;
using BGI.Common.UtilWPF.Helpers;
using BGI.Scripting;
using System.Xml;
using BGI.LoaderV8Seq.Engineer.GUI.Helper;
using BGI.LoaderV8Seq.Engineer.GUI.Views;
using System.Windows.Media;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class V8TemperatureViewModel : MainViewModelBase
    {

        private const string CATEGORY_TEMPERATURE = "V8TempBoard";
        private string _TargetTemp = string.Empty;

        private bool _isInit;
        private string _version;
        private string _pValueSet;
        private float _pValueGet;
        private string _iValueSet;
        private float _iValueGet;
        private string _dValueSet;
        private float _dValueGet;
        private bool _canPowerOnOffFansAndPump;

        private string _enableTCDesc;
        private string _PowerOnOffFansDesc;
        private string _PowerOnOffPumpDesc;

        private ICommand _goInitViewCommad;
        private ICommand _goSetTargetViewCommand;
        private ICommand _goGetTargetViewCommand;
        private ICommand _getVersionCMD;
        private ICommand _getPIDCMD;
        private ICommand _setPIDCMD;
        private ICommand _enableTCCMD;
        private ICommand _disableTCCMD;
        private ICommand _powerOnPumpCMD;
        private ICommand _powerOffPumpCMD;
        private ICommand _powerOnFansCMD;
        private ICommand _powerOffFansCMD;
        private ICommand _SetParaCommand;
        private ICommand _ResetParaCommand;
        //private DeviceManager1 DeviceManagerEntity;
        //private bool _IsV2ChipB = false;
        private string _InitDesc = "Initialize";

        private string _SlideToPT100;
        private string _PT100ToSlide;

        private List<string> _TargetChannels = new string[] { "1", "2" }.ToList();
        private string _TargetChannel;

        private bool _IsTCInit;

        private List<string> _TemperatureBoardList = new List<string>();
        private Dictionary<string, VTemperatureBoardLoaderV8> _TemperatureBoardProxyMap;
        private DeviceManager DeviceManagerEntity;
        private Logger logger = LogMgr.GetLogger(CATEGORY_TEMPERATURE);

        #region this is maybe template properties

        private VTemperatureBoardLoaderV8 TempBoardA;
        private VTemperatureBoardLoaderV8 TempBoardB;
        private VTemperatureBoardLoaderV8 TempBoardC;

        private double _CurrentTarget;
        #endregion
        public V8TemperatureViewModel(string modelName) : base(modelName)
        {
            this.DeviceManagerEntity = DeviceManager.Get;
            this._TemperatureBoardProxyMap = new Dictionary<string, VTemperatureBoardLoaderV8>();

            if (this.TemperatureBoardList.Count > 0) this.TemperatureName = TemperatureBoardList[0];
            this.PT100ToSlide = "0,1";
            this.SlideToPT100 = "0,1";
            this.ChannelA1Visibility = Visibility.Hidden;
            this.ChannelA2Visibility = Visibility.Hidden;
            this.ChannelB1Visibility = Visibility.Hidden;
            this.ChannelB2Visibility = Visibility.Hidden;
            this.ChannelC1Visibility = Visibility.Hidden;
            this.ChannelC2Visibility = Visibility.Hidden;
            this.InitDesc = "Initialize";

            this.IsTCInit = true;
        }

        #region Property

        public bool IsTCInit
        {
            get { return _IsTCInit; }
            set { _IsTCInit = value;
                RaisePropertyChanged("IsTCInit");
            }
        }
        public double CurrentTarget
        {
            get { return _CurrentTarget; }
            set { _CurrentTarget = value;
                RaisePropertyChanged("CurrentTarget");
            }
        }

        #region For Display Current Temperature realtime,this way maybe template

        private string _ChannelA1Name;
        public string ChannelA1Name
        {
            get { return _ChannelA1Name; }
            set { _ChannelA1Name = value;
                RaisePropertyChanged("ChannelA1Name");
            }
        }

        private double _ChannelA1Value;
        public double ChannelA1Value
        {
            get { return _ChannelA1Value; }
            set { _ChannelA1Value = value;
                RaisePropertyChanged("ChannelA1Value");
            }
        }

        private Visibility _ChannelA1Visibility;
        public Visibility ChannelA1Visibility
        {
            get { return _ChannelA1Visibility; }
            set { _ChannelA1Visibility = value;
                RaisePropertyChanged("ChannelA1Visibility");
            }
        }

        private string _ChannelA2Name;
        public string ChannelA2Name
        {
            get { return _ChannelA2Name; }
            set { _ChannelA2Name = value;
                RaisePropertyChanged("ChannelA2Name");
            }
        }

        private double _ChannelA2Value;
        public double ChannelA2Value
        {
            get { return _ChannelA2Value; }
            set { _ChannelA2Value = value;
                RaisePropertyChanged("ChannelA2Value");
            }
        }

        private Visibility _ChannelA2Visibility;
        public Visibility ChannelA2Visibility
        {
            get { return _ChannelA2Visibility; }
            set { _ChannelA2Visibility = value;
                RaisePropertyChanged("ChannelA2Visibility");
            }
        }
        private string _ChannelB1Name;
        public string ChannelB1Name
        {
            get { return _ChannelB1Name; }
            set { _ChannelB1Name = value;
                RaisePropertyChanged("ChannelB1Name");
            }
        }

        private double _ChannelB1Value;
        public double ChannelB1Value
        {
            get { return _ChannelB1Value; }
            set { _ChannelB1Value = value;
                RaisePropertyChanged("ChannelB1Value");
            }
        }

        private Visibility _ChannelB1Visibility;
        public Visibility ChannelB1Visibility
        {
            get { return _ChannelB1Visibility; }
            set { _ChannelB1Visibility = value;
                RaisePropertyChanged("ChannelB1Visibility");
            }
        }

        private string _ChannelB2Name;
        public string ChannelB2Name
        {
            get { return _ChannelB2Name; }
            set { _ChannelB2Name = value;
                RaisePropertyChanged("ChannelB2Name");
            }
        }

        private double _ChannelB2Value;
        public double ChannelB2Value
        {
            get { return _ChannelB2Value; }
            set { _ChannelB2Value = value;
                RaisePropertyChanged("ChannelB2Value");
            }
        }

        private Visibility _ChannelB2Visibility;
        public Visibility ChannelB2Visibility
        {
            get { return _ChannelB2Visibility; }
            set { _ChannelB2Visibility = value; RaisePropertyChanged("ChannelB2Visibility"); }
        }
        private string _ChannelC1Name;
        public string ChannelC1Name
        {
            get { return _ChannelC1Name; }
            set { _ChannelC1Name = value; RaisePropertyChanged("ChannelC1Name"); }
        }

        private double _ChannelC1Value;
        public double ChannelC1Value
        {
            get { return _ChannelC1Value; }
            set { _ChannelC1Value = value; RaisePropertyChanged("ChannelC1Value"); }
        }

        private Visibility _ChannelC1Visibility;
        public Visibility ChannelC1Visibility
        {
            get { return _ChannelC1Visibility; }
            set { _ChannelC1Visibility = value; RaisePropertyChanged("ChannelC1Visibility"); }
        }

        private string _ChannelC2Name;
        public string ChannelC2Name
        {
            get { return _ChannelC2Name; }
            set { _ChannelC2Name = value; RaisePropertyChanged("ChannelC2Name"); }
        }

        private double _ChannelC2Value;
        public double ChannelC2Value
        {
            get { return _ChannelC2Value; }
            set { _ChannelC2Value = value; RaisePropertyChanged("ChannelC2Value"); }
        }

        private Visibility _ChannelC2Visibility;
        public Visibility ChannelC2Visibility
        {
            get { return _ChannelC2Visibility; }
            set { _ChannelC2Visibility = value; RaisePropertyChanged("ChannelC2Visibility"); }
        }


        #endregion


        public string TargetChannel
        {
            get { return _TargetChannel; }
            set { _TargetChannel = value; }
        }
        public List<string> TargetChannels
        {
            get { return _TargetChannels; }
        }

        public string SlideToPT100
        {
            get { return _SlideToPT100; }
            set { _SlideToPT100 = value; }
        }

        public string PT100ToSlide
        {
            get { return _PT100ToSlide; }
            set { _PT100ToSlide = value; }
        }
        public string InitDesc
        {
            get { return _InitDesc; }
            set { _InitDesc = value;
                RaisePropertyChanged("InitDesc");
            }
        }

        public bool IsInit
        {
            get
            {
                return _isInit;
            }

            set
            {
                _isInit = value;
                RaisePropertyChanged("IsInit");
            }
        }


        private VTemperatureBoardLoaderV8 CurrentTempBoard
        {
            get
            {
                if (TemperatureName.EndsWith("_A")) return TempBoardA;
                else if (TemperatureName.EndsWith("_B")) return TempBoardB;

                else if (TemperatureName.EndsWith("_C")) return TempBoardC;
                else return null;
            }
        }

        public List<string> TemperatureBoardList
        {
            get
            {
                if (_TemperatureBoardList != null && _TemperatureBoardList.Count > 0) return _TemperatureBoardList;
                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_A");
                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_B");
                _TemperatureBoardList.Add($"{CATEGORY_TEMPERATURE}_C");
                return _TemperatureBoardList;

            }
        }

        public Dictionary<string, VTemperatureBoardLoaderV8> TemperatureBoardProxyMap
        {
            get
            {
                if (_TemperatureBoardProxyMap != null && _TemperatureBoardProxyMap.Count > 0)
                {
                    return _TemperatureBoardProxyMap;
                }

                TemperatureName = _TemperatureBoardList[0];
                foreach (var item in DeviceManager.Get.TemperatureBoardProxyMap)
                {
                    this._TemperatureBoardProxyMap.Add(item.Key, (VTemperatureBoardLoaderV8)item.Value.VDevice);
                }
                TempBoardA = _TemperatureBoardProxyMap.ElementAt(0).Value;
                TempBoardB = _TemperatureBoardProxyMap.ElementAt(1).Value;
                TempBoardC = _TemperatureBoardProxyMap.ElementAt(2).Value;
                TempBoardA.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                TempBoardA.AtomicValueChanged += TempBoardInt_AtomicValueChanged;

                TempBoardB.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                TempBoardB.AtomicValueChanged += TempBoardInt_AtomicValueChanged;

                TempBoardC.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                TempBoardC.AtomicValueChanged += TempBoardInt_AtomicValueChanged;
                return _TemperatureBoardProxyMap;
            }

        }

        private string _TemperatureName = $"{CATEGORY_TEMPERATURE}_A";
        public string TemperatureName
        {
            get { return _TemperatureName; }
            set
            {
                _TemperatureName = value;
                RaisePropertyChanged("TemperatureName");
            }
        }
        //private DeviceProxy _TemperatureDeviceProxy = null;
        //public DeviceProxy TemperatureDeviceProxy
        //{
        //    get { return _TemperatureDeviceProxy; }
        //    set
        //    {
        //        tempBoardDynamic = value._VDeviceInt;
        //        Version = tempBoardDynamic.GetVersion();
        //        _TemperatureDeviceProxy = value;
        //    }
        //}
        public string TargetTemp
        {
            get { return _TargetTemp; }
            set
            {
                _TargetTemp = value;
                RaisePropertyChanged("TargetTemp");
            }
        }

        public string Version
        {
            get
            {
                if (CurrentTempBoard != null)
                {
                    return CurrentTempBoard.GetVersion();
                }
                return string.Empty;
            }
            set
            {
                _version = value;
                RaisePropertyChanged("Version");
            }
        }
        public string PValueSet
        {
            get
            {
                return _pValueSet;
            }

            set
            {
                _pValueSet = value;
            }
        }

        public float PValueGet
        {
            get
            {
                return _pValueGet;
            }

            set
            {
                _pValueGet = value;
            }
        }

        public string IValueSet
        {
            get
            {
                return _iValueSet;
            }

            set
            {
                _iValueSet = value;
            }
        }

        public float IValueGet
        {
            get
            {
                return _iValueGet;
            }

            set
            {
                _iValueGet = value;
            }
        }

        public string DValueSet
        {
            get
            {
                return _dValueSet;
            }

            set
            {
                _dValueSet = value;
            }
        }

        public float DValueGet
        {
            get
            {
                return _dValueGet;
            }

            set
            {
                _dValueGet = value;
            }
        }

        public bool CanPowerOnOffFansAndPump
        {
            get
            {
                return _canPowerOnOffFansAndPump;
            }

            set
            {
                _canPowerOnOffFansAndPump = value;
                RaisePropertyChanged("_canPowerOnOffFansAndPump");
            }
        }

       
        public Brush ABoardConnected
        {
            get { if (TempBoardA == null) return Brushes.Gray;
                else
                {
                    return TempBoardA.Connected ? Brushes.Green : Brushes.Red;
                }
            }
        }

        public Brush BBoardConnected
        {
            get
            {
                if (TempBoardB == null) return Brushes.Gray;
                else
                {
                    return TempBoardB.Connected ? Brushes.Green : Brushes.Red;
                }
            }
        }
        public Brush CBoardConnected
        {
            get
            {
                if (TempBoardC == null) return Brushes.Gray;
                else
                {
                    return TempBoardC.Connected ? Brushes.Green : Brushes.Red;
                }
            }
        }
        #endregion

        #region Command
        public ICommand ResetParaCommand
        {
            get
            {
                return _ResetParaCommand ?? (_ResetParaCommand = new DelegateCommand2(() =>
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            MessageBoxResult result = MessageBox.Show("Are you sure reset Temperature factor", "Message", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                            if (result == MessageBoxResult.Yes)
                            {
                                string paraSP = "SlideToPT100";
                                string paraPS = "PT100ToSlide";
                                byte channel;
                                string board = string.Empty;
                                CheckChannnel((bool)true, out channel);
                                if (this.CurrentTempBoard != null && this.CurrentTempBoard.PortName != null)
                                {
                                    if (this.CurrentTempBoard.PortName == "COM1") board = "A";
                                    else if (this.CurrentTempBoard.PortName == "COM2") board = "B";
                                    else if (this.CurrentTempBoard.PortName == "COM3") board = "C";
                                }
                                paraSP = "SlideToPT100" + board + channel.ToString();
                                paraPS = "PT100ToSlide" + board + channel.ToString();


                                SetParameter(CATEGORY_TEMPERATURE, (string)paraSP, (object)"0,1");
                                SetParameter(CATEGORY_TEMPERATURE, (string)paraPS, (object)"0,1");
                                MessageBox.Show((string)"Reset Paramerter Success");
                            }
                        }
                        catch (Exception ex)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"Reset Parameters Failed {ex.ToString()}");
                            MessageBox.Show($"Reset Parameters Failed-{ex.Message}");
                        }

                    });
                }));
            }
        }
        public ICommand SetParaCommand
        {
            get
            {
                return _SetParaCommand ?? (_SetParaCommand = new DelegateCommand2(() =>
                {
                    Task.Run(() =>
                    {

                        if (string.IsNullOrEmpty(SlideToPT100) || string.IsNullOrEmpty(PT100ToSlide))
                        {
                            MessageBox.Show("Please Import(Input) Data and Select Calibration Arithmetic");
                            return;
                        }
                        try
                        {
                            string paraSP = "SlideToPT100";
                            string paraPS = "PT100ToSlide";

                            byte channel;
                            string board = string.Empty;
                            CheckChannnel((bool)true, out channel);
                            if (this.CurrentTempBoard != null && this.CurrentTempBoard.PortName != null)
                            {
                                if (this.CurrentTempBoard.PortName == "COM1") board = "A";
                                else if (this.CurrentTempBoard.PortName == "COM2") board = "B";
                                else if (this.CurrentTempBoard.PortName == "COM3") board = "C";
                            }
                            paraSP = "SlideToPT100" + board + channel.ToString();
                            paraPS = "PT100ToSlide" + board + channel.ToString();


                            SetParameter(CATEGORY_TEMPERATURE, (string)paraSP, (object)this.SlideToPT100);
                            SetParameter(CATEGORY_TEMPERATURE, (string)paraPS, (object)this.PT100ToSlide);
                            MessageBox.Show((string)"Set Paramerter Success");
                        }
                        catch (Exception ex)
                        {
                            _Logger.Log()(LogSeverityEnum.Error, $"Set Parameters Error{ex.ToString()}");
                            MessageBox.Show($"Set Paramerter Failed-{ex.Message}");
                        }
                    });

                }));
            }
        }

        public ICommand GoInitViewCmd
        {
            get
            {
                return _goInitViewCommad ?? (_goInitViewCommad = new DelegateCommand2(() =>
                {
                    Task task = Task.Run(() =>
                    {
                        this.IsTCInit = false;
                        try
                        {
                            //if (!_isInit)
                            //{
                            InitTempBoard();
                            if (CurrentTempBoard == null)
                                return;

                            TempBoardA.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                            TempBoardA.AtomicValueChanged += TempBoardInt_AtomicValueChanged;

                            TempBoardB.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                            TempBoardB.AtomicValueChanged += TempBoardInt_AtomicValueChanged;

                            TempBoardC.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                            TempBoardC.AtomicValueChanged += TempBoardInt_AtomicValueChanged;
                            if (this.TargetChannels.Count > 0) this.TargetChannel = TargetChannels[0];
                            if (!TempBoardA.Connected) TempBoardA.Connect();
                            if (!TempBoardB.Connected) TempBoardB.Connect();
                            if (!TempBoardC.Connected) TempBoardC.Connect();

                            TempBoardA.SetTargetTemperatureThreshlod(70, 10, 1);
                            TempBoardA.SetTargetTemperatureThreshlod(70, 10, 2);
                            TempBoardB.SetTargetTemperatureThreshlod(70, 10, 1);
                            TempBoardB.SetTargetTemperatureThreshlod(70, 10, 2);
                            TempBoardC.SetTargetTemperatureThreshlod(70, 1, 2);
                            TempBoardC.SetTargetTemperature(4, 2);
                            //Version = CurrentTempBoard.GetVersion();
                            DisplayChannels();
                           
                            CurrentTempBoard.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                            CurrentTempBoard.AtomicValueChanged += TempBoardInt_AtomicValueChanged;
                            this.IsInit = true;
                            this.IsTCInit = true;
                            this.InitDesc = "REINIT";
                            NotifyToWindow();

                        }
                        catch (Exception ex)
                        {
                            this.IsTCInit = true;
                            MessageHelper.ShowMessage(ex.Message);
                            //MessageBox.Show(string.Format("Init temperature board error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    });

                }));
            }
        }
        private void InitTempBoard()
        {
            if (TemperatureBoardProxyMap != null) return;
            TemperatureName = _TemperatureBoardList[0];
            foreach (var item in DeviceManager.Get.TemperatureBoardProxyMap)
            {
                this._TemperatureBoardProxyMap.Add(item.Key, (VTemperatureBoardLoaderV8)item.Value.VDevice);
            }
            TempBoardA = _TemperatureBoardProxyMap.ElementAt(0).Value;
            TempBoardB = _TemperatureBoardProxyMap.ElementAt(1).Value;
            TempBoardC = _TemperatureBoardProxyMap.ElementAt(2).Value;


        }
        public ICommand ReConnectCmd
        {
            get
            {
                return new DelegateCommand2(() =>
                {
                    Task task = Task.Run(() =>
                    {
                        try
                        {
                            if (_isInit)
                            {

                                //this.TemperatureBoardProxyMap = DeviceManagerEntity.TemperatureBoardProxyMap;

                                if (CurrentTempBoard == null || CurrentTempBoard == null)
                                    return;

                                if (!CurrentTempBoard.Connected)
                                {
                                    CurrentTempBoard.Connect();
                                }

                                CurrentTempBoard.AtomicValueChanged -= TempBoardInt_AtomicValueChanged;
                                CurrentTempBoard.AtomicValueChanged += TempBoardInt_AtomicValueChanged;
                                Version = CurrentTempBoard.GetVersion();

                            }
                        }
                        catch (Exception ex)
                        {
                            this.IsTCInit = true;
                            MessageHelper.ShowMessage(ex.Message);
                            //MessageBox.Show(string.Format("Init temperature board error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    });

                }); }
        }

        public ICommand GoSetTargetViewCommand
        {
            get
            {
                return _goSetTargetViewCommand ?? (_goSetTargetViewCommand = new DelegateCommand2(() =>
                {
                    if (TargetChannel.Trim().Length == 0 || TargetTemp.Trim().Length == 0)
                    {
                        MessageBox.Show("Please input the Channel and TargetTemp！");
                        return;
                    }
                    byte channel;
                    if (CheckChannnel(true, out channel))
                    {
                        try
                        {
                            CurrentTempBoard.SetTargetTemperature(Convert.ToSingle(TargetTemp.Trim()), channel);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(string.Format("Set target temperature error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }

                }));
            }
        }
        public ICommand GoGetTargetViewCommand
        {
            get
            {
                return _goGetTargetViewCommand ?? (_goGetTargetViewCommand = new DelegateCommand2(() =>
                {
                    if (TargetChannel.Trim().Length == 0)
                    {
                        MessageBox.Show("Please input the Channel!");
                        return;
                    }
                    byte channel;
                    if (CheckChannnel(true, out channel))
                    {
                        try
                        {
                            CurrentTarget = CurrentTempBoard.GetTargetTemperature(channel);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(string.Format("Get target temperature error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }

                }));
            }
        }
        public ICommand GetVersionCMD
        {
            get
            {
                return _getVersionCMD ?? (_getVersionCMD = new DelegateCommand2(() =>
                {
                    try
                    {
                        Version = CurrentTempBoard.GetVersion();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Get version error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand GetPIDCMD
        {
            get
            {
                return _getPIDCMD ?? (_getPIDCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!byte.TryParse(TargetChannel.Trim(), out channel))
                    {
                        MessageBox.Show("Please input the Channel！");
                        return;
                    }
                    try
                    {
                        float kp, ki, kd;
                        CurrentTempBoard.GetPIDEngData(out kp, out ki, out kd, channel);
                        this.PValueGet = kp;
                        this.IValueGet = ki;
                        this.DValueGet = kd;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Get PID error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand SetPIDCMD
        {
            get
            {
                return _setPIDCMD ?? (_setPIDCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!byte.TryParse(TargetChannel.Trim(), out channel))
                    {
                        MessageBox.Show("Please input the Channel！");
                        return;
                    }
                    try
                    {
                        float kp, ki, kd;
                        if (float.TryParse(PValueSet, out kp) && float.TryParse(IValueSet, out ki)
                            && float.TryParse(DValueSet, out kd))
                            CurrentTempBoard.SetPIDConfig(kp, ki, kd, channel);
                        else
                            MessageBox.Show(" P, I, D must input a digit value", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Get version error:{0}", ex), "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand EnableTCCMD
        {
            get
            {
                return _enableTCCMD ?? (_enableTCCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;
               
                        DoCommandAction(() => CurrentTempBoard.EnableTC(true, channel), "Enable TC error:");
                 
                }));
            }
        }

        public ICommand DisableTCCMD
        {
            get
            {
                return _disableTCCMD ?? (_disableTCCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;
                    DoCommandAction(() => CurrentTempBoard.EnableTC(false, channel), "Disable TC error:");
                }));
            }
        }

        public ICommand PowerOnPumpCMD
        {
            get
            {
                return _powerOnPumpCMD ?? (_powerOnPumpCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;

                    DoCommandAction(() => CurrentTempBoard.PowerOnOffRefrigerationPump(true, channel), "Power On Refrigeration Pump error:");

                }));
            }
        }
        public ICommand PowerOffPumpCMD
        {
            get
            {
                return _powerOffPumpCMD ?? (_powerOffPumpCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;

                    DoCommandAction(() => CurrentTempBoard.PowerOnOffRefrigerationPump(false, channel), "Power Off Refrigeration Pump error:");

                }));
            }
        }
        public ICommand PowerOnFansCMD
        {
            get
            {
                return _powerOnFansCMD ?? (_powerOnFansCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;

                    DoCommandAction(() => CurrentTempBoard.PowerOnOffFans(true, channel), "Power On Fans error:");

                }));
            }
        }
        public ICommand PowerOffFansCMD
        {
            get
            {
                return _powerOnFansCMD ?? (_powerOnFansCMD = new DelegateCommand2(() =>
                {
                    byte channel;
                    if (!CheckChannnel(true, out channel)) return;

                    DoCommandAction(() => CurrentTempBoard.PowerOnOffFans(false, channel), "Power Off Fans error:");

                }));
            }
        }

        private ICommand _UpdateFirmwareCmd;

        public ICommand UpdateFirmwareCmd
        {
            get
            {
                return _UpdateFirmwareCmd ?? (_UpdateFirmwareCmd = new DelegateCommand2(() =>
                {
                    try
                    {
                        UpdateFirmwareWnd wnd = new UpdateFirmwareWnd();
                        wnd.Title = "Temperature Board Update";
                        wnd.FirmwareName = "Temperature Board";
                        wnd.CurrentVersion = Version;
                        wnd.BoardControlDynamic = CurrentTempBoard;
                        wnd.Width = 650;
                        wnd.Height = 350;
                        wnd.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        wnd.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Update firmeare command error:" + ex.Message);
                    }
                }));
            }
        }
        #endregion

       
        private void TemperatureViewModel_PropertyChanged()
        {

            //if (this.TemperatureName.EndsWith("V2_B"))
            //{
            //    this._IsV2ChipB = true;
            //}
            //else this._IsV2ChipB = false;
            //if (_IsV2ChipB)
            //{
            //    this.TargetChannels = GetChannels(DeviceManagerEntity.TargetTCChannelsB);
            //}
            //else
            //{
            //    this.TargetChannels = GetChannels(DeviceManagerEntity.TargetTCChannels);
            //}
            if (this.TargetChannels.Count > 0) this.TargetChannel = TargetChannels[0];

        }

        private void TempBoardInt_AtomicValueChanged(RPC.AtomicValueEventArgs args)
        {
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_Temperature" || args.av.ValueName == $"{CATEGORY_TEMPERATURE}_A_Channel1")
            {
                if (this.ChannelA1Visibility == Visibility.Visible)
                {
                    this.ChannelA1Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelA1Name} Current Temperature ${ChannelA1Value.ToString("F2")}");
                }
            }
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_A_Channel2")
            {
                if (this.ChannelA2Visibility == Visibility.Visible)
                {
                    this.ChannelA2Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelA2Name} Current Temperature ${ChannelA2Value.ToString("F2")}");
                }
            }
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_B_Channel1")
            {
                if (this.ChannelB1Visibility == Visibility.Visible)
                {
                    this.ChannelB1Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelB1Name} Current Temperature ${ChannelB1Value.ToString("F2")}");
                }
            }
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_B_Channel2")
            {
                if (this.ChannelB2Visibility == Visibility.Visible)
                {
                    this.ChannelB2Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelB2Name} Current Temperature ${ChannelB2Value.ToString("F2")}");
                }
            }
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_C_Channel1")
            {
                if (this.ChannelC1Visibility == Visibility.Visible)
                {
                    this.ChannelC1Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelC1Name}Current Temperature {ChannelC1Value.ToString("F2")}");
                }
            }
            if (args.av.ValueName == $"{CATEGORY_TEMPERATURE}_C_Channel2")
            {
                if (this.ChannelC2Visibility == Visibility.Visible)
                {
                    this.ChannelC2Value = args.av.ValueD;
                    _Logger.Log()(LogSeverityEnum.Info, $"{this.ChannelC2Name}Current Temperature {ChannelC2Value.ToString("F2")}");
                }
            }
            NotifyToWindow();
        }
        private void NotifyToWindow()
        {
            RaisePropertyChanged("ABoardConnected");
            RaisePropertyChanged("BBoardConnected");
            RaisePropertyChanged("CBoardConnected");
            RaisePropertyChanged();
        }
        private void DoCommandAction(Action action, string errorMsg)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("{0}-{1}", errorMsg, ex.Message), "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CheckChannnel(bool isShowAlarmWnd, out byte channel)
        {
            bool ret = byte.TryParse(TargetChannel.Trim(), out channel);
            if (!ret)
            {
                if (isShowAlarmWnd) MessageBox.Show("Please input valid Channel!");
            }
            return ret;
        }

        private List<string> GetChannels(byte[] channels)
        {
            List<string> rslt = new List<string>();
            if (channels != null)
            {
                foreach (byte channel in channels)
                {
                    rslt.Add(channel.ToString());
                }
            }
            return rslt;
        }

        private void DisplayChannels()
        {
            foreach (var item in TemperatureBoardProxyMap.Keys)
            {
                if (TemperatureBoardProxyMap[item].Connected == true)
                {
                    if ($"{CATEGORY_TEMPERATURE}_A".Equals(item))
                    {
                        this.ChannelA1Name = "A-1:";
                        this.ChannelA2Name = "A-2:";
                        this.ChannelA1Visibility = Visibility.Visible;
                        this.ChannelA2Visibility = Visibility.Visible;
                    }
                    if ($"{CATEGORY_TEMPERATURE}_B".Equals(item))
                    {
                        this.ChannelB1Name = "B-1:";
                        this.ChannelB2Name = "B-2:";
                        this.ChannelB1Visibility = Visibility.Visible;
                        this.ChannelB2Visibility = Visibility.Visible;
                    }
                    if ($"{CATEGORY_TEMPERATURE}_C".Equals(item))
                    {
                        this.ChannelC1Name = "C-1:";
                        this.ChannelC2Name = "C-2:";
                        this.ChannelC1Visibility = Visibility.Visible;
                        this.ChannelC2Visibility = Visibility.Visible;
                    }
                }
            }
        }
        public void SetParameter(string category, string paraName, object value)
        {
            try
            {

                string configPath = $@"{ConfigMgr.Get.ConfigPath}\{ConfigMgr.Get.ConfigFileName}.xml";//BGI.ZebraV8Imager.Service
                XmlDocument doc = new XmlDocument();
                doc.Load(configPath);
                //var nodes = doc.SelectNodes($"Configs/InstrumentConfig[Para_Set_Name='{category}'][Para_Name='{paraName}']");
                XmlNode node = doc.SelectSingleNode($"Configs/InstrumentConfig[Para_Set_Name='{category}'][Para_Name='{paraName}']");
                if (node != null)
                {
                    XmlNode valueNode = node.SelectSingleNode("Cur_Value");
                    if (valueNode != null && valueNode.FirstChild != null)
                    {
                        valueNode.FirstChild.Value = value.ToString();
                        doc.Save(configPath);
                    }
                    else
                        throw new BGIException($"Not find Catory={category} ParaName={paraName}");
                }
                ConfigMgr.Get.Load(false);
            }
            catch (Exception ex)
            {
                _Logger.Log()(LogSeverityEnum.Error, ex.ToString());
                throw;
            }
        }

    }
}
