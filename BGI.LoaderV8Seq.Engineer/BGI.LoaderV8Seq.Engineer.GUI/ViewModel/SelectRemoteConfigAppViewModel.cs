﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Configuration;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class SelectRemoteConfigAppViewModel:ViewModelBase
    {
        private IDictionary<string, string> _AppNameCollection;
        
        public IDictionary<string, string> AppNameCollection
        {
            get { return this._AppNameCollection; }
            set { this._AppNameCollection = value; RaisePropertyChanged(); }
        }


        private string _AppName;

        public string AppName
        {
            get { return this._AppName; }
            set { this._AppName = value; RaisePropertyChanged(); }
        }
        public SelectRemoteConfigAppViewModel()
        {
            AppNameCollection = new Dictionary<string, string>();
            //ConfigurationManager.a
            System.Collections.Specialized.NameValueCollection nv = ConfigurationManager.AppSettings;
            for(int i = 0;i < nv.Keys.Count; i++)
            {
                string key = nv.Keys[i];
                if (key.StartsWith("App_"))
                {
                    AppNameCollection.Add(key, nv[key]);
                }
            }
            AppName = "";
        }
    }
}
