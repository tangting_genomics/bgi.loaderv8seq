﻿using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.Utils;
using BGI.Common.UtilWPF.Helpers;
using BGI.LoaderV8Seq.Engineer.GUI.Views;
using BGI.Scripting;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using PropertyChanged;
using System.Collections.ObjectModel;
using BGI.Control.Device.Physical;
using BGI.LoaderV8Seq.Engineer.GUI.Helper;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class MutiOperationLoadVM:ViewModelBase, IEngineerOperations
    {
        public MutiOperationLoadVM()
        {
            ConfigMgr.Get.RegisterLogLevelUpdate(logger);
            _DeviceRawObjectD.AddIfMissing(Category, this);
            OperationInit();
            InitBoard();
        }

        private string Category = "V8MutiLoader";
        private readonly Dictionary<string, object> _DeviceRawObjectD = new Dictionary<string, object>();

        private Logger logger = LogMgr.GetLogger("MutiLoadVM", LogSeverityEnum.Debug);

        private const string LOADINGWASH = "LoadingWash";
        private const string PERFUSION = "Perfusion";
        private const string LOADING = "Loading";
        private const string MDAWash = "MDAWash";
        private const string MDAPERFUSION = "MDAPerfusion";
        private const string MDAPOSTLOADING = "MDAPostLoading";
        private const string BARCODEPOSTLOADING = "BarcodePostLoading";

        private const string IOBoardType = "V8IOBoard";// BoardTypeEnum.V01;
        private const string TCBoardType = "V8TempBoard";
        private const string SyringPumpType = "SyringePump";
        private const string ValveType = "SelectorValve";

        private ObservableCollection<Operation> _OperationList = new ObservableCollection<Operation>();
        public ObservableCollection<Operation> OperationList
        {
            get { return _OperationList; }
            set {
                _OperationList = value;
                RaisePropertyChanged(); }
        }

        public VLoaderBoardV8 IOBoard = null;

        public VIdexUartValve2[] RotaryValves = null;

        public VCavroXLP6KPumpV1 Pump = null;

        public VTemperatureBoardLoaderV8 TemperatureBoardA = null;

        public VTemperatureBoardLoaderV8 TemperatureBoardB = null;

        public VTemperatureBoardLoaderV8 TemperatureBoardC = null;

        #region Init Void
        public IReadOnlyDictionary<string, object> EnumeratePluginHardware()
        {
            return _DeviceRawObjectD;
        }

        public object GetDeviceObject(string objName)
        {
            if (EnumeratePluginHardware().ContainsKey(objName))
            {
                return EnumeratePluginHardware()[objName];
            }
            logger.Log()(Common.Logging.LogSeverityEnum.Error, "Caller asked for Device object for unknown device string ({0}).  Returning null.", objName);
            return null;
        }
        private void InitBoard()
        {
            try
            {

                IOBoard = (VLoaderBoardV8)DeviceManager.Get.IOBoardProxyMap[IOBoardType].VDevice;
                RotaryValves = new VIdexUartValve2[5];
                for (int i = 0; i < DeviceManager.Get.SelectorValveMap.Values.Count; i++)
                {
                    RotaryValves[i] = (VIdexUartValve2)DeviceManager.Get.SelectorValveMap.Values.ElementAt(i).VDevice;
                }
                Pump = (VCavroXLP6KPumpV1)DeviceManager.Get.SyringPumpProxyMap[SyringPumpType].VDevice;
                TemperatureBoardA= (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_A"].VDevice;
                TemperatureBoardB= (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_B"].VDevice;
                TemperatureBoardC= (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_C"].VDevice;

            }
            catch (Exception ex)
            {
                logger.Log("设备初始化失败", ex);
                MessageBox.Show("设备连接失败,请尝试重新连接");
            }
        }
        private void OperationInit()
        {
            try
            {


                List<OperationTunnel> tunnels = new List<OperationTunnel>();
                tunnels.Add(new OperationTunnel { Name = "通道1", Id = 1 });
                tunnels.Add(new OperationTunnel { Name = "通道2", Id = 2 });
                tunnels.Add(new OperationTunnel { Name = "通道3", Id = 3 });
                tunnels.Add(new OperationTunnel { Name = "通道4", Id = 4 });
                tunnels.Add(new OperationTunnel { Name = "通道5", Id = 5 });

                //ScriptBase _ScriptBase1 = new EngineerOperations(this);
                //ScriptControlVM vm1 = new ScriptControlVM(_ScriptBase1);
                //ScriptView1 = new Script();
                //ScriptView1.DataContext = vm1;

                //ScriptBase _ScriptBase2 = new EngineerOperations(this);
                //ScriptControlVM vm2 = new ScriptControlVM(_ScriptBase2);
                //ScriptView2 = new Script();
                //ScriptView2.DataContext = vm2;

                //ScriptBase _ScriptBase3 = new EngineerOperations(this);
                //ScriptControlVM vm3 = new ScriptControlVM(_ScriptBase3);
                //ScriptView3 = new Script();
                //ScriptView3.DataContext = vm3;

                OperationList.Add(new Operation(LOADINGWASH)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, LOADINGWASH),
                    scriptbase = new EngineerOperations(this)
                });// Properties.Settings.Default.LoadingWash,

                OperationList.Add(new Operation(PERFUSION)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, PERFUSION),
                    scriptbase = new EngineerOperations(this)
                });
                OperationList.Add(new Operation(LOADING)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, LOADING),
                    scriptbase = new EngineerOperations(this)
                });
                OperationList.Add(new Operation(MDAWash)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, MDAWash),
                    scriptbase = new EngineerOperations(this)
                });
                OperationList.Add(new Operation(MDAPERFUSION)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, MDAPERFUSION),
                    scriptbase = new EngineerOperations(this)
                });
                OperationList.Add(new Operation(MDAPOSTLOADING)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, MDAPOSTLOADING),
                    scriptbase = new EngineerOperations(this)
                });
                OperationList.Add(new Operation(BARCODEPOSTLOADING)
                {
                    Tunnels = Copy2(tunnels.ToArray()),
                    ScriptName = ConfigMgr.Get.GetCurValue<string>(Category, MDAPOSTLOADING),
                    scriptbase = new EngineerOperations(this)
                });
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "加载操作失败:" + ex.ToString());
                MessageBox.Show(ex.StackTrace);
            }
        }
        public ObservableCollection<OperationTunnel> Copy2(OperationTunnel[] tunnel)
        {
            if (tunnel == null) return null;
            var result = new ObservableCollection<OperationTunnel>();
            foreach (var item in tunnel)
            {
                result.Add(item.Clone() as OperationTunnel);
            }
            return result;
        }

        #endregion

        public Operation GetOperation(string name)
        {
            var operation = this.OperationList.FirstOrDefault(temp => temp.OperationName == name);
            if (operation != null)
            {
                return operation;
            }
            return null;
        }
        public void UpdateStatus(string operationname, string message)
        {
            if (OperationList.FirstOrDefault(x=>x.OperationName==operationname)!=null)
            {
                this.OperationList.First(x => x.OperationName == operationname).CurrentAction = message;
            }
        }
        private object obj = new object();
        public void ConnectDevice()
        {
            lock (obj)
            {
                try
                {

                    if (IOBoard == null) IOBoard = (VLoaderBoardV8)DeviceManager.Get.IOBoardProxyMap[IOBoardType].VDevice;
                    if (!IOBoard.Connected) IOBoard.Connect();
                    IOBoard.SolenoidValve(2, true);

                    if (RotaryValves == null)
                    {
                        RotaryValves = new VIdexUartValve2[5];
                        for (int i = 0; i < DeviceManager.Get.SelectorValveMap.Values.Count; i++)
                        {
                            RotaryValves[i] = (VIdexUartValve2)DeviceManager.Get.SelectorValveMap.Values.ElementAt(i).VDevice;
                        }
                    }
                    for (int i = 0; i < RotaryValves.Count(); i++)
                    {
                        if (!RotaryValves[i].Connected)
                            RotaryValves[i].Connect();
                    }
                    if (Pump == null) Pump = (VCavroXLP6KPumpV1)DeviceManager.Get.SyringPumpProxyMap[SyringPumpType].VDevice;
                    if (!Pump.Connected) Pump.Connect();
                    if (TemperatureBoardA == null) TemperatureBoardA = (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_A"].VDevice;
                    if (!TemperatureBoardA.Connected) TemperatureBoardA.Connect();
                    if (TemperatureBoardB == null) TemperatureBoardB = (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_B"].VDevice;
                    if (!TemperatureBoardB.Connected) TemperatureBoardB.Connect();
                    if (TemperatureBoardC == null) TemperatureBoardC = (VTemperatureBoardLoaderV8)DeviceManager.Get.TemperatureBoardProxyMap[$"{TCBoardType}_C"].VDevice;
                    if (!TemperatureBoardC.Connected) { TemperatureBoardC.Connect(); TemperatureBoardC.SetTargetTemperature(4, 2); }
                }
                catch (Exception ex)
                {
                    logger.Log("Connect Device Error", ex);
                    throw;
                }
            }
        }
        
    }

    #region Operation Class

    public class Operation:ViewModelBase
    {
        public Operation(string name)
        {
            OperationName = name;
        }
        public string OperationName { get; private set; }
        //public OperationTunnel[] Tunnels { get; set; }
        public bool _TunnelsEnabled = true;
        public bool TunnelsEnabled
        {
            get { return _TunnelsEnabled; }
            set
            {
                _TunnelsEnabled = value;
                RaisePropertyChanged();
            }
        }
        public string ScriptName { get; set; }

        public string _CurrentAction;
        public string CurrentAction
        {
            get
            {
                return _CurrentAction;
            }
            set
            {
                _CurrentAction = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<OperationTunnel> Tunnels { get; set; }
        public ICommand InitCommand
        {
            get
            {
                return new DelegateCommand2<string>(RunScript);
            }
        }
        private ScriptBase _scriptbase;
        public ScriptBase scriptbase
        {
            get { return _scriptbase; }
            set
            {
                _scriptbase = value;
                RaisePropertyChanged();
            }
        }
        private ScriptState _State;
        public ScriptState State { get { return _State; } set { _State = value; RaisePropertyChanged(); } }
        private Script script;
        private void RunScript(string filepath)
        {
            if (script == null)
            {
                script = new Script(scriptbase);
                script.tb_ScriptFile.Text = ScriptName;
                script.Title = OperationName;
                script.btnScriptRun.IsEnabled = true;
                script._ScriptBase = scriptbase;
                scriptbase.OnScriptStateChanged -= this.ScriptStatusChanged;
                scriptbase.OnScriptStateChanged += this.ScriptStatusChanged;
            }
            script.Show();
        }

        private void ScriptStatusChanged(object sender, EventArgs e)
        {
            State = scriptbase.State;
        }

        public OperationTunnel[] GetTunnel()
        {
            var selectTunnel = Tunnels.Where(x => x.Selected == true);

            if (selectTunnel == null || selectTunnel.Count() == 0) { MessageBox.Show("请选择操作通道"); return null; }
            return selectTunnel.ToArray();
        }
        public ScriptState GetScriptState()
        {
            RaisePropertyChanged();
            if (script == null) return ScriptState.None;

            if (script != null)
            {
                if (script._ScriptBase.State == ScriptState.Finished)
                    CurrentAction = "Finished";
                if (script._ScriptBase.State == ScriptState.Running)
                {
                    TunnelsEnabled = false;
                }
                else TunnelsEnabled = true;
            }
            else CurrentAction = "Idle";
            return script._ScriptBase.State;

        }
        public void UpdateCurrentAction(string msg)
        {
            if (script!=null)
            {
                if (script._ScriptBase.State==ScriptState.Running)
                {
                    CurrentAction = msg;
                }
            }
            else CurrentAction = "Idle";

        }
        
    }

    public class OperationTunnel :ViewModelBase, ICloneable
    {
        public OperationTunnel()
        {
            Enabled = true;
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public bool _Selected=false;
        public bool Selected
        {
            get { return _Selected; }
            set
            {
                _Selected = value;
                
            }
        }
        public bool _Enabled = false;
        public bool Enabled
        {
            get { return _Enabled; }
            set
            {
                _Enabled = value;
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
    //public class DeviceOperaton
    //{
    //    public DeviceOperaton(int id, string name, bool selected)
    //    {
    //        Id = id;
    //        Name = name;
    //        Selected = selected;
    //    }
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public bool Selected { get; set; }
    //}

    #endregion

}
