﻿using BGI.Common.Utils;
using BGI.Common.UtilWPF.Helpers;
using BGI.Control.Device;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class FrimwareUpdateVM: ViewModelBase
    {
        private const int WaitJumpAppTimeMS = 15000;
        private string _UpdateVersion;
        private string _UpdateFilePath;
        private double _Progress;
        private string _FirmwareName;
        private string _CurrentVersion;
        private dynamic _boardControlDynamic;
        private string _ShowInfoMsg;
        private bool _IsIdle = true;
        private bool _IsError;

        private FirmwareUpdate firmwareUpdate;

        private ICommand _SelectFileCmd;
        private ICommand _UpdateCmd;

        #region Properties
        public string ShowInfoMsg
        {
            get { return _ShowInfoMsg; }
            set
            {
                _ShowInfoMsg = value;
            }
        }
        public dynamic BoardControlDynamic
        {
            get
            {
                return _boardControlDynamic;
            }
            set
            {
                _boardControlDynamic = value;
            }
        }

        public string CurrentVersion
        {
            get
            {
                return _CurrentVersion;
            }
            set
            {
                _CurrentVersion = value;
            }
        }

        public string FirmwareName
        {
            get
            {
                return _FirmwareName;
            }
            set
            {
                _FirmwareName = value;
            }
        }
        public string UpdateVersion
        {
            get
            {
                return _UpdateVersion;
            }
            set
            {
                _UpdateVersion = value;
            }
        }

        public string UpdateFilePath
        {
            get
            {
                return _UpdateFilePath;
            }
            set
            {
                int endIndex = value.LastIndexOf('.');
                int startIndex = value.LastIndexOf('_');
                if (startIndex != -1 && endIndex != -1 && endIndex > startIndex && endIndex - startIndex - 2 > 0)
                {
                    UpdateVersion = value.Substring(startIndex + 2, endIndex - startIndex - 2);
                    _UpdateFilePath = value;
                }
                else
                {
                    ShowInfoMsg = "Update file is not correct";
                    _UpdateFilePath = string.Empty;
                }
                
            }
        }

        public double Progress
        {
            get
            {
                if (_Progress > 100)
                    _Progress = 100;
                return _Progress;
            }
            set
            {
                _Progress = value;
            }
        }

        public bool IsIdle
        {
            get
            {
                return _IsIdle;
            }
            set
            {
                _IsIdle = value;
            }
        }

        public bool IsError
        {
            get
            {
                return _IsError;
            }
            set
            {
                _IsError = value;
            }
        }
        #endregion

        public ICommand SelectFileCmd
        {
            get
            {
                return _SelectFileCmd ?? (_SelectFileCmd = new DelegateCommand2(() =>
                {
                    try
                    {
                        System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
                        dlg.Filter = "(bin *.bin)|*.bin";
                        if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog())
                        {
                            UpdateFilePath = dlg.FileName;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Select file error:" + ex.Message);
                    }
                }));
            }
        }

        public ICommand UpdateCmd
        {
            get
            {
                return _UpdateCmd ?? (_UpdateCmd = new DelegateCommand2(() =>
                {
                    Task.Run(() =>
                    {
                        IsIdle = false;
                        IsError = false;
                        try
                        {
                            if (string.IsNullOrEmpty(UpdateFilePath))
                            {
                                IsError = true;
                                ShowInfoMsg = "Please select update file";
                                return false;
                            }

                            Progress = 0;
                            ShowInfoMsg = "upgrading do not power off, please wait...";

                            //jump to bootloader
                            _boardControlDynamic.JumpToBootLoader();
                           
                            _boardControlDynamic.Disconnect();
                            ThreadUtils.Delay(1000);
                            firmwareUpdate = new FirmwareUpdate(FirmwareName + "firmwareUpdate", _boardControlDynamic.ComPort);
                            firmwareUpdate.UpdateProgessEvent += UpdateProgress;

                            firmwareUpdate.YmodemUploadFile(UpdateFilePath);
                            ShowInfoMsg = "send file success,wait jump to app";
                            return true;
                        }
                        catch (Exception ex)
                        {
                            IsError = true;
                            ShowInfoMsg = "Update firmware error:\r\n" + ex.Message;
                            return false;
                        }
                    }).ContinueWith((t) =>
                    {
                        if (!t.Result)
                            return false;

                        int count = WaitJumpAppTimeMS / 1000;
                        while (count > 0)
                        {
                            ShowInfoMsg = string.Format("send file success,wait jump to app {0}", count);
                            ThreadUtils.Delay(1000);
                            count--;
                        }
                        return true;
                    }).ContinueWith((t) =>
                    {
                        if (!t.Result)
                        {
                            IsIdle = true;
                            return;
                        }

                        try
                        {
                            ShowInfoMsg = "Checking version... ";
                            firmwareUpdate.Close();
                            ThreadUtils.Delay(1000);
                            _boardControlDynamic.Connect();
                            string version = _boardControlDynamic.GetVersion();

                            if (!string.IsNullOrEmpty(version))
                            {
                                int index = version.LastIndexOf('-');
                                int secondindex = version.IndexOf('(');
                                if (index != -1)
                                {
                                    version = version.Substring(index, secondindex- index).Trim();
                                }
                                if (string.Compare(version, UpdateVersion, true) == 0)
                                {
                                    ShowInfoMsg = "upgrate success";
                                }
                                else
                                {
                                    IsError = true;
                                    ShowInfoMsg = string.Format("upgrate failed,version is not equal {0}<>{1}", UpdateVersion, version);
                                }
                            }
                            else
                            {
                                ShowInfoMsg = "Get version error";
                            }
                        }
                        catch (Exception ex)
                        {
                            IsError = true;
                            ShowInfoMsg = "Wait jump to app error:\r\n" + ex.Message;
                        }
                        finally
                        {
                            IsIdle = true;
                        }
                    });
                }));
            }
        }

        private void UpdateProgress(double progressValue)
        {
            Progress = progressValue;
            //_Progress = progressValue;
            //RaisePropertyChanged("Progress");
        }
    }
}
