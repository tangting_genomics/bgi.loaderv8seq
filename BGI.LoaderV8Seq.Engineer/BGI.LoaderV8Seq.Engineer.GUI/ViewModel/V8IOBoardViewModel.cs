﻿using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.UtilWPF.Helpers;
using BGI.Control.Device.Physical;
using BGI.LoaderV8Seq.Engineer.GUI.Helper;
using BGI.LoaderV8Seq.Engineer.GUI.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class V8IOBoardViewModel : ViewModelBase
    {
        public V8IOBoardViewModel()
        {
            ConfigMgr.Get.RegisterLogLevelUpdate(logger);
            ConfigInit();

        }
        #region Init
        private void ConfigInit()
        {
            MotorSpeeds = new int[] { 10, 15, 20, 30, 40, 50, 60, 70, 75 };
            MotorList.Add(new DeviceOperaton(1, "压紧电机1", false));
            MotorList.Add(new DeviceOperaton(2, "压紧电机2", false));
            MotorList.Add(new DeviceOperaton(3, "压紧电机3", false));
            MotorList.Add(new DeviceOperaton(4, "压紧电机4", false));
            MotorList.Add(new DeviceOperaton(5, "压紧电机5", false));
            MotorList.Add(new DeviceOperaton(6, "吸液电机A", false));
            MotorList.Add(new DeviceOperaton(7, "吸液电机B", false));

            PumpSpeeds = new int[] {6, 20, 50, 100, 200 };
            PumpPoss = new string[] { "NO", "NC" };
            PumpList.Add(new DeviceOperaton(1, "注射泵1", false));
            PumpList.Add(new DeviceOperaton(2, "注射泵2", false));
            PumpList.Add(new DeviceOperaton(3, "注射泵3", false));
            PumpList.Add(new DeviceOperaton(4, "注射泵4", false));
            PumpList.Add(new DeviceOperaton(5, "注射泵5", false));

            RotaryValvePos = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            RotaryValveList.Add(new DeviceOperaton(1, "旋转阀1", false));
            RotaryValveList.Add(new DeviceOperaton(2, "旋转阀2", false));
            RotaryValveList.Add(new DeviceOperaton(3, "旋转阀3", false));
            RotaryValveList.Add(new DeviceOperaton(4, "旋转阀4", false));
            RotaryValveList.Add(new DeviceOperaton(5, "旋转阀5", false));

            int a = 1;
            for (int i = 1; i < 26; i++)
            {
                SolenoidValveList.Add(new DeviceOperaton(i, i.ToString(), false));
                if (i % 5 == 0)
                {
                    var num = 25 + a;
                    SolenoidValveList.Add(new DeviceOperaton(num, num.ToString(), false));
                    a = a + 1;
                }
            }
        }
        private const string IOBoardType = "V8IOBoard";// BoardTypeEnum.V01;
        private const string TCBoardType = "V8TempBoard";
        private const string SyringPumpType = "SyringePump";
        private const string ValveType = "SelectorValve";
        private VLoaderBoardV8 IOBoard = null;

        private VIdexUartValve2[] RotaryValves = null;

        private VCavroXLP6KPumpV1 Pump = null;

        private Common.Logging.Logger logger = LogMgr.GetLogger("V8LoaderUI", LogSeverityEnum.Debug);

        private bool init = false;
        private void InitBoard()
        {
            try
            {

                IOBoard = (VLoaderBoardV8)DeviceManager.Get.IOBoardProxyMap[IOBoardType].VDevice;
                RotaryValves = new VIdexUartValve2[5];
                for (int i = 0; i < DeviceManager.Get.SelectorValveMap.Values.Count; i++)
                {
                    RotaryValves[i] = (VIdexUartValve2)DeviceManager.Get.SelectorValveMap.Values.ElementAt(i).VDevice;
                }
                Pump = (VCavroXLP6KPumpV1)DeviceManager.Get.SyringPumpProxyMap[SyringPumpType].VDevice;
            }
            catch (Exception ex)
            {
                logger.Log("设备初始化失败", ex);
                MessageBox.Show("设备连接失败,请尝试重新连接");
            }
        }

        #endregion
        #region Property
        public int[] MotorSpeeds { get; set; }
        public string MotorDistances { get; set; }
        public int[] PumpSpeeds { get; set; }
        public string[] PumpPoss { get; set; }
        public string PumpPos { get; set; }
        public string PumpVolume { get; set; }
        public int[] RotaryValvePos { get; set; }

        private bool _IsInitialize;
        public bool IsInitialize
        {
            get { return _IsInitialize; }
            set
            {
                _IsInitialize = value;
                RaisePropertyChanged("IsInitialize");
            }
        }
        private string _version;
        public string Version
        {
            get
            {
                return _version;
            }

            set
            {
                _version = value;
                RaisePropertyChanged("Version");
            }
        }

        private string _InitDesc = "INIT";
        public string InitDesc
        {
            get { return _InitDesc; }
            set { this._InitDesc = value; RaisePropertyChanged("InitDesc"); }
        }
        private bool _IsIOInit = true;
        public bool IsIOInit
        {
            get { return _IsIOInit; }
            set { _IsIOInit = value; RaisePropertyChanged("IsIOInit"); }
        }
        public ObservableCollection<DeviceOperaton> MotorList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> PumpList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> RotaryValveList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> SolenoidValveList { get; set; } = new ObservableCollection<DeviceOperaton>();


        public Brush IOBoardConntected
        {
            get
            {
                if (IOBoard == null) return Brushes.Gray; else return IOBoard.Connected ? Brushes.Green : Brushes.Red;
            }
        }

        public Brush PumpConntected
        {
            get
            {
                if (Pump == null) return Brushes.Gray; else return Pump.Connected ? Brushes.Green : Brushes.Red;
            }
        }

        public Brush Valve1Conntected
        {
            get { if (RotaryValves == null) return Brushes.Gray; else return RotaryValves[0].Connected ? Brushes.Green : Brushes.Red; }
        }

        public Brush Valve2Conntected {
            get { if (RotaryValves == null) return Brushes.Gray; else return RotaryValves[1].Connected ? Brushes.Green : Brushes.Red; } }

        public Brush Valve3Conntected { get { if (RotaryValves == null) return Brushes.Gray; else return RotaryValves[2].Connected ? Brushes.Green : Brushes.Red; } }


        public Brush Valve4Conntected
        {
            get { if (RotaryValves == null) return Brushes.Gray; else return RotaryValves[3].Connected ? Brushes.Green : Brushes.Red; }
        }
        public Brush Valve5Conntected
        {
            get { if (RotaryValves == null) return Brushes.Gray; else return RotaryValves[4].Connected ? Brushes.Green : Brushes.Red; }
        }
        #endregion

        #region ICommand  
        public ICommand CommandMotorUp { get { return new DelegateCommand2<int?>(EventMotorUp); } }

        public ICommand CommandMotorDown { get { return new DelegateCommand2<int?>(EventMotorDown); } }
        public ICommand CommandAspirate { get { return new DelegateCommand2<int?>(EventAspirate); } }
        public ICommand CommandDispense { get { return new DelegateCommand2<int?>(EventDispense); } }
        public ICommand CommandDispenseAll { get { return new DelegateCommand2<int?>(EventDispenseAll); } }

        public ICommand CommandRotary { get { return new DelegateCommand2<int?>(EventRotary); } }

        public ICommand CommandOpenSolenoidValve { get { return new DelegateCommand2<int?>(OpenSolenoidValve); } }

        public ICommand CommandOffSolenoidValve { get { return new DelegateCommand2<int?>(OffSolenoidValve); } }

        // private ICommand _goInitializeViewCommand= new DelegateCommand2<int?>(InitializeView);
        public ICommand GoInitializeViewCommand
        {
            get
            {
                return new DelegateCommand2<int?>(InitializeView);
            }
        }
        public ICommand IOBoardReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectIOBoard); } }
        public ICommand PumpReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectPump); } }
        public ICommand Valve1ReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectValve1); } }
        public ICommand Valve2ReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectValve2); } }
        public ICommand Valve3ReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectValve3); } }
        public ICommand Valve4ReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectValve4); } }
        public ICommand Valve5ReConnectViewCommand { get { return new DelegateCommand2<int?>(EventConnectValve5); } }

        public ICommand UpdateFirmwareCmd
        {
            get { return new DelegateCommand2(EventUpdateFirmware); }
        }
        #endregion

        #region Event
        private void EventUpdateFirmware()
        {
            try
            {
                UpdateFirmwareWnd wnd = new UpdateFirmwareWnd();
                wnd.Title = "IoBoard Update";
                wnd.FirmwareName = "IoBoard Board";
                wnd.CurrentVersion = Version;
                wnd.BoardControlDynamic = IOBoard;
                wnd.Width = 650;
                wnd.Height = 350;
                wnd.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                wnd.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Update firmeare command error:" + ex.Message);
            }
        }

        private void InitializeView(int? obj)
        {
            Task task = Task.Run(() =>
            {
                try
                {
                    this.IsIOInit = false;

                    InitBoard();
                    if (IOBoard == null) IOBoard = (VLoaderBoardV8)DeviceManager.Get.IOBoardProxyMap[IOBoardType].VDevice;
                    if (RotaryValves == null)
                    {
                        RotaryValves = new VIdexUartValve2[5];
                        for (int i = 0; i < DeviceManager.Get.SelectorValveMap.Values.Count; i++)
                        {
                            RotaryValves[i] = (VIdexUartValve2)DeviceManager.Get.SelectorValveMap.Values.ElementAt(i).VDevice;
                        }
                    }
                    if (Pump == null) Pump = (VCavroXLP6KPumpV1)DeviceManager.Get.SyringPumpProxyMap[SyringPumpType].VDevice;
                    if (!IOBoard.Connected) { IOBoard.Connect(); }
                    for (int i = 0; i < 5; i++)
                    {
                        IOBoard.SolenoidValve((byte)(5 * i + 2), true);
                    }
                    IOBoard.Home();
                    if (!Pump.Connected)
                    {
                        Pump.Connect();
                    }
                    Pump.Home();
                    foreach (var item in RotaryValves)
                    {
                        if (!item.Connected) { item.Connect(); }
                        item.Home();
                    }

                    Version = IOBoard.GetVersion();
                    for (int i = 0; i < 5; i++)
                    {
                        IOBoard.SolenoidValve((byte)(5 * i + 2), false);
                    }
                    this.IsIOInit = true;
                    this.init = true;
                    this.InitDesc = "REINIT";
                    IsInitialize = true;

                }
                catch (Exception ex)
                {
                    this.IsIOInit = true;
                    MessageHelper.ShowMessage(ex.Message);

                    IsInitialize = false;
                }
                finally { NotifyToWindow(); }
            });
        }

        private void EventConnectIOBoard(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "IOBoard Connect Error" + ex.StackTrace);
                MessageBox.Show("IOBoard Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }
        private void EventConnectPump(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Pump Connect Error" + ex.StackTrace);
                MessageBox.Show("Pump Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }


        private void EventConnectValve1(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Valve1 Connect Error" + ex.StackTrace);
                MessageBox.Show("Valve1 Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }


        private void EventConnectValve2(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Valve2 Connect Error" + ex.StackTrace);
                MessageBox.Show("Valve2 Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }


        private void EventConnectValve3(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Valve3 Connect Error" + ex.StackTrace);
                MessageBox.Show("Valve3 Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }
        private void EventConnectValve4(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Valve4 Connect Error" + ex.StackTrace);
                MessageBox.Show("Valve4 Connect Error" + ex.StackTrace);
            }
            finally { NotifyToWindow(); }
        }


        private void EventConnectValve5(int? obj)
        {
            try
            {
                IOBoard.Connect();
            }
            catch (Exception ex)
            {
                logger.Log(LogSeverityEnum.Error, "Valve5 Connect Error" + ex.StackTrace);
                MessageBox.Show("Valve5 Connect Error" + ex.StackTrace);
            }
        }
      
        private void EventMotorUp(int? para)
        {
            try
            {
                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var motors = MotorList.Where(x => x.Selected);

                if (motors == null) { MessageBox.Show("请先选择需要移动的电机"); return; }
                int actual = 0;
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in motors)
                    {
                        switch (item.Id)
                        {
                            case (int)MotorCatogory.MotorA:
                                IOBoard.MoveSuctionMotorAToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.MotorB:
                                IOBoard.MoveSuctionMotorBToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor1:
                                IOBoard.MoveCapMotor1stToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor2:
                                IOBoard.MoveCapMotor2ndToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor3:
                                IOBoard.MoveCapMotor3thToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor4:
                                IOBoard.MoveCapMotor4thToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor5:
                                IOBoard.MoveCapMotor5thToUpper((byte)para, 0, ref actual);
                                break;
                            default:
                                break;
                        }

                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("电机向上移动", ex);
                MessageBox.Show("电机向上移动错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }
        private void EventMotorDown(int? para)
        {
            try
            {
                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var motors = MotorList.Where(x => x.Selected);

                if (motors == null) { MessageBox.Show("请先选择需要移动的电机"); return; }
                int distance;
                if (!int.TryParse(MotorDistances, out distance))
                { MessageBox.Show("请输入正确的距离"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    int actual = 0;
                    foreach (var item in motors)
                    {
                        switch (item.Id)
                        {
                            case (int)MotorCatogory.MotorA:
                                IOBoard.MoveSuctionMotorAToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.MotorB:
                                IOBoard.MoveSuctionMotorBToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor1:
                                IOBoard.MoveCapMotor1stToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor2:
                                IOBoard.MoveCapMotor2ndToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor3:
                                IOBoard.MoveCapMotor3thToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor4:
                                IOBoard.MoveCapMotor4thToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor5:
                                IOBoard.MoveCapMotor5thToLower((byte)para, distance, ref actual);
                                break;
                            default:
                                break;
                        }

                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("电机向下移动", ex);
                MessageBox.Show("电机向下移动错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }

        private void EventAspirate(int? para)
        {
            try
            {
                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var pumps = PumpList.Where(x => x.Selected);

                if (pumps == null) { MessageBox.Show("请先选择需要操作的注射泵"); return; }

                var pos = PumpPos == "NO" ? 0 : 1;
                double volume;
                if (!double.TryParse(PumpVolume, out volume))
                { MessageBox.Show("请输入正确的吸液量"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    try
                    {
                        foreach (var item in pumps)
                        {
                            IOBoard.SolenoidValve((byte)(5*(item.Id-1)+2), true);
                            Pump.SwitchValve(item.Id, pos);
                            Pump.Aspirate(item.Id, volume, (double)para);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageHelper.ShowMessage(ex.StackTrace);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("注射泵吸液", ex);
                MessageBox.Show("注射泵吸液错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }
        private void EventDispense(int? para)
        {
            try
            {

                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var pumps = PumpList.Where(x => x.Selected);

                if (pumps == null) { MessageBox.Show("请先选择需要操作的注射泵"); return; }

                var pos = PumpPos == "NO" ? 0 : 1;
                double volume;
                if (!double.TryParse(PumpVolume, out volume))
                { MessageBox.Show("请输入正确的吸液量"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in pumps)
                    {
                        IOBoard.SolenoidValve((byte)(5 * (item.Id - 1) + 2), true);
                        Pump.SwitchValve(item.Id, pos);
                        Pump.Dispense(item.Id, volume, (double)para);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("注射泵排液", ex);
                MessageBox.Show("注射泵排液错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }


        private void EventDispenseAll(int? para)
        {
            try
            {

                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var pumps = PumpList.Where(x => x.Selected);

                if (pumps == null) { MessageBox.Show("请先选择需要操作的注射泵"); return; }

                var pos = PumpPos == "NO" ? 0 : 1;
               
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in pumps)
                    {
                        IOBoard.SolenoidValve((byte)(5 * (item.Id - 1) + 2), true);
                        Pump.SwitchValve(item.Id, pos);
                        Pump.Dispense(item.Id,(double)para);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("注射泵排空", ex);
                MessageBox.Show("注射泵排空错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }
        private void EventRotary(int? para)
        {
            try
            {

                if (para == null) MessageBox.Show("请先选择阀旋转位置");
                var valves = RotaryValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的旋转阀"); return; }

                foreach (var item in valves)
                {
                    RotaryValves[item.Id-1].GotoPosition(para.Value);
                    //((SelectorValveDevice)GetDevice("RotaryValve" + item.Id.ToString())).Device.GotoPosition(para.Value);
                }
            }
            catch (Exception ex)
            {
                logger.Log("旋转阀旋转位置", ex);
                MessageBox.Show("旋转阀旋转位置发生错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }
        private void OffSolenoidValve(int? para)
        {
            try
            {
                var valves = SolenoidValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的电磁阀"); return; }

                foreach (var item in valves)
                {
                    IOBoard.SolenoidValve((byte)item.Id, false);
                }
            }
            catch (Exception ex)
            {
                logger.Log("关闭电磁阀", ex);
                MessageBox.Show("关闭电磁阀错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }
        private void OpenSolenoidValve(int? para)
        {
            try
            {
                var valves = SolenoidValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的电磁阀"); return; }

                foreach (var item in valves)
                {
                    IOBoard.SolenoidValve((byte)item.Id, true);
                }
            }
            catch (Exception ex)
            {
                logger.Log("打开电磁阀", ex);
                System.Windows.MessageBox.Show("打开电磁阀错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally { NotifyToWindow(); }
        }

        #endregion

        private void NotifyToWindow()
        {
            RaisePropertyChanged("IOBoardConntected");
            RaisePropertyChanged("PumpConntected");
            RaisePropertyChanged("Valve1Conntected");
            RaisePropertyChanged("Valve2Conntected");
            RaisePropertyChanged("Valve3Conntected");
            RaisePropertyChanged("Valve4Conntected");
            RaisePropertyChanged("Valve5Conntected");
        }
    }
    public enum MotorCatogory
    {
        Motor1 = 1,
        Motor2 = 2,
        Motor3 = 3,
        Motor4 = 4,
        Motor5 = 5,
        MotorA = 6,
        MotorB = 7
    }
    public class DeviceOperaton
    {
        public DeviceOperaton(int id, string name, bool selected)
        {
            Id = id;
            Name = name;
            Selected = selected;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

}
