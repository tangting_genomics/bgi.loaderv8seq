﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using BGI.Common.Config;
using BGI.Control.Device;

using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight;
using BGI.Common.Utils;
using System.Threading.Tasks;
using BGI.LoaderV8Seq.Engineer.GUI.Helper;
using BGI.Common.UtilWPF.Helpers;
using static BGI.LoaderV8Seq.Engineer.GUI.Helper.DeviceManager;
using BGI.LoaderV8Seq.Engineer.GUI.Views;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class IOBoardViewModel : MainViewModelBase
    {
        private DeviceProxy deviceProxy;
        private Dictionary<string, DeviceProxy> deviceProxyMap;
        dynamic ioboardDynamic = null;
        private Dictionary<LedStatus, bool> LEDPowerStatus = new Dictionary<LedStatus, bool>();
        private List<LedStatus> _LEDColorSource;
        private List<IOBoardTypeEnum> _IOBoardTypeSource;
        private IOBoardTypeEnum? _IOBoardType;
        private LedStatus _LEDColorType;
        private string _LEDPowerDes = "PowerOn";
        private List<FansType> _FansSource;
        private Dictionary<FansType, bool> FansPowerStatus = new Dictionary<FansType, bool>();
        private FansType _FansType;
        private string _FansPowerDes = "PowerOn";
        private short _FanSpeed;

        private DeviceManager DeviceManagerEntity;
        private Dictionary<string, bool> initMap;
        private string _IOBoardName;
        private bool _IsLaserEnabled;
        private bool _IsBuzzerEnabled;
        private bool _IsLedEnabled;
        private string _InitDesc;
        private Dictionary<string, dynamic> dicIoboardDynamic = new Dictionary<string, dynamic>();
        private bool _IsV2ChipB = false;
        private float _GreenPower = 0;
        private float _RedPower = 0;
        private bool _IsIOInit = true;
        #region (Property)
        public bool IsIOInit
        {
            get { return _IsIOInit; }
            set { _IsIOInit = value; RaisePropertyChanged(); }
        }
        public float GreenPower
        {
            get { return _GreenPower; }
            set { _GreenPower = value; RaisePropertyChanged(); }
        }

        public float RedPower
        {
            get { return _RedPower; }
            set { _RedPower = value; RaisePropertyChanged(); }
        }
        public string InitDesc
        {
            get { return _InitDesc; }
            set { this._InitDesc = value; RaisePropertyChanged(); }
        }

        public bool IsLedEnabled
        {
            get { return _IsLedEnabled; }
        }
        public bool IsBuzzerEnabled
        {
            get { return _IsBuzzerEnabled; }
            set { _IsBuzzerEnabled = value; RaisePropertyChanged(); }
        }
        public bool IsLaserEnabled
        {
            get { return _IsLaserEnabled; }
            set { _IsLaserEnabled = value; RaisePropertyChanged(); }
        }
        
        public string IOBoardName
        {
            get { return _IOBoardName; }
            set { _IOBoardName = value; RaisePropertyChanged(); }
        }

        public List<IOBoardTypeEnum> IOBoardTypeSource
        {
            get
            {
                _IOBoardTypeSource = new List<IOBoardTypeEnum>();
                foreach (string name in Enum.GetNames(typeof(IOBoardTypeEnum)))
                {
                    var type = (IOBoardTypeEnum)Enum.Parse(typeof(IOBoardTypeEnum), name);
                    _IOBoardTypeSource.Add(type);
                }
                return _IOBoardTypeSource;
            }
            set
            {
                _IOBoardTypeSource = value;
                RaisePropertyChanged();
            }
        }
        public IOBoardTypeEnum IOBoardType
        {
            get
            {
                if (_IOBoardType != null) return _IOBoardType.GetValueOrDefault();
                IOBoardTypeEnum tempIOBoardtype;
                Enum.TryParse<IOBoardTypeEnum>(ConfigMgr.Get.GetCurValue<BoardTypeEnum>("System", "IOBoardType").ToString(), out tempIOBoardtype);
                _IOBoardType = tempIOBoardtype;
                return _IOBoardType.GetValueOrDefault();
            }
            set
            {
                _IOBoardType = value;
                RaisePropertyChanged();
            }
        }
        public List<LedStatus> LEDColorSource
        {
            get
            {
                _LEDColorSource = new List<LedStatus>();
                foreach (string name in Enum.GetNames(typeof(LedStatus)))
                {
                    var type = (LedStatus)Enum.Parse(typeof(LedStatus), name);
                    _LEDColorSource.Add(type);
                    LEDPowerStatus.Add(type, false);
                }
                return _LEDColorSource;
            }
            set
            {
                _LEDColorSource = value;
                RaisePropertyChanged("LEDColorSource");
            }
        }
        public LedStatus LEDColorType
        {
            get { return _LEDColorType; }
            set
            {
                _LEDColorType = value;
                RaisePropertyChanged("LEDColorType");
                if (LEDPowerStatus[_LEDColorType]) LEDPowerDes = "PowerOff";
                else LEDPowerDes = "PowerOn";
            }
        }
        public string LEDPowerDes
        {
            get { return _LEDPowerDes; }
            set
            {
                _LEDPowerDes = value;
                RaisePropertyChanged("LEDPowerDes");
            }
        }
        public List<FansType> FansSource
        {
            get
            {
                _FansSource = new List<FansType>();
                foreach (string name in Enum.GetNames(typeof(FansType)))
                {
                    if (name == "RadiatorFanA") continue;//for now ,the machine only has one fan which is named RadiatorFanB
                    var fantype = (FansType)Enum.Parse(typeof(FansType), name);
                    _FansSource.Add(fantype);
                    FansPowerStatus.Add(fantype, false);
                }
                return _FansSource;
            }
            set
            {
                _FansSource = value;
                RaisePropertyChanged("FansSource");
            }
        }
        public FansType FansType
        {
            get { return _FansType; }
            set
            {
                _FansType = value;
                FanSpeed = ioboardDynamic.GetFanSpeed(FansType);
                if (FansPowerStatus[FansType]) FansPowerDes = "PowerOff";
                else FansPowerDes = "PowerOn";
                RaisePropertyChanged("FansType");
            }
        }
        public string FansPowerDes
        {
            get { return _FansPowerDes; }
            set
            {
                _FansPowerDes = value;
                RaisePropertyChanged("FansPowerDes");
            }
        }
        public short FanSpeed
        {
            get { return _FanSpeed; }
            set
            {
                _FanSpeed = value;
                RaisePropertyChanged("FanSpeed");
            }
        }
        private Dictionary<DiaphragmPumpType, bool> DiaphragmPumpPowerStatus = new Dictionary<DiaphragmPumpType, bool>();
        private List<DiaphragmPumpType> _DiaphragmPumpSource;
        public List<DiaphragmPumpType> DiaphragmPumpSource
        {
            get
            {
                _DiaphragmPumpSource = new List<DiaphragmPumpType>();
                foreach (string name in Enum.GetNames(typeof(DiaphragmPumpType)))
                {
                    var type = (DiaphragmPumpType)Enum.Parse(typeof(DiaphragmPumpType), name);
                    if(type == DiaphragmPumpType.VacuoPump)
                    {
                        _DiaphragmPumpSource.Add(type);
                        DiaphragmPumpPowerStatus.Add(type, false);
                    }
                    
                }
                return _DiaphragmPumpSource;
            }
            set
            {
                _DiaphragmPumpSource = value;
                RaisePropertyChanged("DiaphragmPumpSource");
            }
        }
        private DiaphragmPumpType _DiaphragmPumpType;
        public DiaphragmPumpType DiaphragmPumpType
        {
            get { return _DiaphragmPumpType; }
            set
            {
                _DiaphragmPumpType = value;
                DiaphragmPumpSpeed = ioboardDynamic.GetDiaphragmPumpSpeed(DiaphragmPumpType);
                if (DiaphragmPumpPowerStatus[_DiaphragmPumpType]) DiaphragmPumpPowerDes = "PowerOff";
                else DiaphragmPumpPowerDes = "PowerOn";
                RaisePropertyChanged("DiaphragmPumpType");
            }
        }
        private string _DiaphragmPumpPowerDes = "PowerOn";
        public string DiaphragmPumpPowerDes
        {
            get { return _DiaphragmPumpPowerDes; }
            set
            {
                _DiaphragmPumpPowerDes = value;
                RaisePropertyChanged("DiaphragmPumpPowerDes");
            }
        }
        private short _DiaphragmPumpSpeed;
        public short DiaphragmPumpSpeed
        {
            get { return _DiaphragmPumpSpeed; }
            set
            {
                _DiaphragmPumpSpeed = value;
                RaisePropertyChanged("DiaphragmPumpSpeed");
            }
        }
        private bool _IsInitialize;
        public bool IsInitialize
        {
            get { return _IsInitialize; }
            set
            {
                _IsInitialize = value;
                RaisePropertyChanged("IsInitialize");
            }
        }
        private string _greenLaserPower = string.Empty;
        public string GreenLaserPower
        {
            get { return _greenLaserPower; }
            set
            {
                _greenLaserPower = value;
                RaisePropertyChanged("GreenLaserPower");
            }
        }
        private bool _init = false;
        public bool bInit
        {
            get { return _init; }
            set
            {
                _init = value;
                RaisePropertyChanged("bInit");
            }
        }
        private bool _laserEnable = false;
        public bool bLaserEnable
        {
            get
            {
                return _laserEnable;
            }
            set
            {
                _laserEnable = value;
                RaisePropertyChanged("bLaserEnable");
            }
        }
        public bool _laserDisable = false;
        public bool bLaserDisable
        {
            get { return _laserDisable; }
            set
            {
                _laserDisable = value;
                RaisePropertyChanged("bLaserDisable");
            }
        }
        private bool _setGreenLaser = false;
        public bool bSetGreenLaser
        {
            get { return _setGreenLaser; }
            set
            {
                _setGreenLaser = value;
                RaisePropertyChanged("bSetGreenLaser");
            }
        }
        private bool _setRedLaser = false;
        public bool bSetRedLaser
        {
            get { return _setRedLaser; }
            set
            {
                _setRedLaser = value;
                RaisePropertyChanged("bSetRedLaser");
            }
        }
        private bool _onSelectValve = false;
        public bool bOnSelectValve
        {
            get { return _onSelectValve; }
            set
            {
                _onSelectValve = value;
                RaisePropertyChanged("bOnSelectValve");
            }
        }
        private bool _offSelectValve = false;
        public bool bOffSelectValve
        {
            get { return _offSelectValve; }
            set
            {
                _offSelectValve = value;
                RaisePropertyChanged("bOffSelectValve");
            }
        }
        private bool _onSyringPump = false;
        public bool bOnSyringPump
        {
            get { return _onSyringPump; }
            set
            {
                _onSyringPump = value;
                RaisePropertyChanged("bOnSyringPump");
            }
        }
        private bool _offSyringPump = false;

        public bool bOffSyringPump
        {
            get { return _offSyringPump; }
            set
            {
                _offSyringPump = value;
                RaisePropertyChanged("bOffSyringPump");
            }
        }
        private bool _onSMC = false;
        public bool bOnSMC
        {
            get { return _onSMC; }
            set
            {
                _onSMC = value;
                RaisePropertyChanged("bOnSMC");
            }
        }
        private bool _offSMC = false;
        public bool bOffSMC
        {
            get { return _offSMC; }
            set
            {
                _offSMC = value;
                RaisePropertyChanged("bOffSMC");
            }
        }

        private bool _openGreenLaser = false;
        public bool bOpenGreenLaser
        {
            get { return _openGreenLaser; }
            set
            {
                _openGreenLaser = value;
                RaisePropertyChanged("bOpenGreenLaser");
            }
        }
        private bool _closeGreenLaser = false;
        public bool bCloseGreenLaser
        {
            get { return _closeGreenLaser; }
            set
            {
                _closeGreenLaser = value;
                RaisePropertyChanged("bCloseGreenLaser");
            }
        }
        private bool _openRedLaser = false;
        public bool bOpenRedLaser
        {
            get { return _openRedLaser; }
            set
            {
                _openRedLaser = value;
                RaisePropertyChanged("bOpenRedLaser");
            }

        }
        private bool _closeRedLaser = false;
        public bool bCloseRedLaser
        {
            get { return _closeRedLaser; }
            set
            {
                _closeRedLaser = value;
                RaisePropertyChanged("bCloseRedLaser");
            }
        }
        private string _redLaserPower = string.Empty;
        public string RedLaserPower
        {
            get { return _redLaserPower; }
            set
            {
                _redLaserPower = value;
                RaisePropertyChanged("RedLaserPower");
            }
        }
        private string _buzzerPowerDes = "OnBuzzer";
        public string BuzzerPowerDes
        {
            get { return _buzzerPowerDes; }
            set
            {
                _buzzerPowerDes = value;
                RaisePropertyChanged("BuzzerPowerDes");
            }
        }
        private short _BuzzerFrequency = 1000;
        public short BuzzerFrequency
        {
            get { return _BuzzerFrequency; }
            set
            {
                _BuzzerFrequency = value;
                RaisePropertyChanged("BuzzerFrequency");
            }
        }
        private short _BuzzerTotalTime = 1;
        public short BuzzerTotalTime
        {
            get { return _BuzzerTotalTime; }
            set
            {
                _BuzzerTotalTime = value;
                RaisePropertyChanged("BuzzerTotalTime");
            }
        }
        private ushort _BuzzerPWM = 1;
        public ushort BuzzerPWM
        {
            get { return _BuzzerPWM; }
            set
            {
                _BuzzerPWM = value;
                RaisePropertyChanged("BuzzerPWM");
            }
        }
        private bool _BuzzerEnable = false;
        public bool BuzzerEnable
        {
            get { return _BuzzerEnable; }
            set
            {
                _BuzzerEnable = value;
                RaisePropertyChanged("BuzzerEnable");
            }
        }
        #endregion
        //public IOBoardViewModel(DeviceManager dm)
        //{
        //    //this.DeviceManagerEntity = dm;
        //    IOBoardType = (IOBoardTypeEnum)((int)dm.EngineerSeq);
            
        //    // ioBoard  = new VIOBoardV01("IOBoardV01");
        //    //Task.Factory.StartNew(() =>
        //    //{
        //    //    while (true)
        //    //    {
        //    //        Thread.Sleep(2000);
        //    //        if (null == IoBoardInt) continue;
        //    //        GetAllStatus();
        //    //    }
        //    //});
        //}

        public IOBoardViewModel(string modelName):base(modelName)
        {
            this.DeviceManagerEntity = DeviceManager.Get;
            this.IsLaserEnabled = true;
            this.ChipDoorStatus = Brushes.Gray;
            this.FridgeDoorStatus = Brushes.Gray;
            this.VacuoButtonStatus = Brushes.Gray;
            this.VacuoButtonStatusB = Brushes.Gray;
            this.LiquidStatus = Brushes.Gray;
            this.SMCStatus = Brushes.Gray;
            this.SMCStatusB = Brushes.Gray;
            this.KitStatus = Brushes.Gray;
            this.KitStatusB = Brushes.Gray;

            this.DnbStatusA = Brushes.Gray;
            this.DnbStatusB = Brushes.Gray;

            this.TopCoverStatus = Brushes.Gray;
            this.LeftSideCoverStatus = Brushes.Gray;
            this.RightSideCoverStatus = Brushes.Gray;

            this.InitDesc = "Initialize";
            this.IsIOInit = true;
            this.initMap = new Dictionary<string, bool>();
            this.PropertyChanged += IOBoardViewModel_PropertyChanged;
        }

        private void IOBoardViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "IOBoardName")
            {
                if (this.dicIoboardDynamic.ContainsKey(IOBoardName))
                    this.ioboardDynamic = dicIoboardDynamic[IOBoardName];
                else
                {
                    if (this.deviceProxyMap.ContainsKey(IOBoardName))
                    {
                        DeviceProxy proxy = deviceProxyMap[IOBoardName];
                        if (proxy.IsSimulated)
                        {
                            this.ioboardDynamic = proxy._SDeviceInt;
                        }
                        else
                            this.ioboardDynamic = proxy._VDeviceInt;
                        this.dicIoboardDynamic.Add(IOBoardName, ioboardDynamic);

                    }
                }
                if (IOBoardName.EndsWith("V2_B")) this._IsV2ChipB = true;
                else this._IsV2ChipB = false;
            }
        }
        #region(Command) 
        private List<ServiceController> _ServiceSource;
        public List<ServiceController> ServiceSource
        {
            get
            {
                _ServiceSource = SelfCheck.GetISWService;
                return _ServiceSource;
            }
            set
            {
                _ServiceSource = value;
                RaisePropertyChanged("ServiceSource");
            }
        }
        private ServiceController _ServiceType;
        public ServiceController ServiceType
        {
            get { return _ServiceType; }
            set
            {
                _ServiceType = value;
                RaisePropertyChanged("ServiceType");
                if (null != _ServiceType && _ServiceType.Status != ServiceControllerStatus.Stopped) ServiceDes = "Stop";
                else ServiceDes = "Start";
            }
        }
        private string _ServiceDes = "Start";
        public string ServiceDes
        {
            get
            {
                return _ServiceDes;
            }
            set
            {
                _ServiceDes = value;
                RaisePropertyChanged("ServiceDes");
            }
        }
        
        private Brush _ChipDoorStatus;
        public Brush ChipDoorStatus
        {
            get
            {
                return _ChipDoorStatus;
            }
            set
            {
                _ChipDoorStatus = value;
                RaisePropertyChanged("ChipDoorStatus");
            }
        }



        private Brush _LiquidStatus;
        public Brush LiquidStatus
        {
            get { return _LiquidStatus; }
            set
            {
                _LiquidStatus = value;
                RaisePropertyChanged("LiquidStatus");
            }
        }
        private PipleStatus _PipleStatus;
        public PipleStatus PipleStatus
        {
            get { return _PipleStatus; }
            set
            {
                _PipleStatus = value;
                RaisePropertyChanged("PipleStatus");
            }
        }
        private Brush _FridgeDoorStatus;
        public Brush FridgeDoorStatus
        {
            get { return _FridgeDoorStatus; }
            set
            {
                _FridgeDoorStatus = value;
                RaisePropertyChanged("FridgeDoorStatus");
            }
        }

        private Brush _KitStatus;
        public Brush KitStatus
        {
            get { return _KitStatus; }
            set
            {
                _KitStatus = value;
                RaisePropertyChanged("KitStatus");
            }
        }

        private Brush _KitStatusB;
        public Brush KitStatusB
        {
            get { return _KitStatusB; }
            set
            {
                _KitStatusB = value;
                RaisePropertyChanged("KitStatusB");
            }
        }
        private Brush _SMCStatus;
        public Brush SMCStatus
        {
            get { return _SMCStatus; }
            set
            {
                _SMCStatus = value;
                RaisePropertyChanged("SMCStatus");
            }
        }

        private Brush _SMCStatusB;
        public Brush SMCStatusB
        {
            get { return _SMCStatusB; }
            set
            {
                _SMCStatusB = value;
                RaisePropertyChanged("SMCStatusB");
            }
        }

        private Brush _VacuoButtonStatus;
        public Brush VacuoButtonStatus
        {
            get { return _VacuoButtonStatus; }
            set
            {
                _VacuoButtonStatus = value;
                RaisePropertyChanged("VacuoButtonStatus");
            }
        }

        private Brush _VacuoButtonStatusB;
        public Brush VacuoButtonStatusB
        {
            get { return _VacuoButtonStatusB; }
            set
            {
                _VacuoButtonStatusB = value;
                RaisePropertyChanged("VacuoButtonStatusB");
            }
        }

        private Brush _TopCoverStatus;
        public Brush TopCoverStatus
        {
            get { return _TopCoverStatus; }
            set
            {
                _TopCoverStatus = value;
                RaisePropertyChanged("TopCoverStatus");
            }
        }

        private Brush _LeftSideCoverStatus;
        public Brush LeftSideCoverStatus
        {
            get { return _LeftSideCoverStatus; }
            set
            {
                _LeftSideCoverStatus = value;
                RaisePropertyChanged("LeftSideCoverStatus");
            }
        }

        private Brush _RightSideCoverStatus;
        public Brush RightSideCoverStatus
        {
            get { return _RightSideCoverStatus; }
            set
            {
                _RightSideCoverStatus = value;
                RaisePropertyChanged("RightSideCoverStatus");
            }
        }

        private Brush _DnbStatusA;
        public Brush DnbStatusA
        {
            get { return _DnbStatusA; }
            set
            {
                _DnbStatusA = value;
                RaisePropertyChanged("DnbStatusA");
            }
        }

        private Brush _DnbStatusB;
        public Brush DnbStatusB
        {
            get { return _DnbStatusB; }
            set
            {
                _DnbStatusB = value;
                RaisePropertyChanged("DnbStatusB");
            }
        }

        private string _version;
        public string Version
        {
            get
            {
                if(ioboardDynamic != null)
                {
                    return ioboardDynamic.GetVersion();
                }
                return string.Empty;
            }

            set
            {
                _version = value;
                RaisePropertyChanged("Version");
            }
        }

        #region Commander

        private ICommand _ServiceCommand;
        public ICommand ServiceCommand
        {
            get
            {
                return _ServiceCommand ?? (_ServiceCommand = new RelayCommand(() =>
                {
                    try
                    {
                        if (ServiceDes == "Start")
                        {
                            ServiceType.Start();
                            ServiceDes = "Stop";
                        }
                        else
                        {
                            ServiceType.Stop();
                            ServiceDes = "Start";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }

        #endregion
        private ICommand _goInitializeViewCommand;
        public ICommand GoInitializeViewCommand
        {
            get
            {
                return _goInitializeViewCommand ?? (_goInitializeViewCommand = new RelayCommand(() =>
                {
                    Task task = Task.Run(() =>
                    {
                        try
                        {
                            this.IsIOInit = false;
                            if (this._init)
                            {
                                DeviceManagerEntity.Disconnect(EnigneerDeviceType.IOBoard);
                                bLaserEnable = false;
                                BuzzerEnable = false;
                                IsInitialize = false;
                                this._init = false;
                                this.InitDesc = "Connect";
                                this.dicIoboardDynamic = new Dictionary<string, dynamic>();
                            }
                            else
                            {
                                deviceProxyMap = DeviceManagerEntity.IOBoardProxyMap;
                                if (deviceProxyMap.ContainsKey(this.IOBoardName))
                                    deviceProxy = deviceProxyMap[this.IOBoardName];
                                if (deviceProxy.IsSimulated)
                                {
                                    ioboardDynamic = deviceProxy._SDeviceInt;
                                }
                                else
                                {
                                    ioboardDynamic = deviceProxy._VDeviceInt;
                                }

                                Version = ioboardDynamic.GetVersion();

                                this.InitDesc = "Disconnect";
                                this._init = true;
                                bLaserEnable = true;
                                BuzzerEnable = true;
                                IsInitialize = true;
                                this.IsIOInit = true;
                                this.dicIoboardDynamic.Add(IOBoardName, ioboardDynamic);
                                deviceProxy.AtomicValueChanged -= IOBoardViewModel_AtomicValueChanged;
                                deviceProxy.AtomicValueChanged += IOBoardViewModel_AtomicValueChanged;
                            }
                        }
                        catch (Exception ex)
                        {
                            this.IsIOInit = true;
                            //MessageBox.Show(ex.Message);
                            MessageHelper.ShowMessage(ex.Message);
                        }
                    });
                    
                }));
            }
        }

        private bool GetStatus(int value,int index)
        {
            //Console.WriteLine($"Bit16 {}");
            if(index > 7)
            {
                byte result = (byte)((value & (0x01 << index)) >> index);
                return result > 0;
            }
            else if(index == 0)
            {
                BGI.Control.Device.SMCStatus result = (BGI.Control.Device.SMCStatus)(value & 0x0F);
                if (result == BGI.Control.Device.SMCStatus.Normal) return true;
                else return false;
            }
            else if(index == 4)
            {
                BGI.Control.Device.SMCStatus result = (BGI.Control.Device.SMCStatus)((value & 0xF0) >> 4);
                if (result == BGI.Control.Device.SMCStatus.Normal) return true;
                else return false;
            }
            
            return true;
        }

        private void IOBoardViewModel_AtomicValueChanged(RPC.AtomicValueEventArgs args)
        {
            switch (args.av.ValueName)
            {
                case "IOBoard_Status":
                    int value = args.av.ValueI;
                    bool statusSMC = GetStatus(value, 0);

                    if (statusSMC) SMCStatus = Brushes.Green;
                    else SMCStatus = Brushes.Red;

                    bool statusSMCB = GetStatus(value, 4);
                    if(statusSMCB) SMCStatusB = Brushes.Green;
                    else SMCStatusB = Brushes.Red;

                    bool statusChip = GetStatus(value, 8);
                    if (statusChip) ChipDoorStatus = Brushes.Green;
                    else ChipDoorStatus = Brushes.Red;

                    bool statusFridge = GetStatus(value, 9);
                    if (statusFridge) FridgeDoorStatus = Brushes.Green;
                    else FridgeDoorStatus = Brushes.Red;

                    bool statusKitA = GetStatus(value, 10);
                    if (statusKitA) KitStatus = Brushes.Red;
                    else KitStatus = Brushes.Green;

                    bool statusKitB = GetStatus(value, 11);
                    if (statusKitB) KitStatusB = Brushes.Red;
                    else KitStatusB = Brushes.Green;

                    bool statusVacuoA = GetStatus(value, 12);
                    if (statusVacuoA) VacuoButtonStatus = Brushes.Green;
                    else VacuoButtonStatus = Brushes.Red;

                    bool statusVacuoB = GetStatus(value, 13);
                    if (statusVacuoB) VacuoButtonStatusB = Brushes.Green;
                    else VacuoButtonStatusB = Brushes.Red;

                    bool statusTop = GetStatus(value, 14);
                    if (statusTop) TopCoverStatus = Brushes.Green;
                    else TopCoverStatus = Brushes.Red;

                    bool statusLeft = GetStatus(value, 15);
                    if (statusLeft) LeftSideCoverStatus = Brushes.Green;
                    else LeftSideCoverStatus = Brushes.Red;

                    bool statusRight = GetStatus(value, 16);
                    if (statusRight) RightSideCoverStatus = Brushes.Green;
                    else RightSideCoverStatus = Brushes.Red;

                    bool statusDnbA = GetStatus(value, 17);
                    if (statusDnbA) DnbStatusA = Brushes.Red;
                    else DnbStatusA = Brushes.Green;

                    bool statusDnbB = GetStatus(value, 18);
                    if (statusDnbB) DnbStatusB = Brushes.Red;
                    else DnbStatusB = Brushes.Green;

                    bool statusLiquid = GetStatus(value, 19);
                    if (statusLiquid) LiquidStatus = Brushes.Green;
                    else LiquidStatus = Brushes.Red;
                    break;
                case "IOBoard_VacuoButtonStatusA":
                    if(args.av.ValueB) VacuoButtonStatus = Brushes.Green;
                    else VacuoButtonStatus = Brushes.Red;
                    break;
                case "IOBoard_VacuoButtonStatusB":
                    if (args.av.ValueB) VacuoButtonStatusB = Brushes.Green;
                    else VacuoButtonStatusB = Brushes.Red;
                    break;
                case "IOBoard_LiquidStatus":
                    if (args.av.ValueB) LiquidStatus = Brushes.Red;
                    else LiquidStatus = Brushes.Green;
                    break;
                case "IOBoard_SMCStatusA":
                    if (args.av.ValueB) SMCStatus = Brushes.Green;
                    else SMCStatus = Brushes.Red;
                    break;
                case "IOBoard_SMCStatusB":
                    if (args.av.ValueB) SMCStatusB = Brushes.Green;
                    else SMCStatusB = Brushes.Red;
                    break;
                case "IOBoard_ChipDoor":
                    if (args.av.ValueB) ChipDoorStatus = Brushes.Green;
                    else ChipDoorStatus = Brushes.Red;
                    break;
                case "IOBoard_FridgeDoor":
                    if (args.av.ValueB) FridgeDoorStatus = Brushes.Green;
                    else FridgeDoorStatus = Brushes.Red;
                    break;
                case "IOBoard_KitStatusA":
                    if (args.av.ValueB) KitStatus = Brushes.Green;
                    else KitStatus = Brushes.Red;
                    break;
                case "IOBoard_KitStatusB":
                    if (args.av.ValueB) KitStatusB = Brushes.Green;
                    else KitStatusB = Brushes.Red;
                    break;
                case "IOBoard_TopCover":
                    if (args.av.ValueB) TopCoverStatus = Brushes.Green;
                    else TopCoverStatus = Brushes.Red;
                    break;
                case "IOBoard_LeftSideCover":
                    if (args.av.ValueB) LeftSideCoverStatus = Brushes.Green;
                    else LeftSideCoverStatus = Brushes.Red;
                    break;
                case "IOBoard_RightSideCover":
                    if (args.av.ValueB) RightSideCoverStatus = Brushes.Green;
                    else RightSideCoverStatus = Brushes.Red;
                    break;
                case "IOBoard_DNBStatusA":
                    if (args.av.ValueB) DnbStatusA = Brushes.Red;
                    else DnbStatusA = Brushes.Green;
                    break;
                case "IOBoard_DNBStatusB":
                    if (args.av.ValueB) DnbStatusB = Brushes.Red;
                    else DnbStatusB = Brushes.Green;
                    break;
            }
        }

        private ICommand _runScriptCmd;
        public ICommand RunScriptCmd
        {
            get
            {
                return _runScriptCmd ?? (_runScriptCmd = new RelayCommand(() =>
                {
                    try
                    {
                        Script wnd = new Script();
                        Dictionary<string, object> deviceDic = new Dictionary<string, object>();
                        deviceDic.Add("IOBoard", ioboardDynamic);
                        wnd.Title = "IO Board Script";
                        wnd.DeviceRawObjectD = deviceDic;
                        wnd.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        wnd.Show();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Run script command error:" + ex.Message);
                    }
                }));
            }
        }

        private ICommand _buzzerPowerCommand;
        public ICommand BuzzerPowerCommand
        {
            get
            {
                return _buzzerPowerCommand ?? (_buzzerPowerCommand = new RelayCommand(() =>
                {
                    try
                    {
                        if (BuzzerPowerDes == "OnBuzzer")
                        {
                            ioboardDynamic.SetBuzzerPWM(BuzzerPWM);
                            ioboardDynamic.PowerOnOffBuzzer(true);
                            BuzzerPowerDes = "OffBuzzer";
                        }
                        else
                        {
                            ioboardDynamic.PowerOnOffBuzzer(false);
                            BuzzerPowerDes = "OnBuzzer";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }

        private ICommand _goLaserEnableViewCommand;
        public ICommand GoLaserEnableViewCommand
        {
            get
            {
                return _goLaserEnableViewCommand ?? (_goLaserEnableViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        ioboardDynamic.OperateEnableSignal(true);
                        //ioBoard.OperateEnableSignal(true)

                        bLaserEnable = false;
                        bLaserDisable = true;
                        bSetGreenLaser = true;
                        bSetRedLaser = true;
                        bOnSelectValve = true;
                        bOnSyringPump = true;
                        bOnSMC = true;
                        bOpenGreenLaser = true;
                        bOpenRedLaser = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }

        }
        private ICommand _goSetGreenLaserViewCommand;
        public ICommand GoSetGreenLaserViewCommand
        {
            get
            {
                return _goSetGreenLaserViewCommand ?? (_goSetGreenLaserViewCommand = new RelayCommand(() =>
                {
                    if (GreenLaserPower.Trim() == string.Empty)
                    {

                        MessageBox.Show("Please enter laser power!");
                        return;
                    }
                    try
                    {
                        ioboardDynamic.SetLaserPower(Control.Device.LaserColor.Green, Convert.ToSingle(GreenLaserPower.Trim()));
                        // ioBoard.SetLaserPower(Control.Device.LaserColor.Green, Convert.ToSingle(GreenLaserPower.Trim())); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }
        private ICommand _SaveToConfigCommandGreen;
        public ICommand SaveToConfigCommandGreen
        {
            get
            {
                return _SaveToConfigCommandGreen ?? (_SaveToConfigCommandGreen = new RelayCommand(() =>
                {

                    MessageBoxResult mbr = MessageBox.Show("Do you want to set to config?", "message", MessageBoxButton.OKCancel);
                    if (mbr == MessageBoxResult.Cancel) return;
                    this.DeviceManagerEntity.SetParameter("IOBoard", "InitialGreenLaserPower", this.GreenLaserPower);
                }));
            }
        }
        private ICommand _SaveToConfigCommandRed;
        public ICommand SaveToConfigCommandRed
        {
            get
            {
                return _SaveToConfigCommandRed ?? (_SaveToConfigCommandRed = new RelayCommand(() =>
                {

                    MessageBoxResult mbr = MessageBox.Show("Do you want to set to config?", "message", MessageBoxButton.OKCancel);
                    if (mbr == MessageBoxResult.Cancel) return;
                    this.DeviceManagerEntity.SetParameter("IOBoard", "InitialRedLaserPower", this.RedLaserPower);
                }));
            }
        }
        private ICommand _goSetRedLaserViewCommand;
        public ICommand GoSetRedLaserViewCommand
        {
            get
            {
                return _goSetRedLaserViewCommand ?? (_goSetRedLaserViewCommand = new RelayCommand(() =>
                {
                    if (RedLaserPower.Trim() == string.Empty)
                    {
                        MessageBox.Show("Please enter laser power!");
                        return;
                    }
                    try
                    {
                        ioboardDynamic.SetLaserPower(Control.Device.LaserColor.Red, Convert.ToSingle(RedLaserPower.Trim()));
                        //ioBoard.SetLaserPower(Control.Device.LaserColor.Red, Convert.ToSingle(RedLaserPower.Trim())); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }
        private ICommand _goOpenGreenLaserViewCommand;
        public ICommand GoOpenGreenLaserViewCommand
        {
            get
            {
                return _goOpenGreenLaserViewCommand ?? (_goOpenGreenLaserViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        bOpenGreenLaser = false;
                        bCloseGreenLaser = true;
                        ioboardDynamic.OpenLaser(Control.Device.LaserColor.Green, true);
                        // ioBoard.OpenLaser(Control.Device.LaserColor.Green, true); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }
        private ICommand _goCloseGreenLaserViewCommand;
        public ICommand GoCloseGreenLaserViewCommand
        {
            get
            {
                return _goCloseGreenLaserViewCommand ?? (_goCloseGreenLaserViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        bCloseGreenLaser = false;
                        bOpenGreenLaser = true;
                        ioboardDynamic.OpenLaser(Control.Device.LaserColor.Green, false);
                        //ioBoard.OpenLaser(Control.Device.LaserColor.Green, false); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }

        }
        private ICommand _goOpenRedLaserViewCommand;
        public ICommand GoOpenRedLaserViewCommand
        {
            get
            {
                return _goOpenRedLaserViewCommand ?? (_goOpenRedLaserViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        bOpenRedLaser = false;
                        bCloseRedLaser = true;
                        ioboardDynamic.OpenLaser(Control.Device.LaserColor.Red, true);
                        //ioBoard.OpenLaser(Control.Device.LaserColor.Red, true); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }
        private ICommand _goCloseRedLaserViewCommand;
        public ICommand GoCloseRedLaserViewCommand
        {
            get
            {
                return _goCloseRedLaserViewCommand ?? (_goCloseRedLaserViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        bCloseRedLaser = false;
                        bOpenRedLaser = true;
                        ioboardDynamic.OpenLaser(Control.Device.LaserColor.Red, false);
                        //ioBoard.OpenLaser(Control.Device.LaserColor.Red, false); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }
        private ICommand _goLaserDisableViewCommand;
        public ICommand GoLaserDisableViewCommand
        {
            get
            {
                return _goLaserDisableViewCommand ?? (_goLaserDisableViewCommand = new RelayCommand(() =>
                {
                    try
                    {
                        bLaserDisable = false;
                        bLaserEnable = true;
                        bSetGreenLaser = false;
                        bSetRedLaser = false;
                        bOnSelectValve = false;
                        bOnSyringPump = false;
                        bOnSMC = false;
                        bOpenGreenLaser = false;
                        bOpenRedLaser = false;

                        ioboardDynamic.OperateEnableSignal(false);
                        //ioBoard.OperateEnableSignal(false); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }

        private DelegateCommand _LockControl;

        public DelegateCommand LockControl
        {
            get
            {
                return _LockControl ?? (_LockControl = new DelegateCommand((para) =>
                {
                    try
                    {
                        if(para != null)
                        {
                            bool isOpen = false;
                            if (para.ToString() == "1") isOpen = true;
                            ioboardDynamic.UnLock(isOpen);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageHelper.ShowMessage(ex.Message);
                    }
                }));
            }
        }


        private DelegateCommand _DevciePowerViewCommand;

        public DelegateCommand DevciePowerViewCommand
        {
            get
            {
                return _DevciePowerViewCommand ?? (_DevciePowerViewCommand = new DelegateCommand((para) =>
                {
                    try
                    {
                        bool isParaValid = false;
                        if(para != null)
                        {
                            string strPara = para.ToString();
                            string[] temp = strPara.Split(',');
                            if(temp != null && temp.Length == 3)
                            {
                                bool isOn = temp[0] == "0";
                                bool isDiaphragmPum = temp[1] == "1";
                                int tempType = Convert.ToInt32(temp[2]);
                                if (_IsV2ChipB) tempType += 1;
                                isParaValid = true;
                                if (!isDiaphragmPum)
                                {
                                    DeviceType type = (DeviceType)tempType;
                                    ioboardDynamic.PowerOnOff(type, isOn);
                                }
                                else
                                {
                                    DiaphragmPumpType type = (DiaphragmPumpType)tempType;
                                    ioboardDynamic.PowerOnOffDiaphragmPump(type, isOn);
                                }

                            }
                        }
                        if (!isParaValid) throw new BGIException("Invalid Parameters");
                    }
                    catch(Exception ex)
                    {
                        MessageHelper.ShowMessage(ex.Message);
                    }
                }));
            }
        }
        
        private ICommand _LEDPowerCommand;
        public ICommand LEDPowerCommand
        {
            get
            {
                return _LEDPowerCommand ?? (_LEDPowerCommand = new RelayCommand(() =>
                {
                    try
                    {
                        if (LEDPowerDes == "PowerOn")
                        {
                            ioboardDynamic.SetLedColor(LEDColorType);
                            //ioboardDynamic.LEDOnOff(true);
                            LEDPowerStatus[LEDColorType] = true;
                            LEDPowerDes = "PowerOff";
                        }
                        else
                        {
                            ioboardDynamic.LEDOnOff(false);
                            LEDPowerStatus[LEDColorType] = false;
                            LEDPowerDes = "PowerOn";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }
        private ICommand _FansSpeedCommand;
        public ICommand FansSpeedCommand
        {
            get
            {
                return _FansSpeedCommand ?? (_FansSpeedCommand = new RelayCommand(() =>
                {
                    try
                    {
                        ioboardDynamic.SetFanSpeed(FansType, FanSpeed);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }
        private ICommand _PumpSetSpeedCommand;
        public ICommand PumpSetSpeedCommand
        {
            get
            {
                return _PumpSetSpeedCommand ?? (_PumpSetSpeedCommand = new RelayCommand(() =>
                {
                    try
                    {
                        ioboardDynamic.SetDiaphragmPumpSpeed(DiaphragmPumpType, DiaphragmPumpSpeed);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }));
            }
        }

        private DelegateCommand _GetVersionCMD;
        public DelegateCommand GetVersionCMD
        {
            get
            {
                return _GetVersionCMD ?? (_GetVersionCMD = new DelegateCommand((param) =>
                 {
                     Version = ioboardDynamic.GetVersion();
                 }));
            }
        }

        private DelegateCommand _GoGetGreenLaserPowerCommand;

        public DelegateCommand GoGetGreenLaserPowerCommand
        {
            get
            {
                return _GoGetGreenLaserPowerCommand ?? (_GoGetGreenLaserPowerCommand = new DelegateCommand((para) =>
                {
                    this.GreenPower = ioboardDynamic.GetLaserPower(LaserColor.Green) / (4096 / 5);
                }));
            }
        }

        private DelegateCommand _GoGetRedLaserPowerCommand;

        public DelegateCommand GoGetRedLaserPowerCommand
        {
            get
            {
                return _GoGetRedLaserPowerCommand ?? (_GoGetRedLaserPowerCommand = new DelegateCommand((para) =>
                {
                    this.RedPower = ioboardDynamic.GetLaserPower(LaserColor.Red) / (4096 / 5); 
                }));
            }
        }

        private ICommand _UpdateFirmwareCmd;

        public ICommand UpdateFirmwareCmd
        {
            get
            {
                return _UpdateFirmwareCmd ?? (_UpdateFirmwareCmd = new RelayCommand(() =>
                {
                    try
                    {
                        UpdateFirmwareWnd wnd = new UpdateFirmwareWnd();
                        wnd.Title = "IO Board Update";
                        wnd.FirmwareName = "IO Board";
                        wnd.CurrentVersion = Version;
                        wnd.BoardControlDynamic = ioboardDynamic;
                        wnd.Width = 650;
                        wnd.Height = 350;
                        wnd.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        wnd.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Update firmeare command error:" + ex.Message);
                    }
                }));
            }
        }

        #endregion
    }
}
