﻿using BGI.Common.Config;
using BGI.Common.Logging;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class MainViewModelBase:ViewModelBase
    {
        protected Logger _Logger;

        public String ModelName { get; private set; }
        public MainViewModelBase(string modelName)
        {
            this.ModelName = modelName;
            _Logger = LogMgr.GetLogger(ModelName);
            ConfigMgr.Get.RegisterLogLevelUpdate(_Logger);
        }
    }
}
