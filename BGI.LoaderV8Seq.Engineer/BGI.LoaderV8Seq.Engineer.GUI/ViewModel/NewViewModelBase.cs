﻿using BGI.LoaderV8Seq.Engineer.GUI.Helper;
using BGI.RPC;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace BGI.LoaderV8Seq.Engineer.GUI.ViewModel
{
    public class NewViewModelBase : ViewModelBase
    {
        private DeviceManager _DeviceManagerEntity;
        public DeviceManager DeviceManagerEntity
        {
            get
            {
                if (SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.Running) || SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.StopPending))
                {
                    MessageBoxResult dr = MessageBox.Show(String.Join(",", SelfCheck.GetISWService.Where(a => a.Status == ServiceControllerStatus.Running).Select(a => a.ServiceName).ToArray()) + " is Running ,do you want to stop them?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (dr == MessageBoxResult.Yes)
                    {
                        SelfCheck.GetISWService.ForEach(a => a.Stop());
                        Thread.Sleep(50000);
                    }
                    else
                    {
                        return null;
                    }
                    var i = 15;
                    while (SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.Running) || SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.StopPending) && i > 0)
                    {
                        Thread.Sleep(2000);
                        i--;
                    }
                    if (SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.Running) || SelfCheck.GetISWService.Exists(a => a.Status == ServiceControllerStatus.StopPending))
                    {
                        MessageBox.Show("Can't stop " + String.Join(",", SelfCheck.GetISWService.Where(a => a.Status == ServiceControllerStatus.Running || a.Status == ServiceControllerStatus.StopPending).Select(a => a.ServiceName).ToArray()), "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return null;
                    }
                }
                return _DeviceManagerEntity;
            }
            set { _DeviceManagerEntity = value; }
        }
       
        public override void Cleanup()
        {
            Messenger.Default.Unregister(this);
            base.Cleanup();
        }
    }
}
