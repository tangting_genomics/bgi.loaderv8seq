﻿using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.Utils;
using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using BGI.LoaderV8Seq.Engineer.GUI.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using UWPF = BGI.Common.UtilWPF;

namespace BGI.LoaderV8Seq.Engineer.GUI
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static App Instance;
        private Logger _Logger = LogMgr.GetLogger("EngineerApp"); // TODO name
        private MainWindow _MainWindow = null;
        protected override void OnStartup(StartupEventArgs e)
        {
            Instance = this;
            base.OnStartup(e);
            Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            SelectRemoteConfigApp inputDialog = new SelectRemoteConfigApp();
            inputDialog.ShowDialog();
            string selectedApp = null;
            SelectRemoteConfigAppViewModel vm = inputDialog.DataContext as SelectRemoteConfigAppViewModel;
            if (vm == null) return;
            if (!string.IsNullOrEmpty(vm.AppName))
            {
                selectedApp = vm.AppName;
                inputDialog = null;
                Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
                ConfigMgr.Get.ApplicationName = selectedApp;

                if (ConfigMgr.Get.Enabled)
                {
                    Stat s = ConfigMgr.Get.Load(true);
                    if (s.IsOK)
                    {
                        _MainWindow = new MainWindow();
                        _MainWindow.Show();
                    }
                    else
                    {
                        UWPF.Dialog.DialogService.ShowMessage("Cann't load Config File of this Instrument!", "Error", true);
                    }
                }
                else
                {
                    UWPF.Dialog.DialogService.ShowMessage("Cann't load Enabled File!", "Error", true);
                }

            }
            else
            {
                inputDialog = null;
                Environment.Exit(0);
            }

        }

        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Unhandled Exception");

            // If the exception is an AMSException then it is known about and is treated as handled.
            if (e.Exception is BGIException)
            {
                e.Handled = true;
            }

            Application.Current.Shutdown(1);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
