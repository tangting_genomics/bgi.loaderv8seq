﻿using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using System.Windows.Controls;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// IOBoard.xaml 的交互逻辑
    /// </summary>
    public partial class V8IOBoard : UserControl
    {
        public V8IOBoard()
        {
            InitializeComponent();
            this.DataContext = new V8IOBoardViewModel();
        }
    }
}
