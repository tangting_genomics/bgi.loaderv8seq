﻿using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Interaction logic for UpdateFirmwareWnd.xaml
    /// </summary>
    public partial class UpdateFirmwareWnd : Window
    {
        public string FirmwareName { get; set; }
        public string CurrentVersion { get; set; }
        public dynamic BoardControlDynamic { get; set; }

        public UpdateFirmwareWnd()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FrimwareUpdateVM vm = this.DataContext as FrimwareUpdateVM;
            if (vm != null)
            {
                vm.FirmwareName = FirmwareName;
                vm.CurrentVersion = CurrentVersion;
                vm.BoardControlDynamic = BoardControlDynamic;
            }
        }
    }
}
