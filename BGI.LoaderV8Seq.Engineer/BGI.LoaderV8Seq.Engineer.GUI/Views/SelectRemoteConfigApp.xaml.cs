﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Interaction logic for SelectRemoteConfigApp.xaml
    /// </summary>
    public partial class SelectRemoteConfigApp : Window
    {
        private SelectRemoteConfigAppViewModel viewModel;
        public SelectRemoteConfigApp()
        {
            InitializeComponent();
            viewModel = new SelectRemoteConfigAppViewModel();
            this.DataContext = viewModel;
            this.btnSelectRCA.Click += btnSelectRCA_Click;
            this.Closing += SelectRemoteConfigApp_Closing;
        }

        private void SelectRemoteConfigApp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void btnSelectRCA_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(this.viewModel.AppName))
                this.Close();
            else
            {
                MessageBox.Show("Please Select Application", "Warning", MessageBoxButton.OK);
                return;
            }
        }
    }
}
