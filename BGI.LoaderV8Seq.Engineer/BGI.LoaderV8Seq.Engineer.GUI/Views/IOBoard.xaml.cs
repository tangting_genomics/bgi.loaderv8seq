﻿using BGI.Common.Config;
using BGI.Common.UtilForms;
using BGI.Common.Utils;
using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using BGI.Scripting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// IOBoard.xaml 的交互逻辑
    /// </summary>
    public partial class IOBoard : UserControl
    {
        public IOBoard()
        {
            InitializeComponent();
            this.DataContext = new IOBoardViewModel("IOBoardView");
        }
    }
}
