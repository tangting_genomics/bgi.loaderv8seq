﻿using BGI.Common.Config;
using BGI.Common.UtilForms;
using BGI.Common.Utils;
using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using BGI.Scripting;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Script.xaml 的交互逻辑
    /// </summary>
    public partial class Script : Window, INotifyPropertyChanged, IEngineerOperations
    {
        public IScript _ScriptBase = null;
        public Dictionary<string, object> _DeviceRawObjectD = new Dictionary<string, object>();

        public Dictionary<string, object> DeviceRawObjectD
        {
            get
            {
                return _DeviceRawObjectD;
            }
            set
            {
                _DeviceRawObjectD = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                  new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool IsLogAutoScroll { get; set; } = true;

        private string ScriptHomePath
        {
            get
            {
                string retVal = @"C:\BGI\Config\";
                if (ConfigMgr.Get.TryGetCurValue<string>("System", "ScriptHomePath", ref retVal))
                {
                    if (!retVal.EndsWith("\\"))
                    {
                        throw new BGIException("ScriptHomePath value must end in backslash to be valid path.");
                    }
                }
                return retVal;
            }
            set
            {
                // First of all, add the backslash suffix.
                if (!value.EndsWith("\\"))
                {
                    value = String.Format("{0}\\", value);
                }

                // get current value and check if changed
                string curVal = "";
                if (ConfigMgr.Get.TryGetCurValue<string>("System", "ScriptHomePath", ref curVal))
                {

                    // If the value has changed
                    if (!curVal.Equals(value, StringComparison.CurrentCultureIgnoreCase))
                    {
                        ConfigMgr.Get.SetCurValue("System", "ScriptHomePath", value, "User changed Script path while using application.");
                        ConfigMgr.Get.SaveParameter("System", "ScriptHomePath");
                    }
                }
            }
        }
        
        public Script()
        {
            InitializeComponent();
        }
        public Script(IScript script)
        {
            InitializeComponent();
            _ScriptBase = script;
            _ScriptBase.OnStatusMessageTrace -= this.MessageEventHandler;
            _ScriptBase.OnStatusMessageTrace += this.MessageEventHandler;

            _ScriptBase.OnScriptStateChanged -= this.ScriptStatusChanged;
            _ScriptBase.OnScriptStateChanged += this.ScriptStatusChanged;
        }

        private void Button_BrowseClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".py"; // Default file extension 
            dlg.Filter = "Text documents (.py)|*.py"; // Filter files by extension
            dlg.InitialDirectory = System.IO.Path.GetFullPath(ScriptHomePath);    // using GetFullPath is to avoid crashes when using relatives path

            // Show open file dialog box 
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Set the ScriptHomePath to new value if changed.
                FileInfo fi = new FileInfo(dlg.FileName);
                ScriptHomePath = fi.Directory.FullName;

                // Open script 
                tb_ScriptFile.Text = dlg.FileName;
                if (_ScriptBase != null)
                {
                    _ScriptBase.ScriptFileName = dlg.FileName;
                }
                //Reset script object if change file name since we only deal with one script at the time( BIO, Img, wflow)
                _ScriptBase.ScriptFileName = tb_ScriptFile.Text;

                _ScriptBase.OnStatusMessageTrace -= this.MessageEventHandler;
                _ScriptBase.OnScriptStateChanged -= this.ScriptStatusChanged;

                _ScriptBase.OnStatusMessageTrace += this.MessageEventHandler;
                _ScriptBase.OnScriptStateChanged += this.ScriptStatusChanged;
                //InitScriptObject();
            }
        }

        //private void InitScriptObject()
        //{

        //    if (tb_ScriptFile.Text == null)
        //        return;
        //    //_ScriptBase = null;

        //    //_ScriptBase = new EngineerOperations(this);

        //    //_ScriptBase.OnStatusMessageTrace += this.MessageEventHandler;
        //    //_ScriptBase.OnScriptStateChanged += this.ScriptStatusChanged;
            
        //}

        private void ScriptStatusChanged(object sender, EventArgs args)
        {
            switch (_ScriptBase.State)
            {
                case ScriptState.None:
                case ScriptState.Idle:
                    Dispatcher.Invoke(() =>
                    {
                        btnScriptRun.IsEnabled = true;
                        btnScriptStop.IsEnabled = false;
                        btnScriptPause.IsEnabled = false;
                        btnScriptResume.IsEnabled = false;
                        btnScriptBrowse.IsEnabled = true;
                        //btnScriptAbort.IsEnabled = false;
                    });
                    break;
                case ScriptState.Running:
                    Dispatcher.Invoke(() =>
                    {
                        btnScriptRun.IsEnabled = false;
                        btnScriptStop.IsEnabled = true;
                        btnScriptPause.IsEnabled = true;
                        btnScriptResume.IsEnabled = false;
                        btnScriptBrowse.IsEnabled = false;
                        //btnScriptAbort.IsEnabled = true;
                    });
                    break;
                case ScriptState.Stopping:
                    Dispatcher.Invoke(() =>
                    {
                        btnScriptRun.IsEnabled = false;
                        btnScriptStop.IsEnabled = false;
                        btnScriptPause.IsEnabled = false;
                        btnScriptResume.IsEnabled = false;
                        btnScriptBrowse.IsEnabled = false;
                        // btnScriptAbort.IsEnabled = true;
                    });
                    break;
                case ScriptState.Pausing:
                case ScriptState.Paused:
                    Dispatcher.Invoke(() =>
                    {
                        btnScriptRun.IsEnabled = false;
                        btnScriptStop.IsEnabled = false;
                        btnScriptPause.IsEnabled = false;
                        btnScriptResume.IsEnabled = true;
                        btnScriptBrowse.IsEnabled = false;
                        //btnScriptAbort.IsEnabled = true;
                    });
                    break;
            }
        }

        private void MessageEventHandler(object sender, StatusEventArgs agrs)
        {
            Dispatcher.Invoke(() =>
            {
                int itemNr = listViewMessage.Items.Add(agrs);

                if (IsLogAutoScroll)
                    listViewMessage.ScrollIntoView(listViewMessage.Items[itemNr]);
            });
        }


        private void Button_Click_Run(object sender, RoutedEventArgs e)
        {
            listViewMessage.Items.Clear();

            #region use task to run the script

            Task.Run(() =>
            {
                Thread.CurrentThread.Name = "PythonScript";
                try
                {
                    Dispatcher.Invoke(() =>
                    {
                        _ScriptBase.ScriptFileName = tb_ScriptFile.Text; ;
                    });
                    _ScriptBase.InitScriptFile(_ScriptBase.ScriptFileName);
                    _ScriptBase.Run();
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null) ex = ex.InnerException;

                    if ((ex is ScriptAbortException) == false)
                        BGIMessageBox.Error(ex.Message, "Script Error");
                }
                finally
                {
                    _ScriptBase.Reset();
                }
            });

            #endregion
        }

        private void Button_Click_Stop(object sender, RoutedEventArgs e)
        {
            try
            {
                _ScriptBase.RequestStop();
            }
            catch (Exception ex)
            {
                BGIMessageBox.Error(ex, "Script Error");
            }
        }

        private void Button_Click_Pause(object sender, RoutedEventArgs e)
        {
            try
            {
                _ScriptBase.RequestPause();
            }
            catch (Exception ex)
            {
                BGIMessageBox.Error(ex, "Script Error");
            }
        }

        private void Button_Click_Resume(object sender, RoutedEventArgs e)
        {
            try
            {
                _ScriptBase.Resume();
            }
            catch (Exception ex)
            {
                BGIMessageBox.Error(ex, "Script Error");
            }
        }

        private void btnClearMsg_Click(object sender, RoutedEventArgs e)
        {
            listViewMessage.Items.Clear();
        }

        public IReadOnlyDictionary<string, object> EnumeratePluginHardware()
        {
            return _DeviceRawObjectD;
        }

        public object GetDeviceObject(string objName)
        {
            if (EnumeratePluginHardware().ContainsKey(objName))
            {
                return EnumeratePluginHardware()[objName];
            }
            return null;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        
        }
    }
}
