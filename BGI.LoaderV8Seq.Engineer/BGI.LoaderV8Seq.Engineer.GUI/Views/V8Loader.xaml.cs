﻿using BGI.Common.Config;
using BGI.Common.Logging;
using BGI.Common.Utils;
using BGI.Common.UtilWPF;
using BGI.Control.Device.Physical;
using BGI.Scripting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using BGI.Common.UtilWPF.Helpers;
using System.Windows.Threading;
using Microsoft.Research.DynamicDataDisplay;
using System.Windows.Media;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using System.Runtime.Serialization;
using PropertyChanged;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// V8Loader.xaml 的交互逻辑
    /// </summary>
    public partial class V8Loader : Window, IEngineerOperations
    {
        #region Constructor 

        public V8Loader()
        {
            InitializeComponent();
            this.Init();
        }

        private void Init()
        {
            ConfigMgr.Get.RegisterLogLevelUpdate(logger);
            this.DeviceInit();
            this.InitAsync();
            this.OperationInit();
            this.ConfigInit();
        }
        private void InitAsync()
        {
            try
            {
                CanInitialize = false;

                StatusMsg = "Adding Device Manager Device";
                _DeviceRawObjectD.AddIfMissing("V8Loader", this);

                CanOpen = true;
                StatusMsg = "Initialize Complete";
            }
            catch (Exception ex)
            {
                CanInitialize = true;
                StatusMsg = "Error";
                logger.Log("Error while initializing", ex);
                BGIMessageBox.Error(ex.Message, "Error while initializing", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }


        private void ConfigInit()
        {
            MotorSpeeds = new int[] { 10, 15, 20, 30, 40, 50, 60, 70, 75 };
            MotorList.Add(new DeviceOperaton(1, "压紧电机1", false));
            MotorList.Add(new DeviceOperaton(2, "压紧电机2", false));
            MotorList.Add(new DeviceOperaton(3, "压紧电机3", false));
            MotorList.Add(new DeviceOperaton(4, "压紧电机4", false));
            MotorList.Add(new DeviceOperaton(5, "压紧电机5", false));
            MotorList.Add(new DeviceOperaton(6, "吸液电机A", false));
            MotorList.Add(new DeviceOperaton(7, "吸液电机B", false));

            PumpSpeeds = new int[] { 20, 50, 100, 200 };
            PumpPoss = new string[] { "NO", "NC" };
            PumpList.Add(new DeviceOperaton(1, "注射泵1", false));
            PumpList.Add(new DeviceOperaton(2, "注射泵2", false));
            PumpList.Add(new DeviceOperaton(3, "注射泵3", false));
            PumpList.Add(new DeviceOperaton(4, "注射泵4", false));
            PumpList.Add(new DeviceOperaton(5, "注射泵5", false));

            RotaryValvePos = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            RotaryValveList.Add(new DeviceOperaton(1, "旋转阀1", false));
            RotaryValveList.Add(new DeviceOperaton(2, "旋转阀2", false));
            RotaryValveList.Add(new DeviceOperaton(3, "旋转阀3", false));
            RotaryValveList.Add(new DeviceOperaton(4, "旋转阀4", false));
            RotaryValveList.Add(new DeviceOperaton(5, "旋转阀5", false));

            int a = 1;
            for (int i = 1; i < 26; i++)
            {
                SolenoidValveList.Add(new DeviceOperaton(i, i.ToString(), false));
                if (i % 5 == 0)
                {
                    var num = 25 + a;
                    SolenoidValveList.Add(new DeviceOperaton(num, num.ToString(), false));
                    a = a + 1;
                }
            }
        }

        protected void DeviceInit()
        {
            this.DeviceStatsDic.Add(new DeviceStats("LoaderIOBoardV8", "LoaderIOBoard", "LoaderIO控制板"), IOBoardV8);
            this.DeviceStatsDic.Add(new DeviceStats("SyringePump1", "SyringePump", "注射泵"), Pumpdevice);
            this.DeviceStatsDic.Add(new DeviceStats("RotaryValve1", "RotaryValve", "旋转阀1"), RotaryValve1);
            this.DeviceStatsDic.Add(new DeviceStats("RotaryValve2", "RotaryValve", "旋转阀2"), RotaryValve2);
            this.DeviceStatsDic.Add(new DeviceStats("RotaryValve3", "RotaryValve", "旋转阀3"), RotaryValve3);
            this.DeviceStatsDic.Add(new DeviceStats("RotaryValve4", "RotaryValve", "旋转阀4"), RotaryValve4);
            this.DeviceStatsDic.Add(new DeviceStats("RotaryValve5", "RotaryValve", "旋转阀5"), RotaryValve5);
            this.DeviceStatsDic.Add(new DeviceStats("TemperatureBoardA", "TemperatureBoard", "温控板A"), TemperatureBoardA);
            this.DeviceStatsDic.Add(new DeviceStats("TemperatureBoardB", "TemperatureBoard", "温控板B"), TemperatureBoardB);
            this.DeviceStatsDic.Add(new DeviceStats("TemperatureBoardC", "TemperatureBoard", "温控板C"), TemperatureBoardC);

        }

        private void OperationInit()
        {
            List<OperationTunnel> tunnels = new List<OperationTunnel>();
            tunnels.Add(new OperationTunnel { Name = "通道1", Id = 1 });
            tunnels.Add(new OperationTunnel { Name = "通道2", Id = 2 });
            tunnels.Add(new OperationTunnel { Name = "通道3", Id = 3 });
            tunnels.Add(new OperationTunnel { Name = "通道4", Id = 4 });
            tunnels.Add(new OperationTunnel { Name = "通道5", Id = 5 });


            OperationList.Add(LOADINGWASH, new Operation(LOADINGWASH)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
                CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            });

            OperationList.Add(PERFUSION, new Operation(PERFUSION)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
                CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            });
            OperationList.Add(LOADING, new Operation(LOADING)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
                CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            });
            OperationList.Add(MDAWash, new Operation(MDAWash)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
                CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            });
            //OperationList.Add(MDAORBARCODEPERFUSION,new Operation(MDAORBARCODEPERFUSION) {
            //    Tunnels = Copy2(tunnels.ToArray()),
            //    ScriptVM = vm1,
            //    ScriptName = Properties.Settings.Default.MDAOrBarcodePerfusion,
            //    CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
            //    CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            //});
            OperationList.Add(MDAPOSTLOADING, new Operation(MDAPOSTLOADING)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventSaveScript)
            });
            OperationList.Add(BARCODEPOSTLOADING, new Operation(BARCODEPOSTLOADING)
            {
                Tunnels = Copy2(tunnels.ToArray()),
                CommandFileBrowse = new DelegateCommand2<object>(EventFileBrowse),
                CommandSaveScript = new DelegateCommand2<object>(EventSaveScript)
            });

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DeviceStatsDic.Values.OfType<SerialDevice>().ToList<SerialDevice>().ForEach(temp => temp.Disconnect());
        }

        #endregion

        #region Field 


        private Logger logger = LogMgr.GetLogger("V8LoaderUI", LogSeverityEnum.Debug);
        
        private TemperatureDevice TemperatureBoardA = new TemperatureDevice() { BaudRate = 115200, PortName = "COM1" };
        private TemperatureDevice TemperatureBoardB = new TemperatureDevice() { BaudRate = 115200, PortName = "COM2" };
        private TemperatureDevice TemperatureBoardC = new TemperatureDevice() { BaudRate = 115200, PortName = "COM3" };
        private LoaderBoardV8Device IOBoardV8 = new LoaderBoardV8Device() { BaudRate = 115200, PortName = "COM4" };
        private PumpDevice Pumpdevice = new PumpDevice() { BaudRate = 9600, PortName = "COM5" };


        private SelectorValveDevice RotaryValve1 = new SelectorValveDevice() { BaudRate = 19200, PortName = "COM6" };
        private SelectorValveDevice RotaryValve2 = new SelectorValveDevice() { BaudRate = 19200, PortName = "COM7" };
        private SelectorValveDevice RotaryValve3 = new SelectorValveDevice() { BaudRate = 19200, PortName = "COM8" };
        private SelectorValveDevice RotaryValve4 = new SelectorValveDevice() { BaudRate = 19200, PortName = "COM9" };
        private SelectorValveDevice RotaryValve5 = new SelectorValveDevice() { BaudRate = 19200, PortName = "COM10" };

        private VIdexUartValve SelectorValve { get; set; }

        private ScriptBase _ScriptBase = null;

        private readonly Dictionary<string, object> _DeviceRawObjectD = new Dictionary<string, object>();
        private const string LOADINGWASH = "LoadingWash";
        private const string PERFUSION = "Perfusion";
        private const string LOADING = "Loading";
        private const string MDAWash = "MDAWash";
        //private const string MDAORBARCODEPERFUSION = "MDAOrBarcodePerfusion";
        private const string MDAPOSTLOADING = "MDAPostLoading";
        private const string BARCODEPOSTLOADING = "BarcodePostLoading";



        #endregion

        #region Property

        public Dictionary<DeviceStats, SerialParameters> DeviceStatsDic { get; protected set; } = new Dictionary<DeviceStats, SerialParameters>();

        public Dictionary<string, Operation> OperationList { get; set; } = new Dictionary<string, Operation>();

        #region devices

        public ObservableCollection<DeviceOperaton> MotorList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> PumpList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> RotaryValveList { get; set; } = new ObservableCollection<DeviceOperaton>();
        public ObservableCollection<DeviceOperaton> SolenoidValveList { get; set; } = new ObservableCollection<DeviceOperaton>();

        #endregion

        #region Config
        public int[] MotorSpeeds { get; set; }
        public string MotorDistances { get; set; }
        public int[] PumpSpeeds { get; set; }
        public string[] PumpPoss { get; set; }
        public string PumpPos { get; set; }
        public string PumpVolume { get; set; }
        public int[] RotaryValvePos { get; set; }
        #endregion
        public bool CanOpen { get; set; }

        public bool CanInitialize { get; set; }

        public string StatusMsg { get; set; }


        #endregion

        #region ICommand       
        public ICommand CommandConnect { get { return new DelegateCommand2(EventConnect); } }
        public ICommand CommandInit { get { return new DelegateCommand2(EventInit); } }

        public ICommand CommandClose { get { return new DelegateCommand2(EventClose); } }
        public ICommand CommandMotorUp { get { return new DelegateCommand2<int?>(EventMotorUp); } }

        public ICommand CommandMotorDown { get { return new DelegateCommand2<int?>(EventMotorDown); } }
        public ICommand CommandAspirate { get { return new DelegateCommand2<int?>(EventAspirate); } }
        public ICommand CommandDispense { get { return new DelegateCommand2<int?>(EventDispense); } }

        public ICommand CommandRotary { get { return new DelegateCommand2<int?>(EventRotary); } }

        public ICommand CommandOpenSolenoidValve { get { return new DelegateCommand2<int?>(OpenSolenoidValve); } }

        public ICommand CommandOffSolenoidValve { get { return new DelegateCommand2<int?>(OffSolenoidValve); } }

        public ICommand CommandFileBrowse { get { return new DelegateCommand2<object>(EventFileBrowse); } }

        public ICommand CommandTunnelSelected { get { return new DelegateCommand2<object>(EventTunnelSelected); } }

        #endregion

        #region Event
        private void EventInit()
        {
            try
            {
                Task.Factory.StartNew(delegate ()
                {
                    IOBoardV8.Device.Home();
                    for (int i = 1; i < 6; i++)
                    {
                        Pumpdevice.Device.Home(i);
                    }

                    RotaryValve1.Device.Home();
                    RotaryValve2.Device.Home();
                    RotaryValve3.Device.Home();
                    RotaryValve4.Device.Home();
                    RotaryValve5.Device.Home();


                    TemperatureBoardA.Device.Initialize();
                    TemperatureBoardB.Device.Initialize();
                    TemperatureBoardC.Device.Initialize();
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new Action(() => { MessageBox.Show("初始化所有设备"); }));
                });

            }
            catch (Exception ex)
            {
                logger.Log("初始化所有设备", ex);
                MessageBox.Show("设备初始化失败！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EventConnect()
        {
            try
            {
                IOBoardV8.Connect();
                IOBoardV8.Device.SyringePump(true);
                IOBoardV8.Device.RotaryValve(true);
                Pumpdevice.Connect();
                RotaryValve1.Connect();
                RotaryValve2.Connect();
                RotaryValve3.Connect();
                RotaryValve4.Connect();
                RotaryValve5.Connect();
                TemperatureBoardA.Connect();
                TemperatureBoardB.Connect();
                TemperatureBoardC.Connect();

                MessageBox.Show("打开所有设备");
            }
            catch (Exception ex)
            {
                logger.Log("连接所有设备", ex);
                MessageBox.Show("设备连接失败！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EventClose()
        {
            try
            {
                IOBoardV8.Disconnect();
                Pumpdevice.Disconnect();
                RotaryValve1.Disconnect();
                RotaryValve2.Disconnect();
                RotaryValve3.Disconnect();
                RotaryValve4.Disconnect();
                RotaryValve5.Disconnect();

                TemperatureBoardA.Disconnect();
                TemperatureBoardB.Disconnect();
                TemperatureBoardC.Disconnect();
                MessageBox.Show("关闭所有设备");
            }
            catch (Exception ex)
            {
                logger.Log("关闭所有设备", ex);
                MessageBox.Show("关闭所有设备失败！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EventMotorUp(int? para)
        {
            try
            {
                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var motors = MotorList.Where(x => x.Selected);

                if (motors == null) { MessageBox.Show("请先选择需要移动的电机"); return; }
                int actual = 0;
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in motors)
                    {
                        switch (item.Id)
                        {
                            case (int)MotorCatogory.MotorA:
                                IOBoardV8.Device.MoveSuctionMotorAToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.MotorB:
                                IOBoardV8.Device.MoveSuctionMotorBToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor1:
                                IOBoardV8.Device.MoveCapMotor1stToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor2:
                                IOBoardV8.Device.MoveCapMotor2ndToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor3:
                                IOBoardV8.Device.MoveCapMotor3thToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor4:
                                IOBoardV8.Device.MoveCapMotor4thToUpper((byte)para, 0, ref actual);
                                break;
                            case (int)MotorCatogory.Motor5:
                                IOBoardV8.Device.MoveCapMotor5thToUpper((byte)para, 0, ref actual);
                                break;
                            default:
                                break;
                        }

                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("电机向上移动", ex);
                MessageBox.Show("电机向上移动错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EventMotorDown(int? para)
        {
            try
            {
                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var motors = MotorList.Where(x => x.Selected);

                if (motors == null) { MessageBox.Show("请先选择需要移动的电机"); return; }
                int distance;
                if (!int.TryParse(MotorDistances, out distance))
                { MessageBox.Show("请输入正确的距离"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    int actual = 0;
                    foreach (var item in motors)
                    {
                        switch (item.Id)
                        {
                            case (int)MotorCatogory.MotorA:
                                IOBoardV8.Device.MoveSuctionMotorAToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.MotorB:
                                IOBoardV8.Device.MoveSuctionMotorBToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor1:
                                IOBoardV8.Device.MoveCapMotor1stToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor2:
                                IOBoardV8.Device.MoveCapMotor2ndToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor3:
                                IOBoardV8.Device.MoveCapMotor3thToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor4:
                                IOBoardV8.Device.MoveCapMotor4thToLower((byte)para, distance, ref actual);
                                break;
                            case (int)MotorCatogory.Motor5:
                                IOBoardV8.Device.MoveCapMotor5thToLower((byte)para, distance, ref actual);
                                break;
                            default:
                                break;
                        }

                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("电机向下移动", ex);
                MessageBox.Show("电机向下移动错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EventAspirate(int? para)
        {
            try
            {

                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var pumps = PumpList.Where(x => x.Selected);

                if (pumps == null) { MessageBox.Show("请先选择需要操作的注射泵"); return; }

                var pos = PumpPos == "NO" ? 0 : 1;
                double volume;
                if (!double.TryParse(PumpVolume, out volume))
                { MessageBox.Show("请输入正确的吸液量"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in pumps)
                    {
                        Pumpdevice.Device.SwitchValve(item.Id, pos);
                        Pumpdevice.Device.Aspirate(item.Id, volume, (double)para);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("注射泵吸液", ex);
                MessageBox.Show("注射泵吸液错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EventDispense(int? para)
        {
            try
            {

                if (para == null) { MessageBox.Show("请先选择移动速度"); return; }
                var pumps = PumpList.Where(x => x.Selected);

                if (pumps == null) { MessageBox.Show("请先选择需要操作的注射泵"); return; }

                var pos = PumpPos == "NO" ? 0 : 1;
                double volume;
                if (!double.TryParse(PumpVolume, out volume))
                { MessageBox.Show("请输入正确的吸液量"); return; }
                Task.Factory.StartNew(delegate ()
                {
                    foreach (var item in pumps)
                    {
                        Pumpdevice.Device.SwitchValve(item.Id, pos);
                        Pumpdevice.Device.Dispense(item.Id, volume, (double)para);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Log("注射泵排液", ex);
                MessageBox.Show("注射泵排液错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void EventRotary(int? para)
        {
            try
            {

                if (para == null) MessageBox.Show("请先选择阀旋转位置");
                var valves = RotaryValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的旋转阀"); return; }

                foreach (var item in valves)
                {
                    ((SelectorValveDevice)GetDevice("RotaryValve" + item.Id.ToString())).Device.GotoPosition(para.Value);
                }
            }
            catch (Exception ex)
            {
                logger.Log("旋转阀旋转位置", ex);
                MessageBox.Show("旋转阀旋转位置发生错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void OffSolenoidValve(int? para)
        {
            try
            {
                var valves = SolenoidValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的电磁阀"); return; }

                foreach (var item in valves)
                {
                    IOBoardV8.Device.SolenoidValve((byte)item.Id, false);
                }
            }
            catch (Exception ex)
            {
                logger.Log("关闭电磁阀", ex);
                MessageBox.Show("关闭电磁阀错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void OpenSolenoidValve(int? para)
        {
            try
            {
                var valves = SolenoidValveList.Where(x => x.Selected);

                if (valves == null) { MessageBox.Show("请先选择需要操作的电磁阀"); return; }

                foreach (var item in valves)
                {
                    IOBoardV8.Device.SolenoidValve((byte)item.Id, true);
                }
            }
            catch (Exception ex)
            {
                logger.Log("打开电磁阀", ex);
                MessageBox.Show("打开电磁阀错误！", "温馨提示", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EventFileBrowse(object obj)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ".py"; // Default file name 
            dlg.DefaultExt = ".py";
            dlg.Filter = "Text documents (.py)|*.py"; // Filter files by extension

            //Retrieve Home Path from RemoteConfig
            string retVal = "";
            if (ConfigMgr.Get.TryGetCurValue<string>("System", "ScriptHomePath", ref retVal))
            {
                if (!retVal.EndsWith("\\"))
                {
                    throw new BGIException("ScriptHomePath value must end in backslash to be valid path.");
                }
            }

            dlg.InitialDirectory = System.IO.Path.GetFullPath(retVal);
            // Show open file dialog box 
            Nullable<bool> result = dlg.ShowDialog();
            // Process open file dialog box results 
            if (result == true)
            {
                var operation = OperationList.FirstOrDefault(x => (x.Key == obj.ToString()));
                operation.Value.ScriptName = dlg.FileName;


            }
        }
        private void EventSaveScript(object obj)
        {

         
        }

        private void EventTunnelSelected(object obj)
        {

            var currentoperation = OperationList[obj.ToString()];//.First(x => x.OperationName == obj.ToString());

            if (currentoperation != null)
            {
                foreach (var tunnel in currentoperation.Tunnels)
                {

                    foreach (var operation in OperationList)
                    {
                        if (!operation.Value.Equals(currentoperation))
                        {
                            if (tunnel.Selected == true)
                            {
                                operation.Value.Tunnels[tunnel.Id - 1].Enabled = false;
                                operation.Value.Tunnels[tunnel.Id - 1].Selected = false;
                            }
                            else
                            {
                                operation.Value.Tunnels[tunnel.Id - 1].Enabled = true;
                            }
                        }
                    }
                }

                #region 废弃代码

                //switch (currentoperation.OperationName)
                //{
                //    case LOADINGWASH:
                //    case PERFUSION:
                //    case LOADING:

                //        //将其他操作的关联通道（主要是AB电机）设置为相反的enable
                //        var currenttunnels = currentoperation.Tunnels.Where(x => x.Selected && x.Id < 4);

                //        if (currenttunnels != null && currenttunnels.Count() > 0)
                //        {
                //            OperationList[MDAORBARCODEWASH].Tunnels[0].Enabled = false;
                //            OperationList[MDAORBARCODEWASH].Tunnels[1].Enabled = false;
                //            OperationList[MDAORBARCODEWASH].Tunnels[2].Enabled = false;

                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[0].Enabled = false;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[1].Enabled = false;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[2].Enabled = false;

                //            OperationList[MDALOAD].Tunnels[0].Enabled = false;
                //            OperationList[MDALOAD].Tunnels[1].Enabled = false;
                //            OperationList[MDALOAD].Tunnels[2].Enabled = false;

                //            OperationList[BARCODELOAD].Tunnels[0].Enabled = false;
                //            OperationList[BARCODELOAD].Tunnels[1].Enabled = false;
                //            OperationList[BARCODELOAD].Tunnels[2].Enabled = false;

                //        }
                //        else
                //        {
                //            OperationList[MDAORBARCODEWASH].Tunnels[0].Enabled = true;
                //            OperationList[MDAORBARCODEWASH].Tunnels[1].Enabled = true;
                //            OperationList[MDAORBARCODEWASH].Tunnels[2].Enabled = true;

                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[0].Enabled = true;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[1].Enabled = true;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[2].Enabled = true;

                //            OperationList[MDALOAD].Tunnels[0].Enabled = true;
                //            OperationList[MDALOAD].Tunnels[1].Enabled = true;
                //            OperationList[MDALOAD].Tunnels[2].Enabled = true;

                //            OperationList[BARCODELOAD].Tunnels[0].Enabled = true;
                //            OperationList[BARCODELOAD].Tunnels[1].Enabled = true;
                //            OperationList[BARCODELOAD].Tunnels[2].Enabled = true;
                //        }

                //        currenttunnels = currentoperation.Tunnels.Where(x => x.Selected && x.Id >= 4);

                //        if (currenttunnels != null && currenttunnels.Count() > 0)
                //        {
                //            OperationList[MDAORBARCODEWASH].Tunnels[3].Enabled = false;
                //            OperationList[MDAORBARCODEWASH].Tunnels[4].Enabled = false;

                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[3].Enabled = false;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[4].Enabled = false;

                //            OperationList[MDALOAD].Tunnels[3].Enabled = false;
                //            OperationList[MDALOAD].Tunnels[4].Enabled = false;

                //            OperationList[BARCODELOAD].Tunnels[3].Enabled = false;
                //            OperationList[BARCODELOAD].Tunnels[4].Enabled = false;
                //        }
                //        else
                //        {
                //            OperationList[MDAORBARCODEWASH].Tunnels[3].Enabled = true;
                //            OperationList[MDAORBARCODEWASH].Tunnels[4].Enabled = true;

                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[3].Enabled = true;
                //            OperationList[MDAORBARCODEPERFUSION].Tunnels[4].Enabled = true;

                //            OperationList[MDALOAD].Tunnels[3].Enabled = true;
                //            OperationList[MDALOAD].Tunnels[4].Enabled = true;

                //            OperationList[BARCODELOAD].Tunnels[3].Enabled = true;
                //            OperationList[BARCODELOAD].Tunnels[4].Enabled = true;
                //        }



                //        break;
                //    case MDAORBARCODEWASH:
                //    case MDAORBARCODEPERFUSION:
                //    case MDALOAD:
                //    case BARCODELOAD:
                //        currenttunnels = currentoperation.Tunnels.Where(x => x.Selected && x.Id < 4);

                //        if (currenttunnels != null && currenttunnels.Count() > 0)
                //        {
                //            OperationList[LOADINGWASH].Tunnels[0].Enabled = false;
                //            OperationList[LOADINGWASH].Tunnels[1].Enabled = false;
                //            OperationList[LOADINGWASH].Tunnels[2].Enabled = false;

                //            OperationList[PERFUSION].Tunnels[0].Enabled = false;
                //            OperationList[PERFUSION].Tunnels[1].Enabled = false;
                //            OperationList[PERFUSION].Tunnels[2].Enabled = false;

                //            OperationList[LOADING].Tunnels[0].Enabled = false;
                //            OperationList[LOADING].Tunnels[1].Enabled = false;
                //            OperationList[LOADING].Tunnels[2].Enabled = false;
                //        }
                //        else
                //        {
                //            OperationList[LOADINGWASH].Tunnels[0].Enabled = true;
                //            OperationList[LOADINGWASH].Tunnels[1].Enabled = true;
                //            OperationList[LOADINGWASH].Tunnels[2].Enabled = true;

                //            OperationList[PERFUSION].Tunnels[0].Enabled = true;
                //            OperationList[PERFUSION].Tunnels[1].Enabled = true;
                //            OperationList[PERFUSION].Tunnels[2].Enabled = true;

                //            OperationList[LOADING].Tunnels[0].Enabled = true;
                //            OperationList[LOADING].Tunnels[1].Enabled = true;
                //            OperationList[LOADING].Tunnels[2].Enabled = true;
                //        }

                //        currenttunnels = currentoperation.Tunnels.Where(x => x.Selected && x.Id >= 4);

                //        if (currenttunnels != null && currenttunnels.Count() > 0)
                //        {
                //            OperationList[LOADINGWASH].Tunnels[3].Enabled = false;
                //            OperationList[LOADINGWASH].Tunnels[4].Enabled = false;

                //            OperationList[PERFUSION].Tunnels[3].Enabled = false;
                //            OperationList[PERFUSION].Tunnels[4].Enabled = false;

                //            OperationList[LOADING].Tunnels[3].Enabled = false;
                //            OperationList[LOADING].Tunnels[4].Enabled = false;
                //        }
                //        else
                //        {
                //            OperationList[LOADINGWASH].Tunnels[3].Enabled = true;
                //            OperationList[LOADINGWASH].Tunnels[4].Enabled = true;

                //            OperationList[PERFUSION].Tunnels[3].Enabled = true;
                //            OperationList[PERFUSION].Tunnels[4].Enabled = true;

                //            OperationList[LOADING].Tunnels[3].Enabled = true;
                //            OperationList[LOADING].Tunnels[4].Enabled = true;
                //        }
                //        break;
                //    default:
                //        break;
                //}

                #endregion
            }
        }
        /// <summary>
        /// 根据选择的通道以及方式来修改其他的通道的可选择性
        /// </summary>
        /// <param name="sourceTunnel"></param>
        /// <param name="TargetOperation"></param>
        /// <returns></returns>
        private void SetTunnelsEnable(bool result, string direction)
        {
            if (direction == "A" && result)
            {
                OperationList[LOADINGWASH].Tunnels[0].Enabled = true;
                OperationList[LOADINGWASH].Tunnels[1].Enabled = true;
                OperationList[LOADINGWASH].Tunnels[2].Enabled = true;

                OperationList[PERFUSION].Tunnels[0].Enabled = true;
                OperationList[PERFUSION].Tunnels[1].Enabled = true;
                OperationList[PERFUSION].Tunnels[2].Enabled = true;

                OperationList[LOADING].Tunnels[0].Enabled = true;
                OperationList[LOADING].Tunnels[1].Enabled = true;
                OperationList[LOADING].Tunnels[2].Enabled = true;
            }
        }
        #endregion

        #region Method

        #region private Method

        public OperationTunnel[] Copy2(OperationTunnel[] tunnel)
        {
            if (tunnel == null) return null;
            var result = new List<OperationTunnel>();
            foreach (var item in tunnel)
            {
                result.Add(item.Clone() as OperationTunnel);
            }
            return result.ToArray();
        }

        #endregion

        #region public Method

        public IReadOnlyDictionary<string, object> EnumeratePluginHardware()
        {
            return _DeviceRawObjectD;
        }

        public object GetDeviceObject(string objName)
        {
            if (EnumeratePluginHardware().ContainsKey(objName))
            {
                return EnumeratePluginHardware()[objName];
            }
            logger.Log()(Common.Logging.LogSeverityEnum.Error, "Caller asked for Device object for unknown device string ({0}).  Returning null.", objName);
            return null;
        }

        public SerialDevice GetDevice(string deviceName)
        {
            var key = this.DeviceStatsDic.Keys.FirstOrDefault(temp => temp.DeviceID == deviceName);
            if (key != null)
            {
                return this.DeviceStatsDic[key] as SerialDevice;
            }
            return null;
        }

        public Operation GetOperation(string name)
        {
            var operation = this.OperationList[name];//.FirstOrDefault(temp => temp.OperationName == name);
            if (operation != null)
            {
                return operation;
            }
            return null;
        }

        public void UpdateStatus(string operationname, string message)
        {
            if (OperationList.Keys.Contains(operationname))
            {
                this.OperationList[operationname].CurrentAction = message;
            }
        }
        #endregion

        #endregion

    }


    #region Serial Class

    [AddINotifyPropertyChangedInterface]
    public class DeviceStats
    {
        public string DeviceID { get; protected set; }
        public string DeviceGroup { get; set; }
        public string Content { get; set; }

        public DeviceStats(string id, string group, string content)
        {
            this.DeviceID = id;
            this.DeviceGroup = group;
            this.Content = content;
        }

    }

    public class PyException : Exception
    {
        public string Msg { get; set; }

        public PyException(string msg)
        {
            this.Msg = msg;
        }
    }


    /// <summary>
    /// 串口参数
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public class SerialParameters
    {
        public string[] PortNameList { get; set; }
        public int[] BaudRateList { get; private set; } = { 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 43000, 56000, 57600, 115200 };
        public int BaudRate { get; set; }
        public int StopBits
        {
            get; set;
        }
        public int DataBits { get; set; }
        public string PortName
        {
            get; set;
        }
        public int Parity
        {
            get; set;
        }



        public SerialParameters()
        {
            this.StopBits = 1;
            this.DataBits = 8;

            this.PortNameList = GetPortNames();
            this.PortName = this.PortNameList.FirstOrDefault();
        }


        public static string[] GetPortNames()
        {
            try
            {
                var list = SerialPort.GetPortNames().ToList();
                list.Sort();
                return list.ToArray();
            }
            catch
            {
                return new string[0];
            }
        }

    }

    [AddINotifyPropertyChangedInterface]
    public class SerialDevice : SerialParameters
    {
        public bool IsOpened
        {
            get;
            set;
        }

        public ICommand ChangeStatusCommand { get; set; }

        public SerialDevice()
        {
            this.ChangeStatusCommand = new DelegateCommand2(this.EventChangeStatus);
        }

        public virtual void EventChangeStatus()
        {
            if (IsOpened)
            {
                this.Disconnect();
            }
            else
            {
                this.Connect();
            }
        }

        public virtual void Connect() { }

        public virtual void Disconnect() { }

    }


    public class PumpDevice : SerialDevice
    {
        public VCavroXLP6KPumpV1 Device { get; protected set; } = new VCavroXLP6KPumpV1("VCavroXLP6KPumpV1");
        public override void Connect()
        {
            try
            {
                if (!this.Device.Connected || this.Device.serialConn == null || this.Device.serialConn.IsOpen != true)
                {
                    this.Device.BaudRate = this.BaudRate;
                    this.Device.PortName = this.PortName;
                    this.Device.Connect();
                }
                this.IsOpened = this.Device.Connected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public override void Disconnect()
        {
            try
            {
                this.Device.Disconnect();
            }
            finally
            {
                base.Disconnect();
            }
            this.IsOpened = this.Device.Connected;
        }

    }

    public class SelectorValveDevice : SerialDevice
    {
        public VIdexUartValve2 Device { get; protected set; } = new VIdexUartValve2("SelectorValve", -1);

        public override void Connect()
        {
            try
            {
                if (!this.Device.Connected)
                {
                    this.Device.BaudRate = this.BaudRate;
                    this.Device.PortName = this.PortName;
                    this.Device.Connect();
                }
                this.IsOpened = this.Device.Connected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public override void Disconnect()
        {
            try
            {
                this.Device.Disconnect();
            }
            finally
            {
                base.Disconnect();
            }
            this.IsOpened = this.Device.Connected;
        }

    }

    public class LoaderBoardV8Device : SerialDevice
    {
        public VLoaderBoardV8 Device { get; protected set; } = new VLoaderBoardV8("LoaderBoardV8");

        public override void Connect()
        {
            try
            {
                if (!this.Device.Connected || this.Device.ComPort == null || this.Device.ComPort.IsOpen != true)
                {
                    this.Device.BaudRate = this.BaudRate;
                    this.Device.PortName = this.PortName;
                    this.Device.Connect();
                }
                this.IsOpened = this.Device.Connected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public override void Disconnect()
        {
            try
            {
                this.Device.Disconnect();
            }
            finally
            {
                base.Disconnect();
            }
            this.IsOpened = this.Device.Connected;
        }

    }

    public class TemperatureDevice : SerialDevice
    {
        public VTemperatureBoardLoaderV8 Device { get; protected set; } = new VTemperatureBoardLoaderV8("VTemperatureBoardLoaderV8");

        public override void Connect()
        {
            try
            {
                if (!this.Device.Connected)
                {
                    this.Device.BaudRate = this.BaudRate;
                    this.Device.PortName = this.PortName;
                    this.Device.Connect();
                }
                this.IsOpened = this.Device.Connected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public override void Disconnect()
        {
            try
            {
                this.Device.Disconnect();
            }
            finally
            {
                base.Disconnect();
            }
            this.IsOpened = this.Device.Connected;
        }

    }

    #endregion

    #region Operation Class

    [AddINotifyPropertyChangedInterface]
    public class Operation
    {
        public Operation(string name)
        {
            OperationName = name;
        }
        public string OperationName { get; private set; }
        public OperationTunnel[] Tunnels { get; set; }
        public bool TunnelsEnabled { get; set; } = true;
        public string ScriptName { get; set; }
      
        private DelegateCommand _InitCommand;
        public ICommand CommandFileBrowse { get; set; }

        public ICommand CommandSaveScript { get; set; }

        public string CurrentAction { get; set; }

        public ICommand InitCommand
        {
            get
            {
                if (_InitCommand == null)
                {
                    _InitCommand = new DelegateCommand(param => this.RunScript());
                }
                return _InitCommand;
            }
        }

        private void RunScript()
        {
            var selectTunnel = Tunnels.Where(x => x.Selected == true);

            if (selectTunnel == null || selectTunnel.Count() == 0) { MessageBox.Show("请选择操作通道"); return; }
            
        }

        public OperationTunnel[] GetTunnel()
        {
            var selectTunnel = Tunnels.Where(x => x.Selected == true);

            if (selectTunnel == null || selectTunnel.Count() == 0) { MessageBox.Show("请选择操作通道"); return null; }
            return selectTunnel.ToArray();
        }
    
    }

    [AddINotifyPropertyChangedInterface]
    public class OperationTunnel : ICloneable
    {
        public OperationTunnel()
        {
            Enabled = true;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected
        {
            get;
            set;
        }
        public bool Enabled { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
    [AddINotifyPropertyChangedInterface]
    public class DeviceOperaton
    {
        public DeviceOperaton(int id, string name, bool selected)
        {
            Id = id;
            Name = name;
            Selected = selected;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

    #endregion

    #region Enum
    public enum MotorCatogory
    {
        Motor1 = 1,
        Motor2 = 2,
        Motor3 = 3,
        Motor4 = 4,
        Motor5 = 5,
        MotorA = 6,
        MotorB = 7
    }

    #endregion
}




