﻿using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Interaction logic for MutiOperationLoad.xaml
    /// </summary>
    public partial class MutiOperationLoad : UserControl
    {
        public MutiOperationLoad()
        {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var checkbox = (CheckBox)sender;
            var boxlist = GetChildObjects<CheckBox>(this);
            foreach (var item in boxlist)
            {
                if (item.Content==checkbox.Content)
                {
                    if (item!=checkbox)
                    {
                        item.IsChecked = false;
                        item.IsEnabled = false;
                    }
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var checkbox = (CheckBox)sender;
            var boxlist = GetChildObjects<CheckBox>(this);
            foreach (var item in boxlist)
            {
                if (item.Content == checkbox.Content)
                {
                    if (item != checkbox)
                    {
                        item.IsEnabled = true;
                    }
                }
            }
        }

        public List<T> GetChildObjects<T>(DependencyObject obj) where T : FrameworkElement
        {
            DependencyObject child = null;
            List<T> childList = new List<T>();

            for (int i = 0; i <= VisualTreeHelper.GetChildrenCount(obj) - 1; i++)
            {
                child = VisualTreeHelper.GetChild(obj, i);

                if (child is T)
                {
                    //if (((T)child).Name.ToString() == name | string.IsNullOrEmpty(name))
                    {
                        childList.Add((T)child);
                    }
                }
                childList.AddRange(GetChildObjects<T>(child));
            }
            return childList;
        }

    }
}
