﻿using BGI.Control.Device.Physical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using BGI.Common.Utils;
using System.Data;
using System.IO;
using System.Data.OleDb;
using BGI.Common.Config;
using BGI.Control.Device;
using System.Threading;
using BGI.Common.UtilWPF.Helpers;
using System.IO.Ports;
using BGI.Common.Logging;
using System.Xml;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Interaction logic for V8LoaderTempratureBoard.xaml
    /// </summary>
    public partial class V8LoaderTempratureBoard : Window
    {
        private VTemperatureBoardLoaderV8 tempBoard= new VTemperatureBoardLoaderV8("VTemperatureBoardLoaderV8");
        private List<Point> originList = new List<Point>();
        private List<Point> degreeList = new List<Point>();
        //private LineGraph degreeGraph = null;
        //private LineAndMarker<ElementMarkerPointsGraph> sourceGraph = null;
        private int refrigeratorChannel = 0;
        private Logger logger = LogMgr.GetLogger("VTemperatureBoardLoaderV8", LogSeverityEnum.Debug);

        private int interval_ms = 500;
        public V8LoaderTempratureBoard()
        {
            InitializeComponent();
            this.StopBits = 1;
            this.DataBits = 8;

            this.PortNameList = GetPortNames();
            this.PortName = this.PortNameList.FirstOrDefault();

        }
        public static string[] GetPortNames()
        {
            try
            {
                var list = SerialPort.GetPortNames().ToList();
                list.Sort();
                return list.ToArray();
            }
            catch
            {
                return new string[0];
            }
        }
        #region Property
        public string[] PortNameList { get; set; }
        public int[] BaudRateList { get; private set; } = { 9600, 115200 };
        public int BaudRate { get; set; }
        public int StopBits
        {
            get; set;
        }
        public int DataBits { get; set; }
        public bool IsOpened { get; set; }
        public string PortName
        {
            get; set;
        }
        #endregion
        #region ICommand
        public ICommand ChangeStatusCommand { get { return new DelegateCommand2(this.EventChangeStatus); } }

        private void EventChangeStatus()
        {
            try
            {

                if (tempBoard.Connected)
                {
                    tempBoard.Disconnect();
                }
                else
                {
                    tempBoard.PortName = PortName;
                    tempBoard.BaudRate = BaudRate;

                    tempBoard.Connect();
                }
            }
            catch (Exception ex)
            {
                logger.Log("Change Temp Board Status Error",ex);
                MessageBox.Show(ex.Message, "连接或断开连接串口失败");
            }
            IsOpened = tempBoard.Connected;
        }
        #endregion
      
        private void SetPID_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            if (TBPVale.Text.Trim().Length == 0
                || TBIVale.Text.Trim().Length == 0
                || TBDVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input P I D value");
                return;
            }
            try
            {
                tempBoard.SetPID(Convert.ToSingle(TBPVale.Text.Trim()), Convert.ToSingle(TBIVale.Text.Trim()),
                    Convert.ToSingle(TBDVale.Text.Trim()), Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("operation success");
            }
            catch (Exception ex)
            {
                logger.Log("Set Temp Board PID  Error", ex);
                MessageBox.Show(ex.Message, "设置PID失败");
            }
        }

        private void GetVersion_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                string version = tempBoard.GetVersion();
                MessageBox.Show(version);
            }
            catch (Exception ex)
            {
                logger.Log("Get Temp Board Version Error", ex);
                MessageBox.Show(ex.Message,"获取版本失败");
            }
        }
        private void GetTC_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                bool? isEnable;
                tempBoard.GetTCEnable(Convert.ToByte(TBChannelVale.Text.Trim()),out isEnable);
                if (isEnable==null) MessageBox.Show("参数获取错误");

                MessageBox.Show(isEnable.Value?"该通道已设置为使能": "该通道已设置为不使能");
            }
            catch (Exception ex)
            {
                logger.Log("Get Temp Board Version Error", ex);
                MessageBox.Show(ex.Message,"获取温控使能失败");
            }
        }
        private void GetPID_Click(object sender, RoutedEventArgs e)
        {
            float kp, ki, kd;
            try
            {
                tempBoard.GetPIDEngData(out kp, out ki, out kd, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show(string.Format("p={0},i={1},d={2}", kp, ki, kd));
            }
            catch (Exception ex)
            {
                logger.Log("Get PID  Error", ex);
                MessageBox.Show(ex.Message, "获取PID参数失败");
            }
        }
        
        private void GetCurrentTemp_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                float temp = tempBoard.GetCurrentTemperature(Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show(temp.ToString());
            }
            catch (Exception ex)
            {
                logger.Log("Get Current Temp Error", ex);
                MessageBox.Show(ex.Message, "获取当前温度失败");
            }
        }

        private void SetTargetTemp_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {

                tempBoard.SetTargetTemperature(Convert.ToSingle(TBTargetTemp.Text.Trim()),
                    Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                logger.Log("Set Target Temp Error", ex);
                MessageBox.Show(ex.Message, "设置目标温度失败");
            }
        }

        private void GetTargetTemp_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                double t = 1;
                t=tempBoard.GetTargetTemperature(Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show(t.ToString());
            }
            catch (Exception ex)
            {
                logger.Log("Get Target Temp Error", ex);
                MessageBox.Show(ex.Message, "设置目标温度失败");
            }
        }

        private void EnableTC_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {

                tempBoard.EnableTC(true, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DisableTC_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {

                tempBoard.EnableTC(false, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private LineAndMarker<ElementMarkerPointsGraph> AddGraph2(List<Point> datas, string description, Pen pen, ShapeElementPointMarker marker)
        //{
        //    var xData = new EnumerableDataSource<double>(datas.Select(v => v.X));
        //    xData.SetXMapping(x => x);
        //    var yData = new EnumerableDataSource<double>(datas.Select(v => v.Y));
        //    yData.SetYMapping(y => y);
        //    CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
        //    return plotter2.AddLineGraph(compositeDataSource1, pen, marker, new PenDescription(description));
        //}

        //private LineGraph AddGraph2(List<double> xDatas, List<double> yDatas, string description, Pen pen)
        //{
        //    var xData = new EnumerableDataSource<double>(xDatas);
        //    xData.SetXMapping(x => x);
        //    var yData = new EnumerableDataSource<double>(yDatas);
        //    yData.SetYMapping(y => y);

        //    CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
        //    return plotter2.AddLineGraph(compositeDataSource1, pen, new PenDescription(description));
        //}

        //private void Btn_importData(object sender, RoutedEventArgs e)
        //{
        //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
        //    dlg.Filter = "csv documents (.csv)|*.csv";
        //    Nullable<bool> result = dlg.ShowDialog();
        //    // Process open file dialog box results
        //    if (result == true)
        //    {
        //        CBDegree.SelectionChanged -= ComboBox_SelectionChanged;
        //        CBDegree.Text = "";
        //        CBDegree.SelectionChanged += ComboBox_SelectionChanged;
        //        TBFormulae.Text = "";

        //        // Open document
        //        string filename = dlg.FileName;
        //        originList.Clear();
        //        originList = ReadDataFromCSV(filename);

        //        if (originList.Count == 0)
        //        {
        //            return;
        //        }

        //        plotter2.Children.Remove(plotter2.Legend);
        //        if (sourceGraph != null)
        //        {
        //            sourceGraph.MarkerGraph.Remove();
        //        }

        //        if (degreeGraph != null)
        //        {
        //            plotter2.Children.Remove(degreeGraph);
        //            degreeGraph = null;
        //        }

        //        sourceGraph = AddGraph(originList, "origin", new Pen(Brushes.LimeGreen, 0), new CircleElementPointMarker { Size = 9, Brush = Brushes.Red, Fill = Brushes.Orange });
        //        plotter2.FitToView();
        //    }
        //}

        //private List<Point> ReadDataFromCSV(string filePath)
        //{
        //    List<Point> dataList = new List<Point>();
        //    try
        //    {
        //        using (FileStream fs = new FileStream(filePath, FileMode.Open))
        //        {
        //            using (StreamReader sr = new StreamReader(fs))
        //            {
        //                while (!sr.EndOfStream)
        //                {
        //                    string line = sr.ReadLine();
        //                    string[] data = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                    if (data.Length == 2)
        //                    {
        //                        dataList.Add(new Point() { X = Convert.ToDouble(data[0]), Y = Convert.ToDouble(data[1]) });
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show($"Read data error:{ex}");
        //    }
        //    return dataList;
        //}

        //private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (originList.Count == 0)
        //    {
        //        return;
        //    }
        //    Color[] LineColors = new Color[] { Colors.Blue, Colors.Magenta, Colors.Orange, Colors.Green, Colors.Gold, Colors.Red, Colors.Purple, Colors.Brown, Colors.Azure };
        //    int degree = 0;
        //    degree = CBDegree.SelectedIndex + 1;
        //    degreeList.Clear();
        //    plotter2.Children.Remove(plotter2.Legend);
        //    if (degreeGraph != null)
        //    {
        //        plotter2.Children.Remove(degreeGraph);
        //    }
        //    StringBuilder formulaeSB = new StringBuilder();
        //    StringBuilder reverseFormulaeSB = new StringBuilder();
        //    var xData = originList.Select(p => p.X).ToArray();
        //    var yData = originList.Select(p => p.Y).ToArray();
        //    double[] arrFator = MathUtils.PolyFit(xData, yData, degree);
        //    List<double> xlist = new List<double>();
        //    List<double> yList = new List<double>();

        //    //Formulae pt100 to slide
        //    int dimension = arrFator.Length - 1;
        //    Task.Factory.StartNew(() =>
        //    {
        //        formulaeSB.Append("Y=");
        //        for (int i = 0; i <= dimension; i++)
        //        {
        //            formulaeSB.Append(arrFator[i].ToString("0.000")).Append("*").Append("X^").Append(i).Append("+");
        //        }
        //        Application.Current.Dispatcher.Invoke(() => TBFormulae.Text = formulaeSB.Length > 0 ? formulaeSB.Remove(formulaeSB.Length - 1, 1).ToString() : "");
        //    });

        //    //draw graph
        //    for (double i = xData.Min(); i < xData.Max(); i += 0.1)
        //    {
        //        xlist.Add(i);
        //        yList.Add(MathUtils.PolyEquation(i, arrFator));
        //    }
        //    degreeGraph = AddGraph(xlist, yList, $"{degree} degree", new Pen(new SolidColorBrush(LineColors[degree - 1]), 1));

        //    //reverse Formulae , slide to pt100
        //    Task.Factory.StartNew(() =>
        //    {
        //        double[] reverseArrFator = MathUtils.PolyFit(yData, xData, degree);
        //        dimension = arrFator.Length - 1;
        //        reverseFormulaeSB.Append("Y=");
        //        for (int i = 0; i <= dimension; i++)
        //        {
        //            reverseFormulaeSB.Append(reverseArrFator[i].ToString("0.000")).Append("*").Append("X^").Append(i).Append("+");
        //        }
        //        Application.Current.Dispatcher.Invoke(() =>
        //            TBFormulaeSlideToPT100.Text = reverseFormulaeSB.Length > 0 ? reverseFormulaeSB.Remove(reverseFormulaeSB.Length - 1, 1).ToString() : "");
        //    });

        //    plotter2.FitToView();
        //}
        private void SetTargetThreshlod_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            if (TBTargetLow.Text.Trim().Length == 0
                || TBTargetTop.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input threshold value");
                return;
            }
            try
            {
                tempBoard.SetTargetTemperatureThreshlod(Convert.ToSingle(TBTargetTop.Text.Trim()), Convert.ToSingle(TBTargetLow.Text.Trim()),
                                                   Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("operation success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetTargetThreshlod_Click(object sender, RoutedEventArgs e)
        {
            float top, low;
            try
            {
                tempBoard.GetTargetTemperatureThresholds(out top, out low, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show(string.Format("top={0},low={1}", top, low));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void GetCurrentThreshlod_Click(object sender, RoutedEventArgs e)
        {
            float top, low;
            try
            {
                tempBoard.GetCurrentTemperatureThresholds(out top, out low, Convert.ToByte(TBChannelVale.Text.Trim()));
                TBCurrentLow.Text = low.ToString();
                TBCurrentTop.Text = top.ToString();

                MessageBox.Show(string.Format("top={0},low={1}", top,low));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EnablePump_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                tempBoard.EnableRefrigerationPump(true, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void DisablePump_Click(object sender, RoutedEventArgs e)
        {

            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {
                tempBoard.EnableRefrigerationPump(false, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AutoUpgrade_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                tempBoard.Upgrade(true);
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ManualUpgrade_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tempBoard.Upgrade(false);
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

         private void EnableFan_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {

                tempBoard.EnableFans(true, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void DisableFan_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            try
            {

                tempBoard.EnableFans(false, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show("Operaion success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetPIDConfig_Click(object sender, RoutedEventArgs e)
        {
            if (TBChannelVale.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input channel");
                return;
            }
            if (TBtargetTemp.Text.Trim().Length == 0
                || TBMAX_I_OUTPUT.Text.Trim().Length == 0
                || TBMIN_I_OUTPUT.Text.Trim().Length == 0
                || TBMAX_PID_OUTPUT.Text.Trim().Length == 0
                || TBMIN_PID_OUTPUT.Text.Trim().Length == 0)
            {
                MessageBox.Show("please input parameter");
                return;
            }
            try
            {
                tempBoard.SetPIDConfig(Convert.ToSingle(TBtargetTemp.Text.Trim()),
                    Convert.ToSingle(TBMAX_PID_OUTPUT.Text.Trim()),
                    Convert.ToSingle(TBMIN_PID_OUTPUT.Text.Trim()),
                    Convert.ToInt16(TBMAX_I_OUTPUT.Text.Trim()),
                    Convert.ToInt16(TBMIN_I_OUTPUT.Text.Trim()),
                    Convert.ToByte(TBTHRESHOLD.Text.Trim()),
                    Convert.ToByte(TBChannelVale.Text.Trim())
                    );
                MessageBox.Show("operation success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetPIDConfig_Click(object sender, RoutedEventArgs e)
        {
            float targettemperature, maxpid, minpid;
            Int16 maxio,minio;
            byte threshold;
            try
            {
                tempBoard.GetPIDConfig(out targettemperature, out maxpid, out minpid,out maxio,out minio,out threshold, Convert.ToByte(TBChannelVale.Text.Trim()));
                MessageBox.Show(string.Format("targettemperature={0},maxpid={1},minpid={2}", targettemperature, maxpid, minpid));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region TemperatureCalibration

        //public enum GraphEnum
        //{
        //    Slide,
        //    PT100,
        //    Anaylis,
        //}
        //#region Field
        //private Timer _HeartbeatTimer;
        //private int count = 0;
        ////private int interval_ms = 25;
        //private string strFormat = "(PT100) {0:F2}   (Slide) {1:F2}";
        //private string strChFormat = "";
        //private bool isRunning = false;
        //private int totalCount = 0;

        //private Dictionary<string, bool> tunnels;
        ////private bool isCH1 = false;
        ////private bool isCH4 = false;
        //private double targetTemp = 20;
        //private double targetTempAfterCalib = 0;

        ////private Control.Device.ITempControl _TemperatureInt;

        //private LineAndMarker<MarkerPointsGraph> pointGraph = new LineAndMarker<MarkerPointsGraph>();
        //private LineAndMarker<MarkerPointsGraph> point2Graph = new LineAndMarker<MarkerPointsGraph>();


        //private LineAndMarker<MarkerPointsGraph>[] pointLines;

        //private List<KeyValuePair<double, double>>[] pointDatas;

        //private LineAndMarker<MarkerPointsGraph>[] channelLines;
        //private List<KeyValuePair<double, double>>[] channelDatas;

        //private LineAndMarker<MarkerPointsGraph> analysiLine;

        //private List<KeyValuePair<double, double>> Series1Data { get; set; }
        //private List<KeyValuePair<double, double>> Series2Data { get; set; }

        //public List<List<KeyValuePair<double, double>>[]> SeriesData { get; set; }

        //public string SeriesDesc { get; set; }
        //private Brush[] LineBrushes { get; set; } = new Brush[]
        //{ Brushes.Yellow, Brushes.YellowGreen,
        //    Brushes.Orange,Brushes.DarkOrange,
        //    Brushes.Green, Brushes.DarkGreen,
        //    Brushes.Red, Brushes.DarkRed,
        //    Brushes.Blue, Brushes.DarkBlue,
        //    Brushes.Khaki, Brushes.DarkKhaki };
        //public string[] PenDesc { get; set; }
        //#endregion
     
        //private void TemperatureCalibration_Loaded(object sender, RoutedEventArgs e)
        //{
        //    //_TemperatureInt = Helper.DeviceManager.Get.TempBoardInt;
            
        //    this.chGraphCH1.Click += ChGraphCH1_Checked;
        //    this.chGraphCH4.Click += ChGraphCH1_Checked;

        //    this.chPoint1Name.Click += ChPoint1Name_Checked;
        //    this.chPoint2Name.Click += ChPoint1Name_Checked;
        //    this.chPoint3Name.Click += ChPoint1Name_Checked;
        //    this.chPoint4Name.Click += ChPoint1Name_Checked;
        //    this.chPoint5Name.Click += ChPoint1Name_Checked;
        //    this.chPoint6Name.Click += ChPoint1Name_Checked;
        //    this.chPoint7Name.Click += ChPoint1Name_Checked;
        //    this.chPoint8Name.Click += ChPoint1Name_Checked;
        //    this.chPoint9Name.Click += ChPoint1Name_Checked;
        //    this.chPoint10Name.Click += ChPoint1Name_Checked;

        //    this.graphSettings.IsEnabled = true;
        //    //this.btnImprt.IsEnabled = false;

        //    this.chPoint1Name.Visibility = Visibility.Visible;
        //    this.chPoint2Name.Visibility = Visibility.Visible;
        //    this.chPoint3Name.Visibility = Visibility.Visible;
        //    this.chPoint4Name.Visibility = Visibility.Visible;
        //    this.chPoint5Name.Visibility = Visibility.Visible;
        //    this.chPoint6Name.Visibility = Visibility.Visible;
        //    this.chPoint7Name.Visibility = Visibility.Visible;
        //    this.chPoint8Name.Visibility = Visibility.Visible;
        //    this.chPoint9Name.Visibility = Visibility.Visible;
        //    this.chPoint10Name.Visibility = Visibility.Visible;
        //    this.btnStartAgain.IsEnabled = true;
        //    this.cmbSelectAnalysis.SelectionChanged += CmbSelectAnalysis_SelectionChanged;
        //}


        //private List<int> GetSelectPoint()
        //{
        //    List<int> rslt = new List<int>();
        //    if (this.chPoint10Name.IsChecked.HasValue && this.chPoint10Name.IsChecked.Value)
        //    {
        //        rslt.Add(9);
        //    }
        //    if (this.chPoint9Name.IsChecked.HasValue && this.chPoint9Name.IsChecked.Value)
        //    {
        //        rslt.Add(8);
        //    }
        //    if (this.chPoint8Name.IsChecked.HasValue && this.chPoint8Name.IsChecked.Value)
        //    {
        //        rslt.Add(7);
        //    }
        //    if (this.chPoint7Name.IsChecked.HasValue && this.chPoint7Name.IsChecked.Value)
        //    {
        //        rslt.Add(6);
        //    }
        //    if (this.chPoint6Name.IsChecked.HasValue && this.chPoint6Name.IsChecked.Value)
        //    {
        //        rslt.Add(5);
        //    }
        //    if (this.chPoint5Name.IsChecked.HasValue && this.chPoint5Name.IsChecked.Value)
        //    {
        //        rslt.Add(4);
        //    }
        //    if (this.chPoint4Name.IsChecked.HasValue && this.chPoint4Name.IsChecked.Value)
        //    {
        //        rslt.Add(3);
        //    }
        //    if (this.chPoint3Name.IsChecked.HasValue && this.chPoint3Name.IsChecked.Value)
        //    {
        //        rslt.Add(2);
        //    }
        //    if (this.chPoint2Name.IsChecked.HasValue && this.chPoint2Name.IsChecked.Value)
        //    {
        //        rslt.Add(1);
        //    }
        //    if (this.chPoint1Name.IsChecked.HasValue && this.chPoint1Name.IsChecked.Value)
        //    {
        //        rslt.Add(0);
        //    }
        //    return rslt;
        //}

        //private void CmbSelectAnalysis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (this.cmbSelectAnalysis.SelectedIndex < 0) return;
        //    this.btnStartAgain.IsEnabled = true;
        //    int degree = 0;
        //    degree = cmbSelectAnalysis.SelectedIndex + 1;
        //    StringBuilder formulaeSB = new StringBuilder();

        //    List<int> channelIndex = new List<int>();
        //    if (this.chGraphCH1.IsChecked.HasValue && this.chGraphCH1.IsChecked.Value)
        //    {
        //        channelIndex.Add(0);
        //    }
        //    if (this.chGraphCH4.IsChecked.HasValue && this.chGraphCH4.IsChecked.Value)
        //    {
        //        channelIndex.Add(1);
        //    }

        //    List<int> pointIndex = GetSelectPoint();
        //    if (pointIndex.Count == 0 || channelIndex.Count == 0)
        //    {
        //        MessageBox.Show("请选择PT100温度和测试点温度");
        //        return;
        //    }
        //    var xData = GetChannelTemp(channelIndex.ToArray()).ToArray();
        //    var yData = GetSlideTemp(pointIndex.ToArray()).ToArray();
        //    int cnt = Math.Min(xData.Length, yData.Length);
        //    double[] xData1 = new double[cnt];
        //    double[] yData1 = new double[cnt];
        //    Array.Copy(xData, 0, xData1, 0, cnt);
        //    Array.Copy(yData, 0, yData1, 0, cnt);
        //    double[] arrFator = MathUtils.PolyFit(xData1, yData1, degree);

        //    List<double> xlist = new List<double>();
        //    List<double> yList = new List<double>();
        //    int dimension = arrFator.Length - 1;
        //    formulaeSB.Append("Y=");
        //    targetTempAfterCalib = 0;
        //    for (int i = 0; i <= dimension; i++)
        //    {
        //        formulaeSB.Append(arrFator[i].ToString("0.000")).Append("*").Append("X^").Append(i).Append("+");
        //        //tempTar += arrFator[i] * Math.Pow()
        //        targetTempAfterCalib += arrFator[i] * Math.Pow(targetTemp, i);
        //    }
        //    this.txtFormula.Text = formulaeSB.Length > 0 ? formulaeSB.Remove(formulaeSB.Length - 1, 1).ToString() : "";
        //    RemoveGraph();

        //    for (double i = xData.Min(); i < xData.Max(); i += 0.1)
        //    {
        //        xlist.Add(i);
        //        yList.Add(MathUtils.PolyEquation(i, arrFator));
        //    }

        //    AddGraph(xlist, yList, 0, GraphEnum.Anaylis);
        //    this.txtCalibra.Text = targetTempAfterCalib.ToString("F2");

        //}

        //private void ChPoint1Name_Checked(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {

        //        CheckBox cb = sender as CheckBox;

        //        if (cb != null)
        //        {
        //            int index = Convert.ToInt32(cb.Tag) - 1;
        //            if (cb.IsChecked.HasValue)
        //            {
        //                if (cb.IsChecked.Value)
        //                {
        //                    AddGraph(pointDatas[index], index, GraphEnum.Slide);
        //                }
        //                else
        //                {
        //                    plotter2.Children.Remove(pointLines[index].LineGraph);
        //                    plotter2.Children.Remove(pointLines[index].MarkerGraph);
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //private void ChGraphCH1_Checked(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {


        //        CheckBox cb = sender as CheckBox;

        //        if (cb != null)
        //        {
        //            int index = Convert.ToInt32(cb.Tag);
        //            if (cb.IsChecked.HasValue)
        //            {
        //                if (cb.IsChecked.Value)
        //                {
        //                    AddGraph(channelDatas[index], index, GraphEnum.PT100);
        //                }
        //                else
        //                {
        //                    plotter2.Children.Remove(channelLines[index].LineGraph);
        //                    plotter2.Children.Remove(channelLines[index].MarkerGraph);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //private void BtnStop_Click(object sender, RoutedEventArgs e)
        //{
        //    this.btnStart.IsEnabled = true;
        //    this.isRunning = false;
        //    this.strChFormat = string.Empty;

        //    //this.btnImport.IsEnabled = true;
        //    this.graphSettings.IsEnabled = true;

        //    this.chGraphCH1.IsChecked = true;
        //    this.chGraphCH4.IsChecked = true;
        //}
        //private void BtnStartAgain_Click(object sender, RoutedEventArgs e)
        //{
        //    //this.targetTemp = 
        //    this.btnStartAgain.IsEnabled = false;
        //    this.isRunning = true;
        //    this.count = 0;

        //    interval_ms = int.Parse(this.txtInterval.Text);
        //    totalCount = int.Parse(this.txtTotalMinutes.Text) * 60 * 1000 / interval_ms;
        //    this.SeriesData = new List<List<KeyValuePair<double, double>>[]>();
        //    RemoveGraph();
        //    _HeartbeatTimer = new Timer(HeartbeatCallback, null, interval_ms, Timeout.Infinite);
        //    double target = Convert.ToDouble(this.txtCalibra.Text);
        //    tempBoard.SetTargetTemperature(target);
        //    this.btnStart.IsEnabled = false;
        //    this.btnStartAgain.IsEnabled = false;
        //}

        //private void BtnStart_Click(object sender, RoutedEventArgs e)
        //{
        //    this.targetTemp = Convert.ToDouble(this.txtTarget.Text);
        //    RemoveGraph();
        //    this.btnStartAgain.IsEnabled = false;
        //    this.isRunning = true;
        //    this.count = 0;
        //    //_TemperatureInt = Helper.DeviceManager.Get.TempBoardInt;
        //    interval_ms = int.Parse(this.txtInterval.Text);
        //    totalCount = int.Parse(this.txtTotalMinutes.Text) * 60 * 1000 / interval_ms;
        //    this.PenDesc = new string[5] { "CH1", "CH2", "CH3", "CH4" ,"CH5"};
        //    tunnels = new Dictionary<string, bool>();
        //    if (this.chChannel1.IsChecked.HasValue && this.chChannel1.IsChecked.Value)
        //    {
        //        tunnels.Add("CH1",true);
        //    }
           
        //    if (this.chChannel4.IsChecked.HasValue && this.chChannel4.IsChecked.Value)
        //    {
        //        tunnels.Add("CH4", true);
        //    }

        //    if (this.chChannel2.IsChecked.HasValue && this.chChannel2.IsChecked.Value)
        //    {
        //        tunnels.Add("CH2", true);

        //    }
        //    if (this.chChannel3.IsChecked.HasValue && this.chChannel3.IsChecked.Value)
        //    {
        //        tunnels.Add("CH2", true);

        //    }
        //    //if (this.isCH1 && this.isCH4)
        //    //{
        //    //    this.PenDesc = new string[] { "CH1", "CH4" };
        //    //}
        //    //else if (this.isCH1)
        //    //{
        //    //    this.PenDesc = new string[] { "CH1" };
        //    //}
        //    //else if (this.isCH4)
        //    //{
        //    //    this.PenDesc = new string[] { "CH4" };
        //    //}
        //    this.PenDesc = tunnels.Keys.ToArray();
        //    this.channelLines = new LineAndMarker<MarkerPointsGraph>[2];
        //    channelLines[0] = channelLines[1] = new LineAndMarker<MarkerPointsGraph>();
        //    channelDatas = new List<KeyValuePair<double, double>>[2];
        //    channelDatas[0] = channelDatas[1] = new List<KeyValuePair<double, double>>();
        //    SeriesData = new List<List<KeyValuePair<double, double>>[]>();
        //    _HeartbeatTimer = new Timer(HeartbeatCallback, null, interval_ms, Timeout.Infinite);
        //    tempBoard.SetTargetTemperature(targetTemp,1);
        //    tempBoard.SetTargetTemperature(targetTemp, 2);
        //    this.btnStart.IsEnabled = false;
        //}

        //private void BtnImport_Click(object sender, RoutedEventArgs e)
        //{
        //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
        //    dlg.Filter = "csv documents (.csv)|*.csv";
        //    Nullable<bool> result = dlg.ShowDialog();
        //    if (result == true)
        //    {
        //        string filename = dlg.FileName;
        //        Dictionary<string, List<KeyValuePair<double, double>>> rst = GetDataFromCSV(filename);
        //        pointLines = new LineAndMarker<MarkerPointsGraph>[rst.Keys.Count];
        //        pointDatas = new List<KeyValuePair<double, double>>[rst.Keys.Count];
        //        int i = 0;
        //        this.PenDesc = rst.Keys.ToArray();
        //        this.graphSettings.IsEnabled = true;
        //        foreach (string name in rst.Keys)
        //        {
        //            switch (i)
        //            {
        //                case 0:
        //                    this.chPoint1Name.Content = name;
        //                    this.chPoint1Name.Visibility = Visibility.Visible;
        //                    this.chPoint1Name.IsChecked = true;
        //                    break;
        //                case 1:
        //                    this.chPoint2Name.Content = name;
        //                    this.chPoint2Name.Visibility = Visibility.Visible;
        //                    this.chPoint2Name.IsChecked = true;
        //                    break;
        //                case 2:
        //                    this.chPoint3Name.Content = name;
        //                    this.chPoint3Name.Visibility = Visibility.Visible;
        //                    this.chPoint3Name.IsChecked = true;
        //                    break;
        //                case 3:
        //                    this.chPoint4Name.Content = name;
        //                    this.chPoint4Name.Visibility = Visibility.Visible;
        //                    this.chPoint4Name.IsChecked = true;
        //                    break;
        //                case 4:
        //                    this.chPoint5Name.Content = name;
        //                    this.chPoint5Name.Visibility = Visibility.Visible;
        //                    this.chPoint5Name.IsChecked = true;
        //                    break;
        //                case 5:
        //                    this.chPoint6Name.Content = name;
        //                    this.chPoint6Name.Visibility = Visibility.Visible;
        //                    this.chPoint6Name.IsChecked = true;
        //                    break;
        //                case 6:
        //                    this.chPoint7Name.Content = name;
        //                    this.chPoint7Name.Visibility = Visibility.Visible;
        //                    this.chPoint7Name.IsChecked = true;
        //                    break;
        //                case 7:
        //                    this.chPoint8Name.Content = name;
        //                    this.chPoint8Name.Visibility = Visibility.Visible;
        //                    this.chPoint8Name.IsChecked = true;
        //                    break;
        //                case 8:
        //                    this.chPoint9Name.Content = name;
        //                    this.chPoint9Name.Visibility = Visibility.Visible;
        //                    this.chPoint9Name.IsChecked = true;
        //                    break;
        //                case 9:
        //                    this.chPoint10Name.Content = name;
        //                    this.chPoint10Name.Visibility = Visibility.Visible;
        //                    this.chPoint10Name.IsChecked = true;
        //                    break;
        //                default:
        //                    break;
        //            }
        //            pointDatas[i] = rst[name];
        //            AddGraph(rst[name].Select(p => p.Key).ToList(), rst[name].Select(p => p.Value).ToList(), i, GraphEnum.Slide);
        //            i++;
        //        }

        //        double slideAvg = GetAvg(true);
        //        double chAvg = GetAvg(false);
        //        double slideMax = GetMax(true);
        //        double chMax = GetMax(false);
        //        double slideMin = GetMin(true);
        //        double chMin = GetMin(false);

        //        this.txtAvgTemp.Text = string.Format(strFormat, chAvg, slideAvg);
        //        this.txtLowTemp.Text = string.Format(strFormat, chMin, slideMin);
        //        this.txtHighTemp.Text = string.Format(strFormat, chMax, slideMax);

        //        List<double> slidV = GetSlideTemp(new int[] { 0, 1, 2, 3 });
        //    }
        //}

        //#region Private Method

        //private Dictionary<string, List<KeyValuePair<double, double>>> GetDataFromCSV(string fileName)
        //{
        //    int pointCnt = 0;
        //    Dictionary<string, List<KeyValuePair<double, double>>> result = new Dictionary<string, List<KeyValuePair<double, double>>>();
        //    List<string> lstPoint = new List<string>();
        //    using (FileStream fs = new FileStream(fileName, FileMode.Open))
        //    {
        //        using (StreamReader sr = new StreamReader(fs))
        //        {
        //            int pointsCnt = 0;
        //            while (!sr.EndOfStream)
        //            {
        //                string line = sr.ReadLine();
        //                string[] temp = line.Split('\t');
        //                if (pointsCnt == 0)
        //                {
        //                    if (temp.Length == 2)
        //                    {
        //                        int.TryParse(temp[1], out pointsCnt);
        //                    }
        //                }

        //                if (temp.Length == 14)
        //                {
        //                    int point = 0;
        //                    if (int.TryParse(temp[0], out point))
        //                    {
        //                        lstPoint.Add(point.ToString());
        //                        result.Add(point.ToString(), new List<KeyValuePair<double, double>>());
        //                        pointCnt++;
        //                    }
        //                }
        //                else if (temp.Length == pointsCnt * 2 + 2)
        //                {
        //                    int number = 0;
        //                    if (int.TryParse(temp[0], out number))
        //                    {
        //                        for (int i = 0; i < pointCnt; i++)
        //                        {
        //                            if (result.ContainsKey(lstPoint[i]))
        //                            {
        //                                result[lstPoint[i]].Add(new KeyValuePair<double, double>(number, Convert.ToDouble(temp[i * 2 + 2])));
        //                            }
        //                            else
        //                            {
        //                                result.Add(lstPoint[i], new List<KeyValuePair<double, double>>() { new KeyValuePair<double, double>(number, Convert.ToDouble(temp[i * 2 + 2])) });
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}

        //private void AddGraph(List<KeyValuePair<double, double>> datasoure, int index, GraphEnum graphType)
        //{
        //    AddGraph(datasoure.Select(p => p.Key).ToList(), datasoure.Select(p => p.Value).ToList(), index, graphType);
        //}

        //private void AddGraph(List<double> xValue, List<double> yValue, int index, GraphEnum graphType)
        //{
        //    var xData1 = new EnumerableDataSource<double>(xValue);
        //    xData1.SetXMapping(x => x);
        //    var yData1 = new EnumerableDataSource<double>(yValue);
        //    yData1.SetYMapping(y => y);
        //    CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData1, yData1);
        //    if (graphType == GraphEnum.Slide)
        //    {
        //        LineAndMarker<MarkerPointsGraph> tempGraph = plotter2.AddLineGraph(compositeDataSource1, new Pen(LineBrushes[index + 2], 1), new CirclePointMarker() { Size = 3, Fill = LineBrushes[index + 2] }, new PenDescription(PenDesc[index]));
        //        if (pointLines != null)
        //        {
        //            pointLines[index] = tempGraph;
        //        }
        //    }
        //    else if (graphType == GraphEnum.PT100)
        //    {
        //        LineAndMarker<MarkerPointsGraph> tempGraph = plotter2.AddLineGraph(compositeDataSource1, new Pen(LineBrushes[index], 1), new CirclePointMarker() { Size = 3, Fill = LineBrushes[index] }, new PenDescription(PenDesc[index]));
        //        if (channelLines != null)
        //        {
        //            channelLines[index] = tempGraph;
        //        }
        //    }
        //    else
        //    {
        //        LineAndMarker<MarkerPointsGraph> tempGraph = plotter2.AddLineGraph(compositeDataSource1, new Pen(LineBrushes[index], 1), new CirclePointMarker() { Size = 3, Fill = LineBrushes[index] }, new PenDescription(PenDesc[index]));
        //        analysiLine = tempGraph;
        //    }

        //}

        //private void RefershData()
        //{
        //    if (SeriesData != null && SeriesData.Count > 0)
        //    {
        //        RemoveGraph();
        //        int offset = 0;
        //        if (SeriesData.Count > 1) offset = 2;
        //        for (int i = 0; i < SeriesData.Count; i++)
        //        {

        //            List<KeyValuePair<double, double>>[] temp = SeriesData[i];
        //            Series1Data = temp[0];
        //            Series2Data = temp[1];
        //            if (Series1Data != null)
        //            {
        //                var xData1 = new EnumerableDataSource<double>(Series1Data.Select(p => p.Key).ToList());
        //                xData1.SetXMapping(x => x);
        //                var yData1 = new EnumerableDataSource<double>(Series1Data.Select(p => p.Value).ToList());
        //                yData1.SetYMapping(y => y);
        //                CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData1, yData1);
        //                channelLines[0] = plotter2.AddLineGraph(compositeDataSource1, new Pen(LineBrushes[i], 1), new CirclePointMarker() { Size = 3, Fill = LineBrushes[i] }, new PenDescription(PenDesc[i]));
        //            }
        //            if (Series2Data != null)
        //            {
        //                var xData2 = new EnumerableDataSource<double>(Series2Data.Select(p => p.Key).ToList());
        //                xData2.SetXMapping(x => x);
        //                var yData2 = new EnumerableDataSource<double>(Series2Data.Select(p => p.Value).ToList());
        //                yData2.SetYMapping(y => y);
        //                CompositeDataSource compositeDataSource2 = new CompositeDataSource(xData2, yData2);
        //                channelLines[1] = plotter2.AddLineGraph(compositeDataSource2, new Pen(LineBrushes[i + 1], 1), new CirclePointMarker() { Size = 3, Fill = LineBrushes[i + 1] }, new PenDescription(PenDesc[i + 1]));
        //            }
        //        }
        //    }
        //}

        //private void RemoveGraph()
        //{
        //    if (this.channelLines != null)
        //    {
        //        for (int i = 0; i < channelLines.Length; i++)
        //        {
        //            this.plotter2.Children.Remove(channelLines[i].LineGraph);
        //            this.plotter2.Children.Remove(channelLines[i].MarkerGraph);
        //        }
        //    }
        //    if (this.pointLines != null)
        //    {
        //        for (int i = 0; i < pointLines.Length; i++)
        //        {
        //            this.plotter2.Children.Remove(pointLines[i].LineGraph);
        //            this.plotter2.Children.Remove(pointLines[i].MarkerGraph);
        //        }
        //    }

        //    if (this.analysiLine != null)
        //    {
        //        this.plotter2.Children.Remove(analysiLine.LineGraph);
        //        this.plotter2.Children.Remove(analysiLine.MarkerGraph);
        //    }
        //}

        //private void HeartbeatCallback(Object state)
        //{
        //    List<KeyValuePair<double, double>>[] data = new List<KeyValuePair<double, double>>[2];
        //    data[0] = new List<KeyValuePair<double, double>>();
        //    data[1] = new List<KeyValuePair<double, double>>();
        //    if (SeriesData == null || SeriesData.Count == 0)
        //    {
        //        SeriesData = new List<List<KeyValuePair<double, double>>[]>();
        //        SeriesData.Add(data);
        //    }
        //    else
        //    {
        //        data = SeriesData[0];
        //    }

        //    double v1 = tempBoard.GetCurrentTemperature(1);// v * (double)r.Next(57);
        //    double v2 = tempBoard.GetCurrentTemperature(2);//v * (double)r.Next(56);
        //    channelDatas[0].Add(new KeyValuePair<double, double>(count, v1));
        //    channelDatas[1].Add(new KeyValuePair<double, double>(count, v2));
        //    data[0].Add(new KeyValuePair<double, double>((double)count, v1));
        //    data[1].Add(new KeyValuePair<double, double>((double)count, v2));
        //    App.Current.Dispatcher.Invoke(() =>
        //    {

        //        string str = string.Empty;
        //        if (this.chChannel1.IsChecked.HasValue && this.chChannel1.IsChecked.Value)
        //        {
        //            str += $"CH1={v1.ToString("F2")} ";
        //        }
        //        if (this.chChannel2.IsChecked.HasValue && this.chChannel2.IsChecked.Value)
        //        {
        //            str += $"CH2={v2.ToString("F2")} ";
        //        }
        //        if (this.chChannel3.IsChecked.HasValue && this.chChannel3.IsChecked.Value)
        //        {
        //            str += $"CH3={v2.ToString("F2")} ";
        //        }
        //        if (this.chChannel4.IsChecked.HasValue && this.chChannel4.IsChecked.Value)
        //        {
        //            str += $"CH4={v2.ToString("F2")} ";
        //        }
        //        if (this.chChannel5.IsChecked.HasValue && this.chChannel5.IsChecked.Value)
        //        {
        //            str += $"CH5={v2.ToString("F2")} ";
        //        }
        //        this.txtCurrent.Text = str;
        //        RefershData();
        //    });

        //    count++;
        //    _HeartbeatTimer.Change(interval_ms, Timeout.Infinite);
        //    if (count >= totalCount || !isRunning)
        //    {
        //        _HeartbeatTimer.Dispose();
        //        this.Dispatcher.Invoke(() =>
        //        {
        //            this.btnStart.IsEnabled = true;
        //           // this.btnImport.IsEnabled = true;
        //            this.graphSettings.IsEnabled = true;
        //        });

        //    }

        //}

        //private double GetAvg(bool isSlide)
        //{
        //    double val = 0;
        //    if (isSlide)
        //    {

        //        for (int i = 0; i < pointDatas.Length; i++)
        //        {
        //            val += pointDatas[i].Average(a => a.Value);
        //        }
        //        val = val / pointDatas.Length;
        //    }
        //    else
        //    {
        //        if (channelDatas != null)
        //        {
        //            for (int i = 0; i < channelDatas.Length; i++)
        //            {
        //                val += channelDatas[i].Average(a => a.Value);
        //            }
        //            val = val / channelDatas.Length;
        //        }

        //    }
        //    return val;
        //}

        //private double GetMax(bool isSlide)
        //{
        //    double val = 0;
        //    if (isSlide)
        //    {

        //        for (int i = 0; i < pointDatas.Length; i++)
        //        {
        //            double temp = pointDatas[i].Max(a => a.Value);
        //            val = Math.Max(temp, val);
        //        }

        //    }
        //    else
        //    {
        //        if (channelDatas != null)
        //        {
        //            for (int i = 0; i < channelDatas.Length; i++)
        //            {
        //                double temp = channelDatas[i].Max(a => a.Value);
        //                val = Math.Max(temp, val);
        //            }
        //        }

        //    }
        //    return val;
        //}

        //private double GetMin(bool isSlide)
        //{
        //    double val = 200;
        //    if (isSlide)
        //    {

        //        for (int i = 0; i < pointDatas.Length; i++)
        //        {
        //            double temp = pointDatas[i].Min(a => a.Value);
        //            val = Math.Min(temp, val);
        //        }

        //    }
        //    else
        //    {
        //        if (channelDatas != null)
        //        {
        //            for (int i = 0; i < channelDatas.Length; i++)
        //            {
        //                double temp = channelDatas[i].Max(a => a.Value);
        //                val = Math.Min(temp, val);
        //            }
        //        }

        //    }
        //    return val;
        //}

        //public List<double> GetSlideTemp(int[] index)
        //{

        //    try
        //    {

            
        //    if (index != null)
        //    {
        //        if (index.Length == 1)
        //        {
        //            return pointDatas[index[0]].Select(a => a.Value).ToList();
        //        }
        //        else
        //        {
        //            List<double>[] tempList = new List<double>[index.Length];
        //            int minCnt = int.MaxValue;
        //            for (int i = 0; i < index.Length; i++)
        //            {
        //                tempList[i] = pointDatas[index[i]].Select(a => a.Value).ToList();
        //                minCnt = Math.Min(minCnt, tempList[i].Count);
        //            }
        //            List<double> rslt = new List<double>();
        //            for (int i = 0; i < minCnt; i++)
        //            {
        //                double v = 0;
        //                for (int j = 0; j < index.Length; j++)
        //                {
        //                    v += tempList[j][i];
        //                }
        //                v = v / index.Length;
        //                rslt.Add(v);
        //            }
        //            return rslt;
        //        }
        //    }
        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //    return null;

        //}

        //public List<double> GetChannelTemp(int[] index)
        //{
        //    if (index != null)
        //    {
        //        if (index.Length == 1)
        //        {
        //            return channelDatas[index[0]].Select(a => a.Value).ToList();
        //        }
        //        else
        //        {
        //            List<double>[] tempList = new List<double>[index.Length];
        //            int minCnt = int.MaxValue;
        //            for (int i = 0; i < index.Length; i++)
        //            {
        //                tempList[i] = channelDatas[index[i]].Select(a => a.Value).ToList();
        //                minCnt = Math.Min(minCnt, tempList[i].Count);
        //            }
        //            List<double> rslt = new List<double>();
        //            for (int i = 0; i < minCnt; i++)
        //            {
        //                double v = 0;
        //                for (int j = 0; j < index.Length; j++)
        //                {
        //                    v += tempList[j][i];
        //                }
        //                v = v / index.Length;
        //                rslt.Add(v);
        //            }
        //            return rslt;
        //        }
        //    }
        //    return null;
        //}
        //#endregion
        #endregion

        #region UpdateFireware
        //private const int WaitJumpAppTimeMS = 15000;
        //private string _UpdateVersion;
        //private string _UpdateFilePath;
        //private double _Progress;
        //private string _FirmwareName;
        //private string _CurrentVersion;
        //private dynamic _boardControlDynamic;
        //private string _ShowInfoMsg;
        //private bool _IsIdle = true;
        //private bool _IsError;

        //private FirmwareUpdate firmwareUpdate;

        //private ICommand _SelectFileCmd;
        //private ICommand _UpdateCmd;

        //#region Properties
        //public string ShowInfoMsg
        //{
        //    get { return _ShowInfoMsg; }
        //    set
        //    {
        //        _ShowInfoMsg = value;
        //    }
        //}
        //public dynamic BoardControlDynamic
        //{
        //    get
        //    {
        //        return _boardControlDynamic;
        //    }
        //    set
        //    {
        //        _boardControlDynamic = value;
        //    }
        //}

        //public string CurrentVersion
        //{
        //    get
        //    {
        //        return _CurrentVersion;
        //    }
        //    set
        //    {
        //        _CurrentVersion = value;
        //    }
        //}

        //public string FirmwareName
        //{
        //    get
        //    {
        //        return _FirmwareName;
        //    }
        //    set
        //    {
        //        _FirmwareName = value;
        //    }
        //}
        //public string UpdateVersion
        //{
        //    get
        //    {
        //        return _UpdateVersion;
        //    }
        //    set
        //    {
        //        _UpdateVersion = value;
        //    }
        //}

        //public string UpdateFilePath
        //{
        //    get
        //    {
        //        return _UpdateFilePath;
        //    }
        //    set
        //    {
        //        int endIndex = value.LastIndexOf('.');
        //        int startIndex = value.LastIndexOf('_');
        //        if (startIndex != -1 && endIndex != -1 && endIndex > startIndex && endIndex - startIndex - 2 > 0)
        //        {
        //            UpdateVersion = value.Substring(startIndex + 2, endIndex - startIndex - 2);
        //            _UpdateFilePath = value;
        //        }
        //        else
        //        {
        //            ShowInfoMsg = "Update file is not correct";
        //            _UpdateFilePath = string.Empty;
        //        }

        //    }
        //}

        //public double Progress
        //{
        //    get
        //    {
        //        if (_Progress > 100)
        //            _Progress = 100;
        //        return _Progress;
        //    }
        //    set
        //    {
        //        _Progress = value;
        //    }
        //}

        //public bool IsIdle
        //{
        //    get
        //    {
        //        return _IsIdle;
        //    }
        //    set
        //    {
        //        _IsIdle = value;
        //    }
        //}

        //public bool IsError
        //{
        //    get
        //    {
        //        return _IsError;
        //    }
        //    set
        //    {
        //        _IsError = value;
        //    }
        //}
        //#endregion

        //public ICommand SelectFileCmd
        //{
        //    get
        //    {
        //        return _SelectFileCmd ?? (_SelectFileCmd = new DelegateCommand2(() =>
        //        {
        //            try
        //            {
        //                System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
        //                dlg.Filter = "(bin *.bin)|*.bin";
        //                if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog())
        //                {
        //                    UpdateFilePath = dlg.FileName;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show("Select file error:" + ex.Message);
        //            }
        //        }));
        //    }
        //}

        //public ICommand UpdateCmd
        //{
        //    get
        //    {
        //        return _UpdateCmd ?? (_UpdateCmd = new DelegateCommand2(() =>
        //        {
        //            Task.Run(() =>
        //            {
        //                IsIdle = false;
        //                IsError = false;
        //                try
        //                {
        //                    if (string.IsNullOrEmpty(UpdateFilePath))
        //                    {
        //                        IsError = true;
        //                        ShowInfoMsg = "Please select update file";
        //                        return false;
        //                    }

        //                    Progress = 0;
        //                    ShowInfoMsg = "upgrading do not power off, please wait...";

        //                    //jump to bootloader
        //                    _boardControlDynamic.JumpToBootLoader();
        //                    _boardControlDynamic.Disconnect();
        //                    ThreadUtils.Delay(1000);
        //                    firmwareUpdate = new FirmwareUpdate(FirmwareName + "firmwareUpdate", _boardControlDynamic.ComPort);
        //                    firmwareUpdate.UpdateProgessEvent += UpdateProgress;

        //                    firmwareUpdate.YmodemUploadFile(UpdateFilePath);
        //                    ShowInfoMsg = "send file success,wait jump to app";
        //                    return true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    IsError = true;
        //                    ShowInfoMsg = "Update firmware error:\r\n" + ex.Message;
        //                    return false;
        //                }
        //            }).ContinueWith((t) =>
        //            {
        //                if (!t.Result)
        //                    return false;

        //                int count = WaitJumpAppTimeMS / 1000;
        //                while (count > 0)
        //                {
        //                    ShowInfoMsg = string.Format("send file success,wait jump to app {0}", count);
        //                    ThreadUtils.Delay(1000);
        //                    count--;
        //                }
        //                return true;
        //            }).ContinueWith((t) =>
        //            {
        //                if (!t.Result)
        //                {
        //                    IsIdle = true;
        //                    return;
        //                }

        //                try
        //                {
        //                    ShowInfoMsg = "Checking version... ";
        //                    firmwareUpdate.Close();
        //                    ThreadUtils.Delay(1000);
        //                    _boardControlDynamic.Connect();
        //                    string version = _boardControlDynamic.GetVersion();

        //                    if (!string.IsNullOrEmpty(version))
        //                    {
        //                        int index = version.IndexOf('-');
        //                        if (index != -1)
        //                        {
        //                            version = version.Substring(0, index).Trim();
        //                        }
        //                        if (string.Compare(version, UpdateVersion, true) == 0)
        //                        {
        //                            ShowInfoMsg = "upgrate success";
        //                        }
        //                        else
        //                        {
        //                            IsError = true;
        //                            ShowInfoMsg = string.Format("upgrate failed,version is not equal {0}<>{1}", UpdateVersion, version);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ShowInfoMsg = "Get version error";
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    IsError = true;
        //                    ShowInfoMsg = "Wait jump to app error:\r\n" + ex.Message;
        //                }
        //                finally
        //                {
        //                    IsIdle = true;
        //                }
        //            });
        //        }));
        //    }
        //}

        //private void UpdateProgress(double progressValue)
        //{
        //    Progress = progressValue;
        //    //_Progress = progressValue;
        //    //RaisePropertyChanged("Progress");
        //}
        #endregion

        #region New Calibration

        #endregion
    }
}
