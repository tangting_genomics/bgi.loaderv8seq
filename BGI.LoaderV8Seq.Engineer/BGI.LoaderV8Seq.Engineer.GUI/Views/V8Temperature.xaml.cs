﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using BGI.Common.Utils;
using BGI.Common.Logging;
using System.Xml;
using BGI.LoaderV8Seq.Engineer.GUI.ViewModel;

namespace BGI.LoaderV8Seq.Engineer.GUI.Views
{
    /// <summary>
    /// Temperature.xaml 的交互逻辑
    /// </summary>
    public partial class V8Temperature : UserControl
    {
        private List<Point> originList = new List<Point>();
        private LineAndMarker<ElementMarkerPointsGraph> sourceGraph = null;
        private List<Point> degreeList = new List<Point>();
        private LineGraph degreeGraph = null;
        private LineGraph calGraph = null;
        private LineGraph afterCalGraph = null;

        private Dictionary<string, LineGraph> _TemperatureGraphMap = new Dictionary<string, LineGraph>();
        private Dictionary<string, Pen> _PenMap = new Dictionary<string, Pen>();
        private Logger _logger = LogMgr.GetLogger("Temperature_QC");
        private bool isFormCSV = false;
        private V8TemperatureViewModel model;
        private const string SLIDEA1_KEY = "SlideA1";
        private const string SLIDEA2_KEY = "SlideA2";
        private const string SLIDEB1_KEY = "SlideB1";
        private const string SLIDEB2_KEY = "SlideB2";
        private const string SLIDEC1_KEY = "SlideC1";
        private const string SLIDEC2_KEY = "SlideC2";
        public V8Temperature()
        {
            InitializeComponent();
            model = new V8TemperatureViewModel("Temperature");
            this.DataContext = model;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            originList = GetPoints();
            DrawPoint(originList);


            if (originList.Count == 0)
            {

                MessageBox.Show("Please input Slide Data or PT100 Data");

                return;
            }

            TBFormulae.Text = "";

            Color[] LineColors = new Color[] { Colors.Blue, Colors.Magenta, Colors.Orange, Colors.Green, Colors.Gold, Colors.Red, Colors.Purple, Colors.Brown, Colors.Azure };
            int degree = 0;
            degree = CBDegree.SelectedIndex + 1;
            degreeList.Clear();
            ClearGraph();

            TBFormulae.Text = string.Empty;
            this.txtPT100ToSlide.Text = string.Empty;
            this.txtSlideToPT100.Text = string.Empty;
            StringBuilder formulaeSB = new StringBuilder();
            var xData = originList.Select(p => p.X).ToArray();
            var yData = originList.Select(p => p.Y).ToArray();
            double[] arrFator = MathUtils.PolyFit(xData, yData, degree);
            List<double> xlist = new List<double>();
            List<double> yList = new List<double>();

            int dimension = arrFator.Length - 1;
            formulaeSB.Append("PT100ToSlide=");
            for (int i = 0; i <= dimension; i++)
            {
                //formulaeSB.Append(arrFator[i].ToString("0.000")).Append("*").Append("X^").Append(i).Append("+");
                formulaeSB.Append($"{arrFator[i].ToString()},");
                this.txtPT100ToSlide.Text += $"{arrFator[i].ToString()},";
            }
            this.txtPT100ToSlide.Text = this.txtPT100ToSlide.Text.Remove(this.txtPT100ToSlide.Text.Length - 1, 1);
            TBFormulae.Text += formulaeSB.Length > 0 ? formulaeSB.Remove(formulaeSB.Length - 1, 1).ToString() : "";

            for (double i = xData.Min(); i < xData.Max(); i += 0.1)
            {
                xlist.Add(i);
                yList.Add(MathUtils.PolyEquation(i, arrFator));
            }

            degreeGraph = AddGraph(xlist, yList, $"{degree} degree", new Pen(new SolidColorBrush(LineColors[degree - 1]), 1));
            double[] reverseArrFator = MathUtils.PolyFit(yData, xData, degree);
            dimension = reverseArrFator.Length - 1;
            StringBuilder reverseFormulaeSB = new StringBuilder();
            reverseFormulaeSB.Append("SlideToPT100=");
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i <= dimension; i++)
            {
                reverseFormulaeSB.Append($"{reverseArrFator[i].ToString()},");
                this.txtSlideToPT100.Text += $"{reverseArrFator[i].ToString()},";
            }
            this.txtSlideToPT100.Text = this.txtSlideToPT100.Text.Remove(this.txtSlideToPT100.Text.Length - 1, 1);
            TBFormulae.Text += Environment.NewLine;
            TBFormulae.Text += reverseFormulaeSB.Length > 0 ? reverseFormulaeSB.Remove(reverseFormulaeSB.Length - 1, 1).ToString() : "";
            xlist = new List<double>();
            yList = new List<double>();

            //draw graph
            for (double i = xData.Min(); i < xData.Max(); i += 0.1)
            {
                xlist.Add(i);
                yList.Add(MathUtils.PolyEquation(i, reverseArrFator));
            }
            calGraph = AddGraph(xlist, yList, $"{degree} degree", new Pen(new SolidColorBrush(Colors.DarkKhaki), 3));
            plotter.FitToView();

            FillCalibration(this.txtSlideToPT100.Text, this.txtPT100ToSlide.Text);
        }

        //private void ImportData_Click(object sender, RoutedEventArgs e)
        //{

        //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

        //    dlg.Filter = "csv documents (.csv)|*.csv";
        //    Nullable<bool> result = dlg.ShowDialog();
        //    // Process open file dialog box results
        //    if (result == true)
        //    {
        //        CBDegree.SelectionChanged -= ComboBox_SelectionChanged;
        //        CBDegree.Text = "";
        //        CBDegree.SelectionChanged += ComboBox_SelectionChanged;
        //        TBFormulae.Text = "";

        //        // Open document
        //        string filename = dlg.FileName;
        //        originList.Clear();
        //        originList = ReadDataFromCSV(filename);
        //        this.isFormCSV = true;
        //        if (originList.Count == 0)
        //        {
        //            return;
        //        }
        //        FillOrignData(originList);
        //        plotter.Children.Remove(plotter.Legend);
        //        if (sourceGraph != null)
        //        {
        //            sourceGraph.MarkerGraph.Remove();
        //        }

        //        ClearGraph();

        //        sourceGraph = AddGraph(originList, "origin", new Pen(Brushes.LimeGreen, 0), new CircleElementPointMarker { Size = 9, Brush = Brushes.Red, Fill = Brushes.Orange });
        //        plotter.FitToView();
        //    }
        //}

        private void DrawPoint(List<Point> points)
        {
            if (points.Count == 0)
            {
                return;
            }

            plotter.Children.Remove(plotter.Legend);
            if (sourceGraph != null)
            {
                sourceGraph.MarkerGraph.Remove();
            }

            ClearGraph();

            sourceGraph = AddGraph(points, "origin", new Pen(Brushes.LimeGreen, 0), new CircleElementPointMarker { Size = 9, Brush = Brushes.Red, Fill = Brushes.Orange });
            plotter.FitToView();
        }

        private List<Point> GetPoints()
        {
            List<Point> points = new List<Point>();
            string pt100Name = "txtPT100_";
            string slideName = "txtSlide_";
            for (int i = 1; i < 10; i++)
            {
                TextBox txtSlide = this.spData.FindName($"{slideName}{i}") as TextBox;
                TextBox txtPT100 = this.spData.FindName($"{pt100Name}{i}") as TextBox;
                if (txtSlide != null && !string.IsNullOrEmpty(txtSlide.Text) && txtPT100 != null && !string.IsNullOrEmpty(txtPT100.Text))
                {
                    double pt100 = 0;
                    double slide = 0;
                    if (double.TryParse(txtSlide.Text.Trim(), out slide) && double.TryParse(txtPT100.Text.Trim(), out pt100))
                    {
                        Point p = new Point() { X = pt100, Y = slide };
                        points.Add(p);
                    }
                }
            }
            return points;
        }

        private void CheckCalibration()
        {
            string slideToPT100 = this.txtSlideToPT100.Text.Trim();
            string pt100ToSlide = this.txtPT100ToSlide.Text.Trim();
        }

        private void FillOrignData(List<Point> points)
        {
            if (points != null && points.Count > 0)
            {
                int count = points.Count > 9 ? 9 : points.Count;
                string pt100Name = "txtPT100_";
                string slideName = "txtSlide_";
                for (int i = 0; i < count; i++)
                {
                    TextBox txtSlide = this.spData.FindName($"{slideName}{i + 1}") as TextBox;
                    TextBox txtPT100 = this.spData.FindName($"{pt100Name}{i + 1}") as TextBox;
                    if (txtSlide != null && txtPT100 != null)
                    {
                        txtPT100.Text = points[i].X.ToString();
                        txtSlide.Text = points[i].Y.ToString();
                    }
                }
            }
        }

        private void FillCalibration(string formulaeSlideToPT100, string formulaePT100ToSlide)
        {
            if (string.IsNullOrEmpty(formulaeSlideToPT100) ||
                string.IsNullOrEmpty(formulaePT100ToSlide)) return;
            string[] slideToPT100 = formulaeSlideToPT100.Split(',');
            string[] pt100ToSlide = formulaePT100ToSlide.Split(',');
            string pt100Name = "txtPT100_";
            string calibrationName = "txtCalibration_";
            for (int i = 1; i < 10; i++)
            {
                TextBox txtSlide = this.spData.FindName($"{calibrationName}{i}") as TextBox;
                TextBox txtPT100 = this.spData.FindName($"{pt100Name}{i}") as TextBox;
                if (txtPT100 != null && !string.IsNullOrEmpty(txtPT100.Text))
                {
                    double pt100 = 0;
                    if (double.TryParse(txtPT100.Text.Trim(), out pt100))
                    {
                        double target = 0;
                        for (int j = 0; j < slideToPT100.Length; j++)
                        {
                            target += Convert.ToDouble(slideToPT100[j]) * Math.Pow(pt100, j);
                        }
                        //txtSlide.Text = target.ToString("F2");
                        double temp = 0;
                        for (int j = 0; j < pt100ToSlide.Length; j++)
                        {
                            temp += Convert.ToDouble(pt100ToSlide[j]) * Math.Pow(target, j);
                        }

                        txtSlide.Text = $"[{target.ToString("F2")}] [{temp.ToString("F2")}]  [{Math.Abs(pt100 - temp).ToString("F4")}]";
                    }

                }
            }
        }

        private LineAndMarker<ElementMarkerPointsGraph> AddGraph(List<Point> datas, string description, Pen pen, ShapeElementPointMarker marker)
        {
            var xData = new EnumerableDataSource<double>(datas.Select(v => v.X));
            xData.SetXMapping(x => x);
            var yData = new EnumerableDataSource<double>(datas.Select(v => v.Y));
            yData.SetYMapping(y => y);
            CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
            return plotter.AddLineGraph(compositeDataSource1, pen, marker, new PenDescription(description));
        }
        private LineGraph AddGraph(List<double> xDatas, List<double> yDatas, string description, Pen pen)
        {
            var xData = new EnumerableDataSource<double>(xDatas);
            xData.SetXMapping(x => x);
            var yData = new EnumerableDataSource<double>(yDatas);
            yData.SetYMapping(y => y);

            CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
            return plotter.AddLineGraph(compositeDataSource1, pen, new PenDescription(description));
        }

        private LineGraph AddGraph(List<double> xDatas, List<double> xDatas2, List<double> yDatas, List<double> yDatas2, string description, Pen pen)
        {
            var xData = new EnumerableDataSource<double>(xDatas);
            xData.SetXMapping(x => x);
            var yData = new EnumerableDataSource<double>(yDatas);
            yData.SetYMapping(y => y);

            CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
            LineGraph rslt = plotter.AddLineGraph(compositeDataSource1, pen, new PenDescription(description));
            xData = new EnumerableDataSource<double>(xDatas2);
            xData.SetXMapping(x => x);
            yData = new EnumerableDataSource<double>(yDatas2);
            yData.SetYMapping(y => y);

            compositeDataSource1 = new CompositeDataSource(xData, yData);
            plotter.AddLineGraph(compositeDataSource1, new Pen(Brushes.Red, 2), new PenDescription("test"));
            return rslt;
        }

        private void DrawTemperatureGraph(Dictionary<string, Dictionary<int, double>> valuesMap, Dictionary<string, Pen> pensMap, Dictionary<string, bool> showMap)
        {
            if (valuesMap != null && pensMap != null)
            {
                foreach (string key in valuesMap.Keys)
                {
                    Dictionary<int, double> value = valuesMap[key];
                    var xData = new EnumerableDataSource<int>(value.Keys);
                    xData.SetXMapping(x => x);
                    var yData = new EnumerableDataSource<double>(value.Values);
                    yData.SetYMapping(y => y);
                    CompositeDataSource compositeDataSource1 = new CompositeDataSource(xData, yData);
                    if (showMap[key])
                        plotter.AddLineGraph(compositeDataSource1, pensMap[key], new PenDescription(key));
                }
            }
        }
        private List<Point> ReadDataFromCSV(string filePath)
        {
            List<Point> dataList = new List<Point>();
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] data = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (data.Length == 2)
                            {
                                dataList.Add(new Point() { X = Convert.ToDouble(data[0]), Y = Convert.ToDouble(data[1]) });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Read data error:{ex}");
            }
            return dataList;
        }

        private void SaveParaOffline(string slideToPT100, string pt100ToSlide)
        {
            //string slideToPT100 = "0,1";
            //string pt100ToSlide = "0,1";
            //if (!isReset)
            //{
            //    if (!string.IsNullOrEmpty(this.txtSlideToPT100.Text) && !string.IsNullOrEmpty(this.txtPT100ToSlide.Text))
            //    {
            //        slideToPT100 = this.txtSlideToPT100.Text.Trim();
            //        pt100ToSlide = this.txtPT100ToSlide.Text.Trim();
            //    }
            //}
            string configPath = @"C:\BGI\Config\BGI.ZebraV01Seq.Service.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(configPath);
            XmlNode node = doc.SelectSingleNode("Configs/InstrumentConfig[Para_Name='SlideToPT100']").SelectSingleNode("Cur_Value").FirstChild;
            if (node != null) node.Value = slideToPT100;
            node = doc.SelectSingleNode("Configs/InstrumentConfig[Para_Name='PT100ToSlide']").SelectSingleNode("Cur_Value").FirstChild;
            if (node != null) node.Value = pt100ToSlide;
            doc.Save(configPath);
        }

        private bool isStart = false;

        private System.ComponentModel.BackgroundWorker back = new System.ComponentModel.BackgroundWorker();

        private void BtnAfter_Click(object sender, RoutedEventArgs e)
        {
            if (!isStart)
            {
                this.isStart = true;
                showGraphMap = new Dictionary<string, bool>();
                showGraphMap.Add(SLIDEA1_KEY, true);
                showGraphMap.Add(SLIDEA2_KEY, true);
                showGraphMap.Add(SLIDEB1_KEY, true);
                showGraphMap.Add(SLIDEB2_KEY, true);
                showGraphMap.Add(SLIDEC1_KEY, true);
                showGraphMap.Add(SLIDEC2_KEY, true);
                temperatureValuesMap = new Dictionary<string, Dictionary<int, double>>();
                _PenMap = new Dictionary<string, Pen>();
                _PenMap.Add(SLIDEA1_KEY, new Pen(Brushes.Green, 2));
                _PenMap.Add(SLIDEB1_KEY, new Pen(Brushes.Blue, 2));
                _PenMap.Add(SLIDEC1_KEY, new Pen(Brushes.Orange, 2));
                _PenMap.Add(SLIDEA2_KEY, new Pen(Brushes.Black, 2));
                _PenMap.Add(SLIDEB2_KEY, new Pen(Brushes.Brown, 2));
                _PenMap.Add(SLIDEC2_KEY, new Pen(Brushes.Chocolate, 2));
                back = new System.ComponentModel.BackgroundWorker();
                back.WorkerSupportsCancellation = true;
                back.DoWork -= Back_DoWork;
                back.DoWork += Back_DoWork;
                back.RunWorkerAsync();
                this.txtStartTime.Text = DateTime.Now.ToString("dd-HH:mm:ss");
                this.btnCalibration.Content = "Stop";
                this.CBDegree.IsEnabled = false;
            }
            else
            {
                this.isStart = false;
                this.btnCalibration.Content = "Start Collect";
                if (back != null)
                {
                    back.CancelAsync();
                    back = null;
                }
                this.CBDegree.IsEnabled = true;
                showGraphMap.Clear();
                temperatureValuesMap.Clear();
                this.txtStopTime.Text = DateTime.Now.ToString("dd-HH:mm:ss");
                //double max = afterTemp.Max();
                //double min = afterTemp.Min();

                //MessageBox.Show($"Max Temp={max.ToString("F2")} Min Temp={min.ToString("F2")} fluctuation range={(max - min).ToString("F2")}");
            }
        }

        private List<double> afterTemp = new List<double>();
        private List<double> afterCnt = new List<double>();
        private Dictionary<string, Dictionary<int, double>> temperatureValuesMap = new Dictionary<string, Dictionary<int, double>>();
        private int interval_ms = 500;
        private Dictionary<string, bool> showGraphMap = new Dictionary<string, bool>();
        private void Back_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                ClearGraph(true);
            });

            afterTemp = new List<double>();
            afterCnt = new List<double>();
            int count = 0;
            while (this.isStart)
            {

                App.Current.Dispatcher.Invoke(() =>
                {
                    if (!model.IsInit)
                    {
                        isStart = false;
                        showGraphMap.Clear();
                        this.btnCalibration.Content = "Start Collect";
                    }
                    ClearTemperatureGraph();
                    if (this.chkShowSlideA1.IsEnabled)
                    {
                        if (temperatureValuesMap.ContainsKey(SLIDEA1_KEY))
                        {
                            temperatureValuesMap[SLIDEA1_KEY].Add(count, model.ChannelA1Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelA1Value);
                            temperatureValuesMap.Add(SLIDEA1_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideA2.IsEnabled)
                    {
                        if (temperatureValuesMap.ContainsKey(SLIDEA2_KEY))
                        {
                            temperatureValuesMap[SLIDEA2_KEY].Add(count, model.ChannelA2Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelA2Value);
                            temperatureValuesMap.Add(SLIDEA2_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideB1.IsEnabled)
                    {
                        afterCnt.Add(count);
                        if (temperatureValuesMap.ContainsKey(SLIDEB1_KEY))
                        {
                            temperatureValuesMap[SLIDEB1_KEY].Add(count, model.ChannelB1Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelB1Value);
                            temperatureValuesMap.Add(SLIDEB1_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideB2.IsEnabled)
                    {
                        afterCnt.Add(count);
                        if (temperatureValuesMap.ContainsKey(SLIDEB2_KEY))
                        {
                            temperatureValuesMap[SLIDEB2_KEY].Add(count, model.ChannelB2Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelB2Value);
                            temperatureValuesMap.Add(SLIDEB2_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideC1.IsEnabled)
                    {
                        if (temperatureValuesMap.ContainsKey(SLIDEC1_KEY))
                        {
                            temperatureValuesMap[SLIDEC1_KEY].Add(count, model.ChannelC1Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelC1Value);
                            temperatureValuesMap.Add(SLIDEC1_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideC2.IsEnabled)
                    {
                        if (temperatureValuesMap.ContainsKey(SLIDEC2_KEY))
                        {
                            temperatureValuesMap[SLIDEC2_KEY].Add(count, model.ChannelC2Value);
                        }
                        else
                        {
                            Dictionary<int, double> tmp = new Dictionary<int, double>();
                            tmp.Add(count, model.ChannelC2Value);
                            temperatureValuesMap.Add(SLIDEC2_KEY, tmp);
                        }
                    }
                    if (this.chkShowSlideA1.IsChecked.Value)
                    {
                        showGraphMap[SLIDEA1_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEA1_KEY] = false;
                    }
                    if (this.chkShowSlideA2.IsChecked.Value)
                    {
                        showGraphMap[SLIDEA2_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEA2_KEY] = false;
                    }
                    if (this.chkShowSlideB1.IsChecked.Value)
                    {
                        showGraphMap[SLIDEB1_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEB1_KEY] = false;
                    }
                    if (this.chkShowSlideB2.IsChecked.Value)
                    {
                        showGraphMap[SLIDEB2_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEB2_KEY] = false;
                    }
                    if (this.chkShowSlideC1.IsChecked.Value)
                    {
                        showGraphMap[SLIDEC1_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEC1_KEY] = false;
                    }
                    if (this.chkShowSlideC2.IsChecked.Value)
                    {
                        showGraphMap[SLIDEC2_KEY] = true;
                    }
                    else
                    {
                        showGraphMap[SLIDEC2_KEY] = false;
                    }
                    DrawTemperatureGraph(temperatureValuesMap, _PenMap, showGraphMap);
                    plotter.FitToView();
                });
                count++;
                System.Threading.Thread.Sleep(interval_ms);
            }
        }

        private void ClearGraph(bool isTemperatureline = false)
        {
            plotter.Children.Remove(plotter.Legend);
            if (degreeGraph != null)
            {
                plotter.Children.Remove(degreeGraph);
            }
            if (calGraph != null)
            {
                plotter.Children.Remove(calGraph);
            }
            if (afterCalGraph != null)
            {
                plotter.Children.Remove(afterCalGraph);
                afterCalGraph = null;
            }
            if (isTemperatureline)
            {
                if (sourceGraph != null)
                {
                    sourceGraph.MarkerGraph.Remove();
                }
            }
            if (_TemperatureGraphMap != null)
            {
                //plotter.Children.RemoveAll(typeof(LineGraph));
                foreach (LineGraph tmp in _TemperatureGraphMap.Values)
                {
                    if (tmp != null)
                        plotter.Children.Remove(tmp);
                }
                _TemperatureGraphMap.Clear();
            }
            plotter.Children.RemoveAll(typeof(LineGraph));
        }

        private void ClearTemperatureGraph()
        {
            plotter.Children.RemoveAll(typeof(LineGraph));
        }

        private void chkShowTemperature_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
